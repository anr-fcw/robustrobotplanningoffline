#include "Include/ParserPOMDPSparse.h"
#include "Include/ParserSarsopResult.h"
#include "Include/SampleLocalFsc.h"
#include "Include/SampleStochasticLocalFsc.h"
#include "Include/ParserDecPOMDPSparse.h"
#include "Include/RobotRobustPlanModelSparse.h"
#include "Include/BuildFSC.h"

// ---- Please provide a SARSOP solver path here! ----
const string solver_path = "SARSOP_PATH/pomdpsol";
// ---------------------------------------------------

int main()
{

    int human_index = 0;
    int robot_index = 1;
    double error_gap = 0.01;
    int sample_number_start = 0;
    int sample_number_end = 50; // how much sample I want collect
    srand(time(NULL));

    vector<DecPomdpInterface *> decpomdp_models;
    vector<MCTS> vec_mcts;
    vector<vector<AlphaVector>> MpomdpResults;

    string sto_human_fsc_path_1 = "results/Objective0Human.fsc";
    string sto_human_fsc_path_2 = "results/Objective1Human.fsc";

    SampleStochasticLocalFsc sto_human_fsc1(sto_human_fsc_path_1);
    SampleStochasticLocalFsc sto_human_fsc2(sto_human_fsc_path_2);

    vector<SampleStochasticLocalFsc> stochastic_human_fscs = {sto_human_fsc1, sto_human_fsc2};

    string experiment_data_folder_path = "ExperimentsData_big_map/";
    string path_pb_1 = "problem_folder/fcw_icra_human_prefer_left_test_big.dpomdp";
    string path_pb_2 = "problem_folder/fcw_icra_human_prefer_right_test_big.dpomdp";

    DecPomdpInterface *Pb_1 = new ParsedDecPOMDPSparse(path_pb_1);
    DecPomdpInterface *Pb_2 = new ParsedDecPOMDPSparse(path_pb_2);

    decpomdp_models.push_back(Pb_1);
    decpomdp_models.push_back(Pb_2);

    string out_BR_values_file = experiment_data_folder_path + "BR_values.csv";
    ofstream outlogs;
    outlogs.open(out_BR_values_file.c_str());

    // sample different human fscs, robot BRM and robot BR fsc
    for (int i_sample = sample_number_start; i_sample < sample_number_end; i_sample++)
    {
        vector<FSCBase> FSCs_possible_human;

        // -------------- 3. Sampling Human Policies --------------
        for (size_t i = 0; i < stochastic_human_fscs.size(); i++)
        {
            vector<string> human_fsc_folders = {"SampledHumanFSCPreferLeft/", "SampledHumanFSCPreferRight/"};

            string out_name = experiment_data_folder_path + human_fsc_folders[i] + to_string(i_sample) + ".fsc";
            // delete localfsc;
            FSCBase FscI(out_name, decpomdp_models[i]->GetSizeOfObs(human_index), 0);
            FSCs_possible_human.push_back(FscI);
        }

        // ---------- 4. Test Build Robot POMDP ----------
        RobotRobustPlanModelSparse RobotModel(decpomdp_models, FSCs_possible_human, human_index, robot_index);
        string result_pomdp_Path = experiment_data_folder_path + "RobotBestResponsePomdp/" + to_string(i_sample) + "RobotBRM.pomdp";
        RobotModel.ExportPOMDP(result_pomdp_Path);
        // -------------- 5. Solving Robot POMDP -------------------------

        string command = solver_path + " -p " + to_string(error_gap) + " " + result_pomdp_Path;
        const char *p = command.data();
        system(p);
        cout << "SARSOP finished solving the MPOMDP!" << endl;
        const string sarsop_res_path = "out.policy";
        const string output_alphavecs_res_path = experiment_data_folder_path + "AlphaVecsResult";
        transformToMADPformat(sarsop_res_path, output_alphavecs_res_path);
        vector<AlphaVector> Res_AlphaVecs = ImportValueFunction(output_alphavecs_res_path);

        PomdpInterface *BRMPb = new ParsedPOMDPSparse(result_pomdp_Path);
        double V_alphavecs = EvaluationWithAlphaVecs(BRMPb, Res_AlphaVecs); // ADD also this value to the logs result
        cout << "Evaluation with alpha-vector:" << V_alphavecs << endl;
        outlogs << i_sample << ", " << V_alphavecs << endl;
        // -------------- 6. Extract Robot FSC With Evaluation ------------------------------
        // FSC fsc(Res_AlphaVecs, BRMPb, formalization_type, error_gap);
        int robot_fsc_max_size = 1000;
        FSC fsc(Res_AlphaVecs, BRMPb, 0.01, 0.01, robot_fsc_max_size);
        string file_robust_robot_fsc = experiment_data_folder_path + "RobotBestResponseToSampledHumanFSC/" + to_string(i_sample);
        fsc.ExportFSC(file_robust_robot_fsc);
    }
    outlogs.close();

    return 0;
}

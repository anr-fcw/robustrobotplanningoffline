from pkg_resources import safe_extra


class FCWModel:
    def __init__(self, init_map) -> None:
        # init map
        self.init_map_ = init_map
        self.agent_number = 2
        # all position
        self.all_positions_ = self.BuildAllPositions()  # build afterwards
        # Action space
        # self.human_action_space_ = ["Up", "Down", "Left", "Right", "Repair", "PickComponent"]
        # self.robot_action_space_ = ["Up", "Down", "Left", "Right", "Repair", "Maintenance"]
        # Next need to do !!! add wait action
        self.human_action_space_ = [
            "Up", "Down", "Left", "Right", "Repair", "PickComponent", "Wait"]
        self.robot_action_space_ = [
            "Up", "Down", "Left", "Right", "Repair", "Maintenance", "Wait"]
        self.joint_action_indices_space_ = self.BuildJointActionSpace()
        # Devices status space
        self.devices_status_space_ = []  # build afterwards
        self.BuildDevicesStatusSpace()
        # Human status space
        self.human_status_space_ = ["HaveComponent", "NoComponent"]

        # State space
        # each item contains indices of sub-part (human_pos, robot_pos, devices_status, human_status)
        self.state_space_ = []
        self.map_indices_to_state_index = {}
        # self.BuildAllState()
        # human obs space
        self.human_obs_space_ = []
        self.map_indices_to_human_obs_index = {}
        self.human_obs_robot_space = [
            'obs_robot_not_in_same_cell', 'obs_robot_same_cell']
        self.human_obs_devices_space = [
            'obs_device_good', 'obs_device_bad', 'obs_need_maintenance', 'obs_no_seen']
        # self.BuildHumanObservationSpace()
        # robot obs space
        self.robot_obs_space_ = []
        self.map_indices_to_robot_obs_index = {}
        self.robot_obs_devices_space = [
            'obs_device_good', 'obs_device_bad', 'obs_need_maintenance', 'obs_no_seen']
        # self.BuildRobotObservationSpace()

        # some parameters
        # self.human_movement_rates = [0.95, 0.05] # success rate, stay rate
        # self.robot_movement_rates = [0.95, 0.05]
        # # self.devices_repair_rates = [1.0, 0.0]
        # self.device_maintenance_rates = [1.0, 0.0]
        # self.pick_component_rates = [0.8, 0.2]

        self.pb_human_move_success_rate = 1.0
        self.pb_robot_move_success_rate = 1.0  # 0.9
        self.pb_maintenance_success_rate = 1.0  # 0.9
        # self.pb_human_move_success_rate = 1
        # self.pb_robot_move_sucess_rate = 1
        self.reward_finish_task = 100
        self.penalty_invalid_action = -20
        # self.reward_prefer_left = 10
        # self.reward_prefer_right = 10
        self.reward_prefer_left = 50
        self.reward_prefer_right = 50

        self.discount = 0.95
        # TransFuncVec and ObsFuncVec
        self.TransFuncVec = {}
        self.ObsFuncVec = {}
        self.RewardFuncVec = {}

        # human preferences, should do it automatically
        # self.left_device_pos = (0,1) # left device needed to be repaired
        # self.right_device_pos = (0,3) # right device need to be repaired
        self.left_device_pos = (0, 0)  # left device needed to be repaired
        self.right_device_pos = (0, 3)  # right device need to be repaired
        # self.right_device_pos = (0,2) # right device need to be repaired

        self.BuildAllPossibleStateAndObs()

        self.BuildAllTransFunc()
        self.BuildAllObsFunc()
        # self.BuildAllRewardFunc()

        # init distribution
        self.start = self.BuildInitialDistributionDeterministic()
    # --- Tools functions ---

    def GetInitialState(self):

        H_pos = []
        R_pos = []
        devices_status = {}
        H_status = 'NoComponent'
        nbRows = len(self.init_map_)
        nbCols = len(self.init_map_[0])
        for i in range(nbRows):
            for j in range(nbCols):
                item = self.init_map_[i][j]
                pos = [i, j]
                if item == 'H':
                    H_pos = pos
                if item == 'R':
                    R_pos = pos
                if item == 'DB':
                    devices_status[tuple(pos)] = 'B'
                if item == 'DM':
                    devices_status[tuple(pos)] = 'M'

        H_pos_index = self.all_positions_.index(H_pos)
        R_pos_index = self.all_positions_.index(R_pos)
        devices_status_index = self.devices_status_space_.index(devices_status)
        H_status_index = self.human_status_space_.index(H_status)

        state_init = (H_pos_index, R_pos_index,
                      devices_status_index, H_status_index)
        return state_init

    def BuildInitialDistributionDeterministic(self):
        start_dist = []
        for s_index in range(0, len(self.state_space_)):
            start_dist.append(0.0)

        state_init = self.GetInitialState()

        state_init_index = self.map_indices_to_state_index[state_init]

        start_dist[state_init_index] = 1.0

        return start_dist

    def BuildInitialDistribution(self):
        start_dist = []
        for s_index in range(0, len(self.state_space_)):
            start_dist.append(0.0)

        H_pos = []
        R_pos = []
        devices_status = {}
        H_status = 'NoComponent'
        nbRows = len(self.init_map_)
        nbCols = len(self.init_map_[0])
        for i in range(nbRows):
            for j in range(nbCols):
                item = self.init_map_[i][j]
                pos = [i, j]
                # if item == 'H':
                #     H_pos = pos
                if item == 'R':
                    R_pos = pos
                if item == 'DB':
                    devices_status[tuple(pos)] = 'B'
                if item == 'DM':
                    devices_status[tuple(pos)] = 'M'

        nb_position = nbRows * nbCols
        pb = 1 / nb_position
        for i in range(nbRows):
            for j in range(nbCols):
                H_pos = [i, j]

                H_pos_index = self.all_positions_.index(H_pos)
                R_pos_index = self.all_positions_.index(R_pos)
                devices_status_index = self.devices_status_space_.index(
                    devices_status)
                H_status_index = self.human_status_space_.index(H_status)

                state_init = (H_pos_index, R_pos_index,
                              devices_status_index, H_status_index)
                state_init_index = self.map_indices_to_state_index[state_init]

                start_dist[state_init_index] = pb

        return start_dist

    def WriteDpomdpFile(self, file):
        file.writelines("agents: " + str(self.agent_number))
        file.writelines('\ndiscount: ' + str(self.discount))
        file.writelines('\nvalues: reward')
        file.write("\nstates: ")
        self.writestatelist(self.state_space_, file)
        file.write("\nactions: ")
        file.write("\n")
        self.writelist(self.human_action_space_, file)
        file.write("\n")
        self.writelist(self.robot_action_space_, file)
        file.write("\nobservations: ")
        file.write("\n")
        self.writehumanobslist(self.human_obs_space_, file)
        file.write("\n")
        self.writerobotobslist(self.robot_obs_space_, file)
        # write start
        file.write("\nstart: ")
        self.writelist(self.start, file)

        # write T
        self.WriteTransFuncDecPOMDP(file)
        self.WriteObsFuncDecPOMDP(file)
        self.WriteRewardDecPOMDP(file)

    def writelist(self, list, file):
        for item in list:
            if type(item) == float:
                item = str(item)
            file.writelines(item + " ")

    def writestatelist(self, list, file):
        for item in list:
            new_item = "HP"+str(item[0]) + "RP" + str(item[1]) + \
                "DS" + str(item[2]) + "HS" + str(item[3])
            # new_item = "".join(new_item.split())
            file.writelines(new_item + " ")

    # def writehumanobslist(self, list, file):
    #     for item in list:
    #         # new_item = "OS"+ str(self.all_positions_.index(item['Obs_self_pos'])) + "OR"+str(self.all_positions_.index(item['Obs_R_pos'])) + "OD" + str(self.devices_status_space_.index(item['Obs_devices_status']))
    #         new_item = "OS" + str(self.all_positions_.index(item['Obs_self_pos'])) + "OD" + str(self.human_obs_devices_space.index(
    #             item['Obs_devices_status'])) + "OR" + str(self.human_obs_robot_space.index(item['Obs_robot']))

    #         file.writelines(new_item + " ")

    def writehumanobslist(self, list, file):
        for item in list:
            new_item = "OS" + str(item[0]) + "OD" + \
                str(item[1]) + "OR" + str(item[2])
            file.writelines(new_item + " ")

    def writerobotobslist(self, list, file):
        for item in list:
            new_item = "OS" + str(item[0]) + "OD" + \
                str(item[1]) + "OH"+str(item[2])
            file.writelines(new_item + " ")

    def WriteTransFuncDecPOMDP(self, file):
        for action_indices in self.joint_action_indices_space_:
            for sI in range(len(self.state_space_)):
                dist_snew = self.TransFuncVec[(action_indices, sI)]
                for s_newI in dist_snew:
                    str_action_indices = " ".join(
                        str(e) for e in action_indices)
                    file.writelines('\nT: ' + str_action_indices + " : " + str(sI) +
                                    " : " + str(s_newI) + " : " + str(dist_snew[s_newI]))

    def WriteObsFuncDecPOMDP(self, file):
        for action_indices in self.joint_action_indices_space_:
            for sI in range(len(self.state_space_)):
                dist_obs = self.ObsFuncVec[(action_indices, sI)]
                for joint_obs in dist_obs:
                    str_action_indices = " ".join(
                        str(e) for e in action_indices)
                    str_obs_indices = " ".join(str(e) for e in joint_obs)
                    file.writelines('\nO: ' + str_action_indices + " : " + str(sI) +
                                    " : " + str(str_obs_indices) + " : " + str(dist_obs[joint_obs]))

    def WriteTransFuncDecPOMDP(self, file):
        for action_indices in self.joint_action_indices_space_:
            for sI in range(len(self.state_space_)):
                dist_snew = self.TransFuncVec[(action_indices, sI)]
                for s_newI in dist_snew:
                    str_action_indices = " ".join(
                        str(e) for e in action_indices)
                    file.writelines('\nT: ' + str_action_indices + " : " + str(sI) +
                                    " : " + str(s_newI) + " : " + str(dist_snew[s_newI]))

    def WriteRewardDecPOMDP(self, file):
        for action_indices in self.joint_action_indices_space_:
            for sI in range(len(self.state_space_)):
                reward = self.RewardFuncVec[(action_indices, sI)]
                if reward != 0:
                    str_action_indices = " ".join(
                        str(e) for e in action_indices)
                    file.writelines('\nR: ' + str_action_indices +
                                    " : " + str(sI) + " : * : * : " + str(reward))

    def BuildAllTransFunc(self):
        for joint_action_indices in self.joint_action_indices_space_:
            for state_index in range(0, len(self.state_space_)):
                key_pair = (joint_action_indices, state_index)
                pb_dist = self.TransFunc(joint_action_indices, state_index)
                self.TransFuncVec[key_pair] = pb_dist

    def BuildAllObsFunc(self):
        # all_possible_obs = []

        for joint_action_indices in self.joint_action_indices_space_:
            for state_index in range(0, len(self.state_space_)):
                key_pair = (joint_action_indices, state_index)
                pb_dist = self.ObsFunc(joint_action_indices, state_index)
                self.ObsFuncVec[key_pair] = pb_dist

        #         # for test how many possible obs
        #         for obsI in pb_dist:
        #             if obsI not in all_possible_obs:
        #                 all_possible_obs.append(obsI)

        # print("all possible obs:", len(all_possible_obs))

    def BuildAllPositions(self):
        nbRows = len(self.init_map_)
        nbCols = len(self.init_map_[0])
        list_positions = []
        for i in range(nbRows):
            for j in range(nbCols):
                list_positions.append([i, j])
        return list_positions

    def BuildGetAllDevicesFromMap(self):
        nbRows = len(self.init_map_)
        nbCols = len(self.init_map_[0])
        devices = ['DM', 'DB']
        dict_devices = {}
        for i in range(nbRows):
            for j in range(nbCols):
                if self.init_map_[i][j] in devices:
                    dict_devices[(i, j)] = (self.init_map_[i][j])
        return dict_devices

    def RecursiveBuildDevicesStatus(self, dist_devices, depth, devices_status_space, devices_status_temp):
        keys_list = list(dist_devices)
        nb_devices = len(keys_list)

        status_space = ["G", "M"]
        if depth != nb_devices:
            if dist_devices[keys_list[depth]] == "DB":
                status_space.remove("M")
                status_space.append("B")
            for status in status_space:
                devices_status_temp[keys_list[depth]] = (status)
                self.RecursiveBuildDevicesStatus(
                    dist_devices, depth + 1, devices_status_space, devices_status_temp)
                devices_status_temp.pop(keys_list[depth])
        else:
            # print(devices_status_temp)
            devices_status_space.append(devices_status_temp.copy())

    def BuildDevicesStatusSpace(self):
        dict_devices = self.BuildGetAllDevicesFromMap()
        # depth = 0
        self.RecursiveBuildDevicesStatus(
            dict_devices, 0, self.devices_status_space_, {})

    def BuildAllState(self):
        index = 0
        for human_pos_index in range(0, len(self.all_positions_)):
            for robot_pos_index in range(0, len(self.all_positions_)):
                for devices_status_index in range(0, len(self.devices_status_space_)):
                    for human_status_index in range(0, len(self.human_status_space_)):
                        state_temp = (human_pos_index, robot_pos_index,
                                      devices_status_index, human_status_index)
                        self.state_space_.append(state_temp)
                        self.map_indices_to_state_index[state_temp] = index
                        index += 1

    def BuildAllPossibleStateAndObs(self):

        state_Index = 0
        human_obs_Index = -1
        robot_obs_Index = -1
        state_init = self.GetInitialState()
        self.state_space_.append(state_init)
        self.map_indices_to_state_index[state_init] = state_Index
        all_possible_joint_obs = []
        unprocessed_list = []
        unprocessed_list.append(state_init)
        while (len(unprocessed_list) != 0):
            s = unprocessed_list.pop()
            for joint_action_indices in self.joint_action_indices_space_:
                pb_dist = self.TransFuncExplicit(joint_action_indices, s)
                for s_new in pb_dist:
                    if s_new not in self.state_space_:
                        state_Index += 1
                        self.state_space_.append(s_new)
                        self.map_indices_to_state_index[s_new] = state_Index
                        unprocessed_list.append(s_new)

                    pb_dist_obs = self.ObsFuncExplicit(
                        joint_action_indices, s_new)
                    for obs in pb_dist_obs:
                        human_obs = obs[0]
                        robot_obs = obs[1]
                        if human_obs not in self.human_obs_space_:
                            human_obs_Index += 1
                            self.human_obs_space_.append(human_obs)
                            self.map_indices_to_human_obs_index[human_obs] = human_obs_Index
                        if robot_obs not in self.robot_obs_space_:
                            robot_obs_Index += 1
                            self.robot_obs_space_.append(robot_obs)
                            self.map_indices_to_robot_obs_index[robot_obs] = robot_obs_Index

    def PrintState(self, state_index):
        state_indices = self.state_space_[state_index]
        human_pos = self.all_positions_[state_indices[0]]
        robot_pos = self.all_positions_[state_indices[1]]
        devices_status = self.devices_status_space_[state_indices[2]]
        human_status = self.human_status_space_[state_indices[3]]

        state = {'H_pos': human_pos, 'R_pos': robot_pos,
                 'devices': devices_status, 'H_status': human_status}
        print(state)

    def FindStateIndex(self, state):
        human_pos_index = self.all_positions_.index(state['H_pos'])
        robot_pos_index = self.all_positions_.index(state['R_pos'])
        devices_status_index = self.devices_status_space_.index(
            state['devices'])
        human_status_index = self.human_status_space_.index(state['H_status'])
        state_indices = (human_pos_index, robot_pos_index,
                         devices_status_index, human_status_index)
        return self.map_indices_to_state_index[state_indices]

    # def BuildHumanObservationSpace(self):
    #     index = 0
    #     for obs_self_position_index in range(0, len(self.all_positions_)):
    #         for obs_robot_position_index in range(0, len(self.all_positions_)):
    #             # for obs_devices_status_index in range(0, len(self.devices_status_space_)):
    #             for obs_devices_status_index in range(0, len(self.human_obs_devices_space)):
    #                 human_obs_temp = {}
    #                 human_obs_temp['Obs_self_pos'] = self.all_positions_[obs_self_position_index]
    #                 human_obs_temp['Obs_R_pos'] = self.all_positions_[obs_robot_position_index]
    #                 human_obs_temp['Obs_devices_status'] = self.devices_status_space_[obs_devices_status_index]
    #                 self.human_obs_space_.append(human_obs_temp)
    #                 self.map_indices_to_human_obs_index[(obs_self_position_index, obs_robot_position_index, obs_devices_status_index)] = index
    #                 index += 1

    def BuildHumanObservationSpace(self):
        index = 0
        # print(self.human_obs_devices_space)
        # print(self.devices_status_space_)
        for obs_self_position_index in range(0, len(self.all_positions_)):
            # for obs_robot_position_index in range(0, len(self.all_positions_)):
            # for obs_devices_status_index in range(0, len(self.devices_status_space_)):
            for obs_devices_status_index in range(0, len(self.human_obs_devices_space)):
                for obs_robot_index in range(0, len(self.human_obs_robot_space)):
                    human_obs_temp = {}
                    human_obs_temp['Obs_self_pos'] = self.all_positions_[
                        obs_self_position_index]
                    human_obs_temp['Obs_robot'] = self.human_obs_robot_space[obs_robot_index]
                    # human_obs_temp['Obs_devices_status'] = self.devices_status_space_[obs_devices_status_index]
                    human_obs_temp['Obs_devices_status'] = self.human_obs_devices_space[obs_devices_status_index]
                    self.human_obs_space_.append(human_obs_temp)
                    self.map_indices_to_human_obs_index[(
                        obs_self_position_index, obs_devices_status_index, obs_robot_index)] = index
                    index += 1

    def BuildRobotObservationSpace(self):
        index = 0
        for obs_self_position_index in range(0, len(self.all_positions_)):
            for obs_human_position_index in range(0, len(self.all_positions_)):
                for obs_devices_status_index in range(0, len(self.robot_obs_devices_space)):
                    robot_obs_temp = {}
                    robot_obs_temp['Obs_self_pos'] = self.all_positions_[
                        obs_self_position_index]
                    robot_obs_temp['Obs_H_pos'] = self.all_positions_[
                        obs_human_position_index]
                    robot_obs_temp['Obs_devices_status'] = self.robot_obs_devices_space[obs_devices_status_index]
                    self.robot_obs_space_.append(robot_obs_temp)
                    self.map_indices_to_robot_obs_index[(
                        obs_self_position_index, obs_devices_status_index, obs_human_position_index)] = index
                    index += 1

    def BuildJointActionSpace(self):
        joint_action_indices = []
        for human_action_index in range(0, len(self.human_action_space_)):
            for robot_action_index in range(0, len(self.robot_action_space_)):
                joint_action_indices.append(
                    (human_action_index, robot_action_index))
        return tuple(joint_action_indices)

    def TransFuncExplicit(self, joint_action_indices, state):
        map = self.init_map_
        Human_pos = self.all_positions_[state[0]]
        Robot_pos = self.all_positions_[state[1]]
        map_pb_dist = {}

        Human_pos_next_success = Human_pos.copy()
        Robot_pos_next_success = Robot_pos.copy()
        Human_pos_next_stay = Human_pos.copy()
        Robot_pos_next_stay = Robot_pos.copy()

        devices_status_next_index = state[2]
        human_status_next_index = state[3]

        devices_status_next_index_stay = state[2]  # (for maintenance fail)

        # human_status_next_stay_index = state[3]

        # human and robot action indices
        human_action_index = joint_action_indices[0]
        robot_action_index = joint_action_indices[1]

        # res pb dist
        human_pos_next_pb_dist = {}
        robot_pos_next_pb_dist = {}
        devices_status_next_pb_dist = {}
        human_status_next_pb_dist = {}

        # individual transitions for human and robot
        # Human part
        if human_action_index == 0:  # Up
            Human_pos_next_success = self.PosAfterMoveUp(map, Human_pos)
        elif human_action_index == 1:  # Down
            Human_pos_next_success = self.PosAfterMoveDown(map, Human_pos)
        elif human_action_index == 2:  # Left
            Human_pos_next_success = self.PosAfterMoveLeft(map, Human_pos)
        elif human_action_index == 3:  # Right
            Human_pos_next_success = self.PosAfterMoveRight(map, Human_pos)
        elif human_action_index == 4:  # Repair
            if robot_action_index == 4:
                devices_status_next_index, human_status_next_index = self.Repair(
                    state)

                devices_status_next_pb_dist[devices_status_next_index] = 1
                human_status_next_pb_dist[human_status_next_index] = 1

        elif human_action_index == 5:  # Pick Component
            human_status_next_index = self.PickComponent(state)
            human_status_next_pb_dist[human_status_next_index] = 1

        # wait do nothing

        # Robot Part
        if robot_action_index == 0:  # Up
            Robot_pos_next_success = self.PosAfterMoveUp(map, Robot_pos)
        elif robot_action_index == 1:  # Down
            Robot_pos_next_success = self.PosAfterMoveDown(map, Robot_pos)
        elif robot_action_index == 2:  # Left
            Robot_pos_next_success = self.PosAfterMoveLeft(map, Robot_pos)
        elif robot_action_index == 3:  # Right
            Robot_pos_next_success = self.PosAfterMoveRight(map, Robot_pos)
        elif robot_action_index == 4:  # Repair
            if human_action_index == 4:
                devices_status_next_index, human_status_next_index = self.Repair(
                    state)

                devices_status_next_pb_dist[devices_status_next_index] = 1
                human_status_next_pb_dist[human_status_next_index] = 1

        elif robot_action_index == 5:  # Maintenance
            devices_status_next_index = self.Maintenance(state)

            if devices_status_next_index_stay != devices_status_next_index:
                devices_status_next_pb_dist[devices_status_next_index] = self.pb_maintenance_success_rate
                devices_status_next_pb_dist[devices_status_next_index_stay] = 1 - \
                    self.pb_maintenance_success_rate
            else:
                devices_status_next_pb_dist[devices_status_next_index] = 1

        # indices
        Human_pos_next_success_index = self.all_positions_.index(
            Human_pos_next_success)
        Robot_pos_next_success_index = self.all_positions_.index(
            Robot_pos_next_success)
        Human_pos_next_stay_index = self.all_positions_.index(
            Human_pos_next_stay)
        Robot_pos_next_stay_index = self.all_positions_.index(
            Robot_pos_next_stay)

        if Human_pos_next_success_index != Human_pos_next_stay_index:
            human_pos_next_pb_dist[Human_pos_next_success_index] = self.pb_human_move_success_rate
            human_pos_next_pb_dist[Human_pos_next_stay_index] = 1 - \
                self.pb_human_move_success_rate
        else:
            human_pos_next_pb_dist[Human_pos_next_success_index] = 1

        if Robot_pos_next_success_index != Robot_pos_next_stay_index:
            robot_pos_next_pb_dist[Robot_pos_next_success_index] = self.pb_robot_move_success_rate
            robot_pos_next_pb_dist[Robot_pos_next_stay_index] = 1 - \
                self.pb_robot_move_success_rate
        else:
            robot_pos_next_pb_dist[Robot_pos_next_success_index] = 1

        # if not doing repair, pick, or maintenance actions
        if devices_status_next_pb_dist == {}:
            devices_status_next_pb_dist[devices_status_next_index_stay] = 1
        if human_status_next_pb_dist == {}:
            human_status_next_pb_dist[human_status_next_index] = 1

        for human_pos_index in human_pos_next_pb_dist:
            for robot_pos_index in robot_pos_next_pb_dist:
                for devices_status_index in devices_status_next_pb_dist:
                    for human_status_index in human_status_next_pb_dist:
                        next_state = (human_pos_index, robot_pos_index,
                                      devices_status_index, human_status_index)
                        # next_state_index = self.map_indices_to_state_index[next_state]
                        pb = human_pos_next_pb_dist[human_pos_index] * robot_pos_next_pb_dist[robot_pos_index] * \
                            devices_status_next_pb_dist[devices_status_index] * \
                            human_status_next_pb_dist[human_status_index]
                        map_pb_dist[next_state] = pb

        return map_pb_dist

    def ObsFuncExplicit(self, joint_action_indices, state):
        Human_pos_index = state[0]
        Robot_pos_index = state[1]
        devices_status_index = state[2]

        human_pos = tuple(self.all_positions_[Human_pos_index])
        robot_pos = tuple(self.all_positions_[Robot_pos_index])
        devices_status = self.devices_status_space_[devices_status_index]

        map_joint_obs_indices = {}

        # human_obs_device_index = 2 #init, device no see or need maintainance
        human_obs_device_index = 3  # init, human obs device no seen
        robot_obs_device_index = 3  # init, robot obs device no seen

        # human_obs_indices = (Human_pos_index, Robot_pos_index, devices_status_index)
        if human_pos in devices_status:
            if devices_status[human_pos] == 'G':
                human_obs_device_index = 0
            elif devices_status[human_pos] == 'B':
                human_obs_device_index = 1
            elif devices_status[human_pos] == 'M':
                human_obs_device_index = 2
            else:
                human_obs_device_index = 3

        if robot_pos in devices_status:
            if devices_status[robot_pos] == 'G':
                robot_obs_device_index = 0
            elif devices_status[robot_pos] == 'B':
                robot_obs_device_index = 1
            elif devices_status[robot_pos] == 'M':
                robot_obs_device_index = 2
            else:
                robot_obs_device_index = 3

        human_obs_robot_index = 0
        if Human_pos_index == Robot_pos_index:
            human_obs_robot_index = 1

        human_obs_indices = (
            Human_pos_index, human_obs_device_index, human_obs_robot_index)

        robot_obs_indices = (
            Robot_pos_index, robot_obs_device_index, Human_pos_index)

        # human_obs_index = self.map_indices_to_human_obs_index[human_obs_indices]
        # robot_obs_index = self.map_indices_to_robot_obs_index[robot_obs_indices]

        joint_obs = (human_obs_indices, robot_obs_indices)

        map_joint_obs_indices[joint_obs] = 1.0

        return map_joint_obs_indices

    def TransFunc(self, joint_action_indices, state_index):
        map_pb_dist = {}
        state = self.state_space_[state_index]
        dist_next_state = self.TransFuncExplicit(joint_action_indices, state)
        for s_new in dist_next_state:
            s_newI = self.map_indices_to_state_index[s_new]
            map_pb_dist[s_newI] = dist_next_state[s_new]

        return map_pb_dist

    def ObsFunc(self, joint_action_indices, state_index):

        map_joint_obs_indices = {}
        state = self.state_space_[state_index]
        dist_obs = self.ObsFuncExplicit(joint_action_indices, state)

        for joint_obs in dist_obs:
            human_obs_indices = joint_obs[0]
            robot_obs_indices = joint_obs[1]
            human_obs_index = self.map_indices_to_human_obs_index[human_obs_indices]
            robot_obs_index = self.map_indices_to_robot_obs_index[robot_obs_indices]
            joint_obs_indices = (human_obs_index, robot_obs_index)
            map_joint_obs_indices[joint_obs_indices] = dist_obs[joint_obs]

        return map_joint_obs_indices

    def CheckFinalReward(self, joint_action_indices, state_index):
        state = self.state_space_[state_index]
        devices_status_index = state[2]

        pb_dist_next_state = self.TransFunc(joint_action_indices, state_index)
        reward = self.reward_finish_task
        res = 0
        for s_newI in pb_dist_next_state:
            pb_s_new = pb_dist_next_state[s_newI]
            state_new = self.state_space_[s_newI]
            devices_new_status_index = state_new[2]
            # 0 means all devices are good
            if devices_new_status_index == 0 and devices_status_index != 0:
                res += reward*pb_s_new

        return res

    def CheckHumanPreferenceLeft(self, joint_action_indices, state_index):
        state = self.state_space_[state_index]
        devices_status_index = state[2]

        pb_dist_next_state = self.TransFunc(joint_action_indices, state_index)
        # Specify human preferred device location
        first_repair_device_location = self.left_device_pos
        second_repair_device_location = self.right_device_pos
        devices_status_index = state[2]
        devices_status = self.devices_status_space_[devices_status_index]
        reward = self.reward_prefer_left
        res = 0
        for s_newI in pb_dist_next_state:
            pb_s_new = pb_dist_next_state[s_newI]
            state_new = self.state_space_[s_newI]
            devices_new_status_index = state_new[2]
            devices_new_status = self.devices_status_space_[
                devices_new_status_index]

            if devices_status[first_repair_device_location] == 'B' \
                    and devices_new_status[first_repair_device_location] == 'G'\
                    and devices_new_status[second_repair_device_location] == "B":
                res += reward*pb_s_new
        return res

    def CheckHumanPreferenceRight(self, joint_action_indices, state_index):
        state = self.state_space_[state_index]
        devices_status_index = state[2]
        devices_status = self.devices_status_space_[devices_status_index]
        pb_dist_next_state = self.TransFunc(joint_action_indices, state_index)
        # Specify human preferred device location
        first_repair_device_location = self.right_device_pos
        second_repair_device_location = self.left_device_pos

        reward = self.reward_prefer_right
        res = 0
        for s_newI in pb_dist_next_state:
            pb_s_new = pb_dist_next_state[s_newI]
            state_new = self.state_space_[s_newI]
            devices_new_status_index = state_new[2]
            devices_new_status = self.devices_status_space_[
                devices_new_status_index]

            if devices_status[first_repair_device_location] == 'B' \
                    and devices_new_status[first_repair_device_location] == 'G'\
                    and devices_new_status[second_repair_device_location] == "B":
                res += reward*pb_s_new

        return res

    def CheckActionValid(self, joint_action_indices, state_index):
        state = self.state_space_[state_index]
        human_pos_index = state[0]
        robot_pos_index = state[1]
        devices_status_index = state[2]
        human_status_index = state[3]

        Human_pos = self.all_positions_[human_pos_index]
        Robot_pos = self.all_positions_[robot_pos_index]
        human_action_index = joint_action_indices[0]
        robot_action_index = joint_action_indices[1]

        Human_pos_next_success = [-1, -1]  # init a Human next position
        Robot_pos_next_success = [-1, -1]  # init a Robot next position
        # check action valid
        # Human action
        if human_action_index == 0:  # Up
            Human_pos_next_success = self.PosAfterMoveUp(map, Human_pos)
        elif human_action_index == 1:  # Down
            Human_pos_next_success = self.PosAfterMoveDown(map, Human_pos)
        elif human_action_index == 2:  # Left
            Human_pos_next_success = self.PosAfterMoveLeft(map, Human_pos)
        elif human_action_index == 3:  # Right
            Human_pos_next_success = self.PosAfterMoveRight(map, Human_pos)
        # elif human_action_index == 4: # Repair
        #     if robot_action_index == 4:
        #         devices_status_next_index, human_status_next_index = self.Repair(state)
        # elif human_action_index == 5: # Pick Component
        #     human_status_next_index = self.PickComponent(state)

        if Human_pos_next_success == Human_pos:  # Same Position, un valid movement
            return False
        # Robot action

        if robot_action_index == 0:  # Up
            Robot_pos_next_success = self.PosAfterMoveUp(map, Robot_pos)
        elif robot_action_index == 1:  # Down
            Robot_pos_next_success = self.PosAfterMoveDown(map, Robot_pos)
        elif robot_action_index == 2:  # Left
            Robot_pos_next_success = self.PosAfterMoveLeft(map, Robot_pos)
        elif robot_action_index == 3:  # Right
            Robot_pos_next_success = self.PosAfterMoveRight(map, Robot_pos)
        # elif robot_action_index == 4: # Repair
        #     if human_action_index == 4:
        #         devices_status_next_index, human_status_next_index = self.Repair(state)
        # elif robot_action_index == 5: # Maintenance
        #     devices_status_next_index = self.Maintenance(state)
        if Robot_pos_next_success == Robot_pos:
            return False

        return True

    def Step(self, state_index, joint_action):  # maybe not useful right now
        state = self.state_space_[state_index]
        map = self.init_map_
        human_action = self.human_action_space_[joint_action[0]]
        robot_action = self.robot_action_space_[joint_action[1]]
        devices_status = self.devices_status_space_[state[2]]
        human_status = self.human_status_space_[state[3]]
        Human_pos = self.all_positions_[state[0]]
        Robot_pos = self.all_positions_[state[1]]

        # next status
        devices_next_status = devices_status.copy()
        # human_next_status = human_status.copy()
        human_next_status = human_status

        # Human Pos Update
        Human_pos_next = Human_pos.copy()
        if (human_action == "Up"):
            Human_pos_next = self.PosAfterMoveUp(map, Human_pos)
        elif (human_action == "Down"):
            Human_pos_next = self.PosAfterMoveDown(map, Human_pos)
        elif (human_action == "Left"):
            Human_pos_next = self.PosAfterMoveLeft(map, Human_pos)
        elif (human_action == "Right"):
            Human_pos_next = self.PosAfterMoveRight(map, Human_pos)
        elif (human_action == "PickComponent"):
            if human_status == 'NoComponent':
                if map[Human_pos[0]][Human_pos[1]] == 'X':
                    human_next_status = 'HaveComponent'

        # Robot Pos Update
        Robot_pos_next = Robot_pos.copy()
        if (robot_action == "Up"):
            Robot_pos_next = self.PosAfterMoveUp(map, Robot_pos)
        elif (robot_action == "Down"):
            Robot_pos_next = self.PosAfterMoveDown(map, Robot_pos)
        elif (robot_action == "Left"):
            Robot_pos_next = self.PosAfterMoveLeft(map, Robot_pos)
        elif (robot_action == "Right"):
            Robot_pos_next = self.PosAfterMoveRight(map, Robot_pos)
        elif (robot_action == "Maintenance"):
            Robot_pos = tuple(Robot_pos)
            if Robot_pos in devices_status:
                if devices_status[Robot_pos] == 'M':
                    devices_next_status[Robot_pos] = 'G'

        # Repair
        if (human_action == "Repair"):
            if (robot_action == "Repair"):
                if Human_pos == Robot_pos:
                    tuple_human_robot_pos = tuple(Human_pos)
                    if tuple_human_robot_pos in devices_status:
                        if devices_status[tuple_human_robot_pos] == 'B':
                            if human_status == 'HaveComponent':
                                devices_next_status[tuple_human_robot_pos] = 'G'
                                human_next_status = 'NoComponent'
                    # AgentPosAfterLeft = PosAfterMoveLeft(s[0])

        Human_pos_next_index = self.all_positions_.index(Human_pos_next)
        Robot_pos_next_index = self.all_positions_.index(Robot_pos_next)
        devices_next_status_index = self.devices_status_space_.index(
            devices_next_status)
        human_next_status_index = self.human_status_space_.index(
            human_next_status)

        next_state = (Human_pos_next_index, Robot_pos_next_index,
                      devices_next_status_index, human_next_status_index)
        next_state_index = self.map_indices_to_state_index[next_state]

        # next observations
        # human obs
        # human_obs_robot_pos_index = Robot_pos_next_index
        # human_obs_self_pos_index = Human_pos_next_index
        # human_obs_devices_status_index = devices_next_status_index
        # human_obs_indices = (human_obs_self_pos_index, human_obs_robot_pos_index, human_obs_devices_status_index)
        # ## robot obs
        # robot_obs_human_pos_index = Human_pos_next_index
        # robot_obs_self_pos_index = Robot_pos_next_index
        # robot_obs_indices = (robot_obs_self_pos_index, robot_obs_human_pos_index)

        return next_state_index

    #  Tool Functions for Step Func

    def BorderCheck(self, map, pos):
        x = pos[0]
        y = pos[1]

        # index of row bigger than the map rows
        if (x >= len(map)):
            x = len(map) - 1

        # index of row smaller than the 0
        if (x < 0):
            x = 0

        # index of col bigger than the map cols
        if (y >= len(map[0])):
            y = len(map[0]) - 1

        # index of col smaller than 0
        if (y < 0):
            y = 0

        return [x, y]

    def PosAfterMoveUp(self, map, pos):
        # Check if inside the border
        PosAfterUp = self.BorderCheck(map, [pos[0]-1, pos[1]])
        return PosAfterUp

    def PosAfterMoveDown(self, map, pos):
        # Check if inside the border
        PosAfterDown = self.BorderCheck(map, [pos[0] + 1, pos[1]])
        return PosAfterDown

    def PosAfterMoveLeft(self, map, pos):
        # Check if inside the border
        PosAfterLeft = self.BorderCheck(map, [pos[0], pos[1]-1])
        return PosAfterLeft

    def PosAfterMoveRight(self, map, pos):
        # Check if inside the border
        PosAfterRight = self.BorderCheck(map, [pos[0], pos[1] + 1])
        return PosAfterRight

    def PickComponent(self, state):
        human_status = self.human_status_space_[state[3]]
        human_next_status = human_status
        Human_pos = tuple(self.all_positions_[state[0]])
        if human_status == 'NoComponent':
            if map[Human_pos[0]][Human_pos[1]] == 'X':
                human_next_status = 'HaveComponent'

        human_next_status_index = self.human_status_space_.index(
            human_next_status)
        return human_next_status_index

    def Maintenance(self, state):
        devices_status = self.devices_status_space_[state[2]]
        devices_next_status = devices_status.copy()
        Robot_pos = tuple(self.all_positions_[state[1]])
        if Robot_pos in devices_status:
            if devices_status[Robot_pos] == 'M':
                devices_next_status[Robot_pos] = 'G'

        devices_next_status_index = self.devices_status_space_.index(
            devices_next_status)
        return devices_next_status_index

    def Repair(self, state):
        devices_status = self.devices_status_space_[state[2]]
        human_status = self.human_status_space_[state[3]]

        devices_next_status = devices_status.copy()
        human_next_status = human_status

        Human_pos = tuple(self.all_positions_[state[0]])
        Robot_pos = tuple(self.all_positions_[state[1]])
        if Human_pos == Robot_pos:
            if Human_pos in devices_status:
                if devices_status[Human_pos] == 'B':
                    if human_status == 'HaveComponent':
                        devices_next_status[Robot_pos] = 'G'
                        human_next_status = 'NoComponent'

        devices_next_status_index = self.devices_status_space_.index(
            devices_next_status)
        human_next_status_index = self.human_status_space_.index(
            human_next_status)

        return devices_next_status_index, human_next_status_index
        # AgentPosAfterLeft = PosAfterMoveLeft(s[0])

    def WriteVisualizationFile(self, fout):
        for state_indicies in self.state_space_:
            res_temp = []
            for i in range(len(self.init_map_)):
                row = []
                for j in range(len(self.init_map_[0])):
                    if self.init_map_[i][j] == 'X':
                        row.append('X')
                    else:
                        row.append(0)

                res_temp.append(row)

            # print(state_indicies)
            human_pos_index = state_indicies[0]
            robot_pos_index = state_indicies[1]
            devices_status_index = state_indicies[2]
            human_status_index = state_indicies[3]

            human_pos = self.all_positions_[human_pos_index]
            robot_pos = self.all_positions_[robot_pos_index]
            devices_status = self.devices_status_space_[devices_status_index]
            # human_status = self.human_status_space_[human_status_index]

            # plot for human position and human status
            if human_status_index == 0:
                if res_temp[human_pos[0]][human_pos[1]] != 0:
                    res_temp[human_pos[0]][human_pos[1]] = 'HC_' + res_temp[human_pos[0]][human_pos[1]];
                else:
                    res_temp[human_pos[0]][human_pos[1]] = 'HC'
            else:
                if res_temp[human_pos[0]][human_pos[1]] != 0:
                    res_temp[human_pos[0]][human_pos[1]] = 'HN_' + res_temp[human_pos[0]][human_pos[1]];
                else:
                    res_temp[human_pos[0]][human_pos[1]] = 'HN'

            # plot for robot position
            if res_temp[robot_pos[0]][robot_pos[1]] != 0:
                res_temp[robot_pos[0]][robot_pos[1]] = 'R_' +  res_temp[robot_pos[0]][robot_pos[1]];
            else:
                res_temp[robot_pos[0]][robot_pos[1]] = 'R'

            # plot for all_devices
            for key in devices_status:
                if res_temp[key[0]][key[1]] != 0:
                    res_temp[key[0]][key[1]] += '_' + devices_status[key]
                else:
                    res_temp[key[0]][key[1]] = devices_status[key]

            first = True
            for row in res_temp:
                if first:
                    first = False
                else:
                    # fout.writelines(" ")
                    fout.writelines('.')
                    # fout.writelines("\\")

                fout.writelines(str(row))
            fout.write("\n")


# Here define different reward functions

    def RewardFuncHumanPreferLeft(self, joint_action_indices, state_index):
        state = self.state_space_[state_index]
        devices_status_index = state[2]

        reward = 0
        human_action_index = joint_action_indices[0]
        robot_action_index = joint_action_indices[1]
        # check action validation
        if self.CheckActionValid(joint_action_indices, state_index) == False:
            reward += self.penalty_invalid_action

        devices_status = self.devices_status_space_[devices_status_index]
        # Specify human preferred device location
        first_repair_device_location = self.left_device_pos
        second_repair_device_location = self.right_device_pos

        # !!! Human preference
        reward += self.CheckHumanPreferenceLeft(
            joint_action_indices, state_index)

        # human part
        if human_action_index == 6:  # human wait
            # if human waits after repairing all needed devices, relaxing, no penalty
            if devices_status[first_repair_device_location] == 'G' and devices_status[second_repair_device_location] == "G":
                reward += -0
            else:
                # human wait before repairing all needed devices, penalty
                # reward += -5
                reward += -1

        else:
            reward += -2

        # robot part
        if robot_action_index == 6:  # wait:
            reward += 0
        else:
            reward += -2

        # device part
        reward += self.CheckFinalReward(joint_action_indices, state_index)

        return reward

    def RewardFuncHumanPreferRight(self, joint_action_indices, state_index):
        state = self.state_space_[state_index]
        devices_status_index = state[2]

        reward = 0
        human_action_index = joint_action_indices[0]
        robot_action_index = joint_action_indices[1]
        # check action validation
        if self.CheckActionValid(joint_action_indices, state_index) == False:
            reward += self.penalty_invalid_action

        devices_status = self.devices_status_space_[devices_status_index]
        # Specify human preferred device location
        first_repair_device_location = self.right_device_pos
        second_repair_device_location = self.left_device_pos

        # !!! Human preference
        reward += self.CheckHumanPreferenceRight(
            joint_action_indices, state_index)

        # human part
        if human_action_index == 6:  # human wait
            # if human waits after repairing all needed devices, relaxing no penalty
            if devices_status[first_repair_device_location] == 'G' and devices_status[second_repair_device_location] == "G":
                reward += 0
            else:
                # human waits before repairing all needed devices penalty
                # reward += -5
                reward += -1
        else:
            reward += -2

        # robot part
        if robot_action_index == 6:  # wait:
            reward += 0
        else:
            reward += -2

        # device part
        reward += self.CheckFinalReward(joint_action_indices, state_index)

        return reward

    def RewardFuncBase(self, joint_action_indices, state_index):
        state = self.state_space_[state_index]
        devices_status_index = state[2]
        devices_status = self.devices_status_space_[devices_status_index]

        reward = 0
        human_action_index = joint_action_indices[0]
        robot_action_index = joint_action_indices[1]
        # check action validation
        if self.CheckActionValid(joint_action_indices, state_index) == False:
            reward += self.penalty_invalid_action

        # Specify human preferred device location
        # human part
        if human_action_index == 6:  # human wait
            # if human waits after repairing all needed devices, relaxing no penalty
            if devices_status[self.left_device_pos] == 'G' and devices_status[self.right_device_pos] == "G":
                reward += 0
            else:
                # human waits before repairing all needed devices, penalty
                # reward += -5
                reward += -1

        else:
            reward += -2

        # robot part
        if robot_action_index == 6:  # wait:
            reward += 0
        else:
            reward += -2

        # device part
        reward += self.CheckFinalReward(joint_action_indices, state_index)

        return reward

    def BuildAllRewardFuncBase(self):
        self.RewardFuncVec = {}
        for joint_action_indices in self.joint_action_indices_space_:
            for state_index in range(0, len(self.state_space_)):
                key_pair = (joint_action_indices, state_index)
                reward = self.RewardFuncBase(joint_action_indices, state_index)
                self.RewardFuncVec[key_pair] = reward

    def BuildAllRewardFuncHumanPreferLeft(self):
        self.RewardFuncVec = {}
        for joint_action_indices in self.joint_action_indices_space_:
            for state_index in range(0, len(self.state_space_)):
                key_pair = (joint_action_indices, state_index)
                reward = self.RewardFuncHumanPreferLeft(
                    joint_action_indices, state_index)
                self.RewardFuncVec[key_pair] = reward

    def BuildAllRewardFuncHumanPreferRight(self):
        self.RewardFuncVec = {}
        for joint_action_indices in self.joint_action_indices_space_:
            for state_index in range(0, len(self.state_space_)):
                key_pair = (joint_action_indices, state_index)
                reward = self.RewardFuncHumanPreferRight(
                    joint_action_indices, state_index)
                self.RewardFuncVec[key_pair] = reward


if __name__ == "__main__":
    R = 'R'
    H = 'H'
    X = 'X'
    DM = 'DM'
    DB = 'DB'

    # map = [[0, 0, R, 0, 0],
    #     [0, DM, DB, DM, 0],
    #     [0, 0, 0, 0, 0],
    #     [0, 0, H, X, 0]]

    # map = [[0, DB, DM],
    #     [0, 0, 0],
    #     [R, H, X]]

    # map = [[DB, DM, DB],
    #         [0,0,0],
    #         [R, X, H]]

    # map = [[DM, DB, X, DB],
    #         [R, 0, 0, H]]

    # map = [[DB, DM, 0, DB],
    #         [R, 0, X, H]]

    # map = [[DB, DM, DB],
    #         [R, X, H]]

    # map = [[DB, DM, 0, DB],
    #         [R, X, 0, H]]

    # map = [[DB, DM, 0, DB],
    #         [R, 0, X, H]]

    # map = [[DB, DM, 0, DB],
    #        [R, 0, X, H]]

    map = [[DB, DM, 0, DB],
           [0,0,0,0],
           [R, 0, X, H]]

    # map = [[DM, DB, DB],
    #     [R, X, H]]

    # map = [[DB, X, DB],
    #     [R, DM, H]]

    # map = [[DB, 0, DB],
    #         [DM,0,X],
    #         [R, 0, H]]

    # map = [[DM, X, DB],
    #     [R, H, 0]]

    fcwmodel = FCWModel(map)
    print(fcwmodel.devices_status_space_)
    print(len(fcwmodel.devices_status_space_))
    # print(fcwmodel.state_space_)
    print("state size:",len(fcwmodel.state_space_))
    print("human obs size:",len(fcwmodel.human_obs_space_))
    print("robot obs size:",len(fcwmodel.robot_obs_space_))
    print("joint A size:", len(fcwmodel.joint_action_indices_space_))

    output_file = "fcw_icra_base_test_big.dpomdp"
    f = open(output_file, "w")
    fcwmodel.BuildAllRewardFuncBase()
    fcwmodel.WriteDpomdpFile(f)

    output_file = "fcw_icra_human_prefer_left_test_big.dpomdp"
    f = open(output_file, "w")
    fcwmodel.BuildAllRewardFuncHumanPreferLeft()
    fcwmodel.WriteDpomdpFile(f)

    output_file = "fcw_icra_human_prefer_right_test_big.dpomdp"
    f = open(output_file, "w")
    fcwmodel.BuildAllRewardFuncHumanPreferRight()
    fcwmodel.WriteDpomdpFile(f)

    output_file_visualization = "fcw_icra_visualization_test_big"
    f = open(output_file_visualization, "w")
    fcwmodel.WriteVisualizationFile(f)

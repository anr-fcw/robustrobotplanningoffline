#include "Include/ParserPOMDPSparse.h"
#include "Include/BuildFSC.h"
#include "Include/ParserDecPOMDPSparse.h"
#include "Include/SimDecPomdp.h"
#include "Include/BestResponseMomdpModelSparse.h"
#include <numeric>

#include <filesystem>
using std::__fs::filesystem::directory_iterator;

const double error_gap = 0.01;

double DecPomdpPoliciesEvaluation(DecPomdpInterface *Pb, vector<string> &FSCs);
void BuildResSampleTable(vector<PomdpInterface *> all_pomdps, map<int, string> all_sample_BR_robot_fsc_paths, string outfile,
                         vector<double> &all_HLR_best_response_values,
                         map<int, map<int, double>> &HLR_values_i_not_equal_j,
                         map<int, map<int, double>> &HLR_values_all_i_j);
void EvaluationWithAllSampleFSCs(map<int, string> BR_robot_fsc_paths, map<int, string> human_fsc_paths, DecPomdpInterface *pb,
                                 vector<double> &values_i_equal_j,
                                 map<int, map<int, double>> &values_i_not_equal_j,
                                 map<int, map<int, double>> &values_all_i_j,
                                 vector<PomdpInterface *> &BRMs_human_objective);

void BuildFinalResultTableForAllSamples(vector<double> &HL_values_i_equal_j,
                                        map<int, map<int, double>> &HL_values_i_not_equal_j,
                                        map<int, map<int, double>> &HL_values_all_i_j,
                                        vector<double> &HR_values_i_equal_j,
                                        map<int, map<int, double>> &HR_values_i_not_equal_j,
                                        map<int, map<int, double>> &HR_values_all_i_j,
                                        vector<double> &all_HLR_best_response_values,
                                        map<int, map<int, double>> &HLR_values_i_not_equal_j,
                                        map<int, map<int, double>> &HLR_values_all_i_j,
                                        string outfile);
void AverageValueAndSem(vector<double> &vec, double &mean, double &sem);
void AverageValueAndSem(map<int, map<int, double>> &m, double &mean, double &sem);
void BuildAllBRMForEachHumanObjective(DecPomdpInterface *Pb, map<int, string> human_fsc_paths, string out_BRM_folder_path, vector<PomdpInterface *> &all_brms);
void ComputeSem(vector<double> &v, double &mean, double &sem);

int main()
{
    srand(time(NULL));

    string BRM_HL_pomdp_dir_path = "ExperimentsData_big_map/BRM_HL";
    string BRM_HR_pomdp_dir_path = "ExperimentsData_big_map/BRM_HR";
    string BRM_pomdp_dir_path = "ExperimentsData_big_map/RobotBestResponsePomdp";
    string robot_fsc_dir_path = "ExperimentsData_big_map/RobotBestResponseToSampledHumanFSC";
    string human_fsc_prefer_left_dir_path = "ExperimentsData_big_map/SampledHumanFSCPreferLeft";
    string human_fsc_prefer_right_dir_path = "ExperimentsData_big_map/SampledHumanFSCPreferRight";

    size_t size_brm_dir_path_string = BRM_pomdp_dir_path.size();
    size_t size_robot_fsc_dir_path_string = robot_fsc_dir_path.size();
    size_t size_human_fsc_prefer_left_dir_path = human_fsc_prefer_left_dir_path.size();
    size_t size_human_fsc_prefer_right_dir_path = human_fsc_prefer_right_dir_path.size();

    string out_sample_res = "results/EvaluationSamples.csv";
    string out_sample_final_table = "results/EvaluationSamplesFinalTable.csv";

    string decpomdp_human_prefer_left_path = "problem_folder/fcw_icra_human_prefer_left_test_big.dpomdp";
    string decpomdp_human_prefer_right_path = "problem_folder/fcw_icra_human_prefer_right_test_big.dpomdp";

    DecPomdpInterface *Pb_1 = new ParsedDecPOMDPSparse(decpomdp_human_prefer_left_path);
    DecPomdpInterface *Pb_2 = new ParsedDecPOMDPSparse(decpomdp_human_prefer_right_path);

    map<int, string> all_sample_BRM_paths;
    map<int, string> all_sample_BR_robot_fsc_paths;
    map<int, string> all_sample_human_fsc_prefer_left;
    map<int, string> all_sample_human_fsc_prefer_right;

    // 1. Read all fscs path
    for (const auto &file : directory_iterator(BRM_pomdp_dir_path))
    {
        string brm_path = file.path();
        string name = brm_path;
        name.erase(0, size_brm_dir_path_string + 1);
        int sample_index = stoi(name);
        all_sample_BRM_paths[sample_index] = brm_path;
    }

    for (const auto &file : directory_iterator(robot_fsc_dir_path))
    {
        string robot_fsc_path = file.path();
        string name = robot_fsc_path;
        name.erase(0, size_robot_fsc_dir_path_string + 1);
        int sample_index = stoi(name);
        all_sample_BR_robot_fsc_paths[sample_index] = robot_fsc_path;
    }

    for (const auto &file : directory_iterator(human_fsc_prefer_left_dir_path))
    {
        string human_prefer_left_fsc_path = file.path();
        string name = human_prefer_left_fsc_path;
        name.erase(0, size_human_fsc_prefer_left_dir_path + 1);
        int sample_index = stoi(name);
        all_sample_human_fsc_prefer_left[sample_index] = human_prefer_left_fsc_path;
    }

    for (const auto &file : directory_iterator(human_fsc_prefer_right_dir_path))
    {
        string human_prefer_right_fsc_path = file.path();
        string name = human_prefer_right_fsc_path;
        name.erase(0, size_human_fsc_prefer_right_dir_path + 1);
        int sample_index = stoi(name);
        all_sample_human_fsc_prefer_right[sample_index] = human_prefer_right_fsc_path;
    }

    // 2. Read all best responses pomdps
    vector<PomdpInterface *> all_pomdps;    // both objective
    vector<PomdpInterface *> all_pomdps_HL; // human prefer left
    vector<PomdpInterface *> all_pomdps_HR; // human prefer right

    for (size_t sample_brm_index = 0; sample_brm_index < all_sample_BRM_paths.size(); sample_brm_index++)
    {
        cout << all_sample_BRM_paths[sample_brm_index] << endl;
        PomdpInterface *BRMPb = new ParsedPOMDPSparse(all_sample_BRM_paths[sample_brm_index]);
        all_pomdps.push_back(BRMPb);
    }

    BuildAllBRMForEachHumanObjective(Pb_1, all_sample_human_fsc_prefer_left, BRM_HL_pomdp_dir_path, all_pomdps_HL);
    BuildAllBRMForEachHumanObjective(Pb_2, all_sample_human_fsc_prefer_right, BRM_HR_pomdp_dir_path, all_pomdps_HR);

    // storing values for all br robot fscs and sample human fscs
    vector<double> HL_values_i_equal_j;
    map<int, map<int, double>> HL_values_i_not_equal_j;
    map<int, map<int, double>> HL_values_all_i_j;
    vector<double> HR_values_i_equal_j;
    map<int, map<int, double>> HR_values_i_not_equal_j;
    map<int, map<int, double>> HR_values_all_i_j;
    vector<double> all_HLR_best_response_values;
    map<int, map<int, double>> HLR_values_i_not_equal_j;
    map<int, map<int, double>> HLR_values_all_i_j;

    vector<double> robust_robot_HLR_values;

    EvaluationWithAllSampleFSCs(all_sample_BR_robot_fsc_paths, all_sample_human_fsc_prefer_left, Pb_1, HL_values_i_equal_j, HL_values_i_not_equal_j, HL_values_all_i_j, all_pomdps_HL);
    EvaluationWithAllSampleFSCs(all_sample_BR_robot_fsc_paths, all_sample_human_fsc_prefer_right, Pb_2, HR_values_i_equal_j, HR_values_i_not_equal_j, HR_values_all_i_j, all_pomdps_HR);

    BuildResSampleTable(all_pomdps, all_sample_BR_robot_fsc_paths, out_sample_res, all_HLR_best_response_values, HLR_values_i_not_equal_j, HLR_values_all_i_j);

    BuildFinalResultTableForAllSamples(HL_values_i_equal_j,
                                       HL_values_i_not_equal_j,
                                       HL_values_all_i_j,
                                       HR_values_i_equal_j,
                                       HR_values_i_not_equal_j,
                                       HR_values_all_i_j,
                                       all_HLR_best_response_values,
                                       HLR_values_i_not_equal_j,
                                       HLR_values_all_i_j, out_sample_final_table);

    delete Pb_1;
    delete Pb_2;

    FreeAllPomdpModels(all_pomdps);
    FreeAllPomdpModels(all_pomdps_HL);
    FreeAllPomdpModels(all_pomdps_HR);
    return 0;
}

void BuildResSampleTable(vector<PomdpInterface *> all_pomdps, map<int, string> all_sample_BR_robot_fsc_paths, string outfile,
                         vector<double> &all_HLR_best_response_values,
                         map<int, map<int, double>> &HLR_values_i_not_equal_j,
                         map<int, map<int, double>> &HLR_values_all_i_j)
{

    ofstream outlogs;
    outlogs.open(outfile.c_str());
    string first_row_str = " ";
    for (size_t sample_brm_index = 0; sample_brm_index < all_pomdps.size(); sample_brm_index++)
    {
        string name = "sample_brm_" + to_string(sample_brm_index);
        first_row_str += "," + name;
    }

    outlogs << first_row_str << endl;

    for (size_t robot_fsc_i = 0; robot_fsc_i < all_sample_BR_robot_fsc_paths.size(); robot_fsc_i++)
    {
        string row_name = "sample_robot_" + to_string(robot_fsc_i);
        string robot_fsc_path = all_sample_BR_robot_fsc_paths[robot_fsc_i];

        outlogs << row_name;
        for (size_t sample_brm_i = 0; sample_brm_i < all_pomdps.size(); sample_brm_i++)
        {
            PomdpInterface *BRMPb = all_pomdps[sample_brm_i];
            int robot_obs_size = BRMPb->GetSizeOfObs();
            FSCBase robot_fsc(robot_fsc_path, robot_obs_size, 0);
            double v_temp = robot_fsc.PolicyEvaluation(BRMPb, 0.01);

            HLR_values_all_i_j[robot_fsc_i][sample_brm_i] = v_temp;
            if (robot_fsc_i == sample_brm_i)
            {
                all_HLR_best_response_values.push_back(v_temp);
            }
            else
            {
                HLR_values_i_not_equal_j[robot_fsc_i][sample_brm_i] = v_temp;
            }

            outlogs << ", " << v_temp;
        }
        outlogs << endl;
    }
    outlogs.close();
}

void EvaluationWithAllSampleFSCs(map<int, string> BR_robot_fsc_paths, map<int, string> human_fsc_paths, DecPomdpInterface *pb,
                                 vector<double> &values_i_equal_j,
                                 map<int, map<int, double>> &values_i_not_equal_j,
                                 map<int, map<int, double>> &values_all_i_j,
                                 vector<PomdpInterface *> &BRMs_human_objective)
{
    // i is robot
    // j is human

    // vector<double> values_i_equal_j;
    // map<int, map<int, double>> values_i_not_equal_j;
    // map<int, map<int, double>> values_all_i_j;
    int robot_index = 1;
    int obs_robot_size = pb->GetSizeOfObs(robot_index);
    int sample_size = BR_robot_fsc_paths.size();
    for (int i_robot = 0; i_robot < sample_size; i_robot++)
    {
        string robot_fsc_path = BR_robot_fsc_paths[i_robot];
        FSCBase FscRobot(robot_fsc_path, obs_robot_size, 0);

        for (int i_human = 0; i_human < sample_size; i_human++)
        {
            string human_fsc_path = human_fsc_paths[i_human];
            PomdpInterface *BRM = BRMs_human_objective[i_human];
            double V = FscRobot.PolicyEvaluation(BRM, error_gap);

            values_all_i_j[i_robot][i_human] = V;
            if (i_robot == i_human)
            {
                values_i_equal_j.push_back(V);
            }
            else
            {
                values_i_not_equal_j[i_robot][i_human] = V;
            }
        }
    }
}

void BuildFinalResultTableForAllSamples(vector<double> &HL_values_i_equal_j,
                                        map<int, map<int, double>> &HL_values_i_not_equal_j,
                                        map<int, map<int, double>> &HL_values_all_i_j,
                                        vector<double> &HR_values_i_equal_j,
                                        map<int, map<int, double>> &HR_values_i_not_equal_j,
                                        map<int, map<int, double>> &HR_values_all_i_j,
                                        vector<double> &all_HLR_best_response_values,
                                        map<int, map<int, double>> &HLR_values_i_not_equal_j,
                                        map<int, map<int, double>> &HLR_values_all_i_j,
                                        string outfile)
{
    ofstream outlogs;
    outlogs.open(outfile.c_str());

    double avg_V_HL_i_equal_j, avg_V_HL_i_not_equal_j, avg_V_HL_all_i_j, avg_V_HR_i_equal_j, avg_V_HR_i_not_equal_j, avg_V_HR_all_i_j, avg_V_HLR_i_equal_j, avg_V_HLR_i_not_equal_j, avg_V_HLR_all_i_j = 0;
    double sem_V_HL_i_equal_j, sem_V_HL_i_not_equal_j, sem_V_HL_all_i_j, sem_V_HR_i_equal_j, sem_V_HR_i_not_equal_j, sem_V_HR_all_i_j, sem_V_HLR_i_equal_j, sem_V_HLR_i_not_equal_j, sem_V_HLR_all_i_j = 0;

    AverageValueAndSem(HL_values_i_equal_j, avg_V_HL_i_equal_j, sem_V_HL_i_equal_j);
    AverageValueAndSem(HL_values_i_not_equal_j, avg_V_HL_i_not_equal_j, sem_V_HL_i_not_equal_j);
    AverageValueAndSem(HL_values_all_i_j, avg_V_HL_all_i_j, sem_V_HL_all_i_j);
    AverageValueAndSem(HR_values_i_equal_j, avg_V_HR_i_equal_j, sem_V_HR_i_equal_j);
    AverageValueAndSem(HR_values_i_not_equal_j, avg_V_HR_i_not_equal_j, sem_V_HR_i_not_equal_j);
    AverageValueAndSem(HR_values_all_i_j, avg_V_HR_all_i_j, sem_V_HR_all_i_j);
    AverageValueAndSem(all_HLR_best_response_values, avg_V_HLR_i_equal_j, sem_V_HLR_i_equal_j);
    AverageValueAndSem(HLR_values_i_not_equal_j, avg_V_HLR_i_not_equal_j, sem_V_HLR_i_not_equal_j);
    AverageValueAndSem(HLR_values_all_i_j, avg_V_HLR_all_i_j, sem_V_HLR_all_i_j);

    outlogs << "avg_V_HL_i_equal_j, " << avg_V_HL_i_equal_j << endl;
    outlogs << "avg_V_HL_i_not_equal_j, " << avg_V_HL_i_not_equal_j << endl;
    outlogs << "avg_V_HL_all_i_j, " << avg_V_HL_all_i_j << endl;
    outlogs << "avg_V_HR_i_equal_j, " << avg_V_HR_i_equal_j << endl;
    outlogs << "avg_V_HR_i_not_equal_j, " << avg_V_HR_i_not_equal_j << endl;
    outlogs << "avg_V_HR_all_i_j, " << avg_V_HR_all_i_j << endl;
    outlogs << "avg_V_HLR_i_equal_j, " << avg_V_HLR_i_equal_j << endl;
    outlogs << "avg_V_HLR_i_not_equal_j, " << avg_V_HLR_i_not_equal_j << endl;
    outlogs << "avg_V_HLR_all_i_j, " << avg_V_HLR_all_i_j << endl;

    outlogs << endl;

    outlogs << "sem_V_HL_i_equal_j, " << sem_V_HL_i_equal_j << endl;
    outlogs << "sem_V_HL_i_not_equal_j, " << sem_V_HL_i_not_equal_j << endl;
    outlogs << "sem_V_HL_all_i_j, " << sem_V_HL_all_i_j << endl;
    outlogs << "sem_V_HR_i_equal_j, " << sem_V_HR_i_equal_j << endl;
    outlogs << "sem_V_HR_i_not_equal_j, " << sem_V_HR_i_not_equal_j << endl;
    outlogs << "sem_V_HR_all_i_j, " << sem_V_HR_all_i_j << endl;
    outlogs << "sem_V_HLR_i_equal_j, " << sem_V_HLR_i_equal_j << endl;
    outlogs << "sem_V_HLR_i_not_equal_j, " << sem_V_HLR_i_not_equal_j << endl;
    outlogs << "sem_V_HLR_all_i_j, " << sem_V_HLR_all_i_j << endl;

    outlogs.close();
}

void AverageValueAndSem(vector<double> &vec, double &mean, double &sem)
{
    ComputeSem(vec, mean, sem);
}

void AverageValueAndSem(map<int, map<int, double>> &m, double &mean, double &sem)
{
    vector<double> all_values;
    map<int, map<int, double>>::iterator it_i;
    for (it_i = m.begin(); it_i != m.end(); it_i++)
    {
        map<int, double> values = it_i->second;
        map<int, double>::iterator it_j;
        for (it_j = values.begin(); it_j != values.end(); it_j++)
        {
            all_values.push_back(it_j->second);
        }
    }

    ComputeSem(all_values, mean, sem);
}

void ComputeSem(vector<double> &v, double &mean, double &sem)
{
    double sum = std::accumulate(v.begin(), v.end(), 0.0);
    mean = sum / v.size();

    double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size() - mean * mean);
    sem = stdev / sqrt(v.size());
}

// Only for two agent case
double DecPomdpPoliciesEvaluation(DecPomdpInterface *Pb, vector<string> &FSCs)
{
    vector<FSCBase> all_FSCs;
    for (int agentI = 0; agentI < Pb->GetNbAgents(); agentI++)
    {
        int ObsSize = Pb->GetSizeOfObs(agentI);
        FSCBase FscAgentI(FSCs[agentI], ObsSize, 0);
        all_FSCs.push_back(FscAgentI);
    }
    int agent_index = 1; // build a POMDP in point of view of who
    BestResponseMomdpModelSparse *bestresponseMomdpPomdp_temp = new BestResponseMomdpModelSparse(Pb, all_FSCs, agent_index);
    bestresponseMomdpPomdp_temp->ExportPOMDP();
    delete bestresponseMomdpPomdp_temp;
    bestresponseMomdpPomdp_temp = nullptr;

    PomdpInterface *BRMPb = new ParsedPOMDPSparse("./TempFiles/BestResponseForAgentI.pomdp");

    double error_gap = 0.001;
    double v = all_FSCs[agent_index].PolicyEvaluation(BRMPb, error_gap);

    delete BRMPb;
    return v;
}

void BuildAllBRMForEachHumanObjective(DecPomdpInterface *Pb, map<int, string> human_fsc_paths, string out_BRM_folder_path, vector<PomdpInterface *> &all_brms)
{
    int human_index = 0;
    int ObsSize = Pb->GetSizeOfObs(human_index);
    for (size_t i = 0; i < human_fsc_paths.size(); i++)
    {
        string human_fsc_path = human_fsc_paths[i];
        FSCBase FscAgentI(human_fsc_path, ObsSize, 0);
        vector<FSCBase> all_fscs(2);
        all_fscs[human_index] = FscAgentI;
        int optimizing_agent_index = 1; // build a POMDP in point of view of the robot
        BestResponseMomdpModelSparse *bestresponseMomdpPomdp_temp = new BestResponseMomdpModelSparse(Pb, all_fscs, optimizing_agent_index);
        string out_brm_path = out_BRM_folder_path + "/" + to_string(i) + ".pomdp";
        bestresponseMomdpPomdp_temp->ExportPOMDP(out_brm_path);
        delete bestresponseMomdpPomdp_temp;
        bestresponseMomdpPomdp_temp = nullptr;
        PomdpInterface *BRMPb = new ParsedPOMDPSparse(out_brm_path);
        all_brms.push_back(BRMPb);
    }
}

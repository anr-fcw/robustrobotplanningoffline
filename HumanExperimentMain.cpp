#include "Include/SampleStochasticLocalFsc.h"
#include "Include/SimDecPomdp.h"
#include "Include/ParserDecPOMDPSparse.h"
#include <ctime>

using namespace std;
// void GameIntro(SimDecPomdp &Sim);
string CreateLogFileName();
void GameIntro();

vector<vector<int>> ReadParamsFile(string params_file, int subject_index);

int main(int argc, char *argv[])
{
  string str_subject_index = argv[argc - 1];
  int subject_index = stoi(str_subject_index);

  // ---- Hard coded parts ----
  string decpomdp_path = "problem_folder/fcw_icra_base_test_big.dpomdp";
  string vis_path = "./HumanExperimentsData/fcw_icra_visualization_test_big";
  // define robot policy set
  string robot_pi_0 = "./HumanExperimentsData/robot_policies/RobustRobotPolicyT0.fsc";
  string robot_pi_1 = "./HumanExperimentsData/robot_policies/RobustRobotPolicyT03.fsc";
  string robot_pi_2 = "./HumanExperimentsData/robot_policies/RobustRobotPolicyT05.fsc";

  vector<string> robot_pi_list = {robot_pi_0, robot_pi_1, robot_pi_2};
  string params_file = "./HumanExperimentsData/subjects_params_file.txt";
  // ---- Hard coded parts ----

  cout << "Subject index: " << subject_index << endl;
  vector<vector<int>> params = ReadParamsFile(params_file, subject_index);

  int max_step = 30;
  int nb_rounds = 24;
  int Rperiodicity = 8; // robot periodicity
  int Mperiodicity = 2; // map periodicity
  int formalization = 0;
  DecPomdpInterface *Pb = new ParsedDecPOMDPSparse(decpomdp_path);
  int HumanIndex = 0;
  int RobotIndex = 1;

  ofstream outlogs;
  string log_name = CreateLogFileName();
  outlogs.open(log_name.c_str());

  for (int i = 0; i < max_step; i++)
  {
    outlogs << " , step " << i;
  }
  outlogs << ", Finish type, Time used, Steps used, Acc rewards " << endl;

  system("clear");

  GameIntro();

  // terminal reward = 96
  double terminal_reward = 96;

  for (int i = 0; i < nb_rounds; i++)
  {
    SimDecPomdp Sim(Pb, robot_pi_list[params[i][2]], RobotIndex, HumanIndex, vis_path, formalization);
    Sim.SetTerminalReward(terminal_reward);
    outlogs << "Round " << i + 1;
    double acc_rewards = 0;
    int final_step = 0;

    if ((i % Rperiodicity == 0) || (i % Mperiodicity == 0))
      system("clear");

    if (i % Rperiodicity == 0)
    {
      cout << "*****************************" << endl
           << "* Now playing with Robot #" << (i / Rperiodicity) << " *" << endl
           << "*****************************" << endl
           << endl;
    }

    if (i % Mperiodicity == 0)
    {
      cout << "*****************************" << endl
           << "* Now playing on Map #" << (i % Rperiodicity) / Mperiodicity << " *" << endl
           << "*****************************" << endl
           << endl;
    }

    if ((i % Rperiodicity == 0) || (i % Mperiodicity == 0))
    {
      cout << "Press [ENTER] to continue." << endl;
      system("read VAR");
    }

    int finish_type = Sim.SimulateNsteps(max_step, final_step, acc_rewards, outlogs, params[i][0], params[i][1]);

    if (finish_type == 2)
    {
      break;
    }

    cout << endl;

    switch (i)
    {
    case 0:
      cout << "1st ";
      break;
    case 1:
      cout << "2nd ";
      break;
    case 2:
      cout << "3rd ";
      break;
    default:
      cout << i + 1 << "th ";
    }

    /*
          cout << "try of this collaboration game is finished! You use " << final_step << " time steps with accumulated rewards: " << acc_rewards << endl;
          cout << "Press any key to continue." << endl;
    */

    cout << "round finished!" << endl;
    if (final_step != 0)
      cout << "Duration: " << final_step << " time steps." << endl;
    cout << endl;

    cout << "Press [ENTER] to continue." << endl;
    system("read VAR");

    Sim.Reset();
  }

  return 0;
}

vector<vector<int>> ReadParamsFile(string params_file, int subject_index)
{
  vector<string> pi_robot_set = {"T0", "T03", "T05"};
  vector<vector<int>> res;
  // open input file containing a pomdp file
  ifstream infile;
  infile.open(params_file);
  if (!infile.is_open())
    cout << "open params file failure" << endl;

  // TODO rename temp as 'line'
  string line;

  // go through the file line by line
  // First Get discount and all the state, action and observation space
  int current_index = 0;
  while (getline(infile, line))
  {

    // analyse the line
    istringstream is(line);
    string s;

    if (current_index == subject_index)
    {
      // values corresponding to parsing steps
      vector<int> temp_params_round;
      string str_pi_robot = "";

      int nb_round = 0;

      while (is >> s)
      {
        if (s != ",")
        {
          if (s.size() == 1)
          {
            temp_params_round.push_back(stoi(s));
          }
          else
          {
            str_pi_robot += s;
          }
        }
        else
        {
          std::vector<string>::iterator it = std::find(pi_robot_set.begin(), pi_robot_set.end(), str_pi_robot);
          int subject_index = std::distance(pi_robot_set.begin(), it);
          temp_params_round.push_back(subject_index);
          res.push_back(temp_params_round);
          temp_params_round.clear();
          str_pi_robot = "";
          nb_round += 1;
        }
      }
      break;
    }

    current_index += 1;
  }

  return res;
}

// void GameIntro(SimDecPomdp &Sim)
void GameIntro()
{
  system("clear");

  cout << "Before starting, here is a reminder of available commands:" << endl;
  cout << endl;
  cout << "***************************" << endl;
  printInstructions();
  cout << "***************************" << endl;

  cout << endl;

  cout << "***************************" << endl;
  printLegend();
  cout << "***************************" << endl;

  cout << endl;
  cout << "Press [ENTER] to start." << endl;

  /*
    cout << endl;
    cout << "This is a collaboration game between a human player and a robot (AI)." << endl;
    // cout << "The initial state is illustrated as below:" << endl;
    // cout << endl;
    // Sim.VisualizationStateUsingTabulate(0);
    cout << endl;

    cout << "- Goal -" << endl;
    cout << "The goal is to repair all broken devices (symbol B) and maintain the needed device (symbol M)." << endl;
    cout << "To be successful in this game, all device's status need to be in good status (symbol G)." << endl;
    cout << endl;

    cout << "- Ability -" << endl;
    cout << "Your (the human player) ability:" << endl;
    cout << "   1. Movements (Up, Down, Left, Right);" << endl;
    cout << "   2. Repair (Please see the explanation below );" << endl;
    cout << "   3. Pick Component (If you do not have a component (symbol HN) and you are in the toolbox location (symbol X)), once a component is picked, you status will change (symbol HC);" << endl;
    cout << "   4. Wait (Stay at the same position);" << endl;
    cout << "Robot's (AI) ability:" << endl;
    cout << "   1. Movements (Up, Down, Left, Right);" << endl;
    cout << "   2. Repair (Please see the explanation below );" << endl;
    cout << "   3. Maintenance (Only the robot can do the maintenance);" << endl;
    cout << "   4. Wait (Stay at the same position);" << endl;
    cout << endl;

    cout << "- How to repair a broken device? -" << endl;
    cout << "to repair a broken device: " << endl;
    cout << " 1. You need to have a component, you can pick a component at the toolbox location (symbol X );" << endl;
    cout << " 2. You and the robot must be in the same cell with the broken device;" << endl;
    cout << " 3. You and the robot must do the repair action simultaneously;" << endl;
    cout << " 4. After a successful reparation, your picked component will be consumed." << endl;
    cout << endl;

    cout << "You (the human player) are located at the bottom right corner in this map, and the robot is on the bottom left corner." << endl;

    cout << "Please read the game introduction, then press any key to continue." << endl;
  */

  system("read VAR");
}

string CreateLogFileName()
{
  time_t rawtime;
  struct tm *timeinfo;
  char buffer[80];

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer, sizeof(buffer), "-%Y%m%d-%H:%M:%S", timeinfo);
  std::string str_time(buffer);
  string log_file_name = "HumanExperimentsData/log_experiments" + str_time + ".csv";

  return log_file_name;
}

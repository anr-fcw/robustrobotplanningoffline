
#include <iostream>
#include "Include/ParserDecPOMDPSparse.h"
#include "Include/ParserPOMDPSparse.h"
#include "Include/ParserSarsopResult.h"
#include "Include/BuildFSC.h"
#include "Include/RobotRobustPlanModelSparse.h"
#include "Include/SampleLocalFsc.h"
#include "Include/BestResponseMomdpModelSparse.h"
#include "Include/SampleStochasticLocalFsc.h"
#include <stdlib.h>
#include <stdio.h>
#include <filesystem>
#include <getopt.h>

using std::cin;
using std::cout;
using std::endl;
using std::string;
// using std::__fs::filesystem::directory_iterator;

// ---- Please provide a SARSOP solver path here! ----
const string solver_path = "SARSOP_PATH/pomdpsol";
// ---------------------------------------------------
vector<double> Best_Responses_values_alpha_vecs;
void UsageManual();
double DecPomdpPoliciesEvaluation(DecPomdpInterface *Pb, vector<FSCBase> &FSCs);
void BuildMixNashEqilibria(vector<DecPomdpInterface *> decpomdp_models, vector<FSCBase> &human_fscs,
                           vector<FSCBase> &robot_fscs, vector<string> &human_fsc_names, vector<string> &robot_fsc_names);
int main(int argc, char *argv[])
{

    string time_file = "results/time_used.csv";
    ofstream outlogs;
    outlogs.open(time_file.c_str());
    time_t PlanStartTime, PlanEndTime;
    PlanStartTime = time(NULL);

    int index = 0;

    // ------------- Parameters --------------------
    int human_index = 0;
    int robot_index = 1;
    double error_gap = 0.01;
    int formalization_type = 0;        // MOMDP formalization
    int Mpomdp_sampling_init_type = 1; // 1. Stochastic; 2. Deterministic
    int max_node_size = 600;           // max human fsc size
    double T = 0.5;                    // used for softmax
    double min_pb = 0.1;               // min human action pb, lower action pb will be eliminated
    double max_belief_gap = 0.01;      // used for creating human fsc
    double c = 15;                     // used for POMCP
    int init_node_size = 6000;         // mcts
    // string decpomdps_dir_path;
    vector<SampleStochasticLocalFsc> FSCs_possible_human;
    vector<DecPomdpInterface *> decpomdp_models;
    vector<DecPomdpInterface *> noise_decpomdp_models;

    vector<vector<AlphaVector>> MpomdpResults;
    double noise_rate = 0.1; // used for building a MPOMDP with noise robot action execution

    bool evaluate_all_policy_pairs = false;

    // ----------------- 1. Dec-POMDPs -> MPOMDPs ---------------------------

    string path_pb_1 = "problem_folder/fcw_icra_human_prefer_left_test_big.dpomdp";
    string path_pb_2 = "problem_folder/fcw_icra_human_prefer_right_test_big.dpomdp";

    DecPomdpInterface *Pb_1 = new ParsedDecPOMDPSparse(path_pb_1);
    DecPomdpInterface *Pb_2 = new ParsedDecPOMDPSparse(path_pb_2);

    decpomdp_models.push_back(Pb_1);
    decpomdp_models.push_back(Pb_2);

    for (size_t i = 0; i < decpomdp_models.size(); i++)
    {
        string MpomdpName = "Mpomdp" + to_string(i) + ".pomdp";
        ExportMpomdpModel(decpomdp_models[i], MpomdpName);
    }
    time_t DecPOMDPsToMPOMDPsEndTime = time(NULL);

    // --------------- 2. Solve MPOMDPs ------------------

    for (size_t i = 0; i < decpomdp_models.size(); i++)
    {
        string MpomdpPath = "Mpomdp" + to_string(i) + ".pomdp";
        string command = solver_path + " -p " + to_string(error_gap) + " " + MpomdpPath;
        const char *p = command.data();
        system(p);
        cout << "SARSOP launched to solve the MPOMDP!" << endl;
        const string sarsop_res_path = "out.policy";
        const string output_mpomdp_res_path = "MpomdpAlphaVecsTemp";
        transformToMADPformat(sarsop_res_path, output_mpomdp_res_path);
        vector<AlphaVector> MpomdpAlphaVecs = ImportValueFunction(output_mpomdp_res_path);
        MpomdpResults.push_back(MpomdpAlphaVecs);
    }
    time_t SolveMPOMDPsEndTime = time(NULL);

    // -------------- 3. Sampling Human Policies --------------

    for (size_t i = 0; i < MpomdpResults.size(); i++)
    {
        SampleStochasticLocalFsc localfsc;

        MCTS mcts(decpomdp_models[i], MpomdpResults[i]);
        mcts.Init(init_node_size, c);

        localfsc.Init(T, min_pb, max_belief_gap, mcts);
        localfsc.BuildFsc(decpomdp_models[i], human_index, Mpomdp_sampling_init_type, formalization_type, max_node_size);
        string filename = "results/Objective" + to_string(i) + "Human" + ".fsc";
        string graph_name = "results/Objective" + to_string(i) + "Human" + ".dot";
        string graph_pdf_name = "results/Objective" + to_string(i) + "Human" + ".pdf";
        string fsc_belief_file = "results/Objective" + to_string(i) + "Human" + ".beliefs";

        localfsc.SaveGraph(graph_name);
        localfsc.ExportStochasticFSC(filename);
        localfsc.ExportAllNodesBelief(fsc_belief_file);

        FSCs_possible_human.push_back(localfsc);
    }
    time_t SampleHumanFSCsEndTime = time(NULL);

    // ---------- 4. Test Build Robot POMDP ----------
    RobotRobustPlanModelSparse RobotModel(decpomdp_models, FSCs_possible_human, human_index, robot_index);
    string result_pomdp_Path = "RobotRobustPlanningModel.pomdp";
    RobotModel.ExportPOMDP(result_pomdp_Path);

    time_t BuildRobotPOMDPEndTime = time(NULL);
    // -------------- 5. Solving Robot POMDP -------------------------
    string command = solver_path + " -p " + to_string(error_gap) + " " + result_pomdp_Path;
    const char *p = command.data();
    system(p);
    cout << "SARSOP finished solving the MPOMDP!" << endl;
    const string sarsop_res_path = "out.policy";
    const string output_alphavecs_res_path = "AlphaVecsResult";
    transformToMADPformat(sarsop_res_path, output_alphavecs_res_path);
    vector<AlphaVector> Res_AlphaVecs = ImportValueFunction(output_alphavecs_res_path);

    PomdpInterface *BRMPb = new ParsedPOMDPSparse(result_pomdp_Path);
    double V_alphavecs = EvaluationWithAlphaVecs(BRMPb, Res_AlphaVecs); // ADD also this value to the logs result
    cout << "Evaluation with alpha-vector:" << V_alphavecs << endl;
    time_t SolveRobotPOMDPEndTime = time(NULL);

    PlanEndTime = time(NULL);
    double total_time = (double)(PlanEndTime - PlanStartTime);

    double DecPOMDPsToMPOMDPsTime = (double)(DecPOMDPsToMPOMDPsEndTime - PlanStartTime);
    double SolveMPOMDPsTime = (double)(SolveMPOMDPsEndTime - DecPOMDPsToMPOMDPsEndTime);
    double SampleHumanFSCsTime = (double)(SampleHumanFSCsEndTime - SolveMPOMDPsEndTime);
    double BuildRobotPOMDPTime = (double)(BuildRobotPOMDPEndTime - SampleHumanFSCsEndTime);
    double SolveRobotPOMDPTime = (double)(SolveRobotPOMDPEndTime - BuildRobotPOMDPEndTime);

    outlogs << " 1_DecPOMDPs_to_MPOMDPs, " << DecPOMDPsToMPOMDPsTime << endl;
    outlogs << " 2_Solve_MPOMDPs, " << SolveMPOMDPsTime << endl;
    outlogs << " 3_Sample_Human_FSCs, " << SampleHumanFSCsTime << endl;
    outlogs << " 4_Build_Robot_POMDP, " << BuildRobotPOMDPTime << endl;
    outlogs << " 5_Solve_Robot_POMDP, " << SolveRobotPOMDPTime << endl;
    outlogs << "Total, " << total_time << endl;

    outlogs.close();
    // -------------- 6. Extract Robot FSC With Evaluation ------------------------------

    // FSC fsc(Res_AlphaVecs, BRMPb, formalization_type, error_gap);
    FSC fsc(Res_AlphaVecs, BRMPb, 0.01, 0.01, 1000);
    string file_robust_robot_fsc = "results/RobustRobotPolicy.fsc";
    string file_robust_robot_graph = "results/RobustRobotPolicy.dot";

    fsc.ExportFSC(file_robust_robot_fsc);
    double V_fsc = fsc.PolicyEvaluation(); //!!! Takes time !!!
    fsc.SaveGraph(BRMPb, file_robust_robot_graph);
    cout << "Evaluation with final fsc:" << V_fsc << endl;

    string graph_pdf_name = "results/RobustRobotPolicy.pdf";
    cout << "Total Time for robust robot plan:" << total_time << "s" << endl;

    return 0;
}

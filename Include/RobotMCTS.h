#ifndef _ROBOTMCTS_H_
#define _ROBOTMCTS_H_

#include "TreeNode.h"
#include "SoftMaxDist.h"

class RobotMCTS
{
private:
      int current_node_number = 0;
      int max_node_number = 20;
      DecPomdpInterface *pb; // should not have this!!! later should delete it, POMCP/MCTS only need a model generator G!
      TreeNode rootnode;
      // SimPomdpMCTS simulator;
      int size_A;
      // int nb_restarts_simulation = 100;
      // double epsilon = 0.01;
      double discount;
      vector<TreeNode> all_nodes;
      vector<AlphaVector> alpha_vecs;
      int RobotIndex;
      int HumanIndex;
      MCTS *mcts;

public:
      RobotMCTS(DecPomdpInterface *pb, vector<AlphaVector> &alpha_vecs, int human_index, int robot_index, MCTS &mcts);
      ~RobotMCTS(){};
      // int RolloutPolicy(BeliefSparse &b);                                  // give a belief, return a action index
      // double Simulation(BeliefSparse &b, int nb_restarts, double epsilon); // from a belief b, start a simulation using rollout policy and return accumulated rewards
      TreeNode *Selection(TreeNode *start_node);
      void Expansion(TreeNode *node);
      void BackPropagation(TreeNode *node);    // back propagate the value from the leaf node to the parent node untill the root node
      int Plan(BeliefSparse b, double &value); // return an action
};
#endif
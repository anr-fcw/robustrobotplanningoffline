#ifndef _SIMPOMDPMCTS_H_
#define _SIMPOMDPMCTS_H_

#include "PomdpInterface.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <cmath>

using namespace std;

class SimPomdpMCTS
{
private:
      PomdpInterface *PomdpModel;
      bool interactive = false;
      int stateI; // Current State
      vector<string> visualization_states;
      // Cool funcs below
      int SampleStartState();
      void Reset();
      // double Reward(vector<int> actions);
      int GetObsFromState(int jaI);
      void addNewLines(std::string *text);

public:
      SimPomdpMCTS(){};
      SimPomdpMCTS(PomdpInterface *PomdpModel, string VisfilePath);
      SimPomdpMCTS(PomdpInterface *PomdpModel);
      ~SimPomdpMCTS();
      int SelectActions();
      void SimulateNsteps(int N); // Print Executions and average accumlated rewards
      void VisualizationState(int sI);
      void LoadVisualizationFile(string fileName);
      tuple<int, int, bool, double, string> Step(int sI, int aI);
      int GetCurrentState() { return this->stateI; };
};

#endif
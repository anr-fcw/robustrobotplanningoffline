/* This file has been written and/or modified by the following people:
 *
 * Anonymous for AAMAS reviewing process
 * 
 */

#ifndef _PARSERPOMDPSPARSE_H_
#define _PARSERPOMDPSPARSE_H_ 1

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <sstream>    
#include "PomdpInterface.h"
using namespace std;

class ParsedPOMDPSparse: public PomdpInterface
{
private:
    // set of states
    vector<string> States;
    int S_size; 

    // set of actions
    vector<string> Actions;
    int A_size; 

    // set of observations
    vector<string> Observations;
    int Obs_size;

    // initial belief
    vector<double> b0;

    map<int, double> b0_sparse;

    // transition function as A -> S -> P(S)
    // vector< vector< map<int,double> > > TransFuncVecs;
    vector< vector< unordered_map<int,double> > > TransFuncVecs;


    // observation as A -> S' -> O -> proba
    // vector<vector<vector<double>>> ObsFuncVecs;
    vector< vector< map<int,double> > > ObsFuncVecs;


    // reward function as A -> S -> reward
    vector<vector<double>> RewardFuncVecs;

    // discount factor
    double discount;


public:
    // builds a POMDP from a file
    ParsedPOMDPSparse(const string filename);

    // destroys a POMDP
    ~ParsedPOMDPSparse();

    // get discount value
    double GetDiscount();

    // get different sets and corresponding sizes
    vector<string> GetAllStates(){return this->States;};
    vector<string> GetAllActions() {return this->Actions;};
    vector<string> GetAllObservations() {return this->Observations;};
    int GetSizeOfS();
    int GetSizeOfA();
    int GetSizeOfObs();

    // get initia belief
    // vector<double> GetInitBelief();
    map<int, double>* GetInitBeliefSparse(); 


    // get probability values
    double TransFunc(int sI, int aI, int s_newI);
    double ObsFunc(int oI, int s_newI, int aI);
    double Reward(int sI, int aI);

    // for sparse representation use
    // map<int,double>* GetTransProbDist(int sI, int aI);
    unordered_map<int,double>* GetTransProbDist(int sI, int aI);

    map<int,double>* GetObsFuncProbDist(int s_newI, int aI);
};



#endif


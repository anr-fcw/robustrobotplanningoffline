 

#ifndef _SAMPLESTOCHASTICLOCALFSC_H_
#define _SAMPLESTOCHASTICLOCALFSC_H_

#include "LocalFscSochasticNode.h"
#include "DecPomdpInterface.h"
#include "Utils.h"
#include <map>
#include "SoftMaxDist.h"

using namespace std;

Belief build_inital_belief(DecPomdpInterface *decpomdp);

class SampleStochasticLocalFsc
{
private:
    // Pr(n'|oh, n)
    // std::map<node, std::map<size_t, std::map<node, double>>>  eta;
    vector<map<vector<int>, map<int, double>>> eta; // nI -> (ahi, ohi) -> (n_newI, prob)
                                                    // map <node, map<obs(pair_int), map<node, double>>>
    // vector<vector<vector<double>>> eta;             // swap indicies to nodeI, (ohi, ahi) and node_newI
    map<int, vector<int>> _m_obs_fsc; // need a special data structure for the obs of FSC (ah, oh)
    std::vector<LocalFscSochasticNode> Nodes;
    DecPomdpInterface *decpomdp;
    int LocalAgentIndex;
    int formalization_type; // 0 for momdp method, 1 for "init node" method
    // int RobotIndex;
    // std::vector<AlphaVector> AlphaVecs;
    double T = 0.3; // used for softmax
    double min_pb = 0.1;
    double max_belief_gap = 0.01;
    int max_node_size;
    int max_depth = 0;
    MCTS *mcts;

    vector<string> action_space;
    vector<string> observation_space;

    // store all the upper bound belief values
    map<string, double> _m_belief_value;
    map<string, BeliefSparse> _m_id_belief;

public:
    SampleStochasticLocalFsc(){};
    void Init(double T, double min_action_pb, double max_belief_gap, MCTS &mcts);

    // SampleStochasticLocalFsc(vector<AlphaVector> &alpha_vecs, DecPomdpInterface *decpomdp, int LocalAgentIndex, int init_type, int formalization_type);
    void BuildFsc(DecPomdpInterface *decpomdp, int LocalAgentIndex, int init_type,
                  int formalization_type, int max_node_size);
    SampleStochasticLocalFsc(const string filename);

    int CheckAlphaExist(AlphaVector &alpha);
    int CheckBeliefExist(BeliefSparse &b, double max_accept_gap);
    // Need to add it afterwards
    // node StartNode;
    void ProcessNodeWithMaxSize(LocalFscSochasticNode &node_process, std::map<double, vector<LocalFscSochasticNode>> &UnProcessedSet, DecPomdpInterface *decpomdp);
    // void ProcessNode(vector<AlphaVector> &alpha_vecs, LocalFscSochasticNode &node_process, std::map<double, vector<LocalFscSochasticNode>> &UnProcessedSet, DecPomdpInterface *decpomdp, bool max_size_reached);
    // void ProcessNode(vector<AlphaVector> &alpha_vecs, int n_index, std::vector<LocalFscSochasticNode> &UnProcessedSet, DecPomdpInterface *decpomdp, int process_type);
    // void ProcessBranchStochastic(int aHI, int oHI, LocalFscSochasticNode &n, BeliefSparse &b, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, vector<AlphaVector> &alpha_vecs, int n_index,
    //                              std::vector<LocalFscSochasticNode> &UnProcessedSet, DecPomdpInterface *decpomdp);
    // void ProcessBranchDeterministic(int aHI, int oHI, LocalFscSochasticNode &n, BeliefSparse &b, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, vector<AlphaVector> &alpha_vecs, int n_index,
    //                                 std::vector<LocalFscSochasticNode> &UnProcessedSet, DecPomdpInterface *decpomdp);
    void PrintGraph(DecPomdpInterface *decpomdp);
    vector<LocalFscSochasticNode> &GetNodes() { return this->Nodes; };
    ~SampleStochasticLocalFsc(){};
    // void InitNodeProcess(LocalFscSochasticNode n0, DecPomdpInterface *decpomdp);
    void ExportFSC(string filename);
    void SaveGraph(string filename);
    string GiveDescription(map<int, double> &pb_local_actions);
    void BuildAllObsFSC();
    int GetNodeSize() { return this->Nodes.size(); };

    // those two methods should be merged!!!
    void ComputePbDistJAIJOI(int aHI, int oHI, LocalFscSochasticNode &n, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, map<vector<int>, double> &out_res_dist);
    // void ComputePbDistJAIJOI(int aHI, int oHI, LocalFscSochasticNode &n, vector<AlphaVector> &alpha_vecs, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, map<vector<int>, double> &out_res_dist);
    // void GetAllPossibleFscObs(int LocalAgentIndex, LocalFscSochasticNode &n, set<vector<int>> &set_ahI_oHI, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, DecPomdpInterface *decpomdp);
    void GetAllPossibleFscObs(LocalFscSochasticNode &n, set<vector<int>> &set_ahI_oHI, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, DecPomdpInterface *decpomdp);

    double compute_pr_JOI(int LocalAgentIndex, int JOI, map<int, double> &dist_JOIs, int Local_obsI, DecPomdpInterface *decpomdp); // not used
    map<LocalActionIndex, double> &GetHumanActionDist(int NodeIndex) { return this->Nodes[NodeIndex].GetHumanActionDistribution(); };
    map<int, double> *GetNextNodeProb(int NodeIndex, vector<int> &fsc_obs) { return &this->eta[NodeIndex][fsc_obs]; };

    // Compute distance of two node
    // double ComputeNorm1Distance(LocalFscSochasticNode &n1, LocalFscSochasticNode &n2);
    // int GiveSimilarNodeIndex(LocalFscSochasticNode &n_temp, double &min_distance);
    int GiveSimilarNodeIndex(BeliefSparse &b_temp, double &min_distance);
    int GetNodeIndex(LocalFscSochasticNode &n);

    bool CheckFscValidation();

    void ExportStochasticFSC(string filename);

    void ExportAllNodesBelief(string filename);

    void PrintNextNodeActionDistribution(int current_aHI, int oHI, int &current_n);

    vector<string> GetActionSpace() { return this->action_space; };
    vector<string> GetObsSpace() { return this->observation_space; };
    int GetLocalAgentIndex() { return this->LocalAgentIndex; };
};

#endif
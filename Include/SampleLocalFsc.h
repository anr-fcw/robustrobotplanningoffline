 

#ifndef _SAMPLELOCALFSC_H_
#define _SAMPLELOCALFSC_H_

#include "LocalFscNode.h"
#include "DecPomdpInterface.h"
#include "Utils.h"
#include <map>
#include <set>
#include "SoftMaxDist.h"
#include "SampleStochasticLocalFsc.h"
using namespace std;

Belief build_inital_belief(DecPomdpInterface *decpomdp);

class SampleLocalFsc
{
private:
    // Pr(n'|oh, n)
    // std::map<node, std::map<size_t, std::map<node, double>>>  eta;
    vector<vector<vector<double>>> eta; // swap indicies to nodeI, ohi and node_newI
    std::vector<LocalFscNode> Nodes;
    DecPomdpInterface *decpomdp = nullptr;
    int LocalAgentIndex;
    int formalization_type; // 0 for momdp method, 1 for "init node" method
    // int RobotIndex;
    // std::vector<AlphaVector> AlphaVecs;
    string id;
    MCTS *mcts;
    int max_size;

    vector<string> action_space;
    vector<string> observation_space;

public:
    SampleLocalFsc(){};
    SampleLocalFsc(vector<AlphaVector> alpha_vecs, DecPomdpInterface *decpomdp, int LocalAgentIndex, int init_type, int formalization_type);
    int CheckAlphaExist(AlphaVector alpha);
    int CheckBeliefExist(BeliefSparse &b, double max_accept_gap);
    int CheckNodeExistWithID(int id_new);
    int GiveSimilarNodeIndex(BeliefSparse &b, double &min_distance);
    int GetNodeIndex(LocalFscNode &n);

    // Need to add it afterwards
    // node StartNode;
    void ProcessNodeStochastic(vector<AlphaVector> &alpha_vecs, int n_index, std::vector<LocalFscNode> &UnProcessedSet, DecPomdpInterface *decpomdp);
    void ProcessNodeDeterministic(vector<AlphaVector> &alpha_vecs, int n_index, std::vector<LocalFscNode> &UnProcessedSet, DecPomdpInterface *decpomdp);
    double GetTransitionProb(int n_newI, int oHI, int nI)
    {
        // Use this method to avoid return nan value, because some links between nodes are not exist!
        if (this->eta[nI][oHI][n_newI] > 0)
        {
            // cout << "Prob (n_newI:" << n_newI << ", oHI:"<<oHI <<", nI:"<<nI << "): "<< this->eta[nI][oHI][n_newI] << endl;
            return this->eta[nI][oHI][n_newI];
        }
        else
        {
            // Can output how many nans and 0 values
            // cerr << "eta[nI][oHI][n_newI] = " << this->eta[nI][oHI][n_newI] << endl;
            return 0;
        }
    };
    // vector<AlphaVector> GetAlphaVectors();
    void PrintGraph(DecPomdpInterface *decpomdp);
    vector<LocalFscNode> GetNodes();
    ~SampleLocalFsc(){};
    void InitNodeProcess(LocalFscNode n0, DecPomdpInterface *decpomdp);
    void ExportFSC(string filename);
    void SaveGraph(string filename);

    // new added
    string GetId() { return this->id; };
    void SampleOneFsc(DecPomdpInterface *decpomdp, int LocalAgentIndex,
                      double max_belief_gap, double T, double min_action_pb, MCTS &mcts, int MaxSize);
    void SampleOneFsc(SampleStochasticLocalFsc &stochastic_fsc);
    void ProcessNodeWithMaxSize(LocalFscNode &n, std::map<double, vector<LocalFscNode>> &UnProcessedSet,
                                DecPomdpInterface *decpomdp, double max_belief_gap, double T, double min_action_pb);
    void ProcessNode(int n_index_process, vector<LocalFscNode> &Unprocessed_list, SampleStochasticLocalFsc &stochastic_fsc);

    void ComputeFscID();
};

#endif
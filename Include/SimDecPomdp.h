#include "DecPomdpInterface.h"
#include "FSCBase.h"
#include "SampleStochasticLocalFsc.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <cmath>
#include "tabulate/single_include/tabulate/tabulate.hpp"

using namespace tabulate;

using namespace std;

void printInstructions();
void printLegend();

class SimDecPomdp
{
private:
    DecPomdpInterface *DecPomdpModel;
    bool interactive = false;
    vector<FSCBase> FSCs;
    vector<int> FSCs_current_node;
    int HumanIndex;
    int RobotIndex;
    int stateI; // Current State
    vector<string> visualization_states;
    // Cool funcs below
    int SampleStartState();
    // double Reward(vector<int> actions);
    tuple<int, bool, double, string> Step(vector<int> ActionIndicies);
    int GetObsFromState(int jaI);
    SampleStochasticLocalFsc StochasticHumanFsc;
    bool PredictHumanActions = false;
    int current_stochastic_fsc_nI = 0;
    double terminal_reward = -DBL_MAX;

public:
    SimDecPomdp(DecPomdpInterface *DecPomdpModel, vector<string> Policyfiles);
    SimDecPomdp(DecPomdpInterface *DecPomdpModel, vector<string> Policyfiles, string VisfilePath, int formalization_type);
    SimDecPomdp(DecPomdpInterface *DecPomdpModel, string RobotPolicy, int RobotIndex, int HumanIndex, string VisfilePath, int formalization_type);
    ~SimDecPomdp();
    vector<int> SelectActions(double &decision_time, bool flip_vertical, bool flip_horizontal);
    void SimulateNsteps(int N);                                                                                                     // Print Executions and average accumlated rewards
    int SimulateNsteps(int N, int &final_step, double &final_acc_rewards, ofstream &log, bool flip_vertical, bool flip_horizontal); // for human experiments, log data
    void SimulateNstepsUsingFSCs(int N, double &out_sum_rewards, int &success);

    void VisualizationState(int sI);
    void VisualizationStateUsingTabulate(int sI, bool flip_vertical, bool flip_horizontal);

    void LoadVisualizationFile(string fileName);
    void SetStochasticHumanFsc(SampleStochasticLocalFsc &fsc);
    void PredictHumanNextActions(int current_aHI, int oHI, int &current_nI);
    void addNewLines(std::string *text);
    double ValueEvaluation(double error_gap);
    void Reset();
    int GetCurrentStateIndex() { return this->stateI; };
    void SetTerminalReward(double terminal_reward);
};

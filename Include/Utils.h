 
/* Only include this header file once. */
#ifndef _UTILS_H_
#define _UTILS_H_

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <set>
#include "BeliefSparse.h"
#include "DecPomdpInterface.h"
#include "PomdpInterface.h"
#include "AlphaVector.h"
#include <math.h>
#include <float.h>
#include "FSCBase.h"
#include "../Include/ParserSarsopResult.h"
#include "ParserPOMDPSparse.h"

using namespace std;

#define MIN_B_ACC 1e-3

// double ComputeAvgL1Distance(Belief& b,vector<Belief>& bs, int NrB);
// void ExpandBeliefSet(vector<Belief>& bs, vector<Belief>& temp_bs, int NrB );
// double dot(Belief& b, AlphaVector& alpha);
// double ComputeDistanceAlphas(AlphaVector& a, AlphaVector& b);
// bool CheckConvergence(vector<AlphaVector>& a_vecs, vector<AlphaVector>& b_vecs, double err);
// void PrintBeliefSet(vector<Belief>& bs);

// InfJesp functions
bool CheckAlphaExist(vector<AlphaVector> &a_vecs, AlphaVector &alpha);
void PrintVector(vector<double> &V);
void PrintAlphaVectors(vector<AlphaVector> &a_vecs);
void PrintAllAlphaAOVecs(vector<vector<vector<AlphaVector>>> &a_ao_vecs);
double compute_p_oba(int o, BeliefSparse &b, int a, PomdpInterface *pomdp);
double compute_p_oba(int o, BeliefSparse &b, int a, DecPomdpInterface *decpomdp);
map<int, double> compute_dist_p_oba(BeliefSparse &b, int a, DecPomdpInterface *decpomdp);
map<int, double> compute_dist_p_oba(BeliefSparse &b, int a, PomdpInterface *decpomdp);
// Belief Update
BeliefSparse Update(PomdpInterface *Pb, BeliefSparse &b, int aI, int oI, double p_oba);
BeliefSparse Update(DecPomdpInterface *Pb, BeliefSparse &b, int aI, int oI, double p_oba);

// for Human FSC
BeliefSparse UpdateOneSideObservation(DecPomdpInterface *Pb, BeliefSparse &b, map<int, double> &dist_JAI, int human_oI, double pr_oh_ba, int agentIndex);
map<int, double> compute_dist_pr_oh(DecPomdpInterface *Pb, BeliefSparse &b, map<int, double> &dist_JAI, int agentIndex);

// for RobotMCTS
BeliefSparse UpdateOneSideObservation(DecPomdpInterface *Pb, BeliefSparse &b, int robot_aI, map<int, double> &human_aI_dist, int robot_oI, double pr_or_ba);
map<int, double> compute_dist_pr_or(DecPomdpInterface *Pb, BeliefSparse &b, int robot_aI, map<int, double> &human_aI_dist);
double ComputeRWithHumanActionDist(DecPomdpInterface *Pb, BeliefSparse &b, int aRI, map<int, double> &human_aI_dist);

// Belief Update
BeliefSparse Update(PomdpInterface *Pb, BeliefSparse &b, int aI, int oI);
BeliefSparse Update(DecPomdpInterface *Pb, BeliefSparse &b, int aI, int oI);
AlphaVector argmax_alpha_vector(vector<AlphaVector> &alpha_vecs, BeliefSparse *b);
int argmax_alpha(vector<AlphaVector> &alpha_vecs, BeliefSparse &b);
double best_value(vector<AlphaVector> &alpha_vecs, BeliefSparse &b);
int argmax_alpha(vector<AlphaVector> &alpha_vecs, BeliefSparse &b, double &value);

// Import Value Function
vector<AlphaVector> ImportValueFunction(const string &filename);
double EvaluationWithAlphaVecs(PomdpInterface *Pb, vector<AlphaVector> &alpha_vecs);
map<int, double> GetPossibleJOIfromBeliefSparseAndJAI(DecPomdpInterface *Pb, BeliefSparse &b, int JaI);
void GetPossibleJOIfromBeliefSparseAndJAI(DecPomdpInterface *Pb, BeliefSparse &b, map<int, double> &pb_JaI, map<int, double> &out_pb_JOI);

void ExportMpomdpModel(DecPomdpInterface *DecPomdpModel, string filename);
void ExportMpomdpModelWithDiffInitBelief(DecPomdpInterface *DecPomdpModel, BeliefSparse &new_init_b, string filename);
void ExportMpomdpWithNoisyRobotAction(DecPomdpInterface *DecPomdpModel, DecPomdpInterface *out_noise_decpomdp, double noise_rate, int robot_index, string filename);
double NoisyRobotActionTransition(DecPomdpInterface *DecPomdpModel, double noise_rate, int sI, int JAI, int s_newI);

void ExportMpomdpWithNoisyJointAction(DecPomdpInterface *DecPomdpModel, DecPomdpInterface *out_noise_decpomdp, double noise_rate, string filename);
double NoisyJointActionTransition(DecPomdpInterface *DecPomdpModel, double noise_rate, int sI, int JAI, int s_newI);

double ComputeR(DecPomdpInterface *Pb, BeliefSparse &b, int jaI);
double ComputeR(PomdpInterface *Pb, BeliefSparse &b, int jaI);
double ComputeNewValueWithSarsopDiffInitBelief(DecPomdpInterface *Pb, BeliefSparse &new_b, string sarsop_path, map<string, BeliefSparse> &out_map_id_to_belief, map<string, double> &out_map_belief_value);
void FoundAllUpperBoundBeliefValue(PomdpInterface *Pb, vector<AlphaVector> &alpha_vecs, map<string, BeliefSparse> &out_map_id_to_belief, map<string, double> &out_map_belief_value);

double RoughEstimationOfV(DecPomdpInterface *Pb, BeliefSparse &new_b, double error_gap);
int SampleNextStateIndex(DecPomdpInterface *Pb, int current_sI, int jaI);
vector<AlphaVector> SolvePomdpWithZmdp(string zmdp_path, string pomdp_path, string zmdp_policy_converter_path);

double ComputeNorm1Distance(BeliefSparse &belief1, BeliefSparse &belief2);

void PrintMap(map<string, double> &m);

template <typename T>
T RandT(T _min, T _max)
{
	T temp;
	if (_min > _max)
	{
		temp = _max;
		_max = _min;
		_min = temp;
	}
	return rand() / (double)RAND_MAX * (_max - _min) + _min;
};

#endif /* !_UTILS_H_ */

void PrintMap(map<int, double> &m);

int SampleAKeyFromMap(map<int, double> &m);

void RenormalizeMap(map<int, double> &m, double min_pb);

void FreeAllPomdpModels(vector<PomdpInterface *> &all_brms);

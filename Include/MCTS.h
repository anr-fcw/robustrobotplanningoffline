#ifndef _MCTS_H_
#define _MCTS_H_

#include "SimPomdpMCTS.h"
#include "TreeNode.h"
#include "Utils.h"

class MCTS
{
private:
      int current_node_number = 0;
      int max_node_number = 0;
      double c = 0;          // UCB parameter
      DecPomdpInterface *pb; // should not have this!!! later should delete it, POMCP/MCTS only need a model generator G!
      TreeNode rootnode;
      // SimPomdpMCTS simulator;
      int size_A;
      // int nb_restarts_simulation = 100; // not used
      // double epsilon = 0.01;
      double discount;
      vector<TreeNode*> all_nodes; // need to be cleared after each plan
      vector<AlphaVector> alpha_vecs;
      set<string> all_beliefs;
      map<string, double> all_belief_value;
      map<string, int> all_belief_nb_visit;
      map<string, map<int, int>> all_belief_action_nb_visit;
      map<string, map<int, double>> all_belief_action_Q; // Q(b, a)
      void CleanNodes();

public:
      MCTS(){};
      MCTS(DecPomdpInterface *pb, vector<AlphaVector> &alpha_vecs);
      void Init(int nb_nodes, double c);
      ~MCTS(){};
      // int RolloutPolicy(BeliefSparse &b);                                  // give a belief, return a action index
      // double Simulation(BeliefSparse &b, int nb_restarts, double epsilon); // from a belief b, start a simulation using rollout policy and return accumulated rewards
      TreeNode *Selection(TreeNode *start_node);
      void Expansion(TreeNode *node);
      void BackPropagation(TreeNode *node);                  // back propagate the value from the leaf node to the parent node untill the root node
      int Plan(BeliefSparse b, double &value);               // return an action
      void Plan(BeliefSparse b, map<int, double> &belief_Q); // update Q(b, a)
};
#endif
 

/* Only include this header file once. */
#ifndef _LOCALFSCSTOCHASTICNODE_H_
#define _LOCALFSCSTOCHASTICNODE_H_ 1

/* the include directives */
#include <iostream>
#include "AlphaVector.h"
#include "Belief.h"
#include "BeliefSparse.h"

#define WHEREAMI std::cerr << __FILE__ << ":" << __func__ << ":" << __LINE__ << std::endl;
#define ARGH(x)                             \
  {                                         \
    WHEREAMI;                               \
    std::cerr << "MSG: " << x << std::endl; \
    exit(1);                                \
  }

using namespace std;
using JointActionIndex = int;
using LocalActionIndex = int;

class LocalFscSochasticNode
{
  AlphaVector best_alpha;
  Belief belief;
  BeliefSparse belief_sparse;
  map<JointActionIndex, double> _m_pb_joint_actions_;
  map<LocalActionIndex, double> _m_pb_local_actions_;
  // double id; // id is computed using belief;
  string id; // id is computed using belief;

  string description;
  int belief_counter = 1;
  double weight = 1.0; // used for weight merging, first version we check if same alpha is encountered
  int depth = 0;
  //
  map<int, map<int, double>> _m_dist_aHI_to_JAIs;

public:
  LocalFscSochasticNode(AlphaVector alpha, BeliefSparse &belief_sparse,
                        map<JointActionIndex, double> &_m_pb_joint_actions, map<LocalActionIndex, double> _m_pb_local_actions)
  {
    this->best_alpha = alpha;
    this->belief_sparse = belief_sparse;
    this->_m_pb_joint_actions_ = _m_pb_joint_actions;
    this->_m_pb_local_actions_ = _m_pb_local_actions;
    this->id = computeID();
  };

  LocalFscSochasticNode(double weight, BeliefSparse &belief_sparse,
                        map<JointActionIndex, double> &_m_pb_joint_actions, map<LocalActionIndex, double> _m_pb_local_actions)
  {
    this->weight = weight;
    this->belief_sparse = belief_sparse;
    this->_m_pb_joint_actions_ = _m_pb_joint_actions;
    this->_m_pb_local_actions_ = _m_pb_local_actions;
    this->id = computeID();
  };

  LocalFscSochasticNode(double weight, BeliefSparse &belief_sparse,
                        map<LocalActionIndex, map<JointActionIndex, double>> &dist_aHI_to_JAIs, map<LocalActionIndex, double> _m_pb_local_actions)
  {
    this->weight = weight;
    this->belief_sparse = belief_sparse;
    this->_m_dist_aHI_to_JAIs = dist_aHI_to_JAIs;
    this->_m_pb_local_actions_ = _m_pb_local_actions;
    this->id = computeID();
  };

  LocalFscSochasticNode()
  {
    this->description = "InitNode";
  };

  LocalFscSochasticNode(BeliefSparse &belief_sparse)
  {
    this->belief_sparse = belief_sparse;
  };

  LocalFscSochasticNode(map<int, double> &dist_local_actions)
  {
    this->_m_pb_local_actions_ = dist_local_actions;
  }

  ~LocalFscSochasticNode(){};
  void SetDescript(string s) { this->description = s; };
  string GetDescript() { return this->description; };
  map<LocalActionIndex, double> &GetHumanActionDistribution() { return this->_m_pb_local_actions_; };
  map<JointActionIndex, double> &GetJointActionDistribution() { return this->_m_pb_joint_actions_; };

  map<int, map<int, double>> &GetHumanActionToJointActionDistribution() { return this->_m_dist_aHI_to_JAIs; };

  AlphaVector &GetAlphaVector() { return this->best_alpha; };
  BeliefSparse &GetJointBeliefSparse() { return this->belief_sparse; };
  string computeID()
  {
    return this->belief_sparse.GetID();
  }
  double GetWeight()
  {
    return this->weight;
  }

  void SetWeight(double w)
  {
    this->weight = w;
  }
  // Better idea is to compare the alpha vector, need to test later
  bool operator<(const LocalFscSochasticNode &n) const
  {
    if (this->id < n.id)
    {
      return true;
    }
    else
    {
      return false;
    }
  };

  void MergeBelief(BeliefSparse &b)
  {
    vector<double> merged_pb_states(this->belief.GetSize());
    map<int, double> _m_belief_sparse;
    for (size_t i = 0; i < this->belief.GetSize(); i++)
    {
      double pb_i = (this->belief[i] * belief_counter + b[i]) / (belief_counter + 1);
      merged_pb_states[i] = pb_i;
      if (pb_i > 0)
      {
        _m_belief_sparse[i] = pb_i;
      }
    }
    Belief merged_belief(merged_pb_states);
    this->belief = merged_belief;
    this->belief_sparse.GetValues(_m_belief_sparse, merged_pb_states.size());
    this->belief_counter += 1;
  };

  void MergeBeliefWithWeights(BeliefSparse &b, double w_new)
  {
    vector<double> merged_pb_states(this->belief.GetSize());
    double updated_weight = w_new + this->weight;
    map<int, double> _m_belief_sparse;

    for (size_t i = 0; i < this->belief.GetSize(); i++)
    {
      double pb_i = (this->belief[i] * (this->weight / updated_weight) + b[i] * (w_new / updated_weight));
      merged_pb_states[i] = pb_i;
      if (pb_i > 0)
      {
        _m_belief_sparse[i] = pb_i;
      }
    }
    Belief merged_belief(merged_pb_states);
    this->belief = merged_belief;
    this->belief_sparse.GetValues(_m_belief_sparse, merged_pb_states.size());
    this->weight = updated_weight;
  };
  double ComputePbLocalActionToJointAction(int local_aI, int JAI, int local_agent_index, DecPomdpInterface *Pb)
  {
    double temp_sum = 0;
    vector<int> current_action_indices = Pb->JointToIndividualActionsIndices(JAI);
    if (current_action_indices[local_agent_index] != local_aI)
    {
      return 0;
    }

    double pr_JAI = _m_pb_joint_actions_[JAI];
    map<int, double>::iterator it_JAI;
    for (it_JAI = _m_pb_joint_actions_.begin(); it_JAI != _m_pb_joint_actions_.end(); it_JAI++)
    {
      int temp_JAI = it_JAI->first;
      double pr_temp_JAI = it_JAI->second;
      vector<int> actions_indicies = Pb->JointToIndividualActionsIndices(temp_JAI);
      if (actions_indicies[local_agent_index] == local_aI)
      {
        temp_sum += pr_temp_JAI;
      }
      else
      {
        continue;
      }
    }

    return pr_JAI / temp_sum;
  }

  string GetNodeId()
  {
    return this->id;
  }

  void SetDepth(int d)
  {
    this->depth = d;
  }

  int GetDepth()
  {
    return this->depth;
  }
};

#endif

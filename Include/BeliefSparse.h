/* This file has been written and/or modified by the following people:
 *
 * Anonymous for AAMAS reviewing process
 * 
 */
#ifndef _BELIEFSPARSE_H_
#define _BELIEFSPARSE_H_

#include <iostream>
#include <vector>
#include <map>
#include <math.h>

using namespace std;

class BeliefSparse
{
private:
    std::map<int, double> pb_states;
    int state_size = -1;
    string id;

public:
    BeliefSparse();
    BeliefSparse(std::vector<double> &vec_pb_states);
    BeliefSparse(const std::map<int, double> &b, int state_size);
    void PrintBeliefSparse();
    ~BeliefSparse(){};
    void GetValues(std::map<int, double> &b, int state_size);
    std::map<int, double> *GetBeliefSparse();
    BeliefSparse &operator=(const BeliefSparse &o);
    double operator[](int i);
    bool operator==(BeliefSparse &o);
    int GetSize();
    void InsertValue(int key, double value);
    void SetSize(int sizeofS);

    // test operator <
    bool operator<(const BeliefSparse &b) const;
    string computeID() const;
    const string GetID() const
    {
        return this->id;
    };
};

#endif /* !_BELIEFSPARSE_H_ */

/* This file has been written and/or modified by the following people:
 *
 * Anonymous for AAMAS reviewing process
 * 
 */

#ifndef _BUILDFSC_H_
#define _BUILDFSC_H_

#include "PomdpNode.h"
#include "../Include/PomdpInterface.h"
#include "../Include/Utils.h"
#include <map>
#include <set>

using namespace std;

class FSC
{
private:
    // std::map<node, std::map<size_t, std::map<node, double>>>  eta;
    // vector<vector<vector<double>>> eta; // swap indicies to nodeI, ohi and node_newI
    // vector<vector<vector<int>>> eta; // swap indicies to nodeI, ohi and node_newI
    vector<int> eta; // swap indicies to nodeI, ohi and node_newI
    double error_gap;
    int ObsSize;
    std::vector<Pomdpnode> Nodes;
    PomdpInterface *pomdp;
    int size;
    int type;
    int max_node_size;
    double max_accept_belief_distance;
    // std::vector<AlphaVector> AlphaVecs;
protected:
    vector<AlphaVector> BuildInitValueFromFSC();
    double IterValueFunc(vector<AlphaVector> &V, double gamma);
    double IterValueFuncMomdp(vector<AlphaVector> &V, double gamma);

    void ProcessEta(vector<vector<vector<int>>> &eta_vec);

public:
    FSC(vector<AlphaVector> &alpha_vecs, PomdpInterface *pomdp, int type, double error_gap);
    FSC(vector<AlphaVector> &alpha_vecs, PomdpInterface *pomdp, double max_accept_belief_distance, double evaluation_error_gap, int max_node_size);
    int CheckAlphaExist(AlphaVector &alpha);
    // Need to add it afterwards
    // node StartNode;
    void ProcessNode(vector<AlphaVector> &alpha_vecs, int n_index, std::vector<Pomdpnode> &UnProcessedSet, PomdpInterface *pomdp, vector<vector<vector<int>>> &eta_vec);
    void ProcessNodeWithUniqueBelief(vector<AlphaVector> &alpha_vecs, Pomdpnode& node_process, std::map<double, vector<Pomdpnode>> &UnProcessedSet, PomdpInterface *pomdp, vector<vector<vector<int>>> &eta_vec);
    double GetTransitionProb(int n_newI, int oI, int nI);

    // vector<AlphaVector> GetAlphaVectors();
    void PrintGraph(PomdpInterface *pomdp);
    vector<Pomdpnode> &GetNodes();
    ~FSC(){};
    void InitNodeProcess(Pomdpnode n0, vector<vector<vector<int>>> &eta_vec);
    void ExportFSC(string filename);
    int GetNodesSize() { return this->size; };
    int NextNode(int nI, int oI);
    double PolicyEvaluation();
    void SaveGraph(PomdpInterface *pomdp, string filename);

    // Not used
    // int CheckBeliefExist(Belief& belief); // New added, to test if a belief is already exist
    double MaxNormDiff(BeliefSparse &b1, BeliefSparse &b2);

    int GetNodeIndex(Pomdpnode& n);
    int GiveSimilarNodeIndex(BeliefSparse &b, double &min_distance);
    int CheckBeliefExist(BeliefSparse &b, double max_accept_gap);

};

#endif
/* This file has been written and/or modified by the following people:
 *
 * Anonymous for AAMAS reviewing process
 * 
 */

#include "DecPomdpInterface.h"
#include "PomdpInterface.h"
#include "FSCBase.h"
#include <map>
#include <unordered_map>
#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cfloat>
#include "Utils.h"
#include "SampleStochasticLocalFsc.h"
using namespace std;

// currently only consider two agent, one robot, one human
class RobotRobustPlanModelSparse : public PomdpInterface
{
private:
    DecPomdpInterface *DecPomdpModel;
    vector<DecPomdpInterface *> decpomdp_models;
    vector<FSCBase> Human_possible_FSCs;
    vector<SampleStochasticLocalFsc> Human_FSCs;

    int robot_index;
    int human_index;
    int AgentNb;
    vector<string> States;
    vector<string> Actions;
    vector<string> Observations;
    vector<double> b0;
    map<int, double> b0_sparse;
    map<int, double> initial_prob_dist_human_fscs;
    vector<vector<int>> _m_ExtendedStateIndicies;          // vector (eI -> {sI,oI,human_fsc_I, nI})
    map<vector<int>, int> _m_IndiciesToExtendedStateIndex; // map ({sI,oI,human_fsc_I, nI} -> eI)
    map<vector<int>, int> _m_FSCsNodes_Indicies;
    // vector<vector<int> > _m_IndiciesOfFSCsNodes;  // NI -> <n1,n2 ... n>
    // vector<vector<int> > _m_IndiciesOfObs; // OI -> <o1, o2 ... on>
    // vector<double> _m_res_SumPrObsAgentI; // <N_newI, oI, NI> -> pb what if using unordered_map<string, int> / map<vector, int>?

    map<int, double> result_NI_new_prob_dist_temp;
    int max_extended_state_size;
    // transition function as A -> E -> P(E)
    vector<vector<map<int, double>>> TransFuncVecs;

    // observation as A -> E' -> O -> proba
    vector<vector<vector<double>>> ObsFuncVecs;

    // reward function as A -> E -> reward
    vector<vector<double>> RewardFuncVecs;

    // double ComputeSumPrObsAgentI(DecPomdpInterface* decpomdp, int optimizing_agentI, int oI, int s_newI, int JAI);
    // void RecursiveBuildAllFSCsNodesIndicies(int depth, int &NI, vector<int> NodesIndicies);
    // void RecursiveBuildNextNIProbDist(int depth, vector<map<int, double> *> &all_nodes_trans_dist, vector<int> NodeIndicies, double Pb_NI_new, map<int, double> &NextNIProbDist);

    int NItoActionsIndex(int fscI, int nI);
    double ProbAllNodesTrans(int N_newI, vector<int> &ObsIndicies, int NI);
    // for sparse representation
    // map<int, double> *GetAllNodesTransProbDist(int NI, vector<int> &ObsIndicies);

    int NI_size;
    int SizeOfS;
    int SizeOfA;
    int SizeOfObs;
    int SizeJAI;
    int SizeDecPomdpStateSpace;

protected:
    void ProcessForOneExtendedState(int eI, vector<int> &UnProcessedSet);
    void GenerateAllReachableExtendedStates();
    vector<double> ConvertBeliefSparseToNoSparse(map<int, double> &b0_sparse);
    void InitialHumanFSCProbDist();
    void GenerateAllReachableExtendedStatesForStochasticFSC();
    void ProcessForOneExtendedStateStochasticFSC(int eI, vector<int> &UnProcessedSet);

public:
    RobotRobustPlanModelSparse(vector<DecPomdpInterface *> models, vector<FSCBase> &human_possible_FSCs, int human_index, int robot_index);
    RobotRobustPlanModelSparse(vector<DecPomdpInterface *> models, vector<SampleStochasticLocalFsc> &Human_FSCs, int human_index, int robot_index);

    ~RobotRobustPlanModelSparse(){};
    // int GetCurrentOptmizingAgentIndex();
    double GetDiscount() { return this->DecPomdpModel->GetDiscount(); };
    int GetSizeOfS() { return this->SizeOfS; };
    int GetSizeOfA() { return this->SizeOfA; };
    int GetSizeOfObs() { return this->SizeOfObs; };
    // std::vector<double> GetInitBelief() { return this->b0; };
    map<int, double> *GetInitBeliefSparse() { return &this->b0_sparse; };
    double TransFunc(int eI, int aI_opt_agentI, int e_newI);
    double ObsFunc(int oI_opt_agentI, int e_newI, int aI_opt_agentI);
    double Reward(int eI, int aI_opt_agentI);
    void ExportPOMDP(string filename);
    int GetSizeOfStateSizeBeforeElimination() { return this->max_extended_state_size; };

    vector<int> DecomposeBestResponseState(int eI) { return this->_m_ExtendedStateIndicies[eI]; };
    vector<string> GetAllStates() { return this->States; };
    vector<string> GetAllActions() { return this->Actions; };
    vector<string> GetAllObservations() { return this->Observations; };
    DecPomdpInterface *GetDecPomdpModel() { return this->DecPomdpModel; };
};

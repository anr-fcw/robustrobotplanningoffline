#include "PomdpInterface.h"
#include "FSCBase.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <cmath>

using namespace std;

class SimPomdp
{
private:
      PomdpInterface *PomdpModel;
      bool interactive = false;
      FSCBase FSC;
      int FSCs_current_node;
      int stateI; // Current State
      vector<string> visualization_states;
      // Cool funcs below
      int SampleStartState();
      // double Reward(vector<int> actions);
      int GetObsFromState(int jaI);
      double terminal_reward = -DBL_MAX;

public:
      SimPomdp(PomdpInterface *PomdpModel, string fsc_path, double terminal_reward);
      SimPomdp(PomdpInterface *PomdpModel, string fsc_path, string VisfilePath);
      SimPomdp(PomdpInterface *PomdpModel, string VisfilePath);
      ~SimPomdp();
      int SelectActions();
      void SimulateNsteps(int N);            // Print Executions and average accumlated rewards
      int SimulateNstepsCheckSuccess(int N); // Print Executions and average accumlated rewards
      int SimulateNstepsCheckSuccess(int N, double &sum_rewards);

      void Reset();
      void VisualizationState(int sI);
      void LoadVisualizationFile(string fileName);
      tuple<int, bool, double, string> Step(int aI);
};
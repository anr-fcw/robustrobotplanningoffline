#ifndef _SOFTMAX_H_
#define _SOFTMAX_H_

#include "MCTS.h"

// void SoftmaxActions(DecPomdpInterface *Pb, BeliefSparse &b, vector<AlphaVector> &alpha_vecs, map<int, double> &pb_joint_actions,
// 					map<int, double> &pb_local_actions, int agentIndex, double T, double min_pb);
// double ComputeQ(DecPomdpInterface *Pb, BeliefSparse &b, int jaI, vector<AlphaVector> &alpha_vecs);
// double ComputeQ(DecPomdpInterface *Pb, BeliefSparse &b, int jaI, vector<AlphaVector> &alpha_vecs,
// 				map<string, BeliefSparse> &out_map_id_to_belief, map<string, double> &out_map_belief_value);
// void SoftmaxActions(DecPomdpInterface *Pb, BeliefSparse &b, vector<AlphaVector> &alpha_vecs, map<int, map<int, double>> &dist_aHI_to_JAIs,
// 					map<int, double> &pb_local_actions, int agentIndex, double T);
// void SoftmaxActions(DecPomdpInterface *Pb, BeliefSparse &b, vector<AlphaVector> &alpha_vecs, map<int, map<int, double>> &dist_aHI_to_JAIs,
// 					map<int, double> &pb_local_actions, int agentIndex, double T, double min_pb);
// void SoftmaxActions(DecPomdpInterface *Pb, BeliefSparse &b, vector<AlphaVector> &alpha_vecs, map<int, map<int, double>> &dist_aHI_to_JAIs,
// 					map<int, double> &pb_local_actions, int agentIndex, double T, double min_pb,
// 					map<string, BeliefSparse> &out_map_id_to_belief, map<string, double> &out_map_belief_value);
double SoftmaxActions(DecPomdpInterface *Pb, BeliefSparse &b, map<int, map<int, double>> &dist_aHI_to_JAIs,
					  map<int, double> &pb_local_actions, int agentIndex, double T, double min_pb, MCTS &mcts);

double ComputeQ(DecPomdpInterface *Pb, BeliefSparse &b, int jaI, MCTS &mcts);

// double ComputeQ(DecPomdpInterface *Pb, BeliefSparse &b, int jaI, vector<AlphaVector> &alpha_vecs, MCTS &mcts);

#endif
/* This file has been written and/or modified by the following people:
 *
 * Anonymous for AAMAS reviewing process
 * 
 */
#ifndef _BELIEF_H_
#define _BELIEF_H_

#include <iostream>
#include <vector>
#include "DecPomdpInterface.h"
#include "PomdpInterface.h"
#include <map>

class Belief
{
private:
    std::vector<double> pb_states;
public:
    Belief();
    Belief(const std::vector<double>& b);
    void PrintBelief();
    ~Belief();

    // Saw in MADP, they used two const, one in front, one in the end
    const std::vector<double> GetBelief() const;
    Belief& operator=(const Belief& o);
    double operator[](int i);
    bool operator==(Belief& o);
    unsigned long GetSize() const;

};



#endif /* !_BELIEF_H_ */

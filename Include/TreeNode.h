#ifndef _TREENODE_H_
#define _TREENODE_H_

#include <iostream>
#include <fstream>
#include <string>
#include "BeliefSparse.h"

class TreeNode
{
private:
      BeliefSparse b_;
      TreeNode *ParentNode_ = NULL;
      // vector<TreeNode *> ChildNodes_;
      map<pair<int, int>, TreeNode *> ChildNodes_; // aI, oI -> n_new
      double value_ = 0;
      // int visits_ = 0;
      // map<int, int> all_action_visits;
      double pr_; // pr_oba, probability of reaching to this node from the parent node
      int best_aI;
      map<int, double> all_action_Q;
      int nb_all_childs = 0;
      string belief_id;

      // for RobotMCTS
      map<int, double> _m_human_actions_dist;
      map<int, map<int, double>> _m_all_pr_or_ba;

public:
      TreeNode(){};
      TreeNode(BeliefSparse b, double pr);
      TreeNode(BeliefSparse b, double pr, string belief_id);
      void AddParentNode(TreeNode *ParentNode);
      void AddChildNode(int aI, int oI, TreeNode *ChildNode);
      map<pair<int, int>, TreeNode *> *GetChildNodes();
      void SetValue(double value);
      double GetValue();
      void SetBestAction(int aI);
      int GetBestAction();
      TreeNode *GetParentNode();
      TreeNode *GetChildNode(int aI, int oI) { return this->ChildNodes_[make_pair(aI, oI)]; };
      BeliefSparse GetBeliefSparse();
      double GetChildNodeValue(int aI, int oI);
      bool isRoot();
      ~TreeNode(){};

      // void AddNbVisit() { this->visits_ += 1; };
      // int GetNbVisit() { return this->visits_; };
      // void AddNbActionsVisit(int aI) { this->all_action_visits[aI] += 1; };
      // map<int, int> GetNbAllActionsVisit() { return this->all_action_visits; };
      // void AddNbChilds(int nb) { this->nb_all_childs += nb; };
      // int GetChildNodeNbVisit(int aI, int oI);

      void SetActionQ(int aI, double Q) { this->all_action_Q[aI] = Q; };
      map<int, double> GetAllActionQ() { return this->all_action_Q; };
      int GetNbAllChilds() { return this->nb_all_childs; };
      string GetBeliefId() { return this->b_.computeID(); };

      // for RobotMCTS
      void SetHumanActionsDist(map<int, double> &human_actions_dist) { this->_m_human_actions_dist = human_actions_dist; };
      void SetPrOrOba(int aRI, map<int, double> &pr_or_oba) { this->_m_all_pr_or_ba[aRI] = pr_or_oba; };
      map<int, double> GetHumanActionDist() { return this->_m_human_actions_dist; };
      map<int, double> GetPrOrOba(int aRI) { return this->_m_all_pr_or_ba[aRI]; };
};

#endif
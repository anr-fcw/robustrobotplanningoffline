#include "Include/ParserPOMDPSparse.h"
#include "Include/BuildFSC.h"
#include "Include/ParserDecPOMDPSparse.h"
#include "Include/SimDecPomdp.h"
#include "Include/BestResponseMomdpModelSparse.h"
#include <numeric>

#include <filesystem>
using std::__fs::filesystem::directory_iterator;
const double error_gap = 0.01;
void BuildRobustRobotFinalTable(DecPomdpInterface *Pb,
                                string robust_robot_fsc_path,
                                vector<PomdpInterface *> &all_brms_HL,
                                vector<PomdpInterface *> &all_brms_HR,
                                vector<PomdpInterface *> &all_brms_HLR,
                                string outfile);
void BuildResRobustRobotTable(vector<PomdpInterface *> all_pomdps,
                              vector<double> &values,
                              string robust_robot_fsc_path,
                              string outfile);

double ComputeSem(vector<double> &v, double &mean);

int main()
{
    string decpomdp_path = "problem_folder/fcw_icra_base_test_big.dpomdp";
    DecPomdpInterface *Pb = new ParsedDecPOMDPSparse(decpomdp_path);

    string robust_robot_fsc_path = "results/RobustRobotPolicy.fsc";

    string BRM_HL_pomdp_dir_path = "ExperimentsData_big_map/BRM_HL";
    string BRM_HR_pomdp_dir_path = "ExperimentsData_big_map/BRM_HR";
    string BRM_HLR_pomdp_dir_path = "ExperimentsData_big_map/RobotBestResponsePomdp";

    string out_robust_robot_res = "results/EvaluationRobustRobot.csv";
    string out_robust_robot_final_table = "results/EvaluationRobustRobotFinalTable.csv";

    map<int, string> all_sample_BRM_HL_paths;
    map<int, string> all_sample_BRM_HR_paths;
    map<int, string> all_sample_BRM_HLR_paths;

    size_t size_brm_HL_dir_path_string = BRM_HL_pomdp_dir_path.size();
    size_t size_brm_HR_dir_path_string = BRM_HR_pomdp_dir_path.size();
    size_t size_brm_HLR_dir_path_string = BRM_HLR_pomdp_dir_path.size();

    for (const auto &file : directory_iterator(BRM_HL_pomdp_dir_path))
    {
        string brm_path = file.path();
        string name = brm_path;
        name.erase(0, size_brm_HL_dir_path_string + 1);
        int sample_index = stoi(name);
        all_sample_BRM_HL_paths[sample_index] = brm_path;
    }

    for (const auto &file : directory_iterator(BRM_HR_pomdp_dir_path))
    {
        string brm_path = file.path();
        string name = brm_path;
        name.erase(0, size_brm_HR_dir_path_string + 1);
        int sample_index = stoi(name);
        all_sample_BRM_HR_paths[sample_index] = brm_path;
    }

    for (const auto &file : directory_iterator(BRM_HLR_pomdp_dir_path))
    {
        string brm_path = file.path();
        string name = brm_path;
        name.erase(0, size_brm_HLR_dir_path_string + 1);
        int sample_index = stoi(name);
        all_sample_BRM_HLR_paths[sample_index] = brm_path;
    }

    vector<PomdpInterface *> all_pomdps_HLR; // both objective
    vector<PomdpInterface *> all_pomdps_HL;  // human prefer left
    vector<PomdpInterface *> all_pomdps_HR;  // human prefer right
    int size_samples = all_sample_BRM_HL_paths.size();
    for (int i = 0; i < size_samples; i++)
    {
        string BRM_HL_path = all_sample_BRM_HL_paths[i];
        string BRM_HR_path = all_sample_BRM_HR_paths[i];
        string BRM_HLR_path = all_sample_BRM_HLR_paths[i];

        PomdpInterface *BRM_HL = new ParsedPOMDPSparse(BRM_HL_path);
        all_pomdps_HL.push_back(BRM_HL);
        PomdpInterface *BRM_HR = new ParsedPOMDPSparse(BRM_HR_path);
        all_pomdps_HR.push_back(BRM_HR);
        PomdpInterface *BRM_HLR = new ParsedPOMDPSparse(BRM_HLR_path);
        all_pomdps_HLR.push_back(BRM_HLR);
    }

    // BuildResRobustRobotTable(all_pomdps, robust_robot_HLR_values, robust_robot_fsc_path, out_robust_robot_res);
    BuildRobustRobotFinalTable(Pb, robust_robot_fsc_path, all_pomdps_HL, all_pomdps_HR, all_pomdps_HLR, out_robust_robot_final_table);

    delete Pb;
    FreeAllPomdpModels(all_pomdps_HLR);
    FreeAllPomdpModels(all_pomdps_HL);
    FreeAllPomdpModels(all_pomdps_HR);
    return 0;
}

void BuildRobustRobotFinalTable(DecPomdpInterface *Pb,
                                string robust_robot_fsc_path,
                                vector<PomdpInterface *> &all_brms_HL,
                                vector<PomdpInterface *> &all_brms_HR,
                                vector<PomdpInterface *> &all_brms_HLR,
                                string outfile)
{

    ofstream outlogs;
    outlogs.open(outfile.c_str());

    outlogs << "HL, HR, HLR" << endl;
    int robot_index = 1;
    int obs_robot_size = Pb->GetSizeOfObs(robot_index);
    FSCBase robust_robot_fsc(robust_robot_fsc_path, obs_robot_size, 0);
    int size_samples = all_brms_HLR.size();

    vector<double> values_HL;
    vector<double> values_HR;
    vector<double> values_HLR;

    // double sum_value_HL = 0;
    // double sum_value_HR = 0;
    // double sum_value_HLR = 0;

    for (int i = 0; i < size_samples; i++)
    {
        // avg value for HL
        PomdpInterface *BRM_HL = all_brms_HL[i];
        double V_HL_i = robust_robot_fsc.PolicyEvaluation(BRM_HL, error_gap);
        // sum_value_HL += V_HL_i;
        values_HL.push_back(V_HL_i);

        // avg value for HR
        PomdpInterface *BRM_HR = all_brms_HR[i];
        double V_HR_i = robust_robot_fsc.PolicyEvaluation(BRM_HR, error_gap);
        // sum_value_HR += V_HR_i;
        values_HR.push_back(V_HR_i);

        // avg value for HLR
        PomdpInterface *BRM_HLR = all_brms_HLR[i];
        double V_HLR_i = robust_robot_fsc.PolicyEvaluation(BRM_HLR, error_gap);
        // sum_value_HLR += V_HLR_i;
        values_HLR.push_back(V_HLR_i);

        outlogs << V_HL_i << "," << V_HR_i << "," << V_HLR_i << endl;
    }


    double avg_V_HL, avg_V_HR, avg_V_HLR = 0;

    double sem_HL = ComputeSem(values_HL, avg_V_HL);
    double sem_HR = ComputeSem(values_HR, avg_V_HR);
    double sem_HLR = ComputeSem(values_HLR, avg_V_HLR);

    outlogs << endl;
    outlogs << "avg_V_HL, " << avg_V_HL << endl;
    outlogs << "avg_V_HR, " << avg_V_HR << endl;
    outlogs << "avg_V_HLR, " << avg_V_HLR << endl;

    outlogs << endl;
    outlogs << "sem_V_HL, " << sem_HL << endl;
    outlogs << "sem_V_HR, " << sem_HR << endl;
    outlogs << "sem_V_HLR, " << sem_HLR << endl;

    outlogs.close();
}

void BuildResRobustRobotTable(vector<PomdpInterface *> all_pomdps, vector<double> &values, string robust_robot_fsc_path, string outfile)
{
    ofstream outlogs;
    outlogs.open(outfile.c_str());
    string first_row_str = " ";
    for (size_t sample_brm_index = 0; sample_brm_index < all_pomdps.size(); sample_brm_index++)
    {
        string name = "sample_brm_" + to_string(sample_brm_index);
        first_row_str += "," + name;
    }

    outlogs << first_row_str << endl;

    string row_name = "robust_robot";
    string robot_fsc_path = robust_robot_fsc_path;

    outlogs << row_name;
    for (size_t sample_brm_i = 0; sample_brm_i < all_pomdps.size(); sample_brm_i++)
    {
        PomdpInterface *BRMPb = all_pomdps[sample_brm_i];
        int robot_obs_size = BRMPb->GetSizeOfObs();
        FSCBase robot_fsc(robot_fsc_path, robot_obs_size, 0);
        double v_temp = robot_fsc.PolicyEvaluation(BRMPb, 0.01);
        values.push_back(v_temp);
        cout << "value: " << v_temp << endl;
        outlogs << ", " << v_temp;
    }
    outlogs << endl;

    outlogs.close();
}

double ComputeSem(vector<double> &v, double &mean)
{
    double sum = std::accumulate(v.begin(), v.end(), 0.0);
    mean = sum / v.size();

    double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size() - mean * mean);
    double sem = stdev / sqrt(v.size());

    return sem;
}
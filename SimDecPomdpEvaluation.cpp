#include <iostream>
#include "Include/ParserDecPOMDPSparse.h"
#include "Include/ParserPOMDPSparse.h"
#include "Include/ParserSarsopResult.h"
#include "Include/BuildFSC.h"
#include "Include/RobotRobustPlanModelSparse.h"
#include "Include/SampleLocalFsc.h"
#include "Include/BestResponseMomdpModelSparse.h"
#include "Include/SampleStochasticLocalFsc.h"
#include "Include/SimDecPomdp.h"
#include "Include/SimPomdp.h"
#include <numeric>

#include <filesystem>
using std::__fs::filesystem::directory_iterator;

double ComputeSem(vector<int> &v, double &mean);
double ComputeSem(vector<double> &v, double &mean);
void SimulationWithFSCs(string human_fsc_path,
                        string robot_fsc_path,
                        int sim_index,
                        vector<DecPomdpInterface *> &all_pbs,
                        double terminal_reward,
                        int steps,
                        double &out_sum_rewards,
                        int &out_success);
int main()
{
    srand(time(NULL));

    string experiment_data_prefer_left_folder_path = "ExperimentsData_big_map/SampledHumanFSCPreferLeft";
    string experiment_data_prefer_right_folder_path = "ExperimentsData_big_map/SampledHumanFSCPreferRight";
    vector<string> human_fsc_folders = {experiment_data_prefer_left_folder_path, experiment_data_prefer_right_folder_path};

    string path_pb_1 = "problem_folder/fcw_icra_human_prefer_left_test_big.dpomdp";
    string path_pb_2 = "problem_folder/fcw_icra_human_prefer_right_test_big.dpomdp";

    DecPomdpInterface *Pb1 = new ParsedDecPOMDPSparse(path_pb_1);
    DecPomdpInterface *Pb2 = new ParsedDecPOMDPSparse(path_pb_2);

    vector<DecPomdpInterface *> all_pbs = {Pb1, Pb2};

    string robot_policy_decpomdp = "HumanExperimentsData/robot_policies/robot_policy_decpomdp.fsc";
    string robot_policy_T0 = "results/RobustRobotPolicyT0.0.fsc";
    string robot_policy_T03S200 = "results/RobustRobotPolicyT0.3s200.fsc";
    string robot_policy_T03S400 = "results/RobustRobotPolicyT0.3s400.fsc";
    string robot_policy_T03S600 = "results/RobustRobotPolicyT0.3s600.fsc";
    string robot_policy_T05S200 = "results/RobustRobotPolicyT0.5s200.fsc";
    string robot_policy_T05S400 = "results/RobustRobotPolicyT0.5s400.fsc";
    string robot_policy_T05S600 = "results/RobustRobotPolicyT0.5s600.fsc";

    vector<string> robot_fsc_paths = {robot_policy_decpomdp, robot_policy_T0, robot_policy_T03S200, robot_policy_T03S400, robot_policy_T03S600, robot_policy_T05S200, robot_policy_T05S400, robot_policy_T05S600};

    string outfile = "sum_rewards_success_rates_robot_policies.csv";
    ofstream outlogs;
    outlogs.open(outfile.c_str());

    int size_human_fscs = 50;
    int restarts = 10;
    double terminal_reward = 96;
    int steps = 30;

    map<int, vector<int>> all_robot_policy_success_counts_both_objectives;
    map<int, vector<int>> all_robot_policy_success_counts_HL;
    map<int, vector<int>> all_robot_policy_success_counts_HR;
    map<int, vector<double>> all_robot_policy_acc_rewards_both_objectives;
    map<int, vector<double>> all_robot_policy_acc_rewards_HL;
    map<int, vector<double>> all_robot_policy_acc_rewards_HR;

    for (size_t robot_index = 0; robot_index < robot_fsc_paths.size(); robot_index++)
    {
        string robot_fsc_path = robot_fsc_paths[robot_index];
        // ------- for both objectives -------
        for (int i = 0; i < size_human_fscs; i++)
        {
            for (size_t j = 0; j < restarts; j++)
            {
                int sim_index = RandT<int>(0, all_pbs.size());
                string human_fsc_path = human_fsc_folders[sim_index] + "/" + to_string(i) + ".fsc";
                double sum_reward = 0;
                int success = 0;
                SimulationWithFSCs(human_fsc_path, robot_fsc_path, sim_index, all_pbs, terminal_reward, steps, sum_reward, success);
                all_robot_policy_acc_rewards_both_objectives[robot_index].push_back(sum_reward);
                all_robot_policy_success_counts_both_objectives[robot_index].push_back(success);
                // cout << "-----" << endl;
                // cout << sum_reward << endl;
                // cout << success << endl;
            }
        }

        // ------- for HL -------
        for (int i = 0; i < size_human_fscs; i++)
        {
            string human_fsc_path = experiment_data_prefer_left_folder_path + "/" + to_string(i) + ".fsc";
            double sum_reward = 0;
            int success = 0;
            int sim_index = 0;
            SimulationWithFSCs(human_fsc_path, robot_fsc_path, sim_index, all_pbs, terminal_reward, steps, sum_reward, success);
            all_robot_policy_acc_rewards_HL[robot_index].push_back(sum_reward);
            all_robot_policy_success_counts_HL[robot_index].push_back(success);
        }

        // ------- for HR -------
        for (int i = 0; i < size_human_fscs; i++)
        {
            string human_fsc_path = experiment_data_prefer_right_folder_path + "/" + to_string(i) + ".fsc";
            double sum_reward = 0;
            int success = 0;
            int sim_index = 1;
            SimulationWithFSCs(human_fsc_path, robot_fsc_path, sim_index, all_pbs, terminal_reward, steps, sum_reward, success);
            all_robot_policy_acc_rewards_HR[robot_index].push_back(sum_reward);
            all_robot_policy_success_counts_HR[robot_index].push_back(success);
        }
    }

    outlogs << "robot, mean HL, sem HL, mean HR, sem HR, mean both, sem both" << endl;
    for (size_t j = 0; j < robot_fsc_paths.size(); j++)
    {
        double mean_both, mean_HL, mean_HR = 0;
        double sem_both = ComputeSem(all_robot_policy_success_counts_both_objectives[j], mean_both);
        double sem_HL = ComputeSem(all_robot_policy_success_counts_HL[j], mean_HL);
        double sem_HR = ComputeSem(all_robot_policy_success_counts_HR[j], mean_HR);

        double mean_return_both, mean_return_HL, mean_return_HR = 0;
        double sem_return_both = ComputeSem(all_robot_policy_acc_rewards_both_objectives[j], mean_return_both);
        double sem_return_HL = ComputeSem(all_robot_policy_acc_rewards_HL[j], mean_return_HL);
        double sem_return_HR = ComputeSem(all_robot_policy_acc_rewards_HR[j], mean_return_HR);

        outlogs << j << ",";
        outlogs << mean_return_HL << "; " << mean_HL << "," << sem_return_HL << "; " << sem_HL << ",";
        outlogs << mean_return_HR << "; " << mean_HR << "," << sem_return_HR << "; " << sem_HR << ",";
        outlogs << mean_return_both << "; " << mean_both << "," << sem_return_both << "; " << sem_both << endl;
    }
    outlogs.close();

    return 0;
}

double ComputeSem(vector<int> &v, double &mean)
{
    int sum = std::accumulate(v.begin(), v.end(), 0);
    mean = (1.0 * sum) / v.size();
    cout << "v size success rate: " << v.size() << endl;
    cout << "sum success rate: " << sum << endl;

    double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size() - mean * mean);
    double sem = stdev / sqrt(v.size());
    return sem;

    // return stdev;
}

double ComputeSem(vector<double> &v, double &mean)
{
    double sum = std::accumulate(v.begin(), v.end(), 0.0);
    mean = sum / v.size();

    cout << "v size value: " << v.size() << endl;
    cout << "sum value: " << sum << endl;

    double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size() - mean * mean);
    double sem = stdev / sqrt(v.size());
    return sem;

    // return stdev;
}

void SimulationWithFSCs(string human_fsc_path,
                        string robot_fsc_path,
                        int sim_index,
                        vector<DecPomdpInterface *> &all_pbs,
                        double terminal_reward,
                        int steps,
                        double &out_sum_rewards,
                        int &out_success)
{
    vector<string> fscs = {human_fsc_path, robot_fsc_path};
    DecPomdpInterface *pb = all_pbs[sim_index];
    SimDecPomdp sim(pb, fscs);
    sim.SetTerminalReward(terminal_reward);
    sim.SimulateNstepsUsingFSCs(steps, out_sum_rewards, out_success);
}

#include <iostream>
#include "Include/ParserDecPOMDPSparse.h"
#include "Include/ParserPOMDPSparse.h"
#include "Include/ParserSarsopResult.h"
#include "Include/BuildFSC.h"
#include "Include/RobotRobustPlanModelSparse.h"
#include "Include/SampleLocalFsc.h"
#include "Include/BestResponseMomdpModelSparse.h"
#include "Include/SampleStochasticLocalFsc.h"
#include "Include/SimDecPomdp.h"
#include "Include/SimPomdp.h"
#include <numeric>

#include <filesystem>
using std::__fs::filesystem::directory_iterator;

double terminal_reward = 96;
int max_steps = 30; // stay the same with the real human experiments

int main()
{
    srand(time(NULL));
    string robot_fsc = "HumanExperimentsData/robot_policies/RobustRobotPolicyT05.fsc";
    string BRM_POMDP = "ExperimentsData_big_map/BRM_HR/";

    string outfile = "test_success_rates_robot_policies.csv";
    ofstream outlogs;
    outlogs.open(outfile.c_str());

    for (size_t i = 0; i < 50; i++)
    {

        for (size_t j = 0; j < 1; j++)
        {
            string BRM_POMDP_path = BRM_POMDP + to_string(i) + ".pomdp";
            PomdpInterface *BRM = new ParsedPOMDPSparse(BRM_POMDP_path);
            SimPomdp sim(BRM, robot_fsc, terminal_reward);
            double sum_rewards = 0;
            int sucess_result = sim.SimulateNstepsCheckSuccess(max_steps, sum_rewards);
            cout << "sucess: " << sucess_result << endl;
            outlogs << sum_rewards << "," << sucess_result;
        }
        outlogs << endl;
    }

    outlogs.close();

    return 0;
}

from cmath import nan
from re import sub
import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import os
import glob

file_adapt_evaluation = "../Subject_adapt_evaluation.csv"
file_params_order = "../Subject_params_order.csv"
file_preference = "../Subject_preference.csv"
file_rating = "../Subject_rating.csv"
user_log_path = "../Subject_data"

user_log_csv_files = sorted(glob.glob(os.path.join(user_log_path, "*.csv")))

subject_questionnaire_files = [file_params_order, file_rating, file_adapt_evaluation, file_preference]

all_subject = []

df_subject_params_order =  pd.read_csv(file_params_order)


arrs_subject_name = df_subject_params_order.iloc[:,0].values


print(arrs_subject_name)


def AnalyzeRating(file_rating):
    df = pd.read_csv(file_rating)
    avg_rates = df[["T0","T03","T05"]].mean()
    std = df[["T0","T03","T05"]].std()


    # Plot rating data
    p = avg_rates.plot.bar(yerr=std)
    # p.set_title("Average rate for three robot policies", fontsize=18)
    p.set_xlabel("Robot policy", fontsize=12)
    p.set_ylabel("Rating", fontsize=12)
    p.set_ylim(0,6)
    plt.savefig("fig_rating.pdf")
    plt.close()

    dict_avg_rates = avg_rates.to_dict()
    dict_stds = std.to_dict()

    return dict_avg_rates, dict_stds

def AnalyzeAdapt(file_adapt_evaluation):
    NA = "No adaptation"
    HAR = "Human adapt robot"
    RAH = "Robot adapt human"
    MA = "Mutual adaptation"
    data = {NA:[], HAR:[], RAH: [], MA: []}
    No_adapt_list = []
    H_adapt_R_list = []
    R_adapt_H_list = []
    Mutual_adapt_list = []
    index = ['T0', 'T03', 'T05']
    df = pd.read_csv(file_adapt_evaluation)
    for ind in index:
        nb_NA = 0 
        nb_HAR = 0
        nb_RAH= 0
        nb_MA = 0
        for x in df[ind]:
            if x == 'HAR':
                nb_HAR += 1
            elif x == 'RAH':
                nb_RAH += 1
            elif x == 'MA':
                nb_MA += 1
            else:
                nb_NA += 1
        No_adapt_list.append(nb_NA)
        H_adapt_R_list.append(nb_HAR)
        R_adapt_H_list.append(nb_RAH)
        Mutual_adapt_list.append(nb_MA)

    data[NA] = No_adapt_list
    data[HAR] = H_adapt_R_list
    data[RAH] = R_adapt_H_list
    data[MA] = Mutual_adapt_list

    df = pd.DataFrame(data, index=index)
    p = df.plot.bar(rot=0)
    # p.set_title("Evaluation of adaptions for three robot policies", fontsize=18)
    p.set_xlabel("Robot policy", fontsize=12)
    p.set_ylabel("Evaluation", fontsize=12)
    p.legend(loc=1, fontsize=8)

    plt.savefig("fig_adaption.pdf")
    plt.close()

    return data

def AnalyzeFavoredPolicy(file_preference):
    df = pd.read_csv(file_preference)
    data = {'lab': ['T0', 'T03', 'T05'],'val':[]}
    nb_T0 = 0
    nb_T03 = 0
    nb_T05 = 0
    for x in df['Preference']:
        if x == 'T03':
            nb_T03 += 1
        elif x == 'T05':
            nb_T05 += 1
        else:
            nb_T0 += 1   

    data['val'] = [nb_T0, nb_T03, nb_T05]

    df = pd.DataFrame(data)
    p = df.plot.bar(x = 'lab', y='val',rot=0, legend=False)
    # p.set_title("Human favored robot policy", fontsize=18)
    p.set_xlabel("Robot policy", fontsize=12)
    p.set_ylabel("Numbers", fontsize=12)
    plt.savefig("fig_favored_policy.pdf")  

    return data


def AnalyzeUserLogFiles(user_log_csv_files):

    index = ['T0', 'T03', 'T05']

    subject_index = 0
    dict_success = {}
    dict_acc_rewards = {}
    for user_log_file in user_log_csv_files:
        print(user_log_file)
        subject_i_params_order = df_subject_params_order.loc[subject_index,:]
        subject_name = subject_i_params_order[0]
        dict_success[subject_i_params_order[0]] = []
        AnalyzeSingleSubjectSuccessRate(user_log_file, subject_i_params_order, subject_name, dict_success)
        AnalyzeSingleSubjectAccRewards(user_log_file,subject_i_params_order,subject_name,dict_acc_rewards)
        subject_index += 1


    ## -------- success number and rate -----------

    # individual success fig
    df = pd.DataFrame(dict_success, index=index)
    p1 = df.plot.bar(rot=0, legend=False)
    # p1.set_title("Success numbers for three robot policies", fontsize=18)
    p1.set_xlabel("Robot policy", fontsize=12)
    p1.set_ylabel("Number", fontsize=12)
    plt.savefig("fig_success.pdf")
    plt.close()

    # mean and std
    avg_success = df.mean(axis=1)
    sem = df.sem(axis=1)

    p2 = avg_success.plot.bar(yerr=sem)
    # p2.set_title("Average success numbers for three robot policies", fontsize=18)
    p2.set_xlabel("Robot policy", fontsize=12)
    p2.set_ylabel("Number", fontsize=12)
    p2.set_ylim(0,10)
    plt.savefig("fig_avg_std_success.pdf")
    plt.close()

    # compute mean success rate and std
    rate_success_mean = avg_success/8
    rate_success_sem = sem/8

    ## ------- Acc rewards ------------
    # individual mean acc rewards fig
    df = pd.DataFrame(dict_acc_rewards, index=index)
    p3 = df.plot.bar()
    # p3.set_title("Mean acc rewards", fontsize=18)
    p3.set_xlabel("Robot policy", fontsize=12)
    p3.set_ylabel("Avg acc rewards", fontsize=12)
    # p3.set_ylim(-100,30)
    plt.savefig("fig_each_subject_acc_rewards.pdf")
    plt.close()

    # mean and std

    avg_acc_rewards = df.mean(axis=1)
    sem_acc_rewards = df.sem(axis=1)

    p4 = avg_acc_rewards.plot.bar(yerr=sem_acc_rewards)
    # p4.set_title("Mean acc rewards for all subjects", fontsize=18)
    p4.set_xlabel("Robot policy", fontsize=12)
    p4.set_ylabel("Mean acc rewards", fontsize=12)
    # p4.set_ylim(0,10)
    plt.savefig("fig_avg_std_acc_rewards.pdf")
    plt.close()


    print(rate_success_mean)
    print(rate_success_sem)
    print(avg_acc_rewards)
    print(sem_acc_rewards)
    return rate_success_mean, rate_success_sem, avg_acc_rewards, sem_acc_rewards



def AnalyzeSingleSubjectSuccessRate(user_log_file, subject_params_order, subject_name, dict_sucess):
    period = 8
    df_user_log = pd.read_csv(user_log_file)
    # finsh label in col 31; 0: out of time; 1: sucess; 2: abort.
    temp_sucess_log = {"T0":0, "T03":0, "T05":0}
    current_round = 0
    for x in df_user_log.iloc[:,31]:
        robot_policy_index = current_round // period
        robot_label = subject_params_order[robot_policy_index + 1]
        # print(robot_label, x)
        if x == 1:
            temp_sucess_log[robot_label] += 1
        current_round += 1
    dict_sucess[subject_name] = [temp_sucess_log["T0"], temp_sucess_log["T03"], temp_sucess_log["T05"]]


def AnalyzeSingleSubjectAccRewards(user_log_file, subject_params_order, subject_name, dict_mean_acc_rewards):
    period = 8
    df_user_log = pd.read_csv(user_log_file)
    temp_rewards_log = {"T0":0, "T03":0, "T05":0}
    current_round = 0
    for x in df_user_log.iloc[:,34]:
        robot_policy_index = current_round // period
        robot_label = subject_params_order[robot_policy_index + 1]
        temp_rewards_log[robot_label] += x
        current_round += 1
    dict_mean_acc_rewards[subject_name] = [temp_rewards_log["T0"]/8, temp_rewards_log["T03"]/8, temp_rewards_log["T05"]/8]


AnalyzeRating(file_rating)
AnalyzeAdapt(file_adapt_evaluation)
AnalyzeFavoredPolicy(file_preference)
AnalyzeUserLogFiles(user_log_csv_files)
# AnalyzeSingleSubjectSucessRate(test_log_file, subject_Jacques_params_order)
from ast import For
import random
from itertools import permutations 


# 1.flip (vertical * horizontal)

all_flips = [[0,0],[0,1],[1,0], [1,1]]


all_flips_perm = list(permutations(all_flips))

# 2.policy_robot (T0.0, T0.3, T0.5, DecPOMDP, BR)
pi_r_set = ["T0", "T03", "T05"]
# smart_pi_r_set = ["T03", "T05"]


all_pi_r_index_permutations = [[0,1,2], [0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]]

output_file = "subjects_params_file.txt"
f = open(output_file, "w")

nb_subjects = 15
nb_rounds = 24


def writelist(list, file):
    for item in list:
        if type(item) == float:
            item = str(item)
        file.writelines(item + " ")


def GenerateParamsFile(file, all_flips_perm, all_pi_r_index_perm):
    for i in range(0, nb_subjects):
        pi_r = random.choice(all_pi_r_index_perm)
        flips = random.choice(all_flips_perm)
        for pi_r_index in pi_r:
            for flip in flips:
                tmp_params = [str(flip[0]), str(flip[1]), pi_r_set[pi_r_index]]
                for j in range(0, 2):
                    writelist(tmp_params, file)
                    file.write(" , ")

        file.write("\n")

# print(subject_rounds_params_flips)
# print(len(subject_rounds_params_flips))

# print(subject_rounds_params_pi_robot)
# print(len(subject_rounds_params_pi_robot))

GenerateParamsFile(f, all_flips_perm, all_pi_r_index_permutations)
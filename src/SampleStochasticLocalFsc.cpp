#include "../Include/SampleStochasticLocalFsc.h"

void SampleStochasticLocalFsc::Init(double T, double min_action_pb, double max_belief_gap, MCTS &mcts)
{
    this->T = T;
    this->min_pb = min_action_pb;
    this->max_belief_gap = max_belief_gap;
    this->mcts = &mcts;
}

int SampleStochasticLocalFsc::CheckAlphaExist(AlphaVector &alpha)
{
    for (unsigned int i = 0; i < this->Nodes.size(); i++)
    {
        // check two alpha vector equal or not
        AlphaVector current_alpha = this->Nodes[i].GetAlphaVector();
        if (current_alpha.GetValues() == alpha.GetValues() && current_alpha.GetActionIndex() == alpha.GetActionIndex())
        {
            return i;
        }
    }

    return -1;
};

int SampleStochasticLocalFsc::CheckBeliefExist(BeliefSparse &b, double max_accept_gap)
{

    double min_distance = 0;
    int similiar_node_index = this->GiveSimilarNodeIndex(b, min_distance);
    if (min_distance < max_accept_gap)
    {
        return similiar_node_index;
    }
    else
    {
        return -1;
    }
};

// SampleStochasticLocalFsc::SampleStochasticLocalFsc(vector<AlphaVector> &alpha_vecs, DecPomdpInterface *decpomdp, int LocalAgentIndex, int init_type, int formalization_type)
// {
//     // Initialize eta
//     this->LocalAgentIndex = LocalAgentIndex;
//     this->decpomdp = decpomdp;

//     // if use checkbelief, node size will be very big! need to change the eta size!!
//     // this->eta.resize(alpha_vecs.size() + 1);
//     this->eta.resize(100000);
//     vector<LocalFscSochasticNode> UnProcessedSet; // Initlize a set to store the unprocessed nodes, Call it openlist
//     BeliefSparse *b0 = decpomdp->GetInitialBeliefSparse();

//     AlphaVector alpha0 = argmax_alpha_vector(alpha_vecs, b0);

//     map<int, double> pb_joint_acions;
//     map<int, double> pb_local_acions;

//     SoftmaxActions(decpomdp, *b0, alpha_vecs, pb_joint_acions, pb_local_acions, LocalAgentIndex, this->T, this->min_pb);

//     LocalFscSochasticNode n0(alpha0, *b0, pb_joint_acions, pb_local_acions);
//     // add a descript of action name
//     // n0.SetDescript(decpomdp->GetAllActionsVecs()[LocalAgentIndex][aHI]);
//     string description = GiveDescription(pb_local_acions);
//     n0.SetDescript(description);

//     this->formalization_type = formalization_type;
//     int n_index = 0; // start with n0
//     int type = formalization_type;
//     // commented for test momdp formalization
//     if (type == 0)
//     {
//         this->Nodes.push_back(n0);
//     }
//     else if (type == 1)
//     {
//         cerr << "Please dont use type 1 formalization which has a useless init node!" << endl;
//         throw("");
//     }
//     else
//     {
//         cerr << "Wrong argument for FSC type!" << endl;
//         throw("");
//     }
//     UnProcessedSet.push_back(n0);

//     while (!UnProcessedSet.empty())
//     {
//         // std::vector<node>::iterator it = UnProcessedSet.begin();
//         // node n = *it;
//         // Attention, need to check if node n is still exist
//         UnProcessedSet.erase(UnProcessedSet.begin());
//         // cout << "UnProcessedSet Size: " << UnProcessedSet.size() << endl;

//         // Need modify ProcessNode deterministic
//         ProcessNode(alpha_vecs, n_index, UnProcessedSet, decpomdp, init_type);

//         n_index++;
//     }
// }

void SampleStochasticLocalFsc::BuildFsc(DecPomdpInterface *decpomdp, int LocalAgentIndex, int init_type, int formalization_type, int max_node_size)
{
    (void)init_type;
    cout << "Start building the human FSC with stochastic actions!" << endl;

    this->action_space = decpomdp->GetAllActionsVecs()[LocalAgentIndex];
    this->observation_space = decpomdp->GetAllObservationsVecs()[LocalAgentIndex];
    // Initialize eta
    this->LocalAgentIndex = LocalAgentIndex;
    this->decpomdp = decpomdp;
    this->max_node_size = max_node_size;
    this->eta.resize(max_node_size);
    // use a set
    map<double, vector<LocalFscSochasticNode>> UnProcessedSet; // Initlize a map to store the unprocessed nodes, Call it openlist
    BeliefSparse *b0 = decpomdp->GetInitialBeliefSparse();
    map<int, map<int, double>> dist_joint_acions;
    map<int, double> pb_local_acions;
    // SoftmaxActions(decpomdp, *b0, alpha_vecs, dist_joint_acions, pb_local_acions, LocalAgentIndex, this->T);
    double V_b0 = SoftmaxActions(decpomdp, *b0, dist_joint_acions, pb_local_acions, LocalAgentIndex, this->T, this->min_pb, *this->mcts);

    // SoftmaxActions(decpomdp, *b0, alpha_vecs, dist_joint_acions, pb_local_acions, LocalAgentIndex, this->T, this->min_pb, this->_m_id_belief, this->_m_belief_value);

    double weight = 1.0;
    LocalFscSochasticNode n0(weight, *b0, dist_joint_acions, pb_local_acions);
    string description = GiveDescription(pb_local_acions);
    n0.SetDescript(description);
    UnProcessedSet[weight * V_b0].push_back(n0);

    this->formalization_type = formalization_type;
    // int n_index = 0; // start with n0
    int type = formalization_type;
    // commented for test momdp formalization
    if (type == 0)
    {
        this->Nodes.push_back(n0);
    }
    else if (type == 1)
    {
        cerr << "Please dont use type 1 formalization which has a useless init node!" << endl;
        throw("");
    }
    else
    {
        cerr << "Wrong argument for FSC type!" << endl;
        throw("");
    }

    while (!UnProcessedSet.empty())
    {
        // bool max_size_reached = false;
        // if (this->Nodes.size() >= this->max_node_size)
        // {
        //     max_size_reached = true;
        // }

        LocalFscSochasticNode n = UnProcessedSet.rbegin()->second.front();
        int n_index = this->GetNodeIndex(n);

        // map<double, vector<LocalFscSochasticNode>>::iterator it;
        // for (it = UnProcessedSet.begin(); it != UnProcessedSet.end(); it++)
        // {
        //     double key = it->first;
        //     vector<LocalFscSochasticNode> value = it->second;
        //     cout << "weight: " << key << endl;
        //     for (size_t i = 0; i < value.size(); i++)
        //     {
        //         cout << "  node index: " << this->GetNodeIndex(value[i]) << endl;
        //     }
        // }

        // later change it to set, set has order
        // n.GetJointBeliefSparse().PrintBeliefSparse();
        UnProcessedSet.rbegin()->second.erase(UnProcessedSet.rbegin()->second.begin());
        if (UnProcessedSet.rbegin()->second.size() == 0)
        {
            double key = UnProcessedSet.rbegin()->first;
            UnProcessedSet.erase(key);
        }

        // ProcessNode(alpha_vecs, n, UnProcessedSet, this->decpomdp, max_size_reached);
        ProcessNodeWithMaxSize(n, UnProcessedSet, this->decpomdp);

        // cout << "bool_max_node: " << max_size_reached << endl;
        cout << "max node size: " << this->max_node_size << endl;
        cout << "fsc node size: " << this->Nodes.size() << endl;
        cout << "G remain size: " << UnProcessedSet.size() << endl;
        cout << "Processed node index: " << n_index << endl;
    }

    cout << "max depth is " << this->max_depth << endl;
    // this->CheckFscValidation();
    // cout << "FSC validation check finished!" << endl;
}

string SampleStochasticLocalFsc::GiveDescription(map<int, double> &pb_local_actions)
{
    string res;
    map<int, double>::iterator it_aI;
    res = "{";
    for (it_aI = pb_local_actions.begin(); it_aI != pb_local_actions.end(); it_aI++)
    {
        int aHI = it_aI->first;
        string name_action = decpomdp->GetAllActionsVecs()[LocalAgentIndex][aHI];
        string pb = to_string(it_aI->second);
        string s_temp = name_action + ":" + pb + ", ";
        res += s_temp;
    }
    res += "}";
    return res;
}

// void SampleStochasticLocalFsc::ProcessNodeWithMaxSize(vector<AlphaVector> &alpha_vecs, LocalFscSochasticNode &node_process, std::map<double, vector<LocalFscSochasticNode>> &UnProcessedSet, DecPomdpInterface *decpomdp)

void SampleStochasticLocalFsc::ProcessNodeWithMaxSize(LocalFscSochasticNode &node_process, std::map<double, vector<LocalFscSochasticNode>> &UnProcessedSet, DecPomdpInterface *decpomdp)
{
    LocalFscSochasticNode n = node_process;
    int n_index = this->GetNodeIndex(node_process);

    if (n_index == -1)
    {
        cout << "Cannot find the given node!" << endl;
        throw "";
    }

    BeliefSparse b = n.GetJointBeliefSparse();

    double weight = n.GetWeight();
    int node_depth = n.GetDepth();

    map<int, map<int, double>> dist_joint_acions = n.GetHumanActionToJointActionDistribution();
    map<int, double> dist_aHI = n.GetHumanActionDistribution();

    for (int aHI = 0; aHI < decpomdp->GetSizeOfA(this->LocalAgentIndex); aHI++)
    {
        double pb_aHI = dist_aHI[aHI];
        map<int, double> all_possible_oHI = compute_dist_pr_oh(decpomdp, b, dist_joint_acions[aHI], this->LocalAgentIndex);
        // cout << "all_possible oHI: ";
        // PrintMap(all_possible_oHI);

        for (int oHI = 0; oHI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); oHI++)
        {
            vector<int> fsc_obs = {aHI, oHI};

            if (all_possible_oHI.count(oHI) && pb_aHI > this->min_pb)
            {
                double pr_oh_ba = all_possible_oHI[oHI];
                BeliefSparse b_new = UpdateOneSideObservation(decpomdp, b, dist_joint_acions[aHI], oHI, pr_oh_ba, this->LocalAgentIndex);
                // cout << "B_new: ";
                // b_new.PrintBeliefSparse();

                double new_node_weight = pb_aHI * pr_oh_ba * weight;

                // Check belief exist or not
                int FLAG_Exist = CheckBeliefExist(b_new, this->max_belief_gap);
                // if belief not exist and node size is OK
                if (FLAG_Exist == -1 && int(this->Nodes.size()) < this->max_node_size)
                {
                    map<int, map<int, double>> new_dist_joint_actions;
                    map<int, double> new_pb_local_actions;
                    double V_b_new = SoftmaxActions(decpomdp, b_new, new_dist_joint_actions, new_pb_local_actions, LocalAgentIndex, this->T, this->min_pb, *this->mcts);
                    LocalFscSochasticNode n_new(new_node_weight, b_new, new_dist_joint_actions, new_pb_local_actions);
                    string description = GiveDescription(new_pb_local_actions);
                    n_new.SetDescript(description);
                    // add this new node to N and G
                    // add depth
                    int new_depth = node_depth + 1;
                    n_new.SetDepth(new_depth);
                    this->Nodes.push_back(n_new);
                    // UnProcessedSet[new_node_weight].push_back(n_new);
                    UnProcessedSet[new_node_weight * V_b_new].push_back(n_new);
                    int new_node_Index = this->Nodes.size() - 1;
                    this->eta[n_index][fsc_obs][new_node_Index] = 1;

                    if (new_depth > this->max_depth)
                    {
                        this->max_depth = new_depth;
                    }
                }
                else
                {
                    // if max node size is reached
                    if (int(this->Nodes.size()) >= this->max_node_size)
                    {
                        double similiar_distance = 0.0;
                        int similiar_nI = this->GiveSimilarNodeIndex(b_new, similiar_distance);
                        this->eta[n_index][fsc_obs][similiar_nI] = 1;
                        double w_updated = this->Nodes[similiar_nI].GetWeight();
                        w_updated += new_node_weight;
                        this->Nodes[similiar_nI].SetWeight(w_updated);
                    }
                    // if belief already exist
                    else
                    {
                        double w_updated = this->Nodes[FLAG_Exist].GetWeight();
                        w_updated += new_node_weight;
                        this->Nodes[FLAG_Exist].SetWeight(w_updated);
                        // if this new belief already exist, link to the node with existed alpha vector
                        this->eta[n_index][fsc_obs][FLAG_Exist] = 1;
                    }
                }

                // if (FLAG_Exist == -1)
                // {

                //     map<int, map<int, double>> new_dist_joint_actions;
                //     map<int, double> new_pb_local_actions;
                //     double V_b_new = SoftmaxActions(decpomdp, b_new, alpha_vecs, new_dist_joint_actions, new_pb_local_actions, LocalAgentIndex, this->T, this->min_pb, *this->mcts);
                //     // new_node_weight *= V_b_new;
                //     // only multiply the value when I do the sort.
                //     LocalFscSochasticNode n_new(new_node_weight, b_new, new_dist_joint_actions, new_pb_local_actions);
                //     string description = GiveDescription(new_pb_local_actions);
                //     n_new.SetDescript(description);
                //     if (this->Nodes.size() >= this->max_node_size)
                //     {
                //         // max node sized already, cannot add this new node to N
                //         double similar_distance = 0.0;
                //         int similar_nI = this->GiveSimilarNodeIndex(n_new, similar_distance);
                //         this->eta[n_index][fsc_obs][similar_nI] = 1;
                //         double w = this->Nodes[similar_nI].GetWeight();
                //         double added_w = w + new_node_weight;
                //         this->Nodes[similar_nI].SetWeight(added_w);
                //     }
                //     else
                //     {
                //         // add this new node to N and G
                //         // add depth
                //         int new_depth = node_depth + 1;
                //         n_new.SetDepth(new_depth);
                //         this->Nodes.push_back(n_new);
                //         // UnProcessedSet[new_node_weight].push_back(n_new);
                //         UnProcessedSet[new_node_weight * V_b_new].push_back(n_new);
                //         int new_node_Index = this->Nodes.size() - 1;
                //         this->eta[n_index][fsc_obs][new_node_Index] = 1;

                //         if (new_depth > this->max_depth)
                //         {
                //             this->max_depth = new_depth;
                //         }
                //     }
                // }
                // else
                // {
                //     // add up the weight to the existing node
                //     double w = this->Nodes[FLAG_Exist].GetWeight();
                //     double added_w = w + new_node_weight;
                //     this->Nodes[FLAG_Exist].SetWeight(added_w);
                //     this->eta[n_index][fsc_obs][FLAG_Exist] = 1;
                // }
            }
            else
            {
                this->eta[n_index][fsc_obs][n_index] = 1; // Point back to the current node
            }
        }
    }
}

// void SampleStochasticLocalFsc::ProcessNode(vector<AlphaVector> &alpha_vecs, LocalFscSochasticNode &node_process, std::map<double, vector<LocalFscSochasticNode>> &UnProcessedSet, DecPomdpInterface *decpomdp, bool max_size_reached)
// {
//     LocalFscSochasticNode n = node_process;
//     int n_index = this->GetNodeIndex(node_process);

//     if (n_index == -1)
//     {
//         cout << "Cannot find the given node!" << endl;
//         throw "";
//     }

//     BeliefSparse b = n.GetJointBeliefSparse();

//     double weight = n.GetWeight();
//     map<int, map<int, double>> dist_joint_acions = n.GetHumanActionToJointActionDistribution();
//     map<int, double> dist_aHI = n.GetHumanActionDistribution();

//     for (int aHI = 0; aHI < decpomdp->GetSizeOfA(this->LocalAgentIndex); aHI++)
//     {
//         double pb_aHI = dist_aHI[aHI];
//         map<int, double> all_possible_oHI = compute_dist_pr_oh(decpomdp, b, dist_joint_acions[aHI], this->LocalAgentIndex);
//         // cout << "all_possible oHI: ";
//         // PrintMap(all_possible_oHI);

//         for (int oHI = 0; oHI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); oHI++)
//         {
//             vector<int> fsc_obs = {aHI, oHI};

//             if (all_possible_oHI.count(oHI) && pb_aHI > this->min_pb)
//             {
//                 double pr_oh_ba = all_possible_oHI[oHI];
//                 BeliefSparse b_new = UpdateOneSideObservation(decpomdp, b, dist_joint_acions[aHI], oHI, pr_oh_ba, this->LocalAgentIndex);
//                 // cout << "B_new: ";
//                 // b_new.PrintBeliefSparse();
//                 double new_node_weight = pb_aHI * pr_oh_ba * weight;

//                 // Check belief exist or not
//                 int FLAG_Exist = CheckBeliefExist(b_new, this->max_belief_gap);
//                 if (FLAG_Exist == -1)
//                 {
//                     map<int, map<int, double>> new_dist_joint_actions;
//                     map<int, double> new_pb_local_actions;
//                     SoftmaxActions(decpomdp, b_new, alpha_vecs, new_dist_joint_actions, new_pb_local_actions, LocalAgentIndex, this->T);
//                     LocalFscSochasticNode n_new(new_node_weight, b_new, new_dist_joint_actions, new_pb_local_actions);
//                     string description = GiveDescription(new_pb_local_actions);
//                     n_new.SetDescript(description);
//                     if (max_size_reached)
//                     {
//                         // max node sized already, cannot add this new node to N
//                         double similar_distance = 0.0;
//                         int similar_nI = this->GiveSimilarNodeIndex(n_new, similar_distance);
//                         this->eta[n_index][fsc_obs][similar_nI] = 1;
//                     }
//                     else
//                     {
//                         // add this new node to N and G
//                         this->Nodes.push_back(n_new);
//                         UnProcessedSet[new_node_weight].push_back(n_new);
//                         int new_node_Index = this->Nodes.size() - 1;
//                         this->eta[n_index][fsc_obs][new_node_Index] = 1;
//                     }
//                 }
//                 else
//                 {
//                     // add up the weight to the existing node
//                     double w = this->Nodes[FLAG_Exist].GetWeight();
//                     double added_w = w + new_node_weight;
//                     this->Nodes[FLAG_Exist].SetWeight(added_w);
//                     this->eta[n_index][fsc_obs][FLAG_Exist] = 1;
//                 }
//             }
//             else
//             {
//                 this->eta[n_index][fsc_obs][n_index] = 1; // Point back to the current node
//             }
//         }
//     }
// }

// void SampleStochasticLocalFsc::ProcessNode(vector<AlphaVector> &alpha_vecs, int n_index,
//                                            std::vector<LocalFscSochasticNode> &UnProcessedSet, DecPomdpInterface *decpomdp, int process_type)
// {

//     LocalFscSochasticNode n = this->Nodes[n_index];
//     BeliefSparse b = n.GetJointBeliefSparse();
//     set<vector<int>> set_fsc_obs;
//     map<vector<int>, set<vector<int>>> _m_set_JAI_JOI;
//     GetAllPossibleFscObs(LocalAgentIndex, n, set_fsc_obs, _m_set_JAI_JOI, decpomdp);

//     set<vector<int>>::iterator it_aHI_oHI;

//     for (int aHI = 0; aHI < decpomdp->GetSizeOfA(this->LocalAgentIndex); aHI++)
//     {
//         for (int oHI = 0; oHI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); oHI++)
//         {
//             vector<int> fsc_obs = {aHI, oHI};
//             if (set_fsc_obs.find(fsc_obs) != set_fsc_obs.end())
//             {
//                 if (process_type == 1)
//                 {
//                     this->ProcessBranchStochastic(aHI, oHI, n, b, _m_set_JAI_JOI, alpha_vecs, n_index, UnProcessedSet, decpomdp);
//                 }
//                 else if (process_type == 2)
//                 {
//                     this->ProcessBranchDeterministic(aHI, oHI, n, b, _m_set_JAI_JOI, alpha_vecs, n_index, UnProcessedSet, decpomdp);
//                 }
//                 else
//                 {
//                     cout << "Wrong init type argument!" << endl;
//                     throw("");
//                 }
//             }
//             else
//             {
//                 this->eta[n_index][fsc_obs][n_index] = 1; // Point back to the current node
//             }
//         }
//     }
// }

// void SampleStochasticLocalFsc::ProcessBranchStochastic(int aHI, int oHI, LocalFscSochasticNode &n, BeliefSparse &b, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, vector<AlphaVector> &alpha_vecs, int n_index,
//                                                        std::vector<LocalFscSochasticNode> &UnProcessedSet, DecPomdpInterface *decpomdp)
// {

//     // cout << "-------" << endl;
//     // cout << "aHI: " << aHI << endl;
//     // cout << "oHI: " << oHI << endl;
//     // cout << "-------" << endl;

//     vector<int> fsc_obs = {aHI, oHI};
//     map<vector<int>, double> out_JAI_JOI_dist;
//     ComputePbDistJAIJOI(aHI, oHI, n, alpha_vecs, _m_set_JAI_JOI, out_JAI_JOI_dist);
//     // check if this <aHI, oHI> is possible
//     if (out_JAI_JOI_dist.size() == 0)
//     {
//         // impossible <ahI, oHI> ! Linking back to itself !
//         this->eta[n_index][fsc_obs][n_index] = 1; // Point back to the current node
//     }
//     else
//     {
//         map<vector<int>, double>::iterator it_JAI_JOI;
//         for (it_JAI_JOI = out_JAI_JOI_dist.begin(); it_JAI_JOI != out_JAI_JOI_dist.end(); it_JAI_JOI++)
//         {
//             vector<int> JAI_JOI = it_JAI_JOI->first;
//             double pb_JAI_JOI = it_JAI_JOI->second;
//             BeliefSparse b_new;
//             cout << "JAI: " << JAI_JOI[0] << ", JOI:" << JAI_JOI[1] << endl;
//             cout << "pb: " << pb_JAI_JOI << endl;
//             b_new = Update(decpomdp, b, JAI_JOI[0], JAI_JOI[1]);
//             AlphaVector alpha_new = argmax_alpha_vector(alpha_vecs, &b_new);

//             map<int, double> pb_joint_acions;
//             map<int, double> pb_local_acions;

//             SoftmaxActions(decpomdp, b_new, alpha_vecs, pb_joint_acions, pb_local_acions, LocalAgentIndex, this->T, this->min_pb);

//             LocalFscSochasticNode n_new(alpha_new, b_new, pb_joint_acions, pb_local_acions);
//             string description = GiveDescription(pb_local_acions);
//             n_new.SetDescript(description);

//             // Need to check if an alpha-vector is already exist in FSC
//             int FLAG_Exist = CheckAlphaExist(alpha_new);
//             if (FLAG_Exist == -1)
//             {
//                 this->Nodes.push_back(n_new);
//                 int n_new_index = this->Nodes.size() - 1;
//                 this->eta[n_index][fsc_obs][n_new_index] = pb_JAI_JOI;
//                 UnProcessedSet.push_back(n_new);
//             }
//             else
//             {
//                 cout << "found existed alpha_vector!" << endl;
//                 cout << "b_new ignored:" << endl;
//                 b_new.PrintBeliefSparse();
//                 cout << "b" << endl;
//                 this->Nodes[FLAG_Exist].GetJointBeliefSparse().PrintBeliefSparse();
//                 // Belief Merge Or Not
//                 this->eta[n_index][fsc_obs][FLAG_Exist] += pb_JAI_JOI;
//                 // this->Nodes[FLAG_Exist].MergeBelief(b_new);
//             }
//         }
//     }
// }

// void SampleStochasticLocalFsc::ProcessBranchStochastic(int aHI, int oHI, LocalFscSochasticNode &n, BeliefSparse &b, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, vector<AlphaVector> &alpha_vecs, int n_index,
//                                                        std::vector<LocalFscSochasticNode> &UnProcessedSet, DecPomdpInterface *decpomdp)
// {

//     // cout << "-------" << endl;
//     // cout << "aHI: " << aHI << endl;
//     // cout << "oHI: " << oHI << endl;
//     // cout << "-------" << endl;

//     vector<int> fsc_obs = {aHI, oHI};
//     map<vector<int>, double> out_JAI_JOI_dist;
//     ComputePbDistJAIJOI(aHI, oHI, n, alpha_vecs, _m_set_JAI_JOI, out_JAI_JOI_dist);
//     // check if this <aHI, oHI> is possible
//     if (out_JAI_JOI_dist.size() == 0)
//     {
//         // impossible <ahI, oHI> ! Linking back to itself !
//         this->eta[n_index][fsc_obs][n_index] = 1; // Point back to the current node
//     }
//     else
//     {
//         map<vector<int>, double>::iterator it_JAI_JOI;
//         for (it_JAI_JOI = out_JAI_JOI_dist.begin(); it_JAI_JOI != out_JAI_JOI_dist.end(); it_JAI_JOI++)
//         {

//             vector<int> JAI_JOI = it_JAI_JOI->first;
//             double pb_JAI_JOI = it_JAI_JOI->second;
//             BeliefSparse b_new;
//             cout << "JAI: " << JAI_JOI[0] << ", JOI:" << JAI_JOI[1] << endl;
//             cout << "pb: " << pb_JAI_JOI << endl;
//             b_new = Update(decpomdp, b, JAI_JOI[0], JAI_JOI[1]);
//             AlphaVector alpha_new = argmax_alpha_vector(alpha_vecs, &b_new);

//             map<int, double> pb_joint_acions;
//             map<int, double> pb_local_acions;

//             SoftmaxActions(decpomdp, b_new, alpha_vecs, pb_joint_acions, pb_local_acions, LocalAgentIndex, this->T, this->min_pb);

//             LocalFscSochasticNode n_new(alpha_new, b_new, pb_joint_acions, pb_local_acions);
//             string description = GiveDescription(pb_local_acions);
//             n_new.SetDescript(description);

//             // Need to check if an alpha-vector is already exist in FSC
//             int FLAG_Exist = CheckAlphaExist(alpha_new);
//             // int FLAG_Exist = CheckBeliefExist(b_new);

//             if (FLAG_Exist == -1)
//             {
//                 this->Nodes.push_back(n_new);
//                 int n_new_index = this->Nodes.size() - 1;
//                 this->eta[n_index][fsc_obs][n_new_index] = pb_JAI_JOI;
//                 UnProcessedSet.push_back(n_new);
//             }
//             else
//             {
//                 cout << "found existed alpha_vector!" << endl;
//                 cout << "b_new ignored:" << endl;
//                 b_new.PrintBeliefSparse();
//                 cout << "b" << endl;
//                 this->Nodes[FLAG_Exist].GetJointBeliefSparse().PrintBeliefSparse();
//                 // Belief Merge Or Not
//                 this->eta[n_index][fsc_obs][FLAG_Exist] += pb_JAI_JOI;
//                 // this->Nodes[FLAG_Exist].MergeBelief(b_new);
//             }
//         }
//     }
// }

// void SampleStochasticLocalFsc::ProcessBranchDeterministic(int aHI, int oHI, LocalFscSochasticNode &n, BeliefSparse &b, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, vector<AlphaVector> &alpha_vecs, int n_index,
//                                                           std::vector<LocalFscSochasticNode> &UnProcessedSet, DecPomdpInterface *decpomdp)
// {

//     vector<int> fsc_obs = {aHI, oHI};
//     map<vector<int>, double> out_JAI_JOI_dist;
//     ComputePbDistJAIJOI(aHI, oHI, n, alpha_vecs, _m_set_JAI_JOI, out_JAI_JOI_dist);
//     // check if this <aHI, oHI> is possible
//     if (out_JAI_JOI_dist.size() == 0)
//     {
//         // impossible <ahI, oHI> ! Linking back to itself !
//         this->eta[n_index][fsc_obs][n_index] = 1; // Point back to the current node
//     }
//     else
//     {
//         map<vector<int>, double>::iterator it_JAI_JOI;

//         vector<int> JAI_JOI_selected;
//         double JAI_JOI_pr_max = 0;
//         for (it_JAI_JOI = out_JAI_JOI_dist.begin(); it_JAI_JOI != out_JAI_JOI_dist.end(); it_JAI_JOI++)
//         {
//             vector<int> JAI_JOI = it_JAI_JOI->first;
//             double pb_JAI_JOI = it_JAI_JOI->second;
//             if (pb_JAI_JOI > 0 && pb_JAI_JOI > JAI_JOI_pr_max)
//             {
//                 JAI_JOI_pr_max = pb_JAI_JOI;
//                 JAI_JOI_selected = JAI_JOI;
//             }
//         }

//         BeliefSparse b_new;
//         b_new = Update(decpomdp, b, JAI_JOI_selected[0], JAI_JOI_selected[1]);
//         AlphaVector alpha_new = argmax_alpha_vector(alpha_vecs, &b_new);

//         map<int, double> pb_joint_acions;
//         map<int, double> pb_local_acions;

//         SoftmaxActions(decpomdp, b_new, alpha_vecs, pb_joint_acions, pb_local_acions, LocalAgentIndex, this->T, this->min_pb);

//         LocalFscSochasticNode n_new(alpha_new, b_new, pb_joint_acions, pb_local_acions);
//         string description = GiveDescription(pb_local_acions);
//         n_new.SetDescript(description);

//         // Need to check if an alpha-vector is already exist in FSC
//         int FLAG_Exist = CheckAlphaExist(alpha_new);
//         if (FLAG_Exist == -1)
//         {
//             this->Nodes.push_back(n_new);
//             int n_new_index = this->Nodes.size() - 1;
//             this->eta[n_index][fsc_obs][n_new_index] = 1;
//             UnProcessedSet.push_back(n_new);
//         }
//         else
//         {
//             // Belief Merge Or Not
//             this->eta[n_index][fsc_obs][FLAG_Exist] = 1;
//             // this->Nodes[FLAG_Exist].MergeBelief(b_new);
//         }
//     }
// }

void SampleStochasticLocalFsc::BuildAllObsFSC()
{
    int size_obs = this->decpomdp->GetSizeOfObs(this->LocalAgentIndex);
    int size_actions = this->decpomdp->GetSizeOfA(this->LocalAgentIndex);
    int index = 0;
    for (int ohi = 0; ohi < size_obs; ohi++)
    {
        for (int ahi = 0; ahi < size_actions; ahi++)
        {
            this->_m_obs_fsc[index] = {ohi, ahi};
            index++;
        }
    }
}

// void SampleStochasticLocalFsc::GetAllPossibleFscObs(int LocalAgentIndex, LocalFscSochasticNode &n,
//                                                     set<vector<int>> &set_ahI_oHI, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, DecPomdpInterface *decpomdp)

void SampleStochasticLocalFsc::GetAllPossibleFscObs(LocalFscSochasticNode &n,
                                                    set<vector<int>> &set_ahI_oHI, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, DecPomdpInterface *decpomdp)
{
    BeliefSparse b = n.GetJointBeliefSparse();
    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double> &pb_joint_actions = n.GetJointActionDistribution();

    // int SizeJointAction = this->decpomdp->GetSizeOfJointA(); // ?? not used ??
    // int SizeJointObs = this->decpomdp->GetSizeOfJointObs(); // ?? not used ??

    map<int, double>::iterator it_JAI;

    for (it_JAI = pb_joint_actions.begin(); it_JAI != pb_joint_actions.end(); it_JAI++)
    {
        int JaI = it_JAI->first;
        double pb_JaI = it_JAI->second;
        vector<int> actions_indices = decpomdp->JointToIndividualActionsIndices(JaI);
        int aHI = actions_indices[this->LocalAgentIndex];
        map<int, double>::iterator it_s;
        for (it_s = belief_sparse->begin(); it_s != belief_sparse->end(); it_s++)
        {
            double pb_sI = it_s->second;
            int sI = it_s->first;

            map<int, double> *prob_dist_trans = this->decpomdp->GetTransProbDist(sI, JaI);
            map<int, double>::iterator it_trans;

            for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
            {
                int s_newI = it_trans->first;
                double pb_s_newI_temp = (it_trans->second);
                if (pb_s_newI_temp > 0)
                {
                    // dist_s_new[s_newI] += pb_s_newI_temp * pb_JaI;
                    map<int, double> *temp_dist_obs = this->decpomdp->GetObsFuncProbDist(s_newI, JaI);
                    map<int, double>::iterator it_obs;
                    for (it_obs = temp_dist_obs->begin(); it_obs != temp_dist_obs->end(); it_obs++)
                    {
                        int JoI = it_obs->first;
                        vector<int> obs_indices = decpomdp->JointToIndividualObsIndices(JoI);
                        int oHI = obs_indices[this->LocalAgentIndex];
                        double pb_obs_temp = it_obs->second;
                        double pb = pb_obs_temp * pb_s_newI_temp * pb_sI * pb_JaI;

                        // if (pb > this->min_pb)
                        if (pb > 0)
                        {
                            vector<int> fsc_obs = {aHI, oHI};
                            vector<int> JAI_JOI = {JaI, JoI};
                            set_ahI_oHI.insert(fsc_obs);
                            _m_set_JAI_JOI[fsc_obs].insert(JAI_JOI);
                        }
                    }
                }
            }
        }
    }
}

// void SampleStochasticLocalFsc::ComputePbDistJAIJOI(int aHI, int oHI, LocalFscSochasticNode &n,
//                                                    vector<AlphaVector> &alpha_vecs, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, map<vector<int>, double> &out_res_dist)

void SampleStochasticLocalFsc::ComputePbDistJAIJOI(int aHI, int oHI, LocalFscSochasticNode &n, map<vector<int>, set<vector<int>>> &_m_set_JAI_JOI, map<vector<int>, double> &out_res_dist)
{
    BeliefSparse b = n.GetJointBeliefSparse();
    map<int, double> &pb_joint_actions = n.GetJointActionDistribution();

    // int SizeJointAction = this->decpomdp->GetSizeOfJointA();
    // int SizeJointObs = this->decpomdp->GetSizeOfJointObs();

    double sum = 0;

    set<vector<int>> set_JAI_JOI = _m_set_JAI_JOI[{aHI, oHI}];
    set<vector<int>>::iterator it_JAI_JOI;

    for (it_JAI_JOI = set_JAI_JOI.begin(); it_JAI_JOI != set_JAI_JOI.end(); it_JAI_JOI++)
    {
        vector<int> JAI_JOI = *it_JAI_JOI;
        int JaI = JAI_JOI[0];
        int JoI = JAI_JOI[1];

        vector<int> joint_action_indices = this->decpomdp->JointToIndividualActionsIndices(JaI);
        vector<int> joint_obs_indices = this->decpomdp->JointToIndividualObsIndices(JoI);
        if (joint_action_indices[LocalAgentIndex] == aHI && joint_obs_indices[LocalAgentIndex] == oHI)
        {

            double pb_jaI = pb_joint_actions[JaI];
            map<int, double> *belief_sparse = b.GetBeliefSparse();
            map<int, double>::iterator it_s;
            double pb_res_temp = 0;
            for (it_s = belief_sparse->begin(); it_s != belief_sparse->end(); it_s++)
            {
                double pb_sI = it_s->second;
                int sI = it_s->first;

                map<int, double> *prob_dist_trans = this->decpomdp->GetTransProbDist(sI, JaI);
                map<int, double>::iterator it_trans;

                for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
                {
                    int s_newI = it_trans->first;
                    double pb_s_newI_temp = (it_trans->second);
                    if (pb_s_newI_temp > 0)
                    {
                        // dist_s_new[s_newI] += pb_s_newI_temp * pb_JaI;
                        map<int, double> *temp_dist_obs = this->decpomdp->GetObsFuncProbDist(s_newI, JaI);
                        double pb_JOI = temp_dist_obs->find(JoI)->second;
                        pb_res_temp += pb_jaI * pb_sI * pb_s_newI_temp * pb_JOI;
                    }
                }
            }

            if (pb_res_temp > 0)
            {
                out_res_dist[{JaI, JoI}] = pb_res_temp;
                sum += pb_res_temp;
            }
        }
    }

    if (sum != 0)
    {

        for (it_JAI_JOI = set_JAI_JOI.begin(); it_JAI_JOI != set_JAI_JOI.end(); it_JAI_JOI++)
        {
            vector<int> JAI_JOI = *it_JAI_JOI;
            int JaI = JAI_JOI[0];
            int JoI = JAI_JOI[1];
            out_res_dist[{JaI, JoI}] = out_res_dist[{JaI, JoI}] / sum;
        }
    }
}

void SampleStochasticLocalFsc::PrintGraph(DecPomdpInterface *decpomdp)
{
    cout << endl;
    cout << " -------- " << endl;
    cout << "digraph agent_" << LocalAgentIndex << " {" << endl;
    // define nodes in Graph
    for (unsigned int i = 0; i < this->Nodes.size(); i++)
    {
        if (i == 0 && this->formalization_type == 1)
        {
            cout << "n" << i << " [label = \" Init Node \"]" << endl;
        }
        else
        {
            cout << "n" << i << "[label = \" aH:  " << this->Nodes[i].GetDescript() << "\"]" << endl;
        }
    }
    cout << endl;

    for (unsigned int n_newI = 0; n_newI < this->Nodes.size(); n_newI++)
    {
        // node n_new = this->Nodes[i];
        for (int OHI = 0; OHI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); OHI++)
        {
            for (int aHI = 0; aHI < decpomdp->GetSizeOfA(this->LocalAgentIndex); aHI++)
            {
                vector<int> fsc_obs = {aHI, OHI};
                for (unsigned int nI = 0; nI < this->Nodes.size(); nI++)
                {
                    // node n = this->Nodes[j];
                    double pr_trans = this->eta[nI][fsc_obs][n_newI];
                    // Dont print self loops
                    // if ( pr_trans > 0 )
                    if (pr_trans > 0 && nI != n_newI)
                    {
                        // decpomdp->GetObservation(HumanIndex, OHI)->GetName(); // Dont work
                        // decpomdp->GetObservationName(OHI, HumanIndex)  ; // Dont work
                        cout << "n" << nI << " -> "
                             << "n" << n_newI << "[label = \"oh: " << decpomdp->GetObservationName(this->LocalAgentIndex, OHI) << " ah: " << decpomdp->GetActionName(this->LocalAgentIndex, aHI) << ", pb: " << pr_trans << " \"]" << endl;
                        // cout << "n"<< nI << " -> " << "n"<< n_newI <<"[label = \"oh: "<< decpomdp->GetAllObservationsVecs()[HumanIndex][OHI] <<", pb: "<<pr_trans <<" \"]"<< endl;
                    }
                }
            }
        }
    }

    cout << "}" << endl;
}
void SampleStochasticLocalFsc::ExportFSC(string filename)
{
    (void)filename;
    // not sure how to do it, need a structure to store the fsc
}

void SampleStochasticLocalFsc::SaveGraph(string filename)
{

    ofstream fp(filename.c_str());
    fp << "digraph agent_" << LocalAgentIndex << " {" << endl;
    // define nodes in Graph
    for (unsigned int i = 0; i < this->Nodes.size(); i++)
    {
        if (i == 0 && this->formalization_type == 1)
        {
            fp << "n" << i << " [label = \" Init Node \"]" << endl;
        }
        else
        {
            fp << "n" << i << "[label = \" aH:  " << this->Nodes[i].GetDescript() << "\"]" << endl;
        }
    }
    fp << endl;

    for (unsigned int n_newI = 0; n_newI < this->Nodes.size(); n_newI++)
    {
        // node n_new = this->Nodes[i];

        for (int aHI = 0; aHI < int(this->action_space.size()); aHI++)
        {
            for (int OHI = 0; OHI < int(this->observation_space.size()); OHI++)
            {
                vector<int> fsc_obs = {aHI, OHI};
                for (unsigned int nI = 0; nI < this->Nodes.size(); nI++)
                {
                    // node n = this->Nodes[j];
                    double pr_trans = this->eta[nI][fsc_obs][n_newI];
                    // Dont print self loops
                    // if ( pr_trans > 0 )
                    if (pr_trans > 0 && nI != n_newI)
                    {
                        fp << "n" << nI << " -> "
                           << "n" << n_newI << "[label = \"oh: " << this->observation_space[OHI] << " ah: " << this->action_space[aHI] << ", pb: " << pr_trans << " \"]" << endl;
                    }
                }
            }
        }
    }

    fp << "}" << endl;
}

double SampleStochasticLocalFsc::compute_pr_JOI(int LocalAgentIndex, int JOI, map<int, double> &dist_JOIs, int Local_obsI, DecPomdpInterface *decpomdp)
{

    double temp_sum = 0;
    vector<int> current_obs = decpomdp->JointToIndividualObsIndices(JOI);
    if (current_obs[LocalAgentIndex] != Local_obsI)
    {
        return 0;
    }

    double pr_JOI = dist_JOIs[JOI];
    map<int, double>::iterator it_JOI;
    for (it_JOI = dist_JOIs.begin(); it_JOI != dist_JOIs.end(); it_JOI++)
    {
        int temp_JOI = it_JOI->first;
        double pr_temp_JOI = it_JOI->second;
        vector<int> Obs_indicies = decpomdp->JointToIndividualObsIndices(temp_JOI);
        if (Obs_indicies[LocalAgentIndex] == Local_obsI)
        {
            temp_sum += pr_temp_JOI;
        }
        else
        {
            continue;
        }
    }

    return pr_JOI / temp_sum;
}

// double SampleStochasticLocalFsc::ComputeNorm1Distance(LocalFscSochasticNode &n1, LocalFscSochasticNode &n2)
// {
//     map<int, double> *b1 = n1.GetJointBeliefSparse().GetBeliefSparse();
//     map<int, double> *b2 = n2.GetJointBeliefSparse().GetBeliefSparse();

//     set<int> all_sI;

//     double res_distance = 0;

//     map<int, double>::iterator it;
//     for (it = b1->begin(); it != b1->end(); it++)
//     {
//         int sI = it->first;
//         all_sI.insert(sI);
//     }
//     for (it = b2->begin(); it != b2->end(); it++)
//     {
//         int sI = it->first;
//         all_sI.insert(sI);
//     }

//     set<int>::iterator it_sI;
//     for (it_sI = all_sI.begin(); it_sI != all_sI.end(); ++it_sI)
//     {
//         int sI = *it_sI;
//         double pb_sI_b1 = 0;
//         double pb_sI_b2 = 0;
//         if (b1->count(sI))
//         {
//             pb_sI_b1 = b1->find(sI)->second;
//         }
//         if (b2->count(sI))
//         {
//             pb_sI_b2 = b2->find(sI)->second;
//         }

//         double norm1_dis_sI = abs(pb_sI_b1 - pb_sI_b2);
//         res_distance += norm1_dis_sI;
//     }

//     return res_distance;
// }

// int SampleStochasticLocalFsc::GiveSimilarNodeIndex(LocalFscSochasticNode &n_temp, double &min_distance)
// {
//     int similiar_nI = -1;
//     double most_close_distance = __DBL_MAX__;
//     for (size_t nI = 0; nI < this->Nodes.size(); nI++)
//     {
//         double dis_temp = ComputeNorm1Distance(this->Nodes[nI], n_temp);
//         if (dis_temp < most_close_distance)
//         {
//             most_close_distance = dis_temp;
//             similiar_nI = nI;
//         }
//     }

//     min_distance = most_close_distance;

//     return similiar_nI;
// }

int SampleStochasticLocalFsc::GiveSimilarNodeIndex(BeliefSparse &b_temp, double &min_distance)
{
    int similiar_nI = -1;
    double most_close_distance = __DBL_MAX__;
    for (size_t nI = 0; nI < this->Nodes.size(); nI++)
    {
        BeliefSparse b_nI = this->Nodes[nI].GetJointBeliefSparse();
        double dis_temp = ComputeNorm1Distance(b_temp, b_nI);
        if (dis_temp < most_close_distance)
        {
            most_close_distance = dis_temp;
            similiar_nI = nI;
        }
    }

    min_distance = most_close_distance;

    return similiar_nI;
}

int SampleStochasticLocalFsc::GetNodeIndex(LocalFscSochasticNode &n)
{
    for (size_t nI = 0; nI < this->Nodes.size(); nI++)
    {
        if (this->Nodes[nI].GetJointBeliefSparse() == n.GetJointBeliefSparse())
        {
            return nI;
        }
    }
    return -1;
}

bool SampleStochasticLocalFsc::CheckFscValidation()
{
    int node_size = this->Nodes.size();

    for (int nI = 0; nI < node_size; nI++)
    {
        // check transitions
        for (int aHI = 0; aHI < decpomdp->GetSizeOfA(this->LocalAgentIndex); aHI++)
        {
            for (int oHI = 0; oHI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); oHI++)
            {
                vector<int> fsc_obs = {aHI, oHI};
                if (this->eta[nI][fsc_obs].empty())
                {
                    cout << "Found empty trans! nI:" << nI << " aHI and oHI: " << aHI << "," << oHI << endl;
                    throw "";
                }
            }
        }

        // check Belief
        map<int, double>::iterator it;
        double sum_sI = 0;
        for (it = this->Nodes[nI].GetJointBeliefSparse().GetBeliefSparse()->begin(); it != this->Nodes[nI].GetJointBeliefSparse().GetBeliefSparse()->end(); it++)
        {
            // int sI = it->first;
            double pb_sI = it->second;
            sum_sI += pb_sI;
        }

        if (sum_sI != 1.0)
        {
            cout << " Found a node with belief sum dont equal to 1! nI:  " << nI << endl;
            this->Nodes[nI].GetJointBeliefSparse().PrintBeliefSparse();
        }

        // check action distribution
        map<LocalActionIndex, double> dist_aHI = this->Nodes[nI].GetHumanActionDistribution();
        map<int, double>::iterator it_aHI;
        double sum_aHI = 0;
        for (it_aHI = dist_aHI.begin(); it_aHI != dist_aHI.end(); it++)
        {
            // int aHI = it_aHI->first;
            double pb_aHI = it_aHI->second;
            sum_aHI += pb_aHI;
        }

        if (sum_aHI != 1.0)
        {
            cout << " Found a node with aHI sum dont equal to 1! nI:  " << nI << endl;
            PrintMap(dist_aHI);
        }
    }

    return true;
}

void SampleStochasticLocalFsc::ExportAllNodesBelief(string filename)
{
    ofstream fp(filename.c_str());
    for (size_t i = 0; i < this->Nodes.size(); i++)
    {
        fp << "n" << i << endl;
        LocalFscSochasticNode n = this->Nodes[i];
        map<int, double> *b = n.GetJointBeliefSparse().GetBeliefSparse();
        map<int, double>::iterator it;
        for (it = b->begin(); it != b->end(); it++)
        {
            int sI = it->first;
            double pb_sI = it->second;
            fp << sI << ": " << pb_sI << ", ";
        }
        fp << endl;
    }
}

void SampleStochasticLocalFsc::ExportStochasticFSC(string filename)
{
    ofstream fp(filename.c_str());
    fp << "agent: "
       << this->LocalAgentIndex << endl;
    fp << "actions: ";
    for (size_t aHI = 0; aHI < this->action_space.size(); aHI++)
    {
        fp << action_space[aHI] << " ";
    }
    fp << endl;

    fp << "observations: ";
    for (size_t oHI = 0; oHI < this->observation_space.size(); oHI++)
    {
        fp << observation_space[oHI] << " ";
    }
    fp << endl;

    fp << "nodes:" << endl;
    // write action distributions for each node
    for (size_t nI = 0; nI < this->Nodes.size(); nI++)
    {
        LocalFscSochasticNode n = this->Nodes[nI];
        map<int, double> dist_aHI = n.GetHumanActionDistribution();
        for (size_t aHI = 0; aHI < action_space.size(); aHI++)
        {
            if (dist_aHI.count(aHI))
            {
                fp << dist_aHI[aHI] << " ";
            }
            else
            {
                fp << "0 ";
            }
        }
        fp << endl;
    }

    // T: obs_I : start-node_I : end-node_I %f
    for (size_t nI = 0; nI < this->Nodes.size(); nI++)
    {
        map<vector<int>, map<int, double>> dist_trans = this->eta[nI];
        map<vector<int>, map<int, double>>::iterator it;
        for (it = dist_trans.begin(); it != dist_trans.end(); it++)
        {
            vector<int> fsc_obs = it->first; // aHI, oHI
            // sparse representation for observation (next o')
            map<int, double> dist_n_new = it->second;
            map<int, double>::iterator it_n_new;

            for (it_n_new = dist_n_new.begin(); it_n_new != dist_n_new.end(); it_n_new++)
            {
                int n_newI = it_n_new->first;
                double pr_trans = it_n_new->second;
                if (pr_trans > 0)
                {
                    fp << "T: " << fsc_obs[0] << " " << fsc_obs[1] << " : " << nI << " : " << n_newI << " " << pr_trans << endl;
                }
            }
        }
    }
}

SampleStochasticLocalFsc::SampleStochasticLocalFsc(const string filename)
{
    this->action_space.resize(0);
    this->observation_space.resize(0);

    ifstream infile;
    infile.open(filename);
    if (!infile.is_open())
    {
        cout << "open file failure" << endl;
    }
    string temp;

    bool ReadNodes = false;
    bool first_part_finished = false;
    // 1: get agent index
    // 2: read action space
    // 3: read observation space
    // 4: read all nodes' action distributions
    // 5: read transitions.
    while (getline(infile, temp) && !first_part_finished)
    {
        istringstream is(temp);
        string s;
        int temp_num = 0;
        bool ReadAgent = false;
        bool ReadActionSpace = false;
        bool ReadObservationSpace = false;

        // an empty dist actions
        map<int, double> temp_dist_actions;

        while (is >> s)
        {
            if (s == "agent:")
            {
                ReadAgent = true;
            }
            else if (s == "actions:")
            {
                ReadActionSpace = true;
            }
            else if (s == "observations:")
            {
                ReadObservationSpace = true;
            }
            else if (s == "nodes:")
            {
                ReadNodes = true;
                temp_num = -1;
            }
            else if (s == "T:")
            {
                first_part_finished = true;
                ReadNodes = false;
            }

            // read contents
            if (ReadAgent && temp_num == 1)
            {
                this->LocalAgentIndex = stoi(s);
            }
            if (ReadActionSpace && temp_num > 0)
            {
                this->action_space.push_back(s);
            }
            if (ReadObservationSpace && temp_num > 0)
            {
                this->observation_space.push_back(s);
            }

            if (ReadNodes && temp_num >= 0)
            {
                double pb = stod(s);
                if (pb != 0)
                {
                    temp_dist_actions[temp_num] = pb;
                }
            }
            temp_num += 1;
        }

        // create a node if finished read a nodes
        if (ReadNodes && temp_num > 0)
        {
            LocalFscSochasticNode n(temp_dist_actions);
            this->Nodes.push_back(n);
        }
    }
    infile.close();

    this->eta.resize(this->Nodes.size());

    infile.open(filename);
    // Get T
    while (getline(infile, temp))
    {
        istringstream is(temp);
        string s;
        int temp_num = 0;
        bool buildTrans = false;
        int oI = 0;
        int aI = 0;
        int nI = 0;
        int n_newI = 0;
        double pb = 0;
        while (is >> s)
        {
            if (s == "T:")
            {
                buildTrans = true;
            }

            if (temp_num == 1 && buildTrans)
            {
                aI = stoi(s);
            }
            else if (temp_num == 2 && buildTrans)
            {
                oI = stoi(s);
            }
            else if (temp_num == 4 && buildTrans)
            {
                nI = stoi(s);
            }
            else if (temp_num == 6 && buildTrans)
            {
                n_newI = stoi(s);
            }
            else if (temp_num == 7 && buildTrans)
            {
                // cout << "nI: " << nI << endl;
                // cout << "aI: " << aI << endl;
                // cout << "oI: " << oI << endl;
                // cout << "n_newI: " << n_newI << endl;
                // cout << "pb: " << s << endl;
                // cout << "node size: " << this->Nodes.size() << endl;

                pb = stod(s);
                vector<int> fsc_obs = {aI, oI};
                this->eta[nI][fsc_obs].insert(std::make_pair(n_newI, pb));
                // this->eta[nI][fsc_obs][n_newI] = pb;
            }

            temp_num += 1;
        }
    }

    infile.close();
}

void SampleStochasticLocalFsc::PrintNextNodeActionDistribution(int current_aHI, int oHI, int &current_n)
{

    // cout << "a: " << this->action_space[current_aHI] << endl;
    // cout << "o: " << this->observation_space[oHI] << endl;
    // cout << "nI: " << current_n << endl;

    int n_nextI = 0;
    // if (current_n != 0)
    // {
    vector<int> fsc_obs = {current_aHI, oHI};
    map<int, double> *dist_next_node = this->GetNextNodeProb(current_n, fsc_obs);
    n_nextI = dist_next_node->begin()->first;
    current_n = n_nextI;

    double pb_n_nextI = dist_next_node->begin()->second;
    if (pb_n_nextI == 0)
    {
        cout << "error! pb_next node should not be 0!" << endl;
        throw "";
    }
    // }

    map<int, double> dist_actions = this->Nodes[n_nextI].GetHumanActionDistribution();

    map<int, double>::iterator it;
    for (it = dist_actions.begin(); it != dist_actions.end(); it++)
    {
        int aHI = it->first;
        double pb_aHI = it->second;
        cout << this->action_space[aHI] << ": " << pb_aHI << ", ";
    }
    cout << endl;
}

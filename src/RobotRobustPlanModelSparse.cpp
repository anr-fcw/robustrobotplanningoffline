 

#include "../Include/RobotRobustPlanModelSparse.h"

void RobotRobustPlanModelSparse::GenerateAllReachableExtendedStates()
{
    vector<int> UnProcessedSet;
    map<int, double> *sI_b0_sparse = this->DecPomdpModel->GetInitialBeliefSparse()->GetBeliefSparse();
    map<int, double>::iterator it_sI;
    map<int, double>::iterator it_human_fscI;

    int nI_init = 0;

    for (it_sI = sI_b0_sparse->begin(); it_sI != sI_b0_sparse->end(); it_sI++)
    {
        int sI = it_sI->first;
        double pr_sI = it_sI->second;
        int JOI_first_possible = this->DecPomdpModel->GetObsFuncProbDist(sI, 0)->begin()->first;
        int oI_init = this->DecPomdpModel->JointToIndividualObsIndices(JOI_first_possible)[this->robot_index];

        for (it_human_fscI = this->initial_prob_dist_human_fscs.begin(); it_human_fscI != this->initial_prob_dist_human_fscs.end(); it_human_fscI++)
        {
            int fscI = it_human_fscI->first;
            double pr_fscI = it_human_fscI->second;
            double pr_eI = pr_sI * pr_fscI;
            vector<int> eI_init = {sI, oI_init, fscI, nI_init};
            this->_m_ExtendedStateIndicies.push_back(eI_init);
            int e_newI = _m_ExtendedStateIndicies.size() - 1;
            _m_IndiciesToExtendedStateIndex[eI_init] = e_newI;
            UnProcessedSet.push_back(e_newI);
            b0_sparse[e_newI] = pr_eI;
            string str_BRState = "s" + to_string(eI_init[0]) + "o" + to_string(eI_init[1]) + "fsc" + to_string(eI_init[2]) + "n" + to_string(eI_init[3]);
            this->States.push_back(str_BRState);
        }
    }

    while (!UnProcessedSet.empty())
    {
        int eI_process = UnProcessedSet[0];
        UnProcessedSet.erase(UnProcessedSet.begin());
        ProcessForOneExtendedState(eI_process, UnProcessedSet);
    }
}

void RobotRobustPlanModelSparse::GenerateAllReachableExtendedStatesForStochasticFSC()
{
    vector<int> UnProcessedSet;
    map<int, double> *sI_b0_sparse = this->DecPomdpModel->GetInitialBeliefSparse()->GetBeliefSparse();
    map<int, double>::iterator it_sI;
    map<int, double>::iterator it_human_fscI;

    int nI_init = 0;

    for (it_sI = sI_b0_sparse->begin(); it_sI != sI_b0_sparse->end(); it_sI++)
    {
        int sI = it_sI->first;
        double pr_sI = it_sI->second;
        int JOI_first_possible = this->DecPomdpModel->GetObsFuncProbDist(sI, 0)->begin()->first;
        int oI_init = this->DecPomdpModel->JointToIndividualObsIndices(JOI_first_possible)[this->robot_index];

        for (it_human_fscI = this->initial_prob_dist_human_fscs.begin(); it_human_fscI != this->initial_prob_dist_human_fscs.end(); it_human_fscI++)
        {
            int fscI = it_human_fscI->first;
            double pr_fscI = it_human_fscI->second;
            double pr_eI = pr_sI * pr_fscI;
            vector<int> eI_init = {sI, oI_init, fscI, nI_init};
            this->_m_ExtendedStateIndicies.push_back(eI_init);
            int e_newI = _m_ExtendedStateIndicies.size() - 1;
            _m_IndiciesToExtendedStateIndex[eI_init] = e_newI;
            UnProcessedSet.push_back(e_newI);
            b0_sparse[e_newI] = pr_eI;
            string str_BRState = "s" + to_string(eI_init[0]) + "o" + to_string(eI_init[1]) + "fsc" + to_string(eI_init[2]) + "n" + to_string(eI_init[3]);
            this->States.push_back(str_BRState);
        }
    }

    while (!UnProcessedSet.empty())
    {
        int eI_process = UnProcessedSet[0];
        UnProcessedSet.erase(UnProcessedSet.begin());
        ProcessForOneExtendedStateStochasticFSC(eI_process, UnProcessedSet);
    }
}

void RobotRobustPlanModelSparse::ProcessForOneExtendedState(int eI, vector<int> &UnProcessedSet)
{
    vector<int> extend_state_vec = DecomposeBestResponseState(eI);

    int sI = extend_state_vec[0];
    // int oI = extend_state_vec[1];
    int fscI = extend_state_vec[2];
    int nI = extend_state_vec[3];

    int aI_human = this->NItoActionsIndex(fscI, nI); // this is human action index is at time t for human fscI, deterministic

    // for a in actions
    for (unsigned int aI = 0; aI < this->Actions.size(); aI++)
    {
        // cout << "------ aI:"<<aI<<endl;
        vector<int> act_indicies(2);
        act_indicies[this->human_index] = aI_human;
        act_indicies[this->robot_index] = aI;
        int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);
        // sparse representation for transition (next s')
        map<int, double> *prob_dist_trans = this->DecPomdpModel->GetTransProbDist(sI, JAI);
        map<int, double>::iterator it_trans;

        for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
        {
            int s_newI = it_trans->first;
            double pr_s_newI = it_trans->second;
            // cout << "---- s_newI:"<< s_newI << endl;

            // sparse representation for observation (next o')
            map<int, double> *prob_dist_obs = this->DecPomdpModel->GetObsFuncProbDist(s_newI, JAI);
            map<int, double>::iterator it_obs_func;

            for (it_obs_func = prob_dist_obs->begin(); it_obs_func != prob_dist_obs->end(); it_obs_func++)
            {
                int joint_obs_newI = it_obs_func->first;
                // cout << "-- joint_obs_newI:"<<joint_obs_newI<<endl;

                double pr_joint_obs_newI = it_obs_func->second;
                vector<int> obs_indicies = this->DecPomdpModel->JointToIndividualObsIndices(joint_obs_newI);
                int oI_new = obs_indicies[this->robot_index];
                int oI_new_human = obs_indicies[this->human_index];
                // cout << "-- oI_new:"<<oI_new<<endl;

                map<int, double> *prob_dist_n_newI = this->Human_possible_FSCs[fscI].GetNodeTransProbDist(nI, oI_new_human);
                // map<int, double> *result_NI_new_prob_dist = this->GetAllNodesTransProbDist(NI, obs_indicies);
                map<int, double>::iterator it_trans_nodes;
                for (it_trans_nodes = prob_dist_n_newI->begin(); it_trans_nodes != prob_dist_n_newI->end(); it_trans_nodes++)
                {
                    int nI_new = it_trans_nodes->first;
                    double pr_nodes_transition = it_trans_nodes->second;
                    // vector<int> new_extended_state = {s_newI, nI_new, oI_new};
                    vector<int> new_extended_state = {s_newI, oI_new, fscI, nI_new};

                    double pb = pr_nodes_transition * pr_joint_obs_newI * pr_s_newI;
                    // check if this new extended state already exist
                    // cout << "pb:"<<pb<<endl;

                    // if key absent
                    if (this->_m_IndiciesToExtendedStateIndex.find(new_extended_state) == this->_m_IndiciesToExtendedStateIndex.end())
                    {
                        this->_m_ExtendedStateIndicies.push_back(new_extended_state);
                        int e_newI = _m_ExtendedStateIndicies.size() - 1;
                        _m_IndiciesToExtendedStateIndex[new_extended_state] = e_newI;
                        UnProcessedSet.push_back(e_newI);

                        string str_BRState = "s" + to_string(new_extended_state[0]) + "o" + to_string(new_extended_state[1]) + "fsc" + to_string(new_extended_state[2]) + "n" + to_string(new_extended_state[3]);
                        this->States.push_back(str_BRState);
                        this->TransFuncVecs[aI][eI].insert(std::make_pair(e_newI, pb));

                        // cout << "NEW Extended state" << endl;
                    }
                    else
                    {
                        int e_newI = _m_IndiciesToExtendedStateIndex[new_extended_state];
                        this->TransFuncVecs[aI][eI][e_newI] += pb;
                        // cout << "Old Extended state" << endl;
                    }
                }
            }
        }
    }
}

void RobotRobustPlanModelSparse::ProcessForOneExtendedStateStochasticFSC(int eI, vector<int> &UnProcessedSet)
{
    vector<int> extend_state_vec = DecomposeBestResponseState(eI);

    int sI = extend_state_vec[0];
    // int oI = extend_state_vec[1];
    int fscI = extend_state_vec[2];
    int nI = extend_state_vec[3];

    map<int, double> human_action_pb_dist = this->Human_FSCs[fscI].GetHumanActionDist(nI);
    map<int, double>::iterator it_ahI;

    // for a in actions
    for (unsigned int aI = 0; aI < this->Actions.size(); aI++)
    {
        for (it_ahI = human_action_pb_dist.begin(); it_ahI != human_action_pb_dist.end(); it_ahI++)
        {
            // cout << "------ aI:"<<aI<<endl;
            int aI_human = it_ahI->first;
            double pb_aI = it_ahI->second;
            vector<int> act_indicies(2);
            act_indicies[this->human_index] = aI_human;
            act_indicies[this->robot_index] = aI;
            int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);
            // sparse representation for transition (next s')
            map<int, double> *prob_dist_trans = this->DecPomdpModel->GetTransProbDist(sI, JAI);
            map<int, double>::iterator it_trans;
            vector<int> fsc_obs(2);
            fsc_obs[0] = aI_human;

            for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
            {
                int s_newI = it_trans->first;
                double pr_s_newI = it_trans->second;
                // cout << "---- s_newI:"<< s_newI << endl;

                // sparse representation for observation (next o')
                map<int, double> *prob_dist_obs = this->DecPomdpModel->GetObsFuncProbDist(s_newI, JAI);
                map<int, double>::iterator it_obs_func;

                for (it_obs_func = prob_dist_obs->begin(); it_obs_func != prob_dist_obs->end(); it_obs_func++)
                {

                    int joint_obs_newI = it_obs_func->first;
                    // cout << "-- joint_obs_newI:"<<joint_obs_newI<<endl;

                    double pr_joint_obs_newI = it_obs_func->second;
                    vector<int> obs_indicies = this->DecPomdpModel->JointToIndividualObsIndices(joint_obs_newI);
                    int oI_new = obs_indicies[this->robot_index];
                    int oI_new_human = obs_indicies[this->human_index];
                    // cout << "-- oI_new:"<<oI_new<<endl;
                    fsc_obs[1] = oI_new_human;

                    map<int, double> *prob_dist_n_newI = this->Human_FSCs[fscI].GetNextNodeProb(nI, fsc_obs);
                    ;
                    // map<int, double> *result_NI_new_prob_dist = this->GetAllNodesTransProbDist(NI, obs_indicies);
                    map<int, double>::iterator it_trans_nodes;
                    for (it_trans_nodes = prob_dist_n_newI->begin(); it_trans_nodes != prob_dist_n_newI->end(); it_trans_nodes++)
                    {

                        int nI_new = it_trans_nodes->first;
                        double pr_nodes_transition = it_trans_nodes->second;
                        // vector<int> new_extended_state = {s_newI, nI_new, oI_new};
                        vector<int> new_extended_state = {s_newI, oI_new, fscI, nI_new};

                        double pb = pr_nodes_transition * pr_joint_obs_newI * pr_s_newI * pb_aI;
                        // check if this new extended state already exist
                        // cout << "pb:"<<pb<<endl;

                        // if key absent
                        if (pb != 0)
                        {

                            if (this->_m_IndiciesToExtendedStateIndex.find(new_extended_state) == this->_m_IndiciesToExtendedStateIndex.end())
                            {
                                this->_m_ExtendedStateIndicies.push_back(new_extended_state);
                                int e_newI = _m_ExtendedStateIndicies.size() - 1;
                                _m_IndiciesToExtendedStateIndex[new_extended_state] = e_newI;
                                UnProcessedSet.push_back(e_newI);

                                string str_BRState = "s" + to_string(new_extended_state[0]) + "o" + to_string(new_extended_state[1]) + "fsc" + to_string(new_extended_state[2]) + "n" + to_string(new_extended_state[3]);
                                this->States.push_back(str_BRState);
                                this->TransFuncVecs[aI][eI].insert(std::make_pair(e_newI, pb));

                                // cout << "NEW Extended state" << endl;
                            }
                            else
                            {
                                int e_newI = _m_IndiciesToExtendedStateIndex[new_extended_state];
                                this->TransFuncVecs[aI][eI][e_newI] += pb;
                                // cout << "Old Extended state" << endl;
                            }
                        }
                    }
                }
            }
        }
    }
}

int RobotRobustPlanModelSparse::NItoActionsIndex(int fscI, int nI)
{
    return this->Human_possible_FSCs[fscI].GetActionIndexForNodeI(nI);
};

RobotRobustPlanModelSparse::RobotRobustPlanModelSparse(vector<DecPomdpInterface *> models,
                                                       vector<FSCBase> &human_possible_FSCs, int human_index, int robot_index)
{
    this->DecPomdpModel = models[0];
    this->decpomdp_models = models;
    this->Human_possible_FSCs = human_possible_FSCs;
    this->human_index = human_index;
    this->robot_index = robot_index;
    this->Actions = DecPomdpModel->GetActionVec(robot_index);
    this->Observations = DecPomdpModel->GetObservationVec(robot_index);
    this->AgentNb = DecPomdpModel->GetNbAgents();
    InitialHumanFSCProbDist();
    int NI_size = 0;
    for (size_t fscI = 0; fscI < this->Human_possible_FSCs.size(); fscI++)
    {
        int SizeOfNodes = this->Human_possible_FSCs[fscI].GetNodesSize();
        NI_size += SizeOfNodes;
    }

    this->SizeJAI = this->DecPomdpModel->GetSizeOfJointA();
    this->SizeDecPomdpStateSpace = this->DecPomdpModel->GetSizeOfS();
    this->SizeOfA = this->DecPomdpModel->GetSizeOfA(this->robot_index);
    this->SizeOfObs = this->DecPomdpModel->GetSizeOfObs(this->robot_index);

    this->max_extended_state_size = SizeDecPomdpStateSpace * NI_size * SizeOfObs;
    // Need to clean the redundant part!!! Because we are using the maximum possible eI size!!!
    this->TransFuncVecs.resize(SizeOfA, vector<map<int, double>>(max_extended_state_size));

    GenerateAllReachableExtendedStates();

    this->SizeOfS = this->_m_ExtendedStateIndicies.size();
    this->b0 = ConvertBeliefSparseToNoSparse(b0_sparse);
};

RobotRobustPlanModelSparse::RobotRobustPlanModelSparse(vector<DecPomdpInterface *> models,
                                                       vector<SampleStochasticLocalFsc> &Human_FSCs, int human_index, int robot_index)
{
    this->DecPomdpModel = models[0];
    this->decpomdp_models = models;
    // this->FSCs_Stochastic = FSCs;
    this->Human_FSCs = Human_FSCs;
    this->robot_index = robot_index;
    this->human_index = human_index;
    this->Actions = DecPomdpModel->GetActionVec(robot_index);
    this->Observations = DecPomdpModel->GetObservationVec(robot_index);
    this->AgentNb = DecPomdpModel->GetNbAgents();

    InitialHumanFSCProbDist();
    int NI_size = 0;
    for (size_t fscI = 0; fscI < this->Human_FSCs.size(); fscI++)
    {
        int SizeOfNodes = this->Human_FSCs[fscI].GetNodeSize();
        NI_size += SizeOfNodes;
    }

    this->SizeJAI = this->DecPomdpModel->GetSizeOfJointA();
    this->SizeDecPomdpStateSpace = this->DecPomdpModel->GetSizeOfS();
    this->SizeOfA = this->DecPomdpModel->GetSizeOfA(robot_index);
    this->SizeOfObs = this->DecPomdpModel->GetSizeOfObs(robot_index);
    this->max_extended_state_size = SizeDecPomdpStateSpace * NI_size * SizeOfObs;
    // Need to clean the redundant part!!! Because we are using the maximum possible eI size!!!
    this->TransFuncVecs.resize(SizeOfA, vector<map<int, double>>(max_extended_state_size));

    cout << "Size_NI:" << NI_size << endl;

    GenerateAllReachableExtendedStatesForStochasticFSC();

    this->SizeOfS = this->_m_ExtendedStateIndicies.size();
    this->b0 = ConvertBeliefSparseToNoSparse(b0_sparse);
}

double RobotRobustPlanModelSparse::TransFunc(int eI, int aI_opt_agentI, int e_newI)
{
    // if key absent
    if ((this->TransFuncVecs[aI_opt_agentI][eI]).find(e_newI) == this->TransFuncVecs[aI_opt_agentI][eI].end())
    {
        // returns proba 0
        return 0.;
    }
    // key present
    else
    {
        // returns associated value
        return this->TransFuncVecs[aI_opt_agentI][eI][e_newI];
    }
};

double RobotRobustPlanModelSparse::ObsFunc(int oI_opt_agentI, int e_newI, int aI_opt_agentI)
{
    (void)(aI_opt_agentI);
    vector<int> new_indicies = this->_m_ExtendedStateIndicies[e_newI];
    int o_newI = new_indicies[1];
    if (oI_opt_agentI == o_newI)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

double RobotRobustPlanModelSparse::Reward(int eI, int aI_opt_agentI)
{
    vector<int> indicies = this->_m_ExtendedStateIndicies[eI];
    int sI = indicies[0];
    int fscI = indicies[2];
    int nI = indicies[3];
    double res = 0;

    if (this->Human_possible_FSCs.size() != 0)
    {
        int aI_human = this->Human_possible_FSCs[fscI].GetActionIndexForNodeI(nI);
        vector<int> act_indicies(2);

        act_indicies[this->human_index] = aI_human;
        act_indicies[this->robot_index] = aI_opt_agentI;
        int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);

        // res = this->DecPomdpModel->Reward(sI, JAI);

        // return the human's objective reward (reward with fscI)
        res = this->decpomdp_models[fscI]->Reward(sI, JAI);
    }
    else
    {
        map<int, double> human_action_pb_dist = this->Human_FSCs[fscI].GetHumanActionDist(nI);
        map<int, double>::iterator it_ahI;
        for (it_ahI = human_action_pb_dist.begin(); it_ahI != human_action_pb_dist.end(); it_ahI++)
        {

            int aHI = it_ahI->first;
            double pb_a = it_ahI->second;
            vector<int> act_indicies(2);
            act_indicies[human_index] = aHI;
            act_indicies[robot_index] = aI_opt_agentI;
            int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);
            double temp_r = this->decpomdp_models[fscI]->Reward(sI, JAI);
            res += temp_r * pb_a;
        }
    }

    return res;
};

void RobotRobustPlanModelSparse::ExportPOMDP(string filename)
{

    ofstream fp(filename.c_str());
    int SizeA = this->GetSizeOfA();
    int SizeS = this->GetSizeOfS();
    int SizeO = this->GetSizeOfObs();

    // 1. discount
    fp << "discount: " << this->GetDiscount() << endl;
    // 2. values
    fp << "values: reward" << endl;

    fp << "states: ";
    cout << "state size:" << this->States.size() << endl;
    for (int eI = 0; eI < SizeS; eI++)
    {
        fp << this->States[eI] << " ";
    }
    fp << endl;

    // 4. actions
    fp << "actions: ";
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        fp << this->Actions[aRI] << " ";
    }
    fp << endl;
    // 5. observations
    fp << "observations: ";
    for (int oRI = 0; oRI < SizeO; oRI++)
    {
        fp << this->Observations[oRI] << " ";
    }
    fp << endl;
    // 6. start
    fp << "start: ";
    for (int eI = 0; eI < SizeS; eI++)
    {
        fp << this->b0[eI] << " ";
    }
    fp << endl;

    // All below only list none-zero prob items

    // 7. T
    // T: action_I : start-state_I : end-state_I %f
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        for (int eI = 0; eI < SizeS; eI++)
        {

            map<int, double> prob_dist_trans = this->TransFuncVecs[aRI][eI];
            map<int, double>::iterator it;
            for (it = prob_dist_trans.begin(); it != prob_dist_trans.end(); it++)
            {
                fp << "T: " << aRI << " : " << eI << " : " << it->first << " " << it->second << endl;
            }
        }
    }

    // 8. O
    // O: action_I : end-state_I : observation_I %f
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        for (int e_newI = 0; e_newI < SizeS; e_newI++)
        {

            vector<int> new_indicies = this->_m_ExtendedStateIndicies[e_newI];
            int oRI = new_indicies[1];

            fp << "O: " << aRI << " : " << e_newI << " : " << oRI << " " << 1 << endl;
        }
    }

    // 9. R
    // R: action_I : start-state_I : * : * %f
    for (int aI = 0; aI < SizeA; aI++)
    {
        for (int eI = 0; eI < SizeS; eI++)
        {
            float temp_r = this->Reward(eI, aI);
            bool flag_nan = isnan(temp_r);
            // cout << temp_r << endl; // have this, the output is oK, without this gonna no R, strange
            if (temp_r != 0 && !flag_nan)
            {
                // this->RewardFuncVecs[aI][eI] = temp_r;
                fp << "R: " << aI << " : " << eI << " : * : * " << temp_r << endl;
                // fp << "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
                // cerr <<"CERR: "<< "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
            }
            else
            {
            }
        }
    }
    fp.flush();
    fp.close();

    // this->AfterExport = true; // All the TranFunc, ObsFunc and Reward are stored in the self vectors
}

vector<double> RobotRobustPlanModelSparse::ConvertBeliefSparseToNoSparse(map<int, double> &b0_sparse)
{
    vector<double> b(SizeOfS, 0);
    map<int, double>::iterator it;
    for (it = b0_sparse.begin(); it != b0_sparse.end(); it++)
    {
        b[it->first] = it->second;
    }

    return b;
};

void RobotRobustPlanModelSparse::InitialHumanFSCProbDist()
{
    int size_human_fsc = this->Human_possible_FSCs.size();
    if (size_human_fsc == 0)
    {
        size_human_fsc = this->Human_FSCs.size();
    }

    double pb_fscI = double(1.0 / size_human_fsc);
    for (int i = 0; i < size_human_fsc; i++)
    {
        this->initial_prob_dist_human_fscs[i] = pb_fscI;
    }
}
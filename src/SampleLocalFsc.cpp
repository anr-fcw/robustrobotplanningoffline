 

#include "../Include/SampleLocalFsc.h"

using namespace std;

// compute a specified observation probability
double compute_pr_JOI(int LocalAgentIndex, int JOI, int Local_obsI, BeliefSparse b, int a, DecPomdpInterface *decpomdp)
{

    double temp_sum = 0;
    vector<int> current_obs = decpomdp->JointToIndividualObsIndices(JOI);
    if (current_obs[LocalAgentIndex] != Local_obsI)
    {
        return 0;
    }

    double pr_JOI = compute_p_oba(JOI, b, a, decpomdp);
    for (int JOI_temp = 0; JOI_temp < decpomdp->GetSizeOfJointObs(); JOI_temp++)
    {
        vector<int> Obs_indicies = decpomdp->JointToIndividualObsIndices(JOI_temp);
        if (Obs_indicies[LocalAgentIndex] == Local_obsI)
        {
            temp_sum += compute_p_oba(JOI_temp, b, a, decpomdp);
        }
        else
        {
            continue;
        }
    }

    return pr_JOI / temp_sum;
};

// An improved one with sparse representation
// double compute_pr_JOI(int LocalAgentIndex, int JOI, map<int, double> &dist_JOIs, int Local_obsI, BeliefSparse b, int a, DecPomdpInterface *decpomdp)
double compute_pr_JOI(int LocalAgentIndex, int JOI, map<int, double> &dist_JOIs, int Local_obsI, DecPomdpInterface *decpomdp)

{

    double temp_sum = 0;
    vector<int> current_obs = decpomdp->JointToIndividualObsIndices(JOI);
    if (current_obs[LocalAgentIndex] != Local_obsI)
    {
        return 0;
    }

    double pr_JOI = dist_JOIs[JOI];
    map<int, double>::iterator it_JOI;
    for (it_JOI = dist_JOIs.begin(); it_JOI != dist_JOIs.end(); it_JOI++)
    {
        int temp_JOI = it_JOI->first;
        double pr_temp_JOI = it_JOI->second;
        vector<int> Obs_indicies = decpomdp->JointToIndividualObsIndices(temp_JOI);
        if (Obs_indicies[LocalAgentIndex] == Local_obsI)
        {
            temp_sum += pr_temp_JOI;
        }
        else
        {
            continue;
        }
    }

    return pr_JOI / temp_sum;
};

// Process to get all possible local agent obsI from dist_JOI
void GetAllPossibleLocalObsI(int LocalAgentIndex, map<int, double> &dist_JOIs, set<int> &set_possible_local_obsI, DecPomdpInterface *decpomdp)
{
    map<int, double>::iterator it_JOI;
    for (it_JOI = dist_JOIs.begin(); it_JOI != dist_JOIs.end(); it_JOI++)
    {
        int JOI = it_JOI->first;
        vector<int> obs_indices = decpomdp->JointToIndividualObsIndices(JOI);
        int local_obsI = obs_indices[LocalAgentIndex];
        set_possible_local_obsI.insert(local_obsI);
    }
}

BeliefSparse *build_inital_belief_Sparse(DecPomdpInterface *decpomdp)
{
    return decpomdp->GetInitialBeliefSparse();
};

// Belief update
BeliefSparse BeliefUpdate(BeliefSparse b, int aI, int JOI, DecPomdpInterface *decpomdp)
{
    BeliefSparse b_new = Update(decpomdp, b, aI, JOI);
    return b_new;
};

BeliefSparse BeliefUpdate(BeliefSparse &b, int aI, int JOI, double pr_oba, DecPomdpInterface *decpomdp)
{
    BeliefSparse b_new = Update(decpomdp, b, aI, JOI, pr_oba);
    return b_new;
};

// vector<AlphaVector> HumanFSC::GetAlphaVectors(){
//     return this->AlphaVecs;
// };

int SampleLocalFsc::CheckAlphaExist(AlphaVector alpha)
{
    for (unsigned int i = 0; i < this->Nodes.size(); i++)
    {
        // check two alpha vector equal or not
        AlphaVector current_alpha = this->Nodes[i].GetAlphaVector();
        if (current_alpha.GetValues() == alpha.GetValues() && current_alpha.GetActionIndex() == alpha.GetActionIndex())
        {
            return i;
        }
    }

    return -1;
};

// Stochasitic
void SampleLocalFsc::ProcessNodeStochastic(vector<AlphaVector> &alpha_vecs, int n_index, std::vector<LocalFscNode> &UnProcessedSet, DecPomdpInterface *decpomdp)
{

    LocalFscNode n = this->Nodes[n_index];
    size_t aI = n.GetJointAction();
    BeliefSparse b = n.GetJointBeliefSparse();
    // b.PrintBeliefSparse();
    map<int, double> dist_JOIs = GetPossibleJOIfromBeliefSparseAndJAI(decpomdp, b, aI);
    set<int> set_possible_local_obs_indices;
    GetAllPossibleLocalObsI(LocalAgentIndex, dist_JOIs, set_possible_local_obs_indices, decpomdp);
    // double w = n.GetWeight(); // Get the current weight

    for (int OHI = 0; OHI < decpomdp->GetSizeOfObs(LocalAgentIndex); OHI++)
    {
        // check if OHI is possible in the current belief
        if (set_possible_local_obs_indices.find(OHI) != set_possible_local_obs_indices.end())
        {
            map<int, double>::iterator it_JOI_temp;
            for (it_JOI_temp = dist_JOIs.begin(); it_JOI_temp != dist_JOIs.end(); it_JOI_temp++)
            {
                int JOI = it_JOI_temp->first;
                // double pr_or = compute_pr_JOI(LocalAgentIndex, JOI, dist_JOIs, OHI, b, aI, decpomdp);
                double pr_or = compute_pr_JOI(LocalAgentIndex, JOI, dist_JOIs, OHI, decpomdp);
                BeliefSparse b_new;
                if (pr_or > 0)
                {
                    b_new = BeliefUpdate(b, aI, JOI, decpomdp);
                }
                else
                {
                    continue;
                }
                // b_new.PrintBelief();
                AlphaVector alpha_new = argmax_alpha_vector(alpha_vecs, &b_new);
                size_t new_aI = alpha_new.GetActionIndex();
                size_t new_aHI = decpomdp->JointToIndividualActionsIndices(new_aI)[LocalAgentIndex];
                LocalFscNode n_new(alpha_new, b_new, new_aI, new_aHI);
                // add a descript of action name
                n_new.SetDescript(decpomdp->GetAllActionsVecs()[LocalAgentIndex][new_aHI]);
                // Need to check if an alpha-vector is already exist in FSC
                int FLAG_Exist = CheckAlphaExist(alpha_new);
                if (FLAG_Exist == -1)
                {
                    this->Nodes.push_back(n_new);
                    int n_new_index = this->Nodes.size() - 1;
                    this->eta[n_index][OHI][n_new_index] = pr_or;
                    UnProcessedSet.push_back(n_new);
                }
                else
                {
                    // Belief Merge Or Not
                    this->eta[n_index][OHI][FLAG_Exist] += pr_or;
                    // this->Nodes[FLAG_Exist].MergeBelief(b_new);
                }
            }
        }
        else
        {
            // it means we found a belief that this observation is impossible
            // ! Linking back to itself !
            this->eta[n_index][OHI][n_index] = 1; // Point back to the current node
            continue;
        }
    }
};

// Deterministic
void SampleLocalFsc::ProcessNodeDeterministic(vector<AlphaVector> &alpha_vecs, int n_index, std::vector<LocalFscNode> &UnProcessedSet, DecPomdpInterface *decpomdp)
{

    LocalFscNode n = this->Nodes[n_index];
    size_t aI = n.GetJointAction();
    BeliefSparse b = n.GetJointBeliefSparse();
    // b.PrintBeliefSparse();
    map<int, double> dist_JOIs = GetPossibleJOIfromBeliefSparseAndJAI(decpomdp, b, aI);
    set<int> set_possible_local_obs_indices;
    GetAllPossibleLocalObsI(LocalAgentIndex, dist_JOIs, set_possible_local_obs_indices, decpomdp);
    double w = n.GetWeight(); // Get the current weight
    for (int OHI = 0; OHI < decpomdp->GetSizeOfObs(LocalAgentIndex); OHI++)
    {
        BeliefSparse b_new;
        double w_new;
        // check if OHI is possible in the current belief
        if (set_possible_local_obs_indices.find(OHI) != set_possible_local_obs_indices.end())
        {
            int JOI_selected = 0;
            double JOI_pr_max = 0;
            map<int, double>::iterator it_JOI_temp;
            for (it_JOI_temp = dist_JOIs.begin(); it_JOI_temp != dist_JOIs.end(); it_JOI_temp++)
            {
                int JOI = it_JOI_temp->first;
                double pr_or = compute_pr_JOI(LocalAgentIndex, JOI, dist_JOIs, OHI, decpomdp);

                // double pr_or = compute_pr_JOI(LocalAgentIndex, JOI, dist_JOIs, OHI, b, aI, decpomdp);
                // cout <<"OHI: "<<OHI << ", JOI: "<<JOI <<", aI:" <<aI <<" ,Pr_or: " << pr_or << endl;
                if (pr_or > 0 && pr_or > JOI_pr_max)
                {
                    JOI_pr_max = pr_or;
                    JOI_selected = JOI;
                }
            }
            double pr_oba = compute_p_oba(JOI_selected, b, aI, decpomdp);
            // cout << "choose JOI:"<< JOI_selected << ", pr_oba:"<< pr_oba<< endl;
            w_new = w * pr_oba;
            b_new = BeliefUpdate(b, aI, JOI_selected, pr_oba, decpomdp);
            cout << "choose JOI:" << JOI_selected << ", pr_oba:" << pr_oba << endl;

            // Process Node
            // Belief b_new = BeliefUpdate(b,aI,JOI_selected, decpomdp);
            // Belief b_new = BeliefUpdate(b,aI,JOI_selected, decpomdp);
            // b_new.PrintBelief();
            AlphaVector alpha_new = argmax_alpha_vector(alpha_vecs, &b_new);
            size_t new_aI = alpha_new.GetActionIndex();
            size_t new_aHI = decpomdp->JointToIndividualActionsIndices(new_aI)[LocalAgentIndex];
            LocalFscNode n_new(alpha_new, b_new, new_aI, new_aHI);
            // add a descript of action name
            n_new.SetDescript(decpomdp->GetAllActionsVecs()[LocalAgentIndex][new_aHI]);
            n_new.SetWeight(w_new);

            // Need to check if an alpha-vector is already exist in FSC
            int FLAG_Exist = CheckAlphaExist(alpha_new);
            if (FLAG_Exist == -1)
            {
                this->Nodes.push_back(n_new);
                // cout << "New Processed Node Belief: ";
                // n_new.GetJointBelief().PrintBelief();
                // this->AlphaVecs.push_back(alpha_new);
                int n_new_index = this->Nodes.size() - 1;
                // this->eta[n_index][OHI][n_new_index] = 1;
                this->eta[n_index][OHI][n_new_index] = 1;
                UnProcessedSet.push_back(n_new);
            }
            else
            {
                // should use the node with same alpha-vector not n_new (!!! IMPORTANT !!!)
                // Check what will happen if the link is not exist !!!
                // Maybe n_alpha exist but not with the link from n and OHI

                // node n_alpha_exist = this->Nodes[FLAG_Exist];
                // this->eta[n_index][OHI][FLAG_Exist] = 1;
                this->eta[n_index][OHI][FLAG_Exist] = 1;
            }
        }
        else
        {
            // it means we found a belief that this observation is impossible
            // ! Linking back to itself !
            this->eta[n_index][OHI][n_index] = 1; // Point back to the current node
            continue;
        }
    }
};

SampleLocalFsc::SampleLocalFsc(vector<AlphaVector> alpha_vecs, DecPomdpInterface *decpomdp, int LocalAgentIndex, int init_type, int formalization_type)
{
    // Initialize eta
    this->LocalAgentIndex = LocalAgentIndex;
    this->decpomdp = decpomdp;
    vector<vector<vector<double>>> eta_init(alpha_vecs.size() + 1,
                                            vector<vector<double>>(decpomdp->GetSizeOfObs(LocalAgentIndex),
                                                                   vector<double>(alpha_vecs.size() + 1)));
    this->eta = eta_init;
    vector<LocalFscNode> UnProcessedSet; // Initlize a set to store the unprocessed nodes, Call it openlist
    BeliefSparse *b0 = build_inital_belief_Sparse(decpomdp);

    AlphaVector alpha0 = argmax_alpha_vector(alpha_vecs, b0);

    size_t aI = alpha0.GetActionIndex();
    size_t aHI = decpomdp->JointToIndividualActionsIndices(aI)[LocalAgentIndex];
    LocalFscNode n0(alpha0, *b0, aI, aHI);
    // add a descript of action name
    n0.SetDescript(decpomdp->GetAllActionsVecs()[LocalAgentIndex][aHI]);

    this->formalization_type = formalization_type;
    int n_index = 0; // start with n0
    int type = formalization_type;
    // commented for test momdp formalization
    if (type == 0)
    {
        this->Nodes.push_back(n0);
    }
    else if (type == 1)
    {
        InitNodeProcess(n0, decpomdp);
        n_index = 1; // start with n0
    }
    else
    {
        cerr << "Wrong argument for FSC type!" << endl;
        throw("");
    }
    UnProcessedSet.push_back(n0);

    while (!UnProcessedSet.empty())
    {
        // std::vector<node>::iterator it = UnProcessedSet.begin();
        // node n = *it;
        // Attention, need to check if node n is still exist
        UnProcessedSet.erase(UnProcessedSet.begin());
        // cout << "UnProcessedSet Size: " << UnProcessedSet.size() << endl;

        // Need modify ProcessNode deterministic
        if (init_type == 1)
        {
            ProcessNodeStochastic(alpha_vecs, n_index, UnProcessedSet, decpomdp);
        }
        else if (init_type == 2)
        {
            ProcessNodeDeterministic(alpha_vecs, n_index, UnProcessedSet, decpomdp);
        }
        else
        {
            cout << "Wrong init type argument!" << endl;
            throw("");
        }

        n_index++;
    }
}

void SampleLocalFsc::PrintGraph(DecPomdpInterface *decpomdp)
{
    cout << endl;
    cout << " -------- " << endl;
    cout << "digraph agent_" << LocalAgentIndex << " {" << endl;
    // define nodes in Graph
    for (unsigned int i = 0; i < this->Nodes.size(); i++)
    {
        if (i == 0 && this->formalization_type == 1)
        {
            cout << "n" << i << " [label = \" Init Node \"]" << endl;
        }
        else
        {
            // cout << "n" <<i << "[label = \" aH:  " << this->Nodes[i].GetHumanAction() << ", a_joint:  " << this->Nodes[i].GetJointAction() << "\"]" <<endl;
            cout << "n" << i << "[label = \" aH:  " << this->Nodes[i].GetDescript() << "\"]" << endl;
        }
    }
    cout << endl;

    for (unsigned int n_newI = 0; n_newI < this->Nodes.size(); n_newI++)
    {
        // node n_new = this->Nodes[i];
        for (int OHI = 0; OHI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); OHI++)
        {
            for (unsigned int nI = 0; nI < this->Nodes.size(); nI++)
            {
                // node n = this->Nodes[j];
                double pr_trans = this->eta[nI][OHI][n_newI];
                // Dont print self loops
                // if ( pr_trans > 0 )
                if (pr_trans > 0 && nI != n_newI)
                {
                    // decpomdp->GetObservation(HumanIndex, OHI)->GetName(); // Dont work
                    // decpomdp->GetObservationName(OHI, HumanIndex)  ; // Dont work
                    cout << "n" << nI << " -> "
                         << "n" << n_newI << "[label = \"oh: " << decpomdp->GetObservationName(this->LocalAgentIndex, OHI) << ", pb: " << pr_trans << " \"]" << endl;
                    // cout << "n"<< nI << " -> " << "n"<< n_newI <<"[label = \"oh: "<< decpomdp->GetAllObservationsVecs()[HumanIndex][OHI] <<", pb: "<<pr_trans <<" \"]"<< endl;
                }
            }
        }
    }

    cout << "}" << endl;
}

vector<LocalFscNode> SampleLocalFsc::GetNodes()
{
    return this->Nodes;
};

// be attention! n0 is the start node at b0, not init node which is node at t=-1
void SampleLocalFsc::InitNodeProcess(LocalFscNode n0, DecPomdpInterface *decpomdp)
{
    LocalFscNode InitNode;
    if (this->Nodes.size() != 0)
    {
        cout << "Error! Init Node must be added at beginning with Nodes size = 0!" << endl;
        throw("Error!");
    }

    this->Nodes.push_back(InitNode); // Index 0
    this->Nodes.push_back(n0);       // Index 1
    for (int oI = 0; oI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); oI++)
    {
        this->eta[0][oI][1] = 1;
    }
}

void SampleLocalFsc::ExportFSC(string filename)
{
    // string filename = "MpomdpLocalPolicyAgent" + to_string(this->LocalAgentIndex) + ".fsc";
    ofstream fp(filename.c_str());
    fp << "agent: " << this->LocalAgentIndex << endl;
    fp << "nodes: ";
    int nI_start = 0;
    if (this->formalization_type == 1)
    {
        nI_start = 1;
        fp << "99999 ";
    }
    for (unsigned int nI = nI_start; nI < this->Nodes.size(); nI++)
    {
        fp << this->Nodes[nI].GetAction() << " ";
    }
    fp << endl;

    int Obs_size = -1;
    if (this->decpomdp == nullptr)
    {
        Obs_size = this->observation_space.size();
    }
    else
    {
        Obs_size = this->decpomdp->GetSizeOfObs(this->LocalAgentIndex);
    }

    // T: obs_I : start-node_I : end-node_I %f
    for (int oI = 0; oI < Obs_size; oI++)
    {
        for (unsigned int nI = 0; nI < this->Nodes.size(); nI++)
        {
            for (unsigned int n_newI = 0; n_newI < this->Nodes.size(); n_newI++)
            {
                // check prob is not 0
                double temp_pr = this->eta[nI][oI][n_newI];
                if (temp_pr > 0)
                {
                    fp << "T: " << oI << " : " << nI << " : " << n_newI << " " << temp_pr << endl;
                }
            }
        }
    }
};

void SampleLocalFsc::SaveGraph(string filename)
{
    ofstream fp(filename.c_str());
    fp << "digraph agent_" << LocalAgentIndex << " {" << endl;
    // define nodes in Graph
    for (unsigned int i = 0; i < this->Nodes.size(); i++)
    {
        if (i == 0 && this->formalization_type == 1)
        {
            fp << "n" << i << " [label = \" Init Node \"]" << endl;
        }
        else
        {
            // cout << "n" <<i << "[label = \" aH:  " << this->Nodes[i].GetHumanAction() << ", a_joint:  " << this->Nodes[i].GetJointAction() << "\"]" <<endl;
            fp << "n" << i << "[label = \" a:  " << this->Nodes[i].GetDescript() << "\"]" << endl;
        }
    }
    fp << endl;

    for (unsigned int n_newI = 0; n_newI < this->Nodes.size(); n_newI++)
    {
        // node n_new = this->Nodes[i];
        for (int OHI = 0; OHI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); OHI++)
        {
            for (unsigned int nI = 0; nI < this->Nodes.size(); nI++)
            {
                // node n = this->Nodes[j];
                double pr_trans = this->eta[nI][OHI][n_newI];
                // Dont print self loops
                // if ( pr_trans > 0 )
                if (pr_trans > 0 && nI != n_newI)
                {
                    fp << "n" << nI << " -> "
                       << "n" << n_newI << "[label = \"o: " << decpomdp->GetObservationName(this->LocalAgentIndex, OHI) << ", pb: " << pr_trans << " \"]" << endl;
                }
            }
        }
    }

    fp << "}" << endl;
}

void SampleLocalFsc::SampleOneFsc(DecPomdpInterface *decpomdp, int LocalAgentIndex, double max_belief_gap,
                                  double T, double min_action_pb, MCTS &mcts, int MaxSize)
{
    this->mcts = &mcts;
    // Initialize eta
    this->LocalAgentIndex = LocalAgentIndex;
    this->decpomdp = decpomdp;
    this->max_size = MaxSize;
    vector<vector<vector<double>>> eta_init(max_size,
                                            vector<vector<double>>(decpomdp->GetSizeOfObs(LocalAgentIndex),
                                                                   vector<double>(max_size)));
    this->eta = eta_init;
    std::map<double, vector<LocalFscNode>> UnProcessedSet; // Initlize a set to store the unprocessed nodes, Call it openlist
    BeliefSparse *b0 = build_inital_belief_Sparse(decpomdp);

    map<int, map<int, double>> dist_joint_acions;
    map<int, double> pb_local_acions;
    // SoftmaxActions(decpomdp, *b0, alpha_vecs, dist_joint_acions, pb_local_acions, LocalAgentIndex, this->T);
    SoftmaxActions(decpomdp, *b0, dist_joint_acions, pb_local_acions, LocalAgentIndex, T, min_action_pb, *this->mcts);
    int sample_aHI = SampleAKeyFromMap(pb_local_acions);
    LocalFscNode n0(*b0, sample_aHI, dist_joint_acions[sample_aHI]);

    // add a descript of action name
    n0.SetDescript(decpomdp->GetAllActionsVecs()[LocalAgentIndex][sample_aHI]);

    this->formalization_type = 0;
    int n_index = 0; // start with n0
    // int type = formalization_type;
    // commented for test momdp formalization

    this->Nodes.push_back(n0);
    double weight = 1.0;
    n0.SetWeight(weight);
    UnProcessedSet[weight].push_back(n0);

    while (!UnProcessedSet.empty())
    {
        LocalFscNode n = UnProcessedSet.rbegin()->second.front();
        // int n_index = this->GetNodeIndex(n);
        n_index = this->GetNodeIndex(n);

        UnProcessedSet.rbegin()->second.erase(UnProcessedSet.rbegin()->second.begin());
        if (UnProcessedSet.rbegin()->second.size() == 0)
        {
            double key = UnProcessedSet.rbegin()->first;
            UnProcessedSet.erase(key);
        }

        ProcessNodeWithMaxSize(n, UnProcessedSet, this->decpomdp, max_belief_gap, T, min_action_pb);

        cout << "max node size: " << this->max_size << endl;
        cout << "fsc node size: " << this->Nodes.size() << endl;
        cout << "G remain size: " << UnProcessedSet.size() << endl;
        cout << "Processed node index: " << n_index << endl;
    }

    cout << "processed finished !" << endl;

    // this->ComputeFscID();
}

// void SampleLocalFsc::ProcessNodeWithMaxSize(vector<AlphaVector> &alpha_vecs, LocalFscNode &n, std::map<double, vector<LocalFscNode>> &UnProcessedSet,
//                                             DecPomdpInterface *decpomdp, double max_belief_gap, double T, double min_action_pb)

void SampleLocalFsc::ProcessNodeWithMaxSize(LocalFscNode &n, std::map<double, vector<LocalFscNode>> &UnProcessedSet,
                                            DecPomdpInterface *decpomdp, double max_belief_gap, double T, double min_action_pb)
{

    // LocalFscNode n = this->Nodes[n_index];
    int n_index = this->GetNodeIndex(n);

    double w = n.GetWeight(); // Get the current weight

    map<int, double> *pb_dist_JAI = n.GetPbDistJointAction();
    BeliefSparse b = n.GetJointBeliefSparse();
    // b.PrintBeliefSparse();
    map<int, double> all_possible_oHI = compute_dist_pr_oh(decpomdp, b, *pb_dist_JAI, this->LocalAgentIndex);

    for (int oHI = 0; oHI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); oHI++)
    {
        if (all_possible_oHI.count(oHI))
        {
            double pr_oh_ba = all_possible_oHI[oHI];
            BeliefSparse b_new = UpdateOneSideObservation(decpomdp, b, *pb_dist_JAI, oHI, pr_oh_ba, this->LocalAgentIndex);
            // Check belief exist or not
            int FLAG_Exist = CheckBeliefExist(b_new, max_belief_gap);
            double w_new = w * pr_oh_ba;

            // if belief not exist and node size is OK
            if (FLAG_Exist == -1 && int(this->Nodes.size()) < this->max_size)
            {
                // create a new node
                map<int, map<int, double>> dist_joint_acions;
                map<int, double> pb_local_acions;
                // SoftmaxActions(decpomdp, *b0, alpha_vecs, dist_joint_acions, pb_local_acions, LocalAgentIndex, this->T);
                double v = SoftmaxActions(decpomdp, b_new, dist_joint_acions, pb_local_acions, LocalAgentIndex, T, min_action_pb, *this->mcts);
                int sample_aHI = SampleAKeyFromMap(pb_local_acions);
                LocalFscNode n_new(b_new, sample_aHI, dist_joint_acions[sample_aHI]);
                // add n new to the N and G
                this->Nodes.push_back(n_new);
                int n_new_index = this->Nodes.size() - 1;
                // this->eta[n_index][OHI][n_new_index] = 1;
                this->eta[n_index][oHI][n_new_index] = 1;
                // UnProcessedSet.push_back(n_new);
                UnProcessedSet[w_new * v].push_back(n_new);
            }
            else
            {
                // if max node size is reached
                if (int(this->Nodes.size()) >= this->max_size)
                {
                    double similiar_distance = 0.0;
                    int similiar_nI = this->GiveSimilarNodeIndex(b_new, similiar_distance);
                    this->eta[n_index][oHI][similiar_nI] = 1;
                    double w_updated = this->Nodes[similiar_nI].GetWeight();
                    w_updated += w_new;
                    this->Nodes[similiar_nI].SetWeight(w_updated);
                }
                // if belief already exist
                else
                {
                    double w_updated = this->Nodes[FLAG_Exist].GetWeight();
                    w_updated += w_new;
                    this->Nodes[FLAG_Exist].SetWeight(w_updated);
                    // if this new belief already exist, link to the node with existed alpha vector
                    this->eta[n_index][oHI][FLAG_Exist] = 1;
                }
            }
        }
        else
        {
            this->eta[n_index][oHI][n_index] = 1; // Point back to the current node
        }
    }
}

int SampleLocalFsc::CheckBeliefExist(BeliefSparse &b, double max_accept_gap)
{
    double min_distance = 0;
    int similiar_node_index = this->GiveSimilarNodeIndex(b, min_distance);
    if (min_distance < max_accept_gap)
    {
        return similiar_node_index;
    }
    else
    {
        return -1;
    }
}
int SampleLocalFsc::GiveSimilarNodeIndex(BeliefSparse &b, double &min_distance)
{
    int similiar_nI = -1;
    double most_close_distance = __DBL_MAX__;
    for (size_t nI = 0; nI < this->Nodes.size(); nI++)
    {
        BeliefSparse b_nI = this->Nodes[nI].GetJointBeliefSparse();
        double dis_temp = ComputeNorm1Distance(b, b_nI);
        if (dis_temp < most_close_distance)
        {
            most_close_distance = dis_temp;
            similiar_nI = nI;
        }
    }

    min_distance = most_close_distance;

    return similiar_nI;
}

void SampleLocalFsc::ComputeFscID()
{
    string res = "";
    for (size_t nI = 0; nI < this->Nodes.size(); nI++)
    {
        int aHI = this->Nodes[nI].GetHumanAction();
        res += to_string(nI) + to_string(aHI);
    }

    this->id = res;
}

int SampleLocalFsc::GetNodeIndex(LocalFscNode &n)
{
    for (size_t nI = 0; nI < this->Nodes.size(); nI++)
    {
        if (this->Nodes[nI].GetJointBeliefSparse().computeID() == n.GetJointBeliefSparse().computeID())
        {
            return nI;
        }
    }
    return -1;
}

void SampleLocalFsc::SampleOneFsc(SampleStochasticLocalFsc &stochastic_fsc)
{
    this->action_space = stochastic_fsc.GetActionSpace();
    this->observation_space = stochastic_fsc.GetObsSpace();
    this->LocalAgentIndex = stochastic_fsc.GetLocalAgentIndex();
    this->formalization_type = 0;

    int ObsSize = observation_space.size();

    int MaxSize = stochastic_fsc.GetNodeSize();
    vector<vector<vector<double>>> eta_init(MaxSize,
                                            vector<vector<double>>(ObsSize,
                                                                   vector<double>(MaxSize)));
    this->eta = eta_init;

    // start from node index 0
    int sto_fsc_node_index = 0;
    map<int, double> dist_human_actions = stochastic_fsc.GetHumanActionDist(sto_fsc_node_index);
    int sampled_aHI = SampleAKeyFromMap(dist_human_actions);
    LocalFscNode n0(sampled_aHI, sto_fsc_node_index);
    this->Nodes.push_back(n0);
    vector<LocalFscNode> Unprocessed_list;
    Unprocessed_list.push_back(n0);
    int n_index_process = 0;

    while (!Unprocessed_list.empty())
    {
        Unprocessed_list.erase(Unprocessed_list.begin());
        ProcessNode(n_index_process, Unprocessed_list, stochastic_fsc);
        n_index_process += 1;
    }

    cout << "processed finished !" << endl;
}

int SampleLocalFsc::CheckNodeExistWithID(int id_new)
{
    for (unsigned int i = 0; i < this->Nodes.size(); i++)
    {
        // check two id equal or not
        if (this->Nodes[i].GetID() == id_new)
        {
            return i;
        }
    }

    return -1;
}

void SampleLocalFsc::ProcessNode(int n_index_process, vector<LocalFscNode> &Unprocessed_list, SampleStochasticLocalFsc &stochastic_fsc)
{
    LocalFscNode n = this->Nodes[n_index_process];
    int sto_fsc_n_index = n.GetID();
    int aHI = n.GetHumanAction();

    int obs_size = this->observation_space.size();

    for (int oHI = 0; oHI < obs_size; oHI++)
    {
        vector<int> sto_fsc_obs = {aHI, oHI};
        map<int, double> *dist_next_sto_fsc_node = stochastic_fsc.GetNextNodeProb(sto_fsc_n_index, sto_fsc_obs);
        map<int, double>::iterator it_sto_fsc_next_nI;
        for (it_sto_fsc_next_nI = dist_next_sto_fsc_node->begin(); it_sto_fsc_next_nI != dist_next_sto_fsc_node->end(); it_sto_fsc_next_nI++)
        {
            int sto_fsc_n_next_index = it_sto_fsc_next_nI->first;
            double pb_trans = it_sto_fsc_next_nI->second;

            // check if ID exist
            int FLAG_EXIST = this->CheckNodeExistWithID(sto_fsc_n_next_index);
            if (FLAG_EXIST == -1)
            {
                // creat a new node for this fsc
                map<int, double> dist_human_actions = stochastic_fsc.GetHumanActionDist(sto_fsc_n_next_index);
                int sampled_aHI = SampleAKeyFromMap(dist_human_actions);
                LocalFscNode new_n(sampled_aHI, sto_fsc_n_next_index);
                this->Nodes.push_back(new_n);
                int n_new_index = this->Nodes.size() - 1;
                this->eta[n_index_process][oHI][n_new_index] = pb_trans;
                Unprocessed_list.push_back(new_n);
            }
            else
            {
                this->eta[n_index_process][oHI][FLAG_EXIST] = pb_trans;
            }
        }
    }
}

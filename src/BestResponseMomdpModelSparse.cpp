 

#include "../Include/BestResponseMomdpModelSparse.h"

// Recursively build all combinations of all agents FSCs node possible indicies. NI <-> {n1,n2,n3...} , maybe I can build all NI possible!
void BestResponseMomdpModelSparse::RecursiveBuildAllFSCsNodesIndicies(int depth, int &NI, vector<int> NodesIndicies)
{
    if (depth != this->AgentNb)
    {
        // If the depth is the same with optimizing agent index, we just give a 0 node index (Because we won't use it)
        if (depth == this->CurrentOptimizingAgentI)
        {
            NodesIndicies.push_back(0);
            this->RecursiveBuildAllFSCsNodesIndicies(depth + 1, NI, NodesIndicies);
        }
        else
        {
            // Start with the start node
            // ni is the node index for the agent i (not the optimizing one), NI is the global index for node indicies vector
            for (int ni = 0; ni < this->FSCs[depth].GetNodesSize(); ni++)
            {
                NodesIndicies.push_back(ni);
                this->RecursiveBuildAllFSCsNodesIndicies(depth + 1, NI, NodesIndicies);
                NodesIndicies.pop_back();
            }
        }
    }
    else
    {
        // this->_m_IndiciesOfFSCsNodes[NI] = NodesIndicies;
        this->_m_FSCsNodes_Indicies[NodesIndicies] = NI;
        this->_m_IndiciesOfFSCsNodes.push_back(NodesIndicies);

        NI += 1;
    }
};

// Same logic, Build all possible obs indicies. Build All Obs Indicies possible?
void BestResponseMomdpModelSparse::RecursiveBuildNextNIProbDist(int depth, vector<map<int, double> *> &all_nodes_trans_dist, vector<int> NodeIndicies, double Pb_NI_new, map<int, double> &NextNIProbDist)
{
    if (depth != this->AgentNb)
    {
        // If the depth is the same with optimizing agent index, we just give a 0 obs index (Because we won't use it)
        if (depth == this->CurrentOptimizingAgentI)
        {
            NodeIndicies.push_back(0);
            this->RecursiveBuildNextNIProbDist(depth + 1, all_nodes_trans_dist, NodeIndicies, Pb_NI_new, NextNIProbDist);
        }
        else
        {
            // oi is the node index for the agent i (not the optimizing one), OI is the global index for node indicies vector
            map<int, double> *prob_dist_nodes = all_nodes_trans_dist[depth];
            map<int, double>::iterator it;
            for (it = prob_dist_nodes->begin(); it != prob_dist_nodes->end(); it++)
            {
                NodeIndicies.push_back(it->first);
                Pb_NI_new *= it->second;
                this->RecursiveBuildNextNIProbDist(depth + 1, all_nodes_trans_dist, NodeIndicies, Pb_NI_new, NextNIProbDist);
                NodeIndicies.pop_back();
                Pb_NI_new /= it->second;
            }
        }
    }
    else
    {
        // this->_m_IndiciesOfObs[OI] = ObsIndicies;
        int NI_new = this->_m_FSCsNodes_Indicies[NodeIndicies];
        NextNIProbDist[NI_new] = Pb_NI_new;
    }
};

vector<int> BestResponseMomdpModelSparse::NItoActionsIndicies(int NI)
{
    vector<int> NodesInidices = this->_m_IndiciesOfFSCsNodes[NI];
    vector<int> act_indicies(this->AgentNb, 0);
    for (int agent_i = 0; agent_i < this->AgentNb; agent_i++)
    {
        if (agent_i == this->CurrentOptimizingAgentI)
        {
            continue;
        }
        act_indicies[agent_i] = this->FSCs[agent_i].GetActionIndexForNodeI(NodesInidices[agent_i]);
    }

    return act_indicies;
};

double BestResponseMomdpModelSparse::ProbAllNodesTrans(int N_newI, vector<int> &ObsIndicies, int NI)
{
    double prob = 1;
    vector<int> NewNodesIndicies = this->_m_IndiciesOfFSCsNodes[N_newI];

    // vector<int> ObsIndicies = this->_m_IndiciesOfObs[OI];
    vector<int> NodesIndicies = this->_m_IndiciesOfFSCsNodes[NI];

    for (int agent_i = 0; agent_i < this->AgentNb; agent_i++)
    {
        if (agent_i != this->CurrentOptimizingAgentI)
        {
            int n_newI_agent_i = NewNodesIndicies[agent_i];
            int oI_agent_i = ObsIndicies[agent_i];
            int nI_agent_i = NodesIndicies[agent_i];
            if (this->FSCs[agent_i].ProbTrans(nI_agent_i, oI_agent_i, n_newI_agent_i) > 0)
            {
                prob *= this->FSCs[agent_i].ProbTrans(nI_agent_i, oI_agent_i, n_newI_agent_i);
            }
            else
            {
                return 0;
            }
        }
        else
        {
            continue;
        }
    }

    return prob;
};

void BestResponseMomdpModelSparse::ProcessForOneExtendedState(int eI, vector<int> &UnProcessedSet)
{
    vector<int> extend_state_vec = DecomposeBestResponseState(eI);

    int sI = extend_state_vec[0];
    int NI = extend_state_vec[1];
    // int oI = extend_state_vec[2];
    // cout << sI << endl;
    // cout << NI << endl;

    vector<int> act_indicies = this->NItoActionsIndicies(NI); // this is the vector for actions indicies is at time t, deterministic

    // for a in actions
    for (unsigned int aI = 0; aI < this->Actions.size(); aI++)
    {
        // cout << "------ aI:"<<aI<<endl;
        act_indicies[this->CurrentOptimizingAgentI] = aI;
        int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);
        // sparse representation for transition (next s')
        map<int, double> *prob_dist_trans = this->DecPomdpModel->GetTransProbDist(sI, JAI);
        map<int, double>::iterator it_trans;

        for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
        {
            int s_newI = it_trans->first;
            double pr_s_newI = it_trans->second;
            // cout << "---- s_newI:"<< s_newI << endl;

            // sparse representation for observation (next o')
            map<int, double> *prob_dist_obs = this->DecPomdpModel->GetObsFuncProbDist(s_newI, JAI);
            map<int, double>::iterator it_obs_func;

            for (it_obs_func = prob_dist_obs->begin(); it_obs_func != prob_dist_obs->end(); it_obs_func++)
            {
                int joint_obs_newI = it_obs_func->first;
                // cout << "-- joint_obs_newI:"<<joint_obs_newI<<endl;

                double pr_joint_obs_newI = it_obs_func->second;
                vector<int> obs_indicies = this->DecPomdpModel->JointToIndividualObsIndices(joint_obs_newI);
                int oI_new = obs_indicies[this->CurrentOptimizingAgentI];
                // cout << "-- oI_new:"<<oI_new<<endl;

                map<int, double> *result_NI_new_prob_dist = this->GetAllNodesTransProbDist(NI, obs_indicies);
                map<int, double>::iterator it_trans_nodes;
                for (it_trans_nodes = result_NI_new_prob_dist->begin(); it_trans_nodes != result_NI_new_prob_dist->end(); it_trans_nodes++)
                {
                    int NI_new = it_trans_nodes->first;
                    double pr_nodes_transition = it_trans_nodes->second;
                    vector<int> new_extended_state = {s_newI, NI_new, oI_new};
                    double pb = pr_nodes_transition * pr_joint_obs_newI * pr_s_newI;
                    // check if this new extended state already exist
                    // cout << "pb:"<<pb<<endl;

                    // if key absent
                    if (this->_m_IndiciesToExtendedStateIndex.find(new_extended_state) == this->_m_IndiciesToExtendedStateIndex.end())
                    {
                        this->_m_ExtendedStateIndicies.push_back(new_extended_state);
                        int e_newI = _m_ExtendedStateIndicies.size() - 1;
                        _m_IndiciesToExtendedStateIndex[new_extended_state] = e_newI;
                        UnProcessedSet.push_back(e_newI);

                        string str_BRState = "s" + to_string(new_extended_state[0]) + "N" + to_string(new_extended_state[1]) + "o" + to_string(new_extended_state[2]);
                        this->States.push_back(str_BRState);
                        this->TransFuncVecs[aI][eI].insert(std::make_pair(e_newI, pb));

                        // cout << "NEW Extended state" << endl;
                    }
                    else
                    {
                        int e_newI = _m_IndiciesToExtendedStateIndex[new_extended_state];
                        this->TransFuncVecs[aI][eI][e_newI] += pb;
                        // cout << "Old Extended state" << endl;
                    }
                }
            }
        }
    }
}

void BestResponseMomdpModelSparse::ProcessForOneExtendedStateStochasticFSC(int eI, vector<int> &UnProcessedSet)
{
    vector<int> extend_state_vec = DecomposeBestResponseState(eI);
    int sI = extend_state_vec[0];
    int NI = extend_state_vec[1];
    map<int, double> human_action_pb_dist = this->Human_FSC.GetHumanActionDist(NI);
    map<int, double>::iterator it_ahI;

    // for a_robot in actions
    for (unsigned int aI = 0; aI < this->Actions.size(); aI++)
    {
        for (it_ahI = human_action_pb_dist.begin(); it_ahI != human_action_pb_dist.end(); it_ahI++)
        {
            vector<int> fsc_obs(2);
            fsc_obs[0] = it_ahI->first;
            vector<int> action_indices(2);
            action_indices[Human_index] = it_ahI->first;
            action_indices[CurrentOptimizingAgentI] = aI;
            double pb_actions = it_ahI->second;
            int JAI = this->DecPomdpModel->IndividualToJointActionIndex(action_indices);
            map<int, double> *prob_dist_trans = this->DecPomdpModel->GetTransProbDist(sI, JAI);
            map<int, double>::iterator it_trans;
            for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
            {
                int s_newI = it_trans->first;
                double pr_s_newI = it_trans->second;
                map<int, double> *prob_dist_obs = this->DecPomdpModel->GetObsFuncProbDist(s_newI, JAI);
                map<int, double>::iterator it_obs_func;

                for (it_obs_func = prob_dist_obs->begin(); it_obs_func != prob_dist_obs->end(); it_obs_func++)
                {
                    int joint_obs_newI = it_obs_func->first;
                    double pr_joint_obs_newI = it_obs_func->second;
                    vector<int> obs_indicies = this->DecPomdpModel->JointToIndividualObsIndices(joint_obs_newI);
                    int oI_new = obs_indicies[this->CurrentOptimizingAgentI];
                    int oI_human = obs_indicies[this->Human_index];
                    fsc_obs[1] = oI_human;

                    map<int, double> *result_NI_new_prob_dist = this->Human_FSC.GetNextNodeProb(NI, fsc_obs);
                    map<int, double>::iterator it_trans_nodes;
                    for (it_trans_nodes = result_NI_new_prob_dist->begin(); it_trans_nodes != result_NI_new_prob_dist->end(); it_trans_nodes++)
                    {
                        int NI_new = it_trans_nodes->first;
                        double pr_nodes_transition = it_trans_nodes->second;
                        vector<int> new_extended_state = {s_newI, NI_new, oI_new};
                        double pb = pr_nodes_transition * pr_joint_obs_newI * pr_s_newI * pb_actions;
                        // check if this new extended state already exist
                        // if key absent
                        if (pb != 0)
                        {
                            if (this->_m_IndiciesToExtendedStateIndex.find(new_extended_state) == this->_m_IndiciesToExtendedStateIndex.end())
                            {
                                this->_m_ExtendedStateIndicies.push_back(new_extended_state);
                                int e_newI = _m_ExtendedStateIndicies.size() - 1;
                                _m_IndiciesToExtendedStateIndex[new_extended_state] = e_newI;
                                UnProcessedSet.push_back(e_newI);

                                string str_BRState = "s" + to_string(new_extended_state[0]) + "N" + to_string(new_extended_state[1]) + "o" + to_string(new_extended_state[2]);
                                this->States.push_back(str_BRState);
                                this->TransFuncVecs[aI][eI].insert(std::make_pair(e_newI, pb));

                                // cout << "NEW Extended state" << endl;
                            }
                            else
                            {
                                int e_newI = _m_IndiciesToExtendedStateIndex[new_extended_state];
                                this->TransFuncVecs[aI][eI][e_newI] += pb;
                                // cout << "Old Extended state" << endl;
                            }
                        }
                    }
                }
            }
        }
    }
}

void BestResponseMomdpModelSparse::GenerateAllReachableExtendedStates()
{
    vector<int> UnProcessedSet;
    map<int, double> *sI_b0_sparse = this->DecPomdpModel->GetInitialBeliefSparse()->GetBeliefSparse();
    map<int, double>::iterator it;
    int NI_init = 0;

    for (it = sI_b0_sparse->begin(); it != sI_b0_sparse->end(); it++)
    {

        int sI = it->first;
        double pr_sI = it->second;
        int JOI_first_possible = this->DecPomdpModel->GetObsFuncProbDist(sI, 0)->begin()->first;
        int oI_init = this->DecPomdpModel->JointToIndividualObsIndices(JOI_first_possible)[this->CurrentOptimizingAgentI];
        vector<int> eI_init = {sI, NI_init, oI_init};
        this->_m_ExtendedStateIndicies.push_back(eI_init);
        int e_newI = _m_ExtendedStateIndicies.size() - 1;
        _m_IndiciesToExtendedStateIndex[eI_init] = e_newI;
        UnProcessedSet.push_back(e_newI);
        b0_sparse[e_newI] = pr_sI;
        string str_BRState = "s" + to_string(eI_init[0]) + "N" + to_string(eI_init[1]) + "o" + to_string(eI_init[2]);
        this->States.push_back(str_BRState);
    }

    while (!UnProcessedSet.empty())
    {
        int eI_process = UnProcessedSet[0];
        UnProcessedSet.erase(UnProcessedSet.begin());
        ProcessForOneExtendedState(eI_process, UnProcessedSet);
    }
}

void BestResponseMomdpModelSparse::GenerateAllReachableExtendedStatesForStochasticFSC()
{
    vector<int> UnProcessedSet;
    map<int, double> *sI_b0_sparse = this->DecPomdpModel->GetInitialBeliefSparse()->GetBeliefSparse();
    map<int, double>::iterator it;
    int NI_init = 0;

    for (it = sI_b0_sparse->begin(); it != sI_b0_sparse->end(); it++)
    {

        int sI = it->first;
        double pr_sI = it->second;
        int JOI_first_possible = this->DecPomdpModel->GetObsFuncProbDist(sI, 0)->begin()->first;
        int oI_init = this->DecPomdpModel->JointToIndividualObsIndices(JOI_first_possible)[this->CurrentOptimizingAgentI];
        vector<int> eI_init = {sI, NI_init, oI_init};
        this->_m_ExtendedStateIndicies.push_back(eI_init);
        int e_newI = _m_ExtendedStateIndicies.size() - 1;
        _m_IndiciesToExtendedStateIndex[eI_init] = e_newI;
        UnProcessedSet.push_back(e_newI);
        b0_sparse[e_newI] = pr_sI;
        string str_BRState = "s" + to_string(eI_init[0]) + "N" + to_string(eI_init[1]) + "o" + to_string(eI_init[2]);
        this->States.push_back(str_BRState);
    }

    while (!UnProcessedSet.empty())
    {
        int eI_process = UnProcessedSet[0];
        UnProcessedSet.erase(UnProcessedSet.begin());
        ProcessForOneExtendedStateStochasticFSC(eI_process, UnProcessedSet);
    }
}

BestResponseMomdpModelSparse::BestResponseMomdpModelSparse(DecPomdpInterface *DecPomdp, vector<FSCBase> &FSCs, int optimizing_agentI)
{
    this->DecPomdpModel = DecPomdp;
    this->FSCs = FSCs;
    this->CurrentOptimizingAgentI = optimizing_agentI;
    this->Actions = DecPomdpModel->GetActionVec(optimizing_agentI);
    this->Observations = DecPomdpModel->GetObservationVec(optimizing_agentI);
    this->AgentNb = DecPomdp->GetNbAgents();

    vector<int> Nodes_Indicies;
    // vector<int> Obs_Indicies;
    int NI_start = 0;
    // int OI = 0; // OI start

    this->RecursiveBuildAllFSCsNodesIndicies(0, NI_start, Nodes_Indicies);
    // this->RecursiveBuildAllObsIndicies(0, OI, Obs_Indicies);

    this->NI_size = this->_m_IndiciesOfFSCsNodes.size();
    cout << "NI_size:" << this->NI_size << endl;
    this->SizeJAI = this->DecPomdpModel->GetSizeOfJointA();
    this->SizeDecPomdpStateSpace = this->DecPomdpModel->GetSizeOfS();
    this->SizeOfA = this->DecPomdpModel->GetSizeOfA(this->CurrentOptimizingAgentI);
    this->SizeOfObs = this->DecPomdpModel->GetSizeOfObs(this->CurrentOptimizingAgentI);

    this->max_extended_state_size = SizeDecPomdpStateSpace * NI_size * SizeOfObs;
    // Need to clean the redundant part!!! Because we are using the maximum possible eI size!!!
    this->TransFuncVecs.resize(SizeOfA, vector<map<int, double>>(max_extended_state_size));

    GenerateAllReachableExtendedStates();

    this->SizeOfS = this->_m_ExtendedStateIndicies.size();
    this->b0 = ConvertBeliefSparseToNoSparse(b0_sparse);
};

BestResponseMomdpModelSparse::BestResponseMomdpModelSparse(DecPomdpInterface *DecPomdp,
                                                           SampleStochasticLocalFsc &Human_FSC, int optimizing_agentI)
{
    this->DecPomdpModel = DecPomdp;
    // this->FSCs_Stochastic = FSCs;
    this->Human_FSC = Human_FSC;
    this->CurrentOptimizingAgentI = optimizing_agentI;
    this->Human_index = DecPomdp->GetNbAgents() - optimizing_agentI - 1;
    this->Actions = DecPomdpModel->GetActionVec(optimizing_agentI);
    this->Observations = DecPomdpModel->GetObservationVec(optimizing_agentI);
    this->AgentNb = DecPomdp->GetNbAgents();
    this->NI_size = Human_FSC.GetNodeSize();
    cout << "NI_size:" << this->NI_size << endl;
    this->SizeJAI = this->DecPomdpModel->GetSizeOfJointA();
    this->SizeDecPomdpStateSpace = this->DecPomdpModel->GetSizeOfS();
    this->SizeOfA = this->DecPomdpModel->GetSizeOfA(this->CurrentOptimizingAgentI);
    this->SizeOfObs = this->DecPomdpModel->GetSizeOfObs(this->CurrentOptimizingAgentI);
    this->max_extended_state_size = SizeDecPomdpStateSpace * NI_size * SizeOfObs;
    // Need to clean the redundant part!!! Because we are using the maximum possible eI size!!!
    this->TransFuncVecs.resize(SizeOfA, vector<map<int, double>>(max_extended_state_size));

    GenerateAllReachableExtendedStatesForStochasticFSC();
    this->SizeOfS = this->_m_ExtendedStateIndicies.size();
    this->b0 = ConvertBeliefSparseToNoSparse(b0_sparse);
}

vector<double> BestResponseMomdpModelSparse::ConvertBeliefSparseToNoSparse(map<int, double> &b0_sparse)
{
    vector<double> b(SizeOfS, 0);
    map<int, double>::iterator it;
    for (it = b0_sparse.begin(); it != b0_sparse.end(); it++)
    {
        b[it->first] = it->second;
    }

    return b;
};

double BestResponseMomdpModelSparse::TransFunc(int eI, int aI_opt_agentI, int e_newI)
{
    // if key absent
    if ((this->TransFuncVecs[aI_opt_agentI][eI]).find(e_newI) == this->TransFuncVecs[aI_opt_agentI][eI].end())
    {
        // returns proba 0
        return 0.;
    }
    // key present
    else
    {
        // returns associated value
        return this->TransFuncVecs[aI_opt_agentI][eI][e_newI];
    }
};

double BestResponseMomdpModelSparse::ObsFunc(int oI_opt_agentI, int e_newI, int aI_opt_agentI)
{
    (void)(aI_opt_agentI);
    vector<int> new_indicies = this->_m_ExtendedStateIndicies[e_newI];
    int o_newI = new_indicies[2];
    if (oI_opt_agentI == o_newI)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

double BestResponseMomdpModelSparse::Reward(int eI, int aI_opt_agentI)
{
    vector<int> indicies = this->_m_ExtendedStateIndicies[eI];
    int sI = indicies[0];
    int NI = indicies[1]; // this is the index for the nodes indicies at t
    double res = 0;

    if (this->FSCs.size() != 0)
    {
        vector<int> act_indicies = this->NItoActionsIndicies(NI); // this is the vector for actions indicies is at time t, deterministic
        act_indicies[this->CurrentOptimizingAgentI] = aI_opt_agentI;
        int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);

        res = this->DecPomdpModel->Reward(sI, JAI);
    }
    // if stochastic FSC
    else
    {
        map<int, double> human_action_pb_dist = this->Human_FSC.GetHumanActionDist(NI);
        map<int, double>::iterator it_ahI;
        for (it_ahI = human_action_pb_dist.begin(); it_ahI != human_action_pb_dist.end(); it_ahI++)
        {
            int aHI = it_ahI->first;
            double pb_a = it_ahI->second;
            vector<int> act_indicies(2);
            act_indicies[Human_index] = aHI;
            act_indicies[CurrentOptimizingAgentI] = aI_opt_agentI;
            int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);
            double temp_r = this->DecPomdpModel->Reward(sI, JAI);
            res += temp_r * pb_a;
        }
    }

    return res;
};

map<int, double> *BestResponseMomdpModelSparse::GetAllNodesTransProbDist(int NI, vector<int> &ObsIndicies)
{
    vector<int> NodesIndicies = this->_m_IndiciesOfFSCsNodes[NI];

    vector<map<int, double> *> all_nodes_trans_dist;
    for (int agent_i = 0; agent_i < this->AgentNb; agent_i++)
    {
        if (agent_i != this->CurrentOptimizingAgentI)
        {
            int oI_agent_i = ObsIndicies[agent_i];
            int nI_agent_i = NodesIndicies[agent_i];

            // sparse representation for observation (next o')
            map<int, double> *prob_dist_node_trans = this->FSCs[agent_i].GetNodeTransProbDist(nI_agent_i, oI_agent_i);
            all_nodes_trans_dist.push_back(prob_dist_node_trans);
        }
        else
        {
            map<int, double> *ptr = nullptr;
            all_nodes_trans_dist.push_back(ptr);
            continue;
        }
    }

    this->result_NI_new_prob_dist_temp.clear();
    // map<int,double> result_NI_new_prob_dist;
    RecursiveBuildNextNIProbDist(0, all_nodes_trans_dist, {}, 1, this->result_NI_new_prob_dist_temp);
    return &this->result_NI_new_prob_dist_temp;
};

void BestResponseMomdpModelSparse::ExportPOMDP()
{
    string filename = "./TempFiles/BestResponseForAgentI.pomdp";
    this->ExportPOMDP(filename);
}

void BestResponseMomdpModelSparse::ExportPOMDP(string filename)
{
    ofstream fp(filename.c_str());
    int SizeA = this->GetSizeOfA();
    int SizeS = this->GetSizeOfS();
    int SizeO = this->GetSizeOfObs();

    // 1. discount
    fp << "discount: " << this->GetDiscount() << endl;
    // 2. values
    fp << "values: reward" << endl;

    fp << "states: ";
    for (int eI = 0; eI < SizeS; eI++)
    {
        fp << this->States[eI] << " ";
    }
    fp << endl;

    // 4. actions
    fp << "actions: ";
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        fp << this->Actions[aRI] << " ";
    }
    fp << endl;
    // 5. observations
    fp << "observations: ";
    for (int oRI = 0; oRI < SizeO; oRI++)
    {
        fp << this->Observations[oRI] << " ";
    }
    fp << endl;
    // 6. start
    fp << "start: ";
    for (int eI = 0; eI < SizeS; eI++)
    {
        fp << this->b0[eI] << " ";
    }
    fp << endl;

    // All below only list none-zero prob items

    // 7. T
    // T: action_I : start-state_I : end-state_I %f
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        for (int eI = 0; eI < SizeS; eI++)
        {

            map<int, double> prob_dist_trans = this->TransFuncVecs[aRI][eI];
            map<int, double>::iterator it;
            for (it = prob_dist_trans.begin(); it != prob_dist_trans.end(); it++)
            {
                fp << "T: " << aRI << " : " << eI << " : " << it->first << " " << it->second << endl;
            }
        }
    }

    // 8. O
    // O: action_I : end-state_I : observation_I %f
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        for (int e_newI = 0; e_newI < SizeS; e_newI++)
        {

            vector<int> new_indicies = this->_m_ExtendedStateIndicies[e_newI];
            int oRI = new_indicies[2];

            fp << "O: " << aRI << " : " << e_newI << " : " << oRI << " " << 1 << endl;
        }
    }

    // 9. R
    // R: action_I : start-state_I : * : * %f
    for (int aI = 0; aI < SizeA; aI++)
    {
        for (int eI = 0; eI < SizeS; eI++)
        {
            float temp_r = this->Reward(eI, aI);
            bool flag_nan = isnan(temp_r);
            // cout << temp_r << endl; // have this, the output is oK, without this gonna no R, strange
            if (temp_r != 0 && !flag_nan)
            {
                // this->RewardFuncVecs[aI][eI] = temp_r;
                fp << "R: " << aI << " : " << eI << " : * : * " << temp_r << endl;
                // fp << "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
                // cerr <<"CERR: "<< "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
            }
            else
            {
            }
        }
    }
    fp.flush();
    fp.close();

    // this->AfterExport = true; // All the TranFunc, ObsFunc and Reward are stored in the self vectors
}

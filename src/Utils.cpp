#include "../Include/Utils.h"
#include <cmath>
#include <numeric>

bool CheckAlphaExist(vector<AlphaVector> &a_vecs, AlphaVector &alpha)
{
    if (a_vecs.size() == 0)
        return false;
    for (int i = 0; i < int(a_vecs.size()); i++)
    {
        if (a_vecs[i] == alpha)
        {
            return true;
        }
    }
    return false;
}

void PrintAlphaVectors(vector<AlphaVector> &a_vecs)
{
    for (int i = 0; i < int(a_vecs.size()); i++)
    {
        a_vecs[i].Print();
    }
};

void PrintVector(vector<double> &V)
{
    for (int i = 0; i < int(V.size()); i++)
    {
        cout << V[i] << " ";
    }
    cout << endl;
};

void PrintAllAlphaAOVecs(vector<vector<vector<AlphaVector>>> &a_ao_vecs)
{
    // For all the action index in alpha_a_b
    for (int aI = 0; aI < int(a_ao_vecs.size()); aI++)
    {
        // cout << " ---- aI:"<<aI;
        // For all the oI
        for (int oI = 0; oI < int(a_ao_vecs[aI].size()); oI++)
        {
            // cout << ", oI:"<<oI <<" ---- " << endl;
            // For all alpha idx
            for (int idx = 0; idx < int(a_ao_vecs[aI][oI].size()); idx++)
            {
                cout << " ---- aI:" << aI << ", oI:" << oI << ", alpha_idx:" << idx << " -----" << endl;
                a_ao_vecs[aI][oI][idx].Print();
            }
        }
    }
};

// This function is used for selected the optimal action given a current belief and alpha-vectors

AlphaVector argmax_alpha_vector(vector<AlphaVector> &alpha_vecs, BeliefSparse *b)
{
    // initilized a with -1, later can check if an action is successfully selected
    AlphaVector a_max_val;
    double v_max = -DBL_MAX;

    map<int, double> *belief_sparse = b->GetBeliefSparse();
    map<int, double>::iterator it;

    for (int i = 0; i < int(alpha_vecs.size()); ++i)
    {
        vector<double> alpha_temp = alpha_vecs[i].GetValues();
        if (int(alpha_temp.size()) != b->GetSize())
        {

            cerr << "belief state size:" << b->GetSize() << endl;
            cerr << "alpha state size:" << alpha_temp.size() << endl;
            cerr << "State dimensions between the given alpha-vector and belief don't match!" << endl;
            throw(("State dimensions between the given alpha-vector and belief don't match!"));
        }
        double v_temp = 0;

        for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
        {
            v_temp += alpha_temp[it->first] * (it->second);
        }

        // for (int j = 0; j < int(alpha_temp.size()); ++j){
        //     v_temp += alpha_temp[j]*b[j];
        // }

        if (v_temp > v_max)
        {
            a_max_val = alpha_vecs[i];
            v_max = v_temp;
        }
    }
    if (a_max_val.GetValues().size() == 0)
    {
        cerr << "alpha_vecs.size()=" << alpha_vecs.size() << endl;
        throw("argmax_alpha not found");
    }
    return a_max_val;
}

double EvaluationWithAlphaVecs(PomdpInterface *Pb, vector<AlphaVector> &alpha_vecs)
{
    double v_max = -DBL_MAX;
    map<int, double> *belief_sparse = Pb->GetInitBeliefSparse();
    map<int, double>::iterator it;

    for (int i = 0; i < int(alpha_vecs.size()); ++i)
    {
        vector<double> alpha_temp = alpha_vecs[i].GetValues();
        double v_temp = 0;
        for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
        {
            v_temp += alpha_temp[it->first] * (it->second);
        }

        if (v_temp > v_max)
        {
            v_max = v_temp;
        }
    }
    return v_max;
};

int argmax_alpha(vector<AlphaVector> &alpha_vecs, BeliefSparse &b)
{
    // cout << " ------ Current Belief ---------"<<endl;
    // b.PrintBelief();
    double v_max = -__DBL_MAX__;
    int maxI = 0;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (int i = 0; i < int(alpha_vecs.size()); ++i)
    {
        vector<double> alpha_temp = alpha_vecs[i].GetValues();
        double v_temp = 0;
        for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
        {
            v_temp += alpha_temp[it->first] * (it->second);
        }

        if (v_temp > v_max)
        {
            v_max = v_temp;
            maxI = i;
        }
    }

    return maxI;
}

int argmax_alpha(vector<AlphaVector> &alpha_vecs, BeliefSparse &b, double &value)
{
    // cout << " ------ Current Belief ---------"<<endl;
    // b.PrintBelief();
    double v_max = -__DBL_MAX__;
    int maxI = 0;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (int i = 0; i < int(alpha_vecs.size()); ++i)
    {
        vector<double> alpha_temp = alpha_vecs[i].GetValues();
        double v_temp = 0;
        for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
        {
            v_temp += alpha_temp[it->first] * (it->second);
        }

        if (v_temp > v_max)
        {
            v_max = v_temp;
            maxI = i;
        }
    }

    value = v_max;

    return maxI;
}

double best_value(vector<AlphaVector> &alpha_vecs, BeliefSparse &b)
{
    double v_max = -__DBL_MAX__;
    int maxI = 0;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (int i = 0; i < int(alpha_vecs.size()); ++i)
    {
        vector<double> alpha_temp = alpha_vecs[i].GetValues();
        double v_temp = 0;
        for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
        {
            v_temp += alpha_temp[it->first] * (it->second);
        }

        if (v_temp > v_max)
        {
            v_max = v_temp;
            maxI = i;
        }
    }

    return v_max;
}

double compute_p_oba(int o, BeliefSparse &b, int a, DecPomdpInterface *decpomdp)
{
    double res = 0;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
    {
        double pr_o_s_newI = 0;
        double pr_sI = it->second;
        int sI = it->first;

        map<int, double> *prob_dist_trans = decpomdp->GetTransProbDist(sI, a);
        map<int, double>::iterator it_trans;

        for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
        {
            int s_newI = it_trans->first;
            pr_o_s_newI += (it_trans->second) * decpomdp->ObsFunc(o, s_newI, a);
        }
        res += pr_sI * pr_o_s_newI;
    }

    return res;
};

double compute_p_oba(int o, BeliefSparse &b, int a, PomdpInterface *pomdp)
{
    double res = 0;
    // int count = 0;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
    {
        double pr_o_s_newI = 0;
        double pr_sI = it->second;
        int sI = it->first;

        // sparse representation
        // map<int, double>* prob_dist_trans = pomdp->GetTransProbDist(sI,a);
        // map<int, double>::iterator it_trans;

        unordered_map<int, double> *prob_dist_trans = pomdp->GetTransProbDist(sI, a);
        unordered_map<int, double>::iterator it_trans;
        for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
        {
            int s_newI = it_trans->first;
            double pr_s_newI = it_trans->second;
            pr_o_s_newI += pr_s_newI * pomdp->ObsFunc(o, s_newI, a);
        }
        res += pr_sI * pr_o_s_newI;
    }
    return res;
};

map<int, double> compute_dist_p_oba(BeliefSparse &b, int a, DecPomdpInterface *decpomdp)
{
    map<int, double> res;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
    {
        double pr_sI = it->second;
        int sI = it->first;

        map<int, double> *prob_dist_trans = decpomdp->GetTransProbDist(sI, a);
        map<int, double>::iterator it_trans;

        for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
        {
            int s_newI = it_trans->first;
            double pr_s_newI = (it_trans->second);

            map<int, double> *prob_dist_obs = decpomdp->GetObsFuncProbDist(s_newI, a);
            map<int, double>::iterator it_obs;

            for (it_obs = prob_dist_obs->begin(); it_obs != prob_dist_obs->end(); it_obs++)
            {
                int oI = it_obs->first;
                double pr_oI = it_obs->second;
                double pb = pr_sI * pr_s_newI * pr_oI;
                if (pb > 0)
                {
                    res[oI] += pb;
                }
            }
        }
    }

    return res;
}

BeliefSparse Update(PomdpInterface *Pb, BeliefSparse &b, int aI, int oI, double p_oba)
{
    int SizeS = Pb->GetSizeOfS();
    // vector<double> b_aI_value(SizeS);

    map<int, double> pb_states;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it_sI;

    for (it_sI = belief_sparse->begin(); it_sI != belief_sparse->end(); it_sI++)
    {
        int sI = it_sI->first;
        double pr_sI = it_sI->second;
        // sparse representation
        // map<int, double>* prob_dist_trans = Pb->GetTransProbDist(sI,aI);
        // map<int, double>::iterator it;

        unordered_map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, aI);
        unordered_map<int, double>::iterator it;
        for (it = prob_dist_trans->begin(); it != prob_dist_trans->end(); it++)
        {
            int s_newI = it->first;
            double p_s_newI = 0;
            double p_o = Pb->ObsFunc(oI, s_newI, aI);
            if (p_o > 0)
            {
                p_s_newI += it->second * pr_sI;
            }
            double pr_snew_ao = (p_o * p_s_newI) / p_oba;
            pb_states[s_newI] += pr_snew_ao;
        }
    }

    BeliefSparse b_ao(pb_states, SizeS);

    return b_ao;
};

BeliefSparse Update(DecPomdpInterface *Pb, BeliefSparse &b, int aI, int oI, double p_oba)
{
    int SizeS = Pb->GetSizeOfS();

    // cerr << SizeS << endl;
    // vector<double> b_aI_value(SizeS);

    map<int, double> pb_states;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it_sI;

    for (it_sI = belief_sparse->begin(); it_sI != belief_sparse->end(); it_sI++)
    {
        int sI = it_sI->first;
        double pr_sI = it_sI->second;
        // sparse representation
        map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, aI);
        map<int, double>::iterator it;
        for (it = prob_dist_trans->begin(); it != prob_dist_trans->end(); it++)
        {
            int s_newI = it->first;
            double p_s_newI = 0;
            double p_o = Pb->ObsFunc(oI, s_newI, aI);

            p_s_newI += it->second * pr_sI;
            double pr_snew_ao = (p_o * p_s_newI) / p_oba;
            if (pr_snew_ao > 0)
            {
                pb_states[s_newI] += pr_snew_ao;
            }
        }
    }

    BeliefSparse b_ao(pb_states, SizeS);

    return b_ao;
};

BeliefSparse Update(DecPomdpInterface *Pb, BeliefSparse &b, int aI, int oI)
{
    vector<double> b_aI_value(Pb->GetSizeOfS());
    double pr_oba = compute_p_oba(oI, b, aI, Pb);
    if (pr_oba > 0)
    {
        return Update(Pb, b, aI, oI, pr_oba);
    }
    else
    {
        cout << "Error! p_oba is 0!" << endl;
        throw "Error! p_oba is 0!";
    }
};

BeliefSparse Update(PomdpInterface *Pb, BeliefSparse &b, int aI, int oI)
{
    vector<double> b_aI_value(Pb->GetSizeOfS());
    double pr_oba = compute_p_oba(oI, b, aI, Pb);
    if (pr_oba > 0)
    {
        return Update(Pb, b, aI, oI, pr_oba);
    }
    else
    {
        cout << "Error! p_oba is 0!" << endl;
        throw "Error! p_oba is 0!";
    }
};

// From MADP codes, ImportValueFunction
vector<AlphaVector> ImportValueFunction(const string &filename)
{
    vector<AlphaVector> V;

    int lineState = 0; /* lineState=0 -> read action
                        * lineState=1 -> read values
                        * lineState=2 -> empty line, skip */
    int nrStates = -1;
    bool first = true;
    int action = 0;
    double value;
    int n;
    vector<double> values;

    ifstream fp(filename.c_str());
    if (!fp)
    {
        cerr << "AlphaVectorPlanning::ImportValueFunction: failed to "
             << "open file " << filename << endl;
    }

    string buffer;
    while (!getline(fp, buffer).eof())
    {
        switch (lineState)
        {
        case 0:
            // read action
            //            action=strtol(buffer,NULL,10);
            {
                istringstream is(buffer);
                while (is >> n)
                {
                    if (n >= 0)
                    {
                        action = n;
                    }
                }
            }
            lineState++;
            break;
        case 1:
            // read values
            values.clear();

            {
                istringstream is(buffer);
                while (is >> value)
                    values.push_back(value);
            }

            if (first)
            {
                nrStates = values.size();
                first = false;
            }

            // create new alpha vector and store it
            {
                AlphaVector alpha(nrStates);
                alpha.SetAction(action);
                alpha.SetValues(values);
                V.push_back(alpha);
            }

            lineState++;
            break;
        case 2:
            // do nothing, line is empty
            lineState = 0;
            break;
        }
    }

    return (V);
}

void ExportMpomdpModel(DecPomdpInterface *DecPomdpModel, string filename)
{
    ofstream fp(filename.c_str());
    // 1. discount
    fp << "discount: " << DecPomdpModel->GetDiscount() << endl;
    // 2. values
    fp << "values: reward" << endl;

    // 3. states
    fp << "states: ";
    for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
    {
        fp << DecPomdpModel->GetAllStates()[sI] << " ";
    }
    fp << endl;

    // 4. actions
    fp << "actions: ";
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        vector<int> actions_indicies = DecPomdpModel->JointToIndividualActionsIndices(JaI);
        string JointActionName = "";
        for (int i = 0; i < int(actions_indicies.size()); i++)
        {
            JointActionName += DecPomdpModel->GetActionName(i, actions_indicies[i]);
        }
        fp << JointActionName << " ";
    }
    fp << endl;

    // 5. observations
    fp << "observations: ";
    for (int JoI = 0; JoI < DecPomdpModel->GetSizeOfJointObs(); JoI++)
    {
        vector<int> obs_indicies = DecPomdpModel->JointToIndividualObsIndices(JoI);
        string JointObsName = "";
        for (int i = 0; i < int(obs_indicies.size()); i++)
        {
            JointObsName += DecPomdpModel->GetObservationName(i, obs_indicies[i]);
        }
        fp << JointObsName << " ";
    }
    fp << endl;

    // 6. start
    fp << "start: ";
    map<int, double> *b0_sparse = DecPomdpModel->GetInitialBeliefSparse()->GetBeliefSparse();

    for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
    {
        double value = 0;
        map<int, double>::iterator it;
        it = b0_sparse->find(sI);
        if (it != b0_sparse->end())
        {
            value = it->second;
        }
        fp << value << " ";
    }
    fp << endl;

    // 7. T
    // T: action_I : start-state_I : end-state_I %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
        {
            for (int s_newI = 0; s_newI < DecPomdpModel->GetSizeOfS(); s_newI++)
            {
                // check prob is not 0
                double temp_pr = DecPomdpModel->TransFunc(sI, JaI, s_newI);
                if (temp_pr > 0)
                {
                    fp << "T: " << JaI << " : " << sI << " : " << s_newI << " " << temp_pr << endl;
                }
            }
        }
    }

    // 8. O
    // O: action_I : end-state_I : observation_I %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int s_newI = 0; s_newI < DecPomdpModel->GetSizeOfS(); s_newI++)
        {
            for (int JoI = 0; JoI < DecPomdpModel->GetSizeOfJointObs(); JoI++)
            {
                // check prob is not 0
                double temp_pr = DecPomdpModel->ObsFunc(JoI, s_newI, JaI);
                if (temp_pr > 0)
                {
                    fp << "O: " << JaI << " : " << s_newI << " : " << JoI << " " << temp_pr << endl;
                }
            }
        }
    }

    // 9. R
    // R: action_I : start-state_I : * : * %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
        {
            float temp_r = DecPomdpModel->Reward(sI, JaI);
            bool flag_nan = isnan(temp_r);
            // cout << temp_r << endl; // have this, the output is oK, without this gonna no R, strange
            if (temp_r != 0 && !flag_nan)
            {
                fp << "R: " << JaI << " : " << sI << " : * : * " << temp_r << endl;
                // fp << "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
                // cerr <<"CERR: "<< "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
            }
            else
            {
            }
        }
    }
    fp.flush();
    fp.close();
};

void ExportMpomdpModelWithDiffInitBelief(DecPomdpInterface *DecPomdpModel, BeliefSparse &new_init_b, string filename)
{
    ofstream fp(filename.c_str());
    // 1. discount
    fp << "discount: " << DecPomdpModel->GetDiscount() << endl;
    // 2. values
    fp << "values: reward" << endl;

    // 3. states
    fp << "states: ";
    for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
    {
        fp << DecPomdpModel->GetAllStates()[sI] << " ";
    }
    fp << endl;

    // 4. actions
    fp << "actions: ";
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        vector<int> actions_indicies = DecPomdpModel->JointToIndividualActionsIndices(JaI);
        string JointActionName = "";
        for (int i = 0; i < int(actions_indicies.size()); i++)
        {
            JointActionName += DecPomdpModel->GetActionName(i, actions_indicies[i]);
        }
        fp << JointActionName << " ";
    }
    fp << endl;

    // 5. observations
    fp << "observations: ";
    for (int JoI = 0; JoI < DecPomdpModel->GetSizeOfJointObs(); JoI++)
    {
        vector<int> obs_indicies = DecPomdpModel->JointToIndividualObsIndices(JoI);
        string JointObsName = "";
        for (int i = 0; i < int(obs_indicies.size()); i++)
        {
            JointObsName += DecPomdpModel->GetObservationName(i, obs_indicies[i]);
        }
        fp << JointObsName << " ";
    }
    fp << endl;

    // 6. start
    fp << "start: ";
    map<int, double> *b0_sparse = new_init_b.GetBeliefSparse();

    for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
    {
        double value = 0;
        map<int, double>::iterator it;
        it = b0_sparse->find(sI);
        if (it != b0_sparse->end())
        {
            value = it->second;
        }
        fp << value << " ";
    }
    fp << endl;

    // 7. T
    // T: action_I : start-state_I : end-state_I %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
        {
            for (int s_newI = 0; s_newI < DecPomdpModel->GetSizeOfS(); s_newI++)
            {
                // check prob is not 0
                double temp_pr = DecPomdpModel->TransFunc(sI, JaI, s_newI);
                if (temp_pr > 0)
                {
                    fp << "T: " << JaI << " : " << sI << " : " << s_newI << " " << temp_pr << endl;
                }
            }
        }
    }

    // 8. O
    // O: action_I : end-state_I : observation_I %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int s_newI = 0; s_newI < DecPomdpModel->GetSizeOfS(); s_newI++)
        {
            for (int JoI = 0; JoI < DecPomdpModel->GetSizeOfJointObs(); JoI++)
            {
                // check prob is not 0
                double temp_pr = DecPomdpModel->ObsFunc(JoI, s_newI, JaI);
                if (temp_pr > 0)
                {
                    fp << "O: " << JaI << " : " << s_newI << " : " << JoI << " " << temp_pr << endl;
                }
            }
        }
    }

    // 9. R
    // R: action_I : start-state_I : * : * %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
        {
            float temp_r = DecPomdpModel->Reward(sI, JaI);
            bool flag_nan = isnan(temp_r);
            // cout << temp_r << endl; // have this, the output is oK, without this gonna no R, strange
            if (temp_r != 0 && !flag_nan)
            {
                fp << "R: " << JaI << " : " << sI << " : * : * " << temp_r << endl;
                // fp << "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
                // cerr <<"CERR: "<< "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
            }
            else
            {
            }
        }
    }
    fp.flush();
    fp.close();
}

map<int, double> GetPossibleJOIfromBeliefSparseAndJAI(DecPomdpInterface *decpomdp, BeliefSparse &b, int JaI)
{
    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;
    map<int, double> dist_s_new;
    // 1. first get all possible s_newI with pb
    for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
    {
        double pr_sI = it->second;
        int sI = it->first;

        map<int, double> *prob_dist_trans = decpomdp->GetTransProbDist(sI, JaI);
        map<int, double>::iterator it_trans;

        for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
        {
            int s_newI = it_trans->first;
            double pb_s_newI_temp = (it_trans->second) * pr_sI;
            if (pb_s_newI_temp > 0)
            {
                dist_s_new[s_newI] += pb_s_newI_temp;
            }
        }
    }

    // 2. second get all possible joi with pb depending on 1.
    map<int, double> res_JOI_dist;
    map<int, double>::iterator it_s_newI;
    for (it_s_newI = dist_s_new.begin(); it_s_newI != dist_s_new.end(); it_s_newI++)
    {
        int s_newI = it_s_newI->first;
        double pb_s_newI = it_s_newI->second;
        map<int, double> *temp_dist_obs = decpomdp->GetObsFuncProbDist(s_newI, JaI);
        map<int, double>::iterator it_obs_dist;
        for (it_obs_dist = temp_dist_obs->begin(); it_obs_dist != temp_dist_obs->end(); it_obs_dist++)
        {
            int JOI = it_obs_dist->first;
            double pb_JOI_s_newI = it_obs_dist->second;
            double pb_JOI_temp = pb_s_newI * pb_JOI_s_newI;
            if (pb_JOI_temp > 0)
            {
                res_JOI_dist[JOI] += pb_JOI_temp;
            }
        }
    }
    return res_JOI_dist;
};

void ExportMpomdpWithNoisyRobotAction(DecPomdpInterface *DecPomdpModel, DecPomdpInterface *out_noise_decpomdp, double noise_rate, int robot_index, string filename)
{

    (void)robot_index;

    ofstream fp(filename.c_str());
    // 1. discount
    fp << "discount: " << DecPomdpModel->GetDiscount() << endl;
    // 2. values
    fp << "values: reward" << endl;

    // 3. states
    fp << "states: ";
    for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
    {
        fp << DecPomdpModel->GetAllStates()[sI] << " ";
    }
    fp << endl;

    // 4. actions
    fp << "actions: ";
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        vector<int> actions_indicies = DecPomdpModel->JointToIndividualActionsIndices(JaI);
        string JointActionName = "";
        for (int i = 0; i < int(actions_indicies.size()); i++)
        {
            JointActionName += DecPomdpModel->GetActionName(i, actions_indicies[i]);
        }
        fp << JointActionName << " ";
    }
    fp << endl;

    // 5. observations
    fp << "observations: ";
    for (int JoI = 0; JoI < DecPomdpModel->GetSizeOfJointObs(); JoI++)
    {
        vector<int> obs_indicies = DecPomdpModel->JointToIndividualObsIndices(JoI);
        string JointObsName = "";
        for (int i = 0; i < int(obs_indicies.size()); i++)
        {
            JointObsName += DecPomdpModel->GetObservationName(i, obs_indicies[i]);
        }
        fp << JointObsName << " ";
    }
    fp << endl;

    // 6. start
    fp << "start: ";
    map<int, double> *b0_sparse = DecPomdpModel->GetInitialBeliefSparse()->GetBeliefSparse();

    for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
    {
        double value = 0;
        map<int, double>::iterator it;
        it = b0_sparse->find(sI);
        if (it != b0_sparse->end())
        {
            value = it->second;
        }
        fp << value << " ";
    }
    fp << endl;

    // Need to add noise to the robot action executions
    // When robot choose an action to execute
    // Robot has noise rate probability to execute other wrong actions, those actions are distributed uniformally

    // 7. T
    // T: action_I : start-state_I : end-state_I %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
        {
            for (int s_newI = 0; s_newI < DecPomdpModel->GetSizeOfS(); s_newI++)
            {
                // check prob is not 0
                double temp_pr = NoisyRobotActionTransition(DecPomdpModel, noise_rate, sI, JaI, s_newI);
                if (temp_pr > 0)
                {
                    fp << "T: " << JaI << " : " << sI << " : " << s_newI << " " << temp_pr << endl;
                    out_noise_decpomdp->ResetTrans(sI, JaI, s_newI, temp_pr);
                }
            }
        }
    }

    // 8. O
    // O: action_I : end-state_I : observation_I %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int s_newI = 0; s_newI < DecPomdpModel->GetSizeOfS(); s_newI++)
        {
            for (int JoI = 0; JoI < DecPomdpModel->GetSizeOfJointObs(); JoI++)
            {
                // check prob is not 0
                double temp_pr = DecPomdpModel->ObsFunc(JoI, s_newI, JaI);
                if (temp_pr > 0)
                {
                    fp << "O: " << JaI << " : " << s_newI << " : " << JoI << " " << temp_pr << endl;
                }
            }
        }
    }

    // 9. R
    // R: action_I : start-state_I : * : * %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
        {
            float temp_r = DecPomdpModel->Reward(sI, JaI);
            bool flag_nan = isnan(temp_r);
            // cout << temp_r << endl; // have this, the output is oK, without this gonna no R, strange
            if (temp_r != 0 && !flag_nan)
            {
                fp << "R: " << JaI << " : " << sI << " : * : * " << temp_r << endl;
                // fp << "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
                // cerr <<"CERR: "<< "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
            }
            else
            {
            }
        }
    }
    fp.flush();
    fp.close();
};

void ExportMpomdpWithNoisyJointAction(DecPomdpInterface *DecPomdpModel, DecPomdpInterface *out_noise_decpomdp, double noise_rate, string filename)
{
    ofstream fp(filename.c_str());
    // 1. discount
    fp << "discount: " << DecPomdpModel->GetDiscount() << endl;
    // 2. values
    fp << "values: reward" << endl;

    // 3. states
    fp << "states: ";
    for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
    {
        fp << DecPomdpModel->GetAllStates()[sI] << " ";
    }
    fp << endl;

    // 4. actions
    fp << "actions: ";
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        vector<int> actions_indicies = DecPomdpModel->JointToIndividualActionsIndices(JaI);
        string JointActionName = "";
        for (int i = 0; i < int(actions_indicies.size()); i++)
        {
            JointActionName += DecPomdpModel->GetActionName(i, actions_indicies[i]);
        }
        fp << JointActionName << " ";
    }
    fp << endl;

    // 5. observations
    fp << "observations: ";
    for (int JoI = 0; JoI < DecPomdpModel->GetSizeOfJointObs(); JoI++)
    {
        vector<int> obs_indicies = DecPomdpModel->JointToIndividualObsIndices(JoI);
        string JointObsName = "";
        for (int i = 0; i < int(obs_indicies.size()); i++)
        {
            JointObsName += DecPomdpModel->GetObservationName(i, obs_indicies[i]);
        }
        fp << JointObsName << " ";
    }
    fp << endl;

    // 6. start
    fp << "start: ";
    map<int, double> *b0_sparse = DecPomdpModel->GetInitialBeliefSparse()->GetBeliefSparse();

    for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
    {
        double value = 0;
        map<int, double>::iterator it;
        it = b0_sparse->find(sI);
        if (it != b0_sparse->end())
        {
            value = it->second;
        }
        fp << value << " ";
    }
    fp << endl;

    // Need to add noise to the robot action executions
    // When robot choose an action to execute
    // Robot has noise rate probability to execute other wrong actions, those actions are distributed uniformally

    // 7. T
    // T: action_I : start-state_I : end-state_I %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
        {
            for (int s_newI = 0; s_newI < DecPomdpModel->GetSizeOfS(); s_newI++)
            {
                // check prob is not 0
                double temp_pr = NoisyJointActionTransition(DecPomdpModel, noise_rate, sI, JaI, s_newI);
                if (temp_pr > 0)
                {
                    fp << "T: " << JaI << " : " << sI << " : " << s_newI << " " << temp_pr << endl;
                    out_noise_decpomdp->ResetTrans(sI, JaI, s_newI, temp_pr);
                }
            }
        }
    }

    // 8. O
    // O: action_I : end-state_I : observation_I %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int s_newI = 0; s_newI < DecPomdpModel->GetSizeOfS(); s_newI++)
        {
            for (int JoI = 0; JoI < DecPomdpModel->GetSizeOfJointObs(); JoI++)
            {
                // check prob is not 0
                double temp_pr = DecPomdpModel->ObsFunc(JoI, s_newI, JaI);
                if (temp_pr > 0)
                {
                    fp << "O: " << JaI << " : " << s_newI << " : " << JoI << " " << temp_pr << endl;
                }
            }
        }
    }

    // 9. R
    // R: action_I : start-state_I : * : * %f
    for (int JaI = 0; JaI < DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < DecPomdpModel->GetSizeOfS(); sI++)
        {
            float temp_r = DecPomdpModel->Reward(sI, JaI);
            bool flag_nan = isnan(temp_r);
            // cout << temp_r << endl; // have this, the output is oK, without this gonna no R, strange
            if (temp_r != 0 && !flag_nan)
            {
                fp << "R: " << JaI << " : " << sI << " : * : * " << temp_r << endl;
                // fp << "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
                // cerr <<"CERR: "<< "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
            }
            else
            {
            }
        }
    }
    fp.flush();
    fp.close();
};

double NoisyRobotActionTransition(DecPomdpInterface *DecPomdpModel, double noise_rate, int sI, int JAI, int s_newI)
{
    int RobotIndex = 1; // if different need fix
    // uniform distribution over rest actions
    double pb_aRI_noise = noise_rate / (DecPomdpModel->GetSizeOfA(RobotIndex) - 1);
    // loop for all robot actions
    vector<int> action_indices = DecPomdpModel->JointToIndividualActionsIndices(JAI);

    double pb_res = 0;
    for (int aRI = 0; aRI < DecPomdpModel->GetSizeOfA(RobotIndex); aRI++)
    {
        double pb_temp;
        vector<int> real_executed_action_indices = action_indices;
        real_executed_action_indices[RobotIndex] = aRI;
        int real_executed_JAI = DecPomdpModel->IndividualToJointActionIndex(real_executed_action_indices);

        if (action_indices[RobotIndex] == aRI)
        {
            pb_temp = (1 - noise_rate) * DecPomdpModel->TransFunc(sI, real_executed_JAI, s_newI);
        }
        else
        {
            pb_temp = pb_aRI_noise * DecPomdpModel->TransFunc(sI, real_executed_JAI, s_newI);
        }
        pb_res += pb_temp;
    }
    return pb_res;
};

double NoisyJointActionTransition(DecPomdpInterface *DecPomdpModel, double noise_rate, int sI, int JAI, int s_newI)
{
    // uniform distribution over rest actions
    double pb_aI_noise = noise_rate / (DecPomdpModel->GetSizeOfJointA() - 1);

    double pb_res = 0;
    for (int aI = 0; aI < DecPomdpModel->GetSizeOfJointA(); aI++)
    {
        double pb_temp;
        int real_executed_JAI = aI;

        if (real_executed_JAI == JAI)
        {
            pb_temp = (1 - noise_rate) * DecPomdpModel->TransFunc(sI, real_executed_JAI, s_newI);
        }
        else
        {
            pb_temp = pb_aI_noise * DecPomdpModel->TransFunc(sI, real_executed_JAI, s_newI);
        }
        pb_res += pb_temp;
    }
    return pb_res;
};

double ComputeR(DecPomdpInterface *Pb, BeliefSparse &b, int jaI)
{
    double sum_r = 0;
    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it_s;
    for (it_s = belief_sparse->begin(); it_s != belief_sparse->end(); it_s++)
    {
        int sI = it_s->first;
        double pb_sI = it_s->second;
        double r_temp = Pb->Reward(sI, jaI) * pb_sI;
        sum_r += r_temp;
    }

    return sum_r;
}

double ComputeNewValueWithSarsopDiffInitBelief(DecPomdpInterface *Pb, BeliefSparse &new_b, string sarsop_path, map<string, BeliefSparse> &out_map_id_to_belief, map<string, double> &out_map_belief_value)
{
    string id_new_b = new_b.computeID();
    if (!out_map_belief_value.count(id_new_b))
    {
        string result_pomdp_Path = "MpomdpDiffInitBelief.pomdp";
        ExportMpomdpModelWithDiffInitBelief(Pb, new_b, result_pomdp_Path);
        string command = sarsop_path + " -p " + to_string(0.01) + " " + result_pomdp_Path;
        const char *p = command.data();
        system(p);
        cout << "SARSOP launched to solve the MPOMDP!" << endl;
        const string sarsop_res_path = "out.policy";
        const string output_alphavecs_res_path = "AlphaVecsResult";
        transformToMADPformat(sarsop_res_path, output_alphavecs_res_path);
        vector<AlphaVector> Res_AlphaVecs = ImportValueFunction(output_alphavecs_res_path);

        PomdpInterface *mpomdp = new ParsedPOMDPSparse(result_pomdp_Path);
        double V_alphavecs = EvaluationWithAlphaVecs(mpomdp, Res_AlphaVecs);

        // Add all upper bound belief value
        FoundAllUpperBoundBeliefValue(mpomdp, Res_AlphaVecs, out_map_id_to_belief, out_map_belief_value);

        return V_alphavecs;
    }
    else
    {
        return out_map_belief_value[id_new_b];
    }
}

void GetPossibleJOIfromBeliefSparseAndJAI(DecPomdpInterface *Pb, BeliefSparse &b, map<int, double> &pb_JaI, map<int, double> &out_pb_JOI)
{
    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;
    map<int, double> dist_s_new;
    // 1. first get all possible s_newI with pb
    for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
    {
        double pr_sI = it->second;
        int sI = it->first;
        map<int, double>::iterator it_JAI;
        for (it_JAI = pb_JaI.begin(); it_JAI != pb_JaI.end(); it_JAI++)
        {
            int JaI = it_JAI->first;
            double pb_JaI = it_JAI->second;
            map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, JaI);
            map<int, double>::iterator it_trans;

            for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
            {
                int s_newI = it_trans->first;
                double pb_s_newI_temp = (it_trans->second) * pr_sI;
                if (pb_s_newI_temp > 0)
                {
                    // dist_s_new[s_newI] += pb_s_newI_temp * pb_JaI;
                    map<int, double> *temp_dist_obs = Pb->GetObsFuncProbDist(s_newI, JaI);
                    map<int, double>::iterator it_obs_dist;
                    for (it_obs_dist = temp_dist_obs->begin(); it_obs_dist != temp_dist_obs->end(); it_obs_dist++)
                    {
                        int JOI = it_obs_dist->first;
                        double pb_JOI_s_newI = it_obs_dist->second;
                        double pb_JOI_temp = pb_s_newI_temp * pb_JOI_s_newI * pb_JaI;
                        if (pb_JOI_temp > 0)
                        {
                            out_pb_JOI[JOI] += pb_JOI_temp;
                        }
                    }
                }
            }
        }
    }
}

double RoughEstimationOfV(DecPomdpInterface *Pb, BeliefSparse &new_b, double error_gap)
{
    int max_iter = 100;
    double discount = 1;
    int JAI_size = Pb->GetSizeOfJointA();
    // use a random rollout policy
    // srand((unsigned)time(0));
    BeliefSparse b = new_b;
    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it_s;

    double sum_r = 0;
    for (it_s = belief_sparse->begin(); it_s != belief_sparse->end(); it_s++)
    {
        int sI = it_s->first;
        double pb_sI = it_s->second;
        double v_sI = 0;
        for (int i = 0; i < max_iter; i++)
        {
            int jaI_random = rand() % JAI_size;
            double r_temp = Pb->Reward(sI, jaI_random);
            v_sI += discount * r_temp;
            sI = SampleNextStateIndex(Pb, sI, jaI_random);
            discount *= Pb->GetDiscount();
            if (discount < error_gap)
            {
                break;
            }
        }

        sum_r += v_sI * pb_sI;
    }

    return sum_r;
}

int SampleNextStateIndex(DecPomdpInterface *Pb, int current_sI, int jaI)
{
    map<int, double> *prob_dist_trans = Pb->GetTransProbDist(current_sI, jaI);
    map<int, double>::iterator it_trans;
    double temp_p = 0;
    double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);

    int s_newI = -1;
    for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
    {
        s_newI = it_trans->first;
        double pb_s_newI = it_trans->second;

        temp_p += pb_s_newI;
        // if the current sum_pb > rand, then select this new state
        if (temp_p >= random_p)
        {
            break;
        }
    }

    return s_newI;
}

vector<AlphaVector> SolvePomdpWithZmdp(string zmdp_path, string pomdp_path, string zmdp_policy_converter_path)
{
    string command = zmdp_path + " " + pomdp_path + " -o zmdp_result.policy";
    const char *p = command.data();
    system(p);

    string command2 = "python " + zmdp_policy_converter_path;
    const char *p2 = command2.data();
    system(p2);

    string out_alpha_vecs_path = "./zmdp_result_alpha_vecs";
    vector<AlphaVector> AlphaVecs = ImportValueFunction(out_alpha_vecs_path);

    return AlphaVecs;
}

map<int, double> compute_dist_p_oba(BeliefSparse &b, int a, PomdpInterface *pomdp)
{
    map<int, double> res;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
    {
        double pr_sI = it->second;
        int sI = it->first;

        unordered_map<int, double> *prob_dist_trans = pomdp->GetTransProbDist(sI, a);
        unordered_map<int, double>::iterator it_trans;

        for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
        {
            int s_newI = it_trans->first;
            double pr_s_newI = (it_trans->second);

            map<int, double> *prob_dist_obs = pomdp->GetObsFuncProbDist(s_newI, a);
            map<int, double>::iterator it_obs;

            for (it_obs = prob_dist_obs->begin(); it_obs != prob_dist_obs->end(); it_obs++)
            {
                int oI = it_obs->first;
                double pr_oI = it_obs->second;
                double pb = pr_sI * pr_s_newI * pr_oI;
                if (pb > 0)
                {
                    res[oI] += pb;
                }
            }
        }
    }

    return res;
}

double ComputeR(PomdpInterface *Pb, BeliefSparse &b, int jaI)
{
    double sum_r = 0;
    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it_s;
    for (it_s = belief_sparse->begin(); it_s != belief_sparse->end(); it_s++)
    {
        int sI = it_s->first;
        double pb_sI = it_s->second;
        double r_temp = Pb->Reward(sI, jaI) * pb_sI;
        sum_r += r_temp;
    }

    return sum_r;
}

BeliefSparse UpdateOneSideObservation(DecPomdpInterface *Pb, BeliefSparse &b, int robot_aI, map<int, double> &human_aI_dist, int robot_oI, double pr_or_ba)
{
    int SizeS = Pb->GetSizeOfS();
    map<int, double> pb_states;

    map<int, double>::iterator it_aHI;
    for (it_aHI = human_aI_dist.begin(); it_aHI != human_aI_dist.end(); it_aHI++)
    {
        int aHI = it_aHI->first;
        double pb_aHI = it_aHI->second;
        // human index = 0, robot index = 1
        vector<int> action_indices = {aHI, robot_aI};
        int JaI = Pb->IndividualToJointActionIndex(action_indices);
        map<int, double> *belief_sparse = b.GetBeliefSparse();
        map<int, double>::iterator it_sI;
        for (it_sI = belief_sparse->begin(); it_sI != belief_sparse->end(); it_sI++)
        {
            double pr_sI = it_sI->second;
            int sI = it_sI->first;

            map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, JaI);
            map<int, double>::iterator it_trans;
            for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
            {
                int s_newI = it_trans->first;
                double pb_s_newI = it_trans->second;

                map<int, double> *prob_dist_obs = Pb->GetObsFuncProbDist(s_newI, JaI);
                map<int, double>::iterator it_obs;

                for (it_obs = prob_dist_obs->begin(); it_obs != prob_dist_obs->end(); it_obs++)
                {
                    int JoI = it_obs->first;
                    double pr_JoI = it_obs->second;
                    vector<int> obs_indices = Pb->JointToIndividualObsIndices(JoI);
                    int oRI = obs_indices[1];

                    if (oRI == robot_oI)
                    {
                        double pr_snew_ao = (pr_sI * pb_s_newI * pr_JoI * pb_aHI) / pr_or_ba;
                        if (pr_snew_ao > 0)
                        {
                            pb_states[s_newI] += pr_snew_ao;
                        }
                    }
                }
            }
        }
    }
    BeliefSparse b_ao(pb_states, SizeS);
    return b_ao;
}

map<int, double> compute_dist_pr_or(DecPomdpInterface *Pb, BeliefSparse &b, int robot_aI, map<int, double> &human_aI_dist)
{
    // PrintMap(human_aI_dist);

    map<int, double> res;

    map<int, double>::iterator it_aHI;
    for (it_aHI = human_aI_dist.begin(); it_aHI != human_aI_dist.end(); it_aHI++)
    {
        int aHI = it_aHI->first;
        double pb_aHI = it_aHI->second;
        // human index = 0, robot index = 1
        vector<int> action_indices = {aHI, robot_aI};
        int JaI = Pb->IndividualToJointActionIndex(action_indices);
        map<int, double> *belief_sparse = b.GetBeliefSparse();
        map<int, double>::iterator it_sI;
        for (it_sI = belief_sparse->begin(); it_sI != belief_sparse->end(); it_sI++)
        {
            double pr_sI = it_sI->second;
            int sI = it_sI->first;

            map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, JaI);
            map<int, double>::iterator it_trans;
            for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
            {
                int s_newI = it_trans->first;
                double pb_s_newI = it_trans->second;

                map<int, double> *prob_dist_obs = Pb->GetObsFuncProbDist(s_newI, JaI);
                map<int, double>::iterator it_obs;

                for (it_obs = prob_dist_obs->begin(); it_obs != prob_dist_obs->end(); it_obs++)
                {
                    int JoI = it_obs->first;
                    double pr_JoI = it_obs->second;
                    vector<int> obs_indices = Pb->JointToIndividualObsIndices(JoI);
                    int oRI = obs_indices[1];
                    // cout << "pr_sI:" << pr_sI << endl;
                    // cout << "pb_s_newI:" << pb_s_newI << endl;
                    // cout << "pr_JoI:" << pr_JoI << endl;
                    // cout << "pr_aHI:" << pb_aHI << endl;

                    double pb = pr_sI * pb_s_newI * pr_JoI * pb_aHI;

                    res[oRI] += pb;
                }
            }
        }
    }

    return res;
}

BeliefSparse UpdateOneSideObservation(DecPomdpInterface *Pb, BeliefSparse &b, map<int, double> &dist_JAI, int human_oI, double pr_oh_ba, int agentIndex)
{
    // PrintMap(dist_JAI);
    // cout << pr_oh_ba << endl;

    int SizeS = Pb->GetSizeOfS();
    map<int, double> pb_states;

    map<int, double>::iterator it_JAI;
    for (it_JAI = dist_JAI.begin(); it_JAI != dist_JAI.end(); it_JAI++)
    {
        int JaI = it_JAI->first;
        double pb_JAI = it_JAI->second;
        map<int, double> *belief_sparse = b.GetBeliefSparse();
        map<int, double>::iterator it_sI;
        for (it_sI = belief_sparse->begin(); it_sI != belief_sparse->end(); it_sI++)
        {
            double pr_sI = it_sI->second;
            int sI = it_sI->first;

            map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, JaI);
            map<int, double>::iterator it_trans;
            for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
            {
                int s_newI = it_trans->first;
                double pb_s_newI = it_trans->second;

                map<int, double> *prob_dist_obs = Pb->GetObsFuncProbDist(s_newI, JaI);
                map<int, double>::iterator it_obs;

                for (it_obs = prob_dist_obs->begin(); it_obs != prob_dist_obs->end(); it_obs++)
                {
                    int JoI = it_obs->first;
                    double pr_JoI = it_obs->second;
                    vector<int> obs_indices = Pb->JointToIndividualObsIndices(JoI);
                    int oHI = obs_indices[agentIndex];

                    if (oHI == human_oI)
                    {
                        double pr_snew_ao = (pr_sI * pb_s_newI * pr_JoI * pb_JAI) / pr_oh_ba;
                        if (pr_snew_ao > 0)
                        {
                            pb_states[s_newI] += pr_snew_ao;
                        }
                    }
                }
            }
        }
    }

    // PrintMap(pb_states);

    BeliefSparse b_ao(pb_states, SizeS);
    return b_ao;
}

map<int, double> compute_dist_pr_oh(DecPomdpInterface *Pb, BeliefSparse &b, map<int, double> &dist_JAI, int agentIndex)
{

    // b.PrintBeliefSparse();
    // PrintMap(dist_JAI);
    map<int, double> res;

    map<int, double>::iterator it_JAI;
    for (it_JAI = dist_JAI.begin(); it_JAI != dist_JAI.end(); it_JAI++)
    {
        int JaI = it_JAI->first;
        double pb_JAI = it_JAI->second;
        map<int, double> *belief_sparse = b.GetBeliefSparse();
        map<int, double>::iterator it_sI;
        for (it_sI = belief_sparse->begin(); it_sI != belief_sparse->end(); it_sI++)
        {
            double pr_sI = it_sI->second;
            int sI = it_sI->first;

            map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, JaI);
            map<int, double>::iterator it_trans;
            for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
            {
                int s_newI = it_trans->first;
                double pb_s_newI = it_trans->second;

                map<int, double> *prob_dist_obs = Pb->GetObsFuncProbDist(s_newI, JaI);
                map<int, double>::iterator it_obs;

                for (it_obs = prob_dist_obs->begin(); it_obs != prob_dist_obs->end(); it_obs++)
                {
                    int JoI = it_obs->first;
                    double pr_JoI = it_obs->second;

                    vector<int> obs_indices = Pb->JointToIndividualObsIndices(JoI);
                    int oHI = obs_indices[agentIndex];
                    double pb = pr_sI * pb_s_newI * pr_JoI * pb_JAI;
                    if (pb > 0)
                    {
                        res[oHI] += pb;
                    }
                }
            }
        }
    }

    // PrintMap(res);

    return res;
}

double ComputeRWithHumanActionDist(DecPomdpInterface *Pb, BeliefSparse &b, int aRI, map<int, double> &human_aI_dist)
{
    double sum_r = 0;
    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it_s;
    for (it_s = belief_sparse->begin(); it_s != belief_sparse->end(); it_s++)
    {
        int sI = it_s->first;
        double pb_sI = it_s->second;
        map<int, double>::iterator it_aHI;
        for (it_aHI = human_aI_dist.begin(); it_aHI != human_aI_dist.end(); it_aHI++)
        {
            int aHI = it_aHI->first;
            double pb_aHI = it_aHI->second;
            // human index = 0, robot index = 1
            vector<int> action_indices = {aHI, aRI};
            int JaI = Pb->IndividualToJointActionIndex(action_indices);
            double r_temp = Pb->Reward(sI, JaI) * pb_sI * pb_aHI;
            sum_r += r_temp;
        }
    }

    return sum_r;
}

void PrintMap(map<int, double> &m)
{
    map<int, double>::iterator it;
    for (it = m.begin(); it != m.end(); it++)
    {
        int key = it->first;
        double value = it->second;
        cout << key << ": " << value << ", ";
    }
    cout << endl;
}

void PrintMap(map<string, double> &m)
{
    map<string, double>::iterator it;
    for (it = m.begin(); it != m.end(); it++)
    {
        string key = it->first;
        double value = it->second;
        cout << key << ": " << value << ", ";
    }
    cout << endl;
}

double ComputeNorm1Distance(BeliefSparse &belief1, BeliefSparse &belief2)
{
    map<int, double> *b1 = belief1.GetBeliefSparse();
    map<int, double> *b2 = belief2.GetBeliefSparse();

    set<int> all_sI;

    double res_distance = 0;

    map<int, double>::iterator it;
    for (it = b1->begin(); it != b1->end(); it++)
    {
        int sI = it->first;
        all_sI.insert(sI);
    }
    for (it = b2->begin(); it != b2->end(); it++)
    {
        int sI = it->first;
        all_sI.insert(sI);
    }

    set<int>::iterator it_sI;
    for (it_sI = all_sI.begin(); it_sI != all_sI.end(); ++it_sI)
    {
        int sI = *it_sI;
        double pb_sI_b1 = 0;
        double pb_sI_b2 = 0;
        if (b1->count(sI))
        {
            pb_sI_b1 = b1->find(sI)->second;
        }
        if (b2->count(sI))
        {
            pb_sI_b2 = b2->find(sI)->second;
        }

        double norm1_dis_sI = abs(pb_sI_b1 - pb_sI_b2);
        res_distance += norm1_dis_sI;
    }

    return res_distance;
}

void FoundAllUpperBoundBeliefValue(PomdpInterface *Pb, vector<AlphaVector> &alpha_vecs, map<string, BeliefSparse> &out_map_id_to_belief, map<string, double> &out_map_belief_value)
{

    vector<string> all_actions = Pb->GetAllActions();

    map<int, double> *b0 = Pb->GetInitBeliefSparse();
    BeliefSparse b(*b0, Pb->GetSizeOfS());
    string id_b = b.computeID();

    vector<BeliefSparse> Unprocessed_beliefs;

    if (!out_map_belief_value.count(id_b))
    {
        Unprocessed_beliefs.push_back(b);
    }

    while (!Unprocessed_beliefs.empty())
    {

        BeliefSparse b_temp = Unprocessed_beliefs.front();
        string id_b_temp = b_temp.computeID();
        b_temp.PrintBeliefSparse();
        cout << "id:" << id_b_temp << endl;

        double value = 0;
        int max_alpha_index = argmax_alpha(alpha_vecs, b_temp, value);
        int aI = alpha_vecs[max_alpha_index].GetActionIndex();
        cout << "aI: " << all_actions[aI] << endl;

        out_map_belief_value[id_b_temp] = value;
        out_map_id_to_belief[id_b_temp] = b_temp;

        for (int OI = 0; OI < Pb->GetSizeOfObs(); OI++)
        {
            double pr_oba = compute_p_oba(OI, b_temp, aI, Pb);
            if (pr_oba == 0)
            {
                continue;
            }
            else
            {
                cout << "oI: " << aI << endl;

                BeliefSparse b_new = Update(Pb, b_temp, aI, OI, pr_oba);
                string id = b_new.computeID();

                b_new.PrintBeliefSparse();

                if (!out_map_belief_value.count(id))
                {
                    Unprocessed_beliefs.push_back(b_new);
                }
            }
        }

        Unprocessed_beliefs.erase(Unprocessed_beliefs.begin());
    }

    // Print the result
    map<string, BeliefSparse>::iterator it;
    for (it = out_map_id_to_belief.begin(); it != out_map_id_to_belief.end(); it++)
    {
        string id = it->first;
        BeliefSparse b = it->second;
        cout << "Belief:";
        b.PrintBeliefSparse();
        cout << "value:" << out_map_belief_value[id] << endl;
    }
}

int SampleAKeyFromMap(map<int, double> &m)
{
    double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    double temp_p = 0;

    map<int, double>::iterator it;

    int key_selected = -1;
    for (it = m.begin(); it != m.end(); it++)
    {
        key_selected = it->first;
        temp_p += it->second;
        if (temp_p >= random_p)
        {
            break;
        }
    }
    return key_selected;
}

void RenormalizeMap(map<int, double> &m, double min_pb)
{
    map<int, double> res;
    map<int, double>::iterator it;
    double sum_pb = 0;
    int nb = 0;
    for (it = m.begin(); it != m.end(); it++)
    {
        int key = it->first;
        double pb = it->second;
        if (pb > min_pb)
        {
            res[key] = pb;
            sum_pb += pb;
            nb += 1;
        }
    }

    for (it = res.begin(); it != res.end(); it++)
    {
        int key = it->first;
        double pb = it->second;
        res[key] = pb / sum_pb;
        // res[key] = 1.0 / nb;
    }
    m = res;
}

void FreeAllPomdpModels(vector<PomdpInterface *> &all_brms)
{
    for (size_t i = 0; i < all_brms.size(); i++)
    {
        delete all_brms[i];
    }

    all_brms.clear();
}
#include "../Include/SoftMaxDist.h"


// return the value at b
double SoftmaxActions(DecPomdpInterface *Pb, BeliefSparse &b, map<int, map<int, double>> &dist_aHI_to_JAIs,
                      map<int, double> &pb_local_actions, int agentIndex, double T, double min_pb, MCTS &mcts)
{
    int size_A = Pb->GetSizeOfJointA();
    // map<int, double> belief_Q;
    // mcts.Plan(b, belief_Q);

    map<int, double> temp_pb_JAI;
    map<int, double> temp_pb_aHI;
    map<int, double> Q_JAI_map;

    double sum_res_JAI = 0;
    double v_max = -DBL_MAX;

    for (int jaI = 0; jaI < size_A; jaI++)
    {
        // double Q = belief_Q[jaI];
        double Q = ComputeQ(Pb, b, jaI, mcts);
        Q_JAI_map[jaI] = Q;
        double fraction = (float)Q / T; // be attention T cannot be 0
        double temp = exp(fraction);
        sum_res_JAI += temp;
        temp_pb_JAI[jaI] = temp;

        if (Q > v_max)
        {
            v_max = Q;
        }
    }

    if (T == 0)
    {
        // only optimal actions!
        sum_res_JAI = 0;
        temp_pb_JAI.clear();
        int nb_optimal_JAI = 0;
        vector<int> optimal_jaIs;
        for (int jaI = 0; jaI < size_A; jaI++)
        {
            if (Q_JAI_map[jaI] == v_max)
            {
                nb_optimal_JAI += 1;
                optimal_jaIs.push_back(jaI);
            }
        }

        for (size_t i = 0; i < optimal_jaIs.size(); i++)
        {
            double temp = (float)1.0 / nb_optimal_JAI;
            sum_res_JAI += temp;
            temp_pb_JAI[optimal_jaIs[i]] = temp;
        }
    }

    for (int jaI = 0; jaI < size_A; jaI++)
    {
        temp_pb_JAI[jaI] = temp_pb_JAI[jaI] / sum_res_JAI;
        int local_aI = Pb->JointToIndividualActionsIndices(jaI)[agentIndex];
        temp_pb_aHI[local_aI] += temp_pb_JAI[jaI];
        dist_aHI_to_JAIs[local_aI][jaI] = temp_pb_JAI[jaI];
    }

    for (int jaI = 0; jaI < size_A; jaI++)
    {
        int local_aI = Pb->JointToIndividualActionsIndices(jaI)[agentIndex];
        dist_aHI_to_JAIs[local_aI][jaI] = dist_aHI_to_JAIs[local_aI][jaI] / temp_pb_aHI[local_aI];
    }

    // eliminate low prob human actions, and normalize aHI
    pb_local_actions = temp_pb_aHI;

    RenormalizeMap(pb_local_actions, min_pb);

    // elinimnate low prob joint actions and renomalize them
    map<int, double>::iterator it;
    for (it = pb_local_actions.begin(); it != pb_local_actions.end(); it++)
    {
        int aHI = it->first;
        RenormalizeMap(dist_aHI_to_JAIs[aHI], min_pb);
    }

    return v_max;
}

double ComputeQ(DecPomdpInterface *Pb, BeliefSparse &b, int jaI, MCTS &mcts)
{
    double r = ComputeR(Pb, b, jaI);
    double discount = Pb->GetDiscount();
    // int size_obs = Pb->GetSizeOfJointObs();
    double sum_future_value = 0;
    map<int, double> dist_pr_oba = compute_dist_p_oba(b, jaI, Pb);
    map<int, double>::iterator it_oI;

    for (it_oI = dist_pr_oba.begin(); it_oI != dist_pr_oba.end(); it_oI++)
    {
        int oI = it_oI->first;
        double pr_oba = it_oI->second;
        BeliefSparse b_updated = Update(Pb, b, jaI, oI, pr_oba);
        double nextV_estimate = 0;
        mcts.Plan(b_updated, nextV_estimate);
        sum_future_value += pr_oba * nextV_estimate;
    }

    double Q = r + discount * sum_future_value;

    return Q;
}
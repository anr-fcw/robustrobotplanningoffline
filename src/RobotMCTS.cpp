#include "../Include/RobotMCTS.h"

RobotMCTS::RobotMCTS(DecPomdpInterface *pb, vector<AlphaVector> &alpha_vecs, int human_index, int robot_index, MCTS &mcts)
{
      this->pb = pb;
      this->discount = pb->GetDiscount();
      this->alpha_vecs = alpha_vecs;
      this->HumanIndex = human_index;
      this->RobotIndex = robot_index;
      this->size_A = pb->GetSizeOfA(RobotIndex);
      this->mcts = &mcts;
      // this->current_node_number += 1;
}

TreeNode *RobotMCTS::Selection(TreeNode *start_node)
{

      // should do
      // 1. First select action according to average observation prob to compute Q
      // 2. POMCP using particles (states) not directly belief
      // 3. Select action using UCB

      // check if have children
      map<pair<int, int>, TreeNode *> *childs = start_node->GetChildNodes();
      if (childs->size() != 0)
      {
            // sampling according to childs' value
            map<pair<int, int>, TreeNode *>::iterator it;
            TreeNode *next_node = nullptr;
            double sum_value = 0;
            for (it = childs->begin(); it != childs->end(); it++)
            {
                  TreeNode *child_node = it->second;
                  double value_child_node = child_node->GetValue();
                  sum_value += value_child_node;
            }

            double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
            double temp_p = 0;
            for (it = childs->begin(); it != childs->end(); it++)
            {
                  TreeNode *child_node = it->second;
                  double value_child_node = child_node->GetValue();
                  double pr_selection = value_child_node / sum_value;
                  temp_p += pr_selection;
                  if (temp_p >= random_p)
                  {
                        next_node = child_node;
                        break;
                  }
            }

            // recursive depth search until the leaf node
            return this->Selection(next_node);
      }
      else
      {
            return start_node;
      }
}

void RobotMCTS::Expansion(TreeNode *node)
{
      // cout << node << endl;
      BeliefSparse b = node->GetBeliefSparse();
      map<int, map<int, double>> pb_joint_acions;
      map<int, double> pb_human_acions;
      double T = 1; // used for softmax
      double min_pb = 0.0;
      SoftmaxActions(this->pb, b, pb_joint_acions, pb_human_acions, HumanIndex, T, min_pb, *this->mcts);
      node->SetHumanActionsDist(pb_human_acions);

      for (int aI = 0; aI < size_A; aI++)
      {

            map<int, double> dist_pr_or_ba = compute_dist_pr_or(pb, b, aI, pb_human_acions);
            node->SetPrOrOba(aI, dist_pr_or_ba);

            // cout << dist_pr_or_ba.size() << endl;
            // PrintMap(dist_pr_or_ba);

            map<int, double>::iterator it_oRI;
            for (it_oRI = dist_pr_or_ba.begin(); it_oRI != dist_pr_or_ba.end(); it_oRI++)
            {
                  int oRI = it_oRI->first;
                  double pr_or_ba = it_oRI->second;

                  BeliefSparse new_b = UpdateOneSideObservation(pb, b, aI, pb_human_acions, oRI, pr_or_ba);
                  string id_new_b = new_b.GetID();
                  TreeNode *child_node = new TreeNode(new_b, pr_or_ba, id_new_b);
                  // add parent
                  child_node->AddParentNode(node);
                  // do a simulation to estimate the value of next node(b_new)
                  // double esti_value = this->Simulation(new_b, this->nb_restarts_simulation, this->epsilon);

                  // directly use alphavecs to estimate the value
                  double esti_value = best_value(alpha_vecs, new_b);

                  // compute the estimate Q
                  child_node->SetValue(esti_value);
                  // add this child node
                  node->AddChildNode(aI, oRI, child_node);
                  this->current_node_number += 1;
            }
      }

      // Backpropagate the value to all parent nodes
      this->BackPropagation(node);
}
void RobotMCTS::BackPropagation(TreeNode *node)
{
      BeliefSparse b = node->GetBeliefSparse();
      map<int, double> pb_human_actions = node->GetHumanActionDist();

      // must have childs, the node which just finish expansion
      double max_Q = -DBL_MAX;
      for (int aI = 0; aI < size_A; aI++)
      {
            double esti_Q = ComputeRWithHumanActionDist(this->pb, b, aI, pb_human_actions);
            map<int, double> dist_pr_or_ba = node->GetPrOrOba(aI);

            map<int, double>::iterator it_oI;
            for (it_oI = dist_pr_or_ba.begin(); it_oI != dist_pr_or_ba.end(); it_oI++)
            {
                  int oI = it_oI->first;
                  double pr_oba = it_oI->second;
                  double v = node->GetChildNodeValue(aI, oI);
                  esti_Q += discount * v * pr_oba;
            }

            if (esti_Q > max_Q)
            {
                  max_Q = esti_Q;
                  node->SetBestAction(aI);
            }
      }

      node->SetValue(max_Q);

      if (!node->isRoot())
      {
            this->BackPropagation(node->GetParentNode());
      }
}

int RobotMCTS::Plan(BeliefSparse b, double &value)
{
      string id = b.GetID();
      TreeNode start_node(b, 1.0, id);
      this->rootnode = start_node;
      this->current_node_number = 0;

      while (this->current_node_number < this->max_node_number)
      {
            TreeNode *node = this->Selection(&this->rootnode);
            // cout << "search finished" << endl;
            Expansion(node);
            // cout << "expansion finished" << endl;

            // cout << "current node number:" << this->current_node_number << endl;
            // cout << "max node number:" << this->max_node_number << endl;
      }

      double v_b0 = this->rootnode.GetValue();
      // cout << "the value at b0:" << v_b0 << endl;
      int best_aI = this->rootnode.GetBestAction();
      // cout << "the best action is: " << this->pb->GetAllActions()[best_aI] << endl;

      value = v_b0;

      return best_aI;
}
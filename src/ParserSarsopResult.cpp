/* This file has been written and/or modified by the following people:
 *
 * Anonymous for AAMAS reviewing process
 * 
 */

#include "../Include/ParserSarsopResult.h"

void transformToMADPformat(string sarsop_path, string output_path)
{
    const char *p = sarsop_path.data();
    TiXmlDocument mydoc(p);         //xml文档对象
    bool loadOk = mydoc.LoadFile(); //加载文档
    ofstream fp(output_path.c_str());
    if (!loadOk)
    {
        cout << "could not load the sarsop result file :" << mydoc.ErrorDesc() << endl;
        exit(1);
    }

    TiXmlElement *RootElement = mydoc.RootElement(); //根元素, Info
    TiXmlElement *pEle = RootElement;

    //遍历该结点
    for (TiXmlElement *StuElement = pEle->FirstChildElement(); //第一个子元素
         StuElement != NULL;
         StuElement = StuElement->NextSiblingElement()) //下一个兄弟元素
    {
        //输出子元素的值
        for (TiXmlElement *sonElement = StuElement->FirstChildElement();
             sonElement;
             sonElement = sonElement->NextSiblingElement())
        {
            TiXmlAttribute *pAttr = sonElement->FirstAttribute(); //第一个属性
            fp << pAttr->Value() << " -1" << endl;
            fp << sonElement->FirstChild()->Value() << endl;
            fp << endl;
        }
    }

    fp.close();
}
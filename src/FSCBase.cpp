/* This file has been written and/or modified by the following people:
 *
 * Anonymous for AAMAS reviewing process
 * 
 */

#include "../Include/FSCBase.h"
#define Randmod(x) rand() % x
using namespace std;

FSCBase::FSCBase(const string filename, int ObsSize, int type)
{
    this->type = type;
    ifstream infile;
    infile.open(filename);
    if (!infile.is_open())
        cout << "open file failure" << endl;

    string temp;
    double sum_eta = 0;

    bool ReadNodes = false; // end at here
    // First get agent name, then all nodes' action indicies and read transitions.
    while (getline(infile, temp) && !ReadNodes)
    {
        istringstream is(temp);
        string s;
        int temp_num = 0;
        bool ReadAgent = false;
        while (is >> s)
        {
            if (s == "agent:")
            {
                ReadAgent = true;
            }
            else if (s == "nodes:")
            {
                ReadNodes = true;
            }

            if (ReadAgent && temp_num == 1)
            {
                this->AgentName = s;
            }

            if (ReadNodes && temp_num > 0)
            {
                this->Nodes.push_back(stoi(s));
            }
            temp_num += 1;
        }
    }
    infile.close();

    this->SizeObs = ObsSize;
    this->SizeNodes = this->Nodes.size();
    // this->eta.resize(SizeNodes*SizeObs*SizeNodes);
    // vector< vector< vector<double> > > T(this->Nodes.size(), vector<vector<double> >(ObsSize, vector<double>(this->Nodes.size()) ));
    this->eta.resize(SizeNodes, vector<map<int, double>>(SizeObs));

    infile.open(filename);
    // Get T
    while (getline(infile, temp))
    {
        istringstream is(temp);
        string s;
        int temp_num = 0;
        bool buildTrans = false;
        int oI = 0;
        int nI = 0;
        int n_newI = 0;
        double pb = 0;
        while (is >> s)
        {
            if (s == "T:")
            {
                buildTrans = true;
            }

            if (temp_num == 1 && buildTrans)
            {
                oI = stoi(s);
            }
            else if (temp_num == 3 && buildTrans)
            {
                nI = stoi(s);
            }
            else if (temp_num == 5 && buildTrans)
            {
                n_newI = stoi(s);
            }
            else if (temp_num == 6 && buildTrans)
            {
                pb = stod(s);
                // int Index = nI*SizeObs*SizeNodes + oI*SizeNodes + n_newI;
                // this->eta[Index] = pb;
                // T[nI][oI][n_newI] = pb;
                // if (pb > 0)
                // {
                //     /* code */
                // }

                this->eta[nI][oI].insert(std::make_pair(n_newI, pb));
                sum_eta += pb;
            }

            temp_num += 1;
        }
    }

    infile.close();
    compute_label(sum_eta);
}

// used for random a FSC
FSCBase::FSCBase(int MaxNodeSize, int ActionsSize, int ObsSize, int type)
{
    // type 0 for Momdp formalization, 1 for "init node" method
    this->AgentName = "RandomAgentFSC";
    this->type = type;
    this->SizeObs = ObsSize;

    // int nI_start = 0;
    if (type == 0)
    {
        // At least we need to have one start node
        this->SizeNodes = Randmod(MaxNodeSize) + 1; //
        if (this->SizeNodes > MaxNodeSize)
        {
            // Make sure the FSC nodes number (not include the preliminary node) < MaxNodesSize
            this->SizeNodes = MaxNodeSize;
        }

        // this->eta.resize(SizeNodes*ObsSize*SizeNodes);
        this->eta.resize(SizeNodes, vector<map<int, double>>(SizeObs));
    }
    else if (type == 1)
    {
        // We have a preliminary node. Thus, at least we need to have two nodes (1 preliminary node, 1 start node)
        this->SizeNodes = Randmod(MaxNodeSize) + 2; //
        if (this->SizeNodes > MaxNodeSize + 1)
        {
            // Make sure the FSC nodes number (not include the preliminary node) < MaxNodesSize
            this->SizeNodes = MaxNodeSize + 1;
        }
        // this->eta.resize(SizeNodes*ObsSize*SizeNodes);
        this->eta.resize(SizeNodes, vector<map<int, double>>(SizeObs));

        // Init node to start
        for (int oI = 0; oI < SizeObs; oI++)
        {
            // int Index = oI*SizeNodes + 1;
            // this->eta[Index] = 1;
            // this->eta[0][oI][1] = 1;
            this->eta[0][oI].insert(std::make_pair(1, 1));
        }

        // nI_start = 1;
    }
    else
    {
        cerr << "Wrong type argument!" << endl;
        throw("");
    }

    vector<int> Nodes_Init(this->SizeNodes);
    this->Nodes = Nodes_Init;

    // for (int nI = nI_start; nI < SizeNodes; nI++)
    // {
    //     int random_oI = -1;
    //     if (nI < SizeNodes - 1)
    //     {
    //         random_oI = Randmod(ObsSize);
    //         int Index = nI*SizeObs*SizeNodes + random_oI*SizeNodes + nI + 1;
    //         this->eta[Index] = 1;
    //         // this->eta[nI][random_oI][nI+1] = 1;
    //     }

    //     for (int oI = 0; oI < ObsSize; oI++)
    //     {
    //         if (oI == random_oI)
    //         {
    //             continue;
    //         }else
    //         {
    //             int random_nI = Randmod(SizeNodes);
    //             if (random_nI == 0 && type == 1)
    //             {
    //                 random_nI = 1;
    //             }

    //             int Index = nI*SizeObs*SizeNodes + oI*SizeNodes + random_nI;
    //             this->eta[Index] = 1;

    //             // this->eta[nI][oI][random_nI] = 1;
    //         }
    //     }

    //     int random_aI = Randmod(ActionsSize);
    //     this->Nodes[nI] = random_aI;

    // }

    int element_size_matrix_nodes_actions_pb_dist = SizeNodes * ActionsSize;
    vector<double> matrix_nodes_actions_pb_dist(element_size_matrix_nodes_actions_pb_dist, (1.0 / element_size_matrix_nodes_actions_pb_dist));

    int element_size_matrix_nodes_transition_pb_dist = SizeNodes * ObsSize * SizeNodes;

    vector<double> matrix_nodes_transition_pb_dist(element_size_matrix_nodes_transition_pb_dist, (1.0 / element_size_matrix_nodes_transition_pb_dist));
    GenerateFSCwithParameters(ActionsSize, ObsSize, matrix_nodes_actions_pb_dist, matrix_nodes_transition_pb_dist);
};

FSCBase::FSCBase(int SizeNodes, int ActionsSize, int ObsSize, int type, vector<double> &matrix_nodes_actions_pb_dist, vector<double> &matrix_nodes_transition_pb_dist)
{
    // type 0 for Momdp formalization, 1 for "init node" method
    this->AgentName = "RandomAgentFSC";
    this->type = type;
    this->SizeObs = ObsSize;

    this->SizeNodes = SizeNodes; //

    // int nI_start = 0;
    if (type == 0)
    {
        // At least we need to have one start node
        this->eta.resize(SizeNodes, vector<map<int, double>>(SizeObs));

        // this->eta.resize(SizeNodes*ObsSize*SizeNodes);
    }
    else if (type == 1)
    {
        // We have a preliminary node. Thus, at least we need to have two nodes (1 preliminary node, 1 start node)

        // this->eta.resize(SizeNodes*ObsSize*SizeNodes);
        this->eta.resize(SizeNodes, vector<map<int, double>>(SizeObs));

        // Init node to start
        for (int oI = 0; oI < SizeObs; oI++)
        {
            // int Index = oI*SizeNodes + 1;
            // this->eta[Index] = 1;
            // this->eta[0][oI][1] = 1;
            this->eta[0][oI].insert(std::make_pair(1, 1));
        }

        // nI_start = 1;
    }
    else
    {
        cerr << "Wrong type argument!" << endl;
        throw("");
    }

    vector<int> Nodes_Init(this->SizeNodes);
    this->Nodes = Nodes_Init;

    GenerateFSCwithParameters(ActionsSize, ObsSize, matrix_nodes_actions_pb_dist, matrix_nodes_transition_pb_dist);

}; // random FSC with given paramter vecs

void FSCBase::GenerateFSCwithParameters(int ActionsSize, int ObsSize, vector<double> &matrix_nodes_actions_pb_dist, vector<double> &matrix_nodes_transition_pb_dist)
{
    srand(time(NULL));
    rand();
    cout << "------" << endl;
    for (int ni = 0; ni < this->SizeNodes; ni++)
    {

        cout << "- ni:" << ni << "-" << endl;
        // Sample an action for each node
        double random_p_ai = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
        double temp_p_ai = 0;

        cout << "random_p_ai:" << random_p_ai << endl;

        for (int ai = 0; ai < ActionsSize; ai++)
        {
            int Index = ni * ActionsSize + ai;
            temp_p_ai += matrix_nodes_actions_pb_dist[Index];

            cout << temp_p_ai << endl;

            if (temp_p_ai >= random_p_ai)
            {
                cout << "found!"
                     << "ni:" << ni << ",ai:" << ai << endl;
                this->Nodes[ni] = ai;
                break;
            }
        }

        // Sample a transition for each observation

        cout << "random_p_trans:" << random_p_ai << endl;

        for (int oi = 0; oi < ObsSize; oi++)
        {
            double temp_p_trans_max = 0;
            for (int n_nexti = 0; n_nexti < this->SizeNodes; n_nexti++)
            {
                int Index = ni * SizeObs * SizeNodes + oi * SizeNodes + n_nexti;
                temp_p_trans_max += matrix_nodes_transition_pb_dist[Index];
            }

            double random_p_trans = RandT<double>(0.0, temp_p_trans_max);
            double temp_p_trans = 0;

            for (int n_nexti = 0; n_nexti < this->SizeNodes; n_nexti++)
            {
                int Index = ni * SizeObs * SizeNodes + oi * SizeNodes + n_nexti;
                temp_p_trans += matrix_nodes_transition_pb_dist[Index];

                cout << temp_p_trans << endl;

                if (temp_p_trans >= random_p_trans)
                {
                    cout << "found!"
                         << "ni:" << ni << ",oi:" << oi << ", n_nexti:" << n_nexti << endl;
                    // this->eta[Index] = 1;
                    this->eta[ni][oi].insert(std::make_pair(n_nexti, 1));

                    break;
                }
            }
        }
    }
};

FSCBase::~FSCBase(){};

string FSCBase::GetAgentDescription()
{
    return this->AgentName;
};

int FSCBase::GetActionIndexForNodeI(int nodeI)
{
    return this->Nodes[nodeI];
};

int FSCBase::GetNodesSize()
{
    return this->SizeNodes;
};

// FSC transition probability
double FSCBase::ProbTrans(int nI, int oI, int n_newI)
{
    // Use this method to avoid return nan value, because some links between nodes are not exist!
    // int Index = nI*SizeObs*SizeNodes + oI*SizeNodes + n_newI;
    // return this->eta[Index];
    return this->eta[nI][oI][n_newI];
};

// Sample a next node from current node and observation
int FSCBase::SampleToNewNodeI(int nI, int oI, double d_rand)
{
    double temp_p = 0;

    int n_newI = -1;
    // sparse representation for observation (next o')
    map<int, double> *prob_dist = GetNodeTransProbDist(nI, oI);
    map<int, double>::iterator it;

    for (it = prob_dist->begin(); it != prob_dist->end(); it++)
    {
        temp_p = it->second;
        if (temp_p >= d_rand)
        {
            n_newI = it->first;
            break;
        }
    }

    // for (size_t n_newI = 0; n_newI < this->Nodes.size(); n_newI++)
    // {
    //     int Index = nI*SizeObs*SizeNodes + oI*SizeNodes + n_newI;
    //     temp_p += this->eta[Index];
    //     if (temp_p >= d_rand)
    //     {
    //         return n_newI;
    //     }
    // }

    // Return -1 for wrong output
    return n_newI;
};

void FSCBase::PrintGraph(DecPomdpInterface *DecPomdp, int agentI)
{
    cout << endl;
    cout << " -------- " << endl;
    cout << "digraph agent_" << agentI << " {" << endl;
    // define nodes in Graph
    // cout << "nodes size:"<< this->Nodes.size()<< endl;
    // cout << "type:"<< this->type << endl;
    for (unsigned int i = 0; i < this->Nodes.size(); i++)
    {
        if (i == 0 && type == 1)
        {
            cout << "n" << i << " [label = \" Init Node \"]" << endl;
        }
        else
        {
            // cout << "n" <<i << "[label = \" aH:  " << this->Nodes[i].GetHumanAction() << ", a_joint:  " << this->Nodes[i].GetJointAction() << "\"]" <<endl;
            cout << "n" << i << "[label = \" a:  " << DecPomdp->GetActionVec(agentI)[Nodes[i]] << "\"]" << endl;
        }
    }
    cout << endl;

    for (int nI = 0; nI < int(this->Nodes.size()); nI++)
    {
        // node n_new = this->Nodes[i];
        for (int OI = 0; OI < DecPomdp->GetSizeOfObs(agentI); OI++)
        {

            // sparse representation for observation (next o')
            map<int, double> *prob_dist = GetNodeTransProbDist(nI, OI);
            map<int, double>::iterator it;

            for (it = prob_dist->begin(); it != prob_dist->end(); it++)
            {
                int n_newI = it->first;
                double pr_trans = it->second;
                if (nI != n_newI)
                {
                    cout << "n" << nI << " -> "
                         << "n" << n_newI << "[label = \"o: " << DecPomdp->GetObservationVec(agentI)[OI] << ", pb: " << pr_trans << " \"]" << endl;
                }
            }
            // for(int n_newI = 0; n_newI < int(this->Nodes.size()); n_newI++){
            //     // node n = this->Nodes[j];
            //     int Index = nI*SizeObs*SizeNodes + OI*SizeNodes + n_newI;
            //     double pr_trans = this->eta[Index];
            //     // double pr_trans = this->eta[nI][OI][n_newI];
            //     // Dont print self loops
            //     // if ( pr_trans > 0 )
            //     if ( pr_trans > 0 && nI != n_newI)
            //     {
            //         // decpomdp->GetObservation(HumanIndex, OHI)->GetName(); // Dont work
            //         // decpomdp->GetObservationName(OHI, HumanIndex)  ; // Dont work
            //         //  cout << "n"<< nI << " -> " << "n"<< n_newI <<"[label = \"oh: "<< OHI <<", pb: "<<pr_trans <<" \"]"<< endl;
            //         cout << "n"<< nI << " -> " << "n"<< n_newI <<"[label = \"o: "<< DecPomdp->GetObservationVec(agentI)[OI] <<", pb: "<<pr_trans <<" \"]"<< endl;
            //     }
            // }
        }
    }

    cout << "}" << endl;
}

void FSCBase::ExportFSC(string filename)
{
    ofstream fp(filename.c_str());
    fp << "agent: "
       << "AgentI" << endl;
    fp << "nodes: ";

    int nI_start = 0;
    if (type == 1)
    {
        nI_start = 1;
        fp << "99999 ";
    }

    for (int nI = nI_start; nI < SizeNodes; nI++)
    {
        fp << this->Nodes[nI] << " ";
    }
    fp << endl;

    // T: obs_I : start-node_I : end-node_I %f

    for (int nI = 0; nI < SizeNodes; nI++)
    {
        for (int oI = 0; oI < this->SizeObs; oI++)
        {

            // sparse representation for observation (next o')
            map<int, double> *prob_dist = GetNodeTransProbDist(nI, oI);
            map<int, double>::iterator it;

            for (it = prob_dist->begin(); it != prob_dist->end(); it++)
            {
                int n_newI = it->first;
                double pr_trans = it->second;
                fp << "T: " << oI << " : " << nI << " : " << n_newI << " " << pr_trans << endl;
            }

            // for (int n_newI = 0; n_newI < SizeNodes; n_newI++)
            // {
            //     // check prob is not 0
            //     int Index = nI*SizeObs*SizeNodes + oI*SizeNodes + n_newI;
            //     double temp_pr = this->eta[Index];
            //     if (temp_pr>0)
            //     {
            //         fp << "T: "<< oI <<" : "<< nI <<" : " << n_newI << " " << temp_pr << endl;
            //     }

            // }
        }
    }
}

// Not sure this implementation is correct

void FSCBase::compute_label(double sum_eta)
{
    int sum_nodes = 0;

    for (auto &n : this->Nodes)
        sum_nodes += n;

    this->label = int(sum_eta) + sum_nodes;
}

bool FSCBase::operator<(const FSCBase &fsc) const
{
    return this->label < fsc.label;
};

// Used for policy evaluation
int FSCBase::NextNode(int nI, int oI)
{
    // need change n_nextI start from 0

    // sparse representation for observation (next o')
    map<int, double> *prob_dist = GetNodeTransProbDist(nI, oI);

    return prob_dist->begin()->first;

    // for (int n_nextI = 0; n_nextI < this->SizeNodes; n_nextI++)
    // {
    //     int Index = nI*SizeObs*SizeNodes + oI*SizeNodes + n_nextI;

    //     if (this->eta[Index])
    //     {
    //         return n_nextI;
    //     }

    // }
    // return -1;
};

map<int, double> *FSCBase::GetNodeTransProbDist(int nI, int oI)
{
    return &this->eta[nI][oI];
};

double FSCBase::PolicyEvaluation(PomdpInterface *pomdp, double error_gap)
{
    bool conv = false;
    int iter = 0;
    double gamma = pomdp->GetDiscount();
    vector<AlphaVector> V_a = BuildInitValueFromFSC(pomdp);
    cout << "State Size: " << pomdp->GetSizeOfS() << endl;
    clock_t startTime, endTime;
    while (!conv)
    {
        double max_norm = 0;
        startTime = clock();
        max_norm = IterValueFunc(pomdp, V_a, gamma);
        endTime = clock();
        cout << "Iter: " << iter << ", error: " << max_norm << ", T: " << (double)(endTime - startTime) / CLOCKS_PER_SEC << "s" << endl;
        if (max_norm < error_gap)
        {
            conv = true;
            cout << "Converged at iter " << iter << endl;
        }
        iter++;
    }
    return EvaluationWithAlphaVecs(pomdp, V_a);
};

double FSCBase::IterValueFunc(PomdpInterface *pomdp, vector<AlphaVector> &V, double gamma)
{
    double max_norm = 0;
    PomdpInterface *Pb = pomdp;
    int SizeNode = this->SizeNodes;
    int SizeS = Pb->GetSizeOfS();
    // int SizeO = Pb->GetSizeOfObs();
    int nI_start = 0;

    for (int nI = nI_start; nI < SizeNode; nI++)
    {
        int aI = GetActionIndexForNodeI(nI);
        for (int sI = 0; sI < SizeS; sI++)
        {
            double alpha_nI_sI_temp = Pb->Reward(sI, aI);
            double sum_temp = 0;

            // sparse representation
            // map<int, double>* prob_dist_trans = Pb->GetTransProbDist(sI,aI);
            // map<int, double>::iterator it;

            unordered_map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, aI);
            unordered_map<int, double>::iterator it;

            for (it = prob_dist_trans->begin(); it != prob_dist_trans->end(); it++)
            {
                int s_newI = it->first;
                double pr_s_newI = it->second;
                map<int, double> *prob_dist_obs = Pb->GetObsFuncProbDist(s_newI, aI);
                map<int, double>::iterator it_obs;

                for (it_obs = prob_dist_obs->begin(); it_obs != prob_dist_obs->end(); it_obs++)
                {
                    int n_newI = this->NextNode(nI, it_obs->first);
                    int V_n_newI = n_newI;
                    if (type == 1)
                    {
                        // be carefull here, NI is start from 1, but AlphaVector index is from 0, so NI -1
                        V_n_newI = n_newI - 1;
                    }
                    sum_temp += pr_s_newI * it_obs->second * V[V_n_newI][s_newI];
                }
            }

            alpha_nI_sI_temp += gamma * sum_temp;
            // be carefull here, NI is start from 1, but AlphaVector index is from 0, so NI -1
            int V_nI = nI;
            if (type == 1)
            {
                V_nI = nI - 1;
            }
            double norm1 = fabs(V[V_nI][sI] - alpha_nI_sI_temp);
            max_norm = max_norm > norm1 ? max_norm : norm1;
            V[V_nI].ChangeValue(sI, alpha_nI_sI_temp);
        }
    }
    return max_norm;
};

vector<AlphaVector> FSCBase::BuildInitValueFromFSC(PomdpInterface *pomdp)
{
    vector<AlphaVector> Gamma;
    int nI_start = 0;
    for (int nI = nI_start; nI < this->SizeNodes; nI++)
    {
        AlphaVector V(pomdp->GetSizeOfS());
        Gamma.push_back(V);
    }

    return Gamma;
}

void FSCBase::SetAgentName(string name)
{
    this->AgentName = name;
}


#include "../Include/SimPomdpMCTS.h"

SimPomdpMCTS::SimPomdpMCTS(PomdpInterface *PomdpModel, string VisfilePath)
{
      this->PomdpModel = PomdpModel;
      this->interactive = true;
      // Sample the init state
      this->stateI = this->SampleStartState();
      this->LoadVisualizationFile(VisfilePath);
}

SimPomdpMCTS::SimPomdpMCTS(PomdpInterface *PomdpModel)
{
      this->PomdpModel = PomdpModel;
      this->stateI = this->SampleStartState();
}

SimPomdpMCTS::~SimPomdpMCTS() {}

int SimPomdpMCTS::SampleStartState()
{
      map<int, double> *b0 = this->PomdpModel->GetInitBeliefSparse();
      double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
      double temp_p = 0;

      map<int, double>::iterator it_sI;

      for (it_sI = b0->begin(); it_sI != b0->end(); it_sI++)
      {
            temp_p += it_sI->second;
            if (temp_p >= random_p)
            {
                  return it_sI->first;
            }
      }

      // return impossibe output
      return -1;
};

int SimPomdpMCTS::GetObsFromState(int jaI)
{

      double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
      // cout << "rand in GetObsFromState is: " << random_p << endl;
      double temp_p = 0;
      map<int, double> *dist_obsI = this->PomdpModel->GetObsFuncProbDist(stateI, jaI);
      map<int, double>::iterator it_oI;

      for (it_oI = dist_obsI->begin(); it_oI != dist_obsI->end(); it_oI++)
      {
            int joI = it_oI->first;
            double pb_obs = it_oI->second;
            if (pb_obs > 0)
            {
                  temp_p += pb_obs;
                  // if the current sum_pb > rand, then select this observation
                  if (temp_p >= random_p)
                  {
                        return joI;
                  }
            }
      }

      // return impossible output
      return -1;
};

// sI, JoI, Done, Reward, info
tuple<int, int, bool, double, string> SimPomdpMCTS::Step(int current_sI, int aI)
{

      // Need a check Done part
      bool done = false;
      // Currently cannot check done for the model in Cassendra .pomdp format

      double reward = this->PomdpModel->Reward(current_sI, aI);

      double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
      // cout << "rand in Step is: " << random_p << endl;
      double temp_p = 0;

      if (!done)
      {

            unordered_map<int, double> *dist_trans = this->PomdpModel->GetTransProbDist(stateI, aI);
            unordered_map<int, double>::iterator it_trans;
            for (it_trans = dist_trans->begin(); it_trans != dist_trans->end(); it_trans++)
            {
                  int s_newI = it_trans->first;
                  double pb_snew = it_trans->second;
                  if (pb_snew > 0)
                  {
                        temp_p += pb_snew;
                        // if the current sum_pb > rand, then select this new state
                        if (temp_p >= random_p)
                        {
                              this->stateI = s_newI;
                              break;
                        }
                  }
            }
      }

      int obsI = this->GetObsFromState(aI);

      // info store the current state string
      string info = this->PomdpModel->GetAllStates()[this->stateI];

      // Build the result
      tuple<int, int, bool, double, string> res(this->stateI, obsI, done, reward, info);

      return res;
};

void SimPomdpMCTS::Reset()
{
      this->stateI = this->SampleStartState();
}

void SimPomdpMCTS::addNewLines(std::string *text)
{
      for (unsigned int i = 0; i < text->length(); i++)
      {
            if ((*text)[i] == '.')
            {
                  (*text)[i] = '\n';
            }
      }
}

void SimPomdpMCTS::VisualizationState(int sI)
{
      string res = this->visualization_states[sI];
      addNewLines(&res);
      cout << res << endl;
};

void SimPomdpMCTS::LoadVisualizationFile(string fileName)
{
      // Open the File
      std::ifstream in(fileName.c_str());
      // Check if object is valid
      if (!in)
      {
            std::cerr << "Cannot open the File : " << fileName << std::endl;
      }
      std::string str;
      // Read the next line from File until it reaches the end.
      while (std::getline(in, str))
      {
            // Line contains string of length > 0 then save it in vector
            if (str.size() > 0)
                  this->visualization_states.push_back(str);
      }
}

int SimPomdpMCTS::SelectActions()
{
      int aI;
      // if (this->interactive)
      // {
      //       int aHI;
      //       cout << "Please enter an action index for the human player" << endl;
      //       vector<string> human_actions = this->PomdpModel->GetAllActions();
      //       for (int i = 0; i < human_actions.size(); i++)
      //       {
      //             cout << i << ":" << human_actions[i] << " ";
      //       }
      //       cout << endl;
      //       cin >> aHI;
      //       aI = aHI;
      // }
      // else
      // {

      //       // first find the current agent fsc and current agent fsc's node index, use fsc and node index to get this agent's action index
      //       aI = this->FSC.GetActionIndexForNodeI(FSCs_current_node);
      // }
      int aHI;
      cout << "Please enter an action index for the human player" << endl;
      vector<string> human_actions = this->PomdpModel->GetAllActions();
      for (unsigned int i = 0; i < human_actions.size(); i++)
      {
            cout << i << ":" << human_actions[i] << " ";
      }
      cout << endl;
      cin >> aHI;
      aI = aHI;
      return aI;
};

void SimPomdpMCTS::SimulateNsteps(int N)
{
      double sum_rewards = 0;
      for (int step = 0; step < N; step++)
      {
            // Print current information
            cout << " --------  Current step is: " << step << "  ---------" << endl;
            cout << "Current state is: " << this->PomdpModel->GetAllStates()[this->stateI] << endl;

            // visualization!
            // need different visulization for interactive interface
            if (this->visualization_states.size() != 0)
            {
                  this->VisualizationState(this->stateI);
            }

            // Select an action
            int aI = this->SelectActions();
            if (aI >= this->PomdpModel->GetSizeOfA())
            {
                  cout << "Error input aI" << endl;
                  throw "";
            }

            cout << "Selected action is: " << this->PomdpModel->GetAllActions()[aI] << " for agent " << endl;

            // Step
            tuple<int, int, bool, double, string> temp_res = this->Step(this->stateI, aI);

            // visulization for interactive interface should be done here
            // Get new obs and print
            int ObsI = get<1>(temp_res);

            cout << "Recieved obs is: " << this->PomdpModel->GetAllObservations()[ObsI] << " for agent " << endl;

            // Get reward and print
            double reward = get<3>(temp_res);
            cout << "Recieved instant reward: " << reward << endl;
            sum_rewards += pow(this->PomdpModel->GetDiscount(), step) * reward;

            // FSCs node update !!!

            if (this->interactive)
            {
                  // nothing to do
            }
            else
            {

                  // double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
                  // int nI = this->FSCs_current_node;
                  // this->FSCs_current_node = this->FSC.SampleToNewNodeI(nI, ObsI, random_p);
            }
            // cout << "something" << endl;

            cout << endl;
      }

      cout << "--------- Overall Information ----------" << endl;
      cout << "Final state after " << N << " steps is: " << this->PomdpModel->GetAllStates()[this->stateI] << endl;
      cout << "Accumulated rewards: " << sum_rewards << endl;
}

 

#include "../Include/SimDecPomdp.h"
#include <sys/time.h>

int GetHumanActionInput(double &decision_time, bool flip_vertical, bool flip_horizontal);

SimDecPomdp::SimDecPomdp(DecPomdpInterface *DecPomdpModel, vector<string> Policyfiles)
{
    this->DecPomdpModel = DecPomdpModel;
    int formalization_type = 0;

    // Check if FSCs number equal to the agents' number in DecPomdp
    if (int(Policyfiles.size()) != DecPomdpModel->GetNbAgents())
    {
        cerr << "FSCs number dont match the agents number!!! Loading failed!!!" << endl;
    }
    else
    {
        for (int agentI = 0; agentI < DecPomdpModel->GetNbAgents(); agentI++)
        {
            int ObsSize = DecPomdpModel->GetSizeOfObs(agentI);
            FSCBase FscAgentI(Policyfiles[agentI], ObsSize, formalization_type);
            this->FSCs.push_back(FscAgentI);
        }
    }

    // modify the name mannualy
    this->FSCs[0].SetAgentName("Human");
    this->FSCs[1].SetAgentName("Robot");

    // Start node = formalization type number
    vector<int> FSCs_start_nodes(DecPomdpModel->GetNbAgents(), formalization_type);
    this->FSCs_current_node = FSCs_start_nodes;

    // Sample the init state
    this->stateI = this->SampleStartState();
}

SimDecPomdp::SimDecPomdp(DecPomdpInterface *DecPomdpModel, vector<string> Policyfiles, string VisfilePath, int formalization_type)
{
    this->DecPomdpModel = DecPomdpModel;

    // Check if FSCs number equal to the agents' number in DecPomdp
    if (int(Policyfiles.size()) != DecPomdpModel->GetNbAgents())
    {
        cerr << "FSCs number dont match the agents number!!! Loading failed!!!" << endl;
    }
    else
    {
        for (int agentI = 0; agentI < DecPomdpModel->GetNbAgents(); agentI++)
        {
            int ObsSize = DecPomdpModel->GetSizeOfObs(agentI);
            FSCBase FscAgentI(Policyfiles[agentI], ObsSize, 1);
            this->FSCs.push_back(FscAgentI);
        }
    }

    // modify the name mannualy
    this->FSCs[0].SetAgentName("Human");
    this->FSCs[1].SetAgentName("Robot");

    // Start node = formalization type number
    vector<int> FSCs_start_nodes(DecPomdpModel->GetNbAgents(), formalization_type);
    this->FSCs_current_node = FSCs_start_nodes;

    // Sample the init state
    this->stateI = this->SampleStartState();

    this->LoadVisualizationFile(VisfilePath);
};

SimDecPomdp::SimDecPomdp(DecPomdpInterface *DecPomdpModel, string RobotPolicy, int RobotIndex, int HumanIndex, string VisfilePath, int formalization_type)
{
    this->DecPomdpModel = DecPomdpModel;
    this->interactive = true;
    int NbAgent = 2;
    this->HumanIndex = HumanIndex;
    this->RobotIndex = RobotIndex;
    int RobotObsSize = DecPomdpModel->GetSizeOfObs(RobotIndex);
    FSCBase RobotFsc(RobotPolicy, RobotObsSize, formalization_type);
    this->FSCs.resize(NbAgent);
    this->FSCs[RobotIndex] = RobotFsc;

    // modify the name mannualy
    this->FSCs[RobotIndex].SetAgentName("Robot");

    // Start node = formalization type number
    vector<int> FSCs_start_nodes(NbAgent, formalization_type);
    this->FSCs_current_node = FSCs_start_nodes;

    // Sample the init state
    this->stateI = this->SampleStartState();

    this->LoadVisualizationFile(VisfilePath);
}

SimDecPomdp::~SimDecPomdp() {}

int SimDecPomdp::SampleStartState()
{
    map<int, double> *b0 = this->DecPomdpModel->GetInitialBeliefSparse()->GetBeliefSparse();
    double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    double temp_p = 0;

    map<int, double>::iterator it_sI;

    for (it_sI = b0->begin(); it_sI != b0->end(); it_sI++)
    {
        temp_p += it_sI->second;
        if (temp_p >= random_p)
        {
            return it_sI->first;
        }
    }

    // return impossibe output
    return -1;
};

int SimDecPomdp::GetObsFromState(int jaI)
{

    double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    // cout << "rand in GetObsFromState is: " << random_p << endl;
    double temp_p = 0;
    for (int joI = 0; joI < this->DecPomdpModel->GetSizeOfJointObs(); joI++)
    {
        if (this->DecPomdpModel->ObsFunc(joI, this->stateI, jaI) > 0)
        {
            temp_p += this->DecPomdpModel->ObsFunc(joI, this->stateI, jaI);
            // if the current sum_pb > rand, then select this observation
            if (temp_p >= random_p)
            {
                return joI;
            }
        }
    }

    // return impossible output
    return -1;
};

// JoI, Done, Reward, info
tuple<int, bool, double, string> SimDecPomdp::Step(vector<int> ActionIndicies)
{

    int jaI = this->DecPomdpModel->IndividualToJointActionIndex(ActionIndicies);

    // Need a check Done part
    bool done = false;
    // Currently cannot check done for the model in Cassendra .pomdp format

    int current_sI = this->stateI;
    double reward = this->DecPomdpModel->Reward(current_sI, jaI);

    double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    // cout << "rand in Step is: " << random_p << endl;
    double temp_p = 0;

    if (!done)
    {
        for (int s_newI = 0; s_newI < this->DecPomdpModel->GetSizeOfS(); s_newI++)
        {
            if (this->DecPomdpModel->TransFunc(current_sI, jaI, s_newI) > 0)
            {
                temp_p += this->DecPomdpModel->TransFunc(current_sI, jaI, s_newI);
                // if the current sum_pb > rand, then select this new state
                if (temp_p >= random_p)
                {
                    this->stateI = s_newI;
                    break;
                }
            }
        }
    }

    int obsI = this->GetObsFromState(jaI);

    // info store the current state string
    string info = this->DecPomdpModel->GetAllStates()[this->stateI];

    // Build the result
    tuple<int, bool, double, string> res(obsI, done, reward, info);

    return res;
};

void SimDecPomdp::Reset()
{
    this->stateI = this->SampleStartState();
    for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
    {
        this->FSCs_current_node[i] = 0;
    }
}

void SimDecPomdp::addNewLines(std::string *text)
{
    for (unsigned int i = 0; i < text->length(); i++)
    {
        if ((*text)[i] == '.')
        {
            (*text)[i] = '\n';
        }
    }
}

void SimDecPomdp::VisualizationState(int sI)
{
    string res = this->visualization_states[sI];
    addNewLines(&res);
    cout << res << endl;
};

void SimDecPomdp::VisualizationStateUsingTabulate(int sI, bool flip_vertical, bool flip_horizontal)
{
    Table table;

    int row_size = 4;

    string res = this->visualization_states[sI];
    vector<vector<string>> all_row_strs;
    vector<string> row_strs;
    string temp_str = "";
    char temp_chr = ' ';                            // default = white_space (no info)
    for (unsigned int i = 0; i < res.length(); i++) // Read string character by character
    {
        if (res[i] >= 48 && res[i] != 91 && res[i] != 93) // skip quotes, ...
        {
            if (res[i] == '0')
            {
                temp_str += '_';
            }
            else if (res[i] == '_')
            {
                temp_str += ' ';
            }
            else
            {
                if (res[i] == 'G' || res[i] == 'B' || res[i] == 'M' || res[i] == 'X')
                    temp_chr = res[i];
                else
                    temp_str += res[i];
            }
        }
        else if (res[i] == ',' || res[i] == ']')
        {
            if (!temp_str.size())
                temp_str += '_';

            if (temp_chr != ' ')
            {
                while (temp_str.size() < 10)
                {
                    temp_str += ' ';
                }
                temp_str += temp_chr;
                temp_chr = ' ';
            }

            row_strs.push_back(temp_str);
            // cout << temp_str << endl;
            temp_str = "";
        }

        if (int(row_strs.size()) == row_size)
        {
            all_row_strs.push_back(row_strs);
            row_strs.clear();
        }
    }

    for (size_t i = 0; i < all_row_strs.size(); i++)
    {
        size_t index_row = i;
        if (flip_vertical)
        {
            index_row = all_row_strs.size() - i - 1;
        }

        if (flip_horizontal)
        {
            table.add_row({all_row_strs[index_row][3],
                           all_row_strs[index_row][2],
                           all_row_strs[index_row][1],
                           all_row_strs[index_row][0]});
        }
        else
        {
            table.add_row({all_row_strs[index_row][0],
                           all_row_strs[index_row][1],
                           all_row_strs[index_row][2],
                           all_row_strs[index_row][3]});
        }
    }

    table[0][0].format().width(15);
    table[0][1].format().width(15);
    table[0][2].format().width(15);
    table[0][3].format().width(15);

    table[1][0].format().width(10);
    table[1][1].format().width(10);
    table[1][2].format().width(10);
    table[1][3].format().width(10);

    table[2][0].format().width(10);
    table[2][1].format().width(10);
    table[2][2].format().width(10);
    table[2][3].format().width(10);

    std::cout << table << std::endl;
}

void SimDecPomdp::LoadVisualizationFile(string fileName)
{
    // Open the File
    std::ifstream in(fileName.c_str());
    // Check if object is valid
    if (!in)
    {
        std::cerr << "Cannot open the File : " << fileName << std::endl;
    }
    std::string str;
    // Read the next line from File until it reaches the end.
    while (std::getline(in, str))
    {
        // Line contains string of length > 0 then save it in vector
        if (str.size() > 0)
            this->visualization_states.push_back(str);
    }
}

void printInstructions()
{
    cout << "Actions for the human player:" << endl;
    cout << "   Up: move Up" << endl;
    cout << " Down: move Down" << endl;
    cout << " Left: move Left" << endl;
    cout << "Right: move Right" << endl;
    cout << "    R: Repair" << endl;
    cout << "    P: Pick component;" << endl;
    cout << "    W: Wait" << endl;
    cout << endl;
    cout << "    S: Stop/abort this round" << endl;
    // cout << "    Q: Quit the experiment." << endl;
}

void printLegend()
{

    cout << "Legend:" << endl;

    Table table;
    table.add_row({"_       ", "empty cell"});
    table.add_row({"_      B", "Broken device"});
    table.add_row({"_      M", "device to be Maintained"});
    table.add_row({"_      G", "Good device"});
    table.add_row({"_      X", "toolboX"});
    table.add_row({"R       ", "Robot"});
    table.add_row({"HN      ", "Human + Nothing in hands"});
    table.add_row({"HC      ", "Human + Component in hands"});

    cout << table << endl;
}

vector<int> SimDecPomdp::SelectActions(double &decision_time, bool flip_vertical, bool flip_horizontal)
{
    vector<int> actionIndicies(this->DecPomdpModel->GetNbAgents(), 0);

    if (this->interactive)
    {
        actionIndicies[RobotIndex] = this->FSCs[RobotIndex].GetActionIndexForNodeI(this->FSCs_current_node[RobotIndex]);
        int aHI;

        cout << endl;
        printInstructions();
        cout << endl;
        printLegend();

        // cin >> aHI;

        do
        {
            aHI = GetHumanActionInput(decision_time, flip_vertical, flip_horizontal);
        } while (aHI == -1);

        // stop this round "S", stop this whole game "Q"
        // human's stop signal will be treated as int "-2"
        // actionIndicies[HumanIndex] = aHI - 1;
        actionIndicies[HumanIndex] = aHI;
    }
    else
    {
        for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
        {
            // first find the current agent fsc and current agent fsc's node index, use fsc and node index to get this agent's action index
            actionIndicies[i] = this->FSCs[i].GetActionIndexForNodeI(this->FSCs_current_node[i]);
        }
    }

    return actionIndicies;
};

void SimDecPomdp::SimulateNstepsUsingFSCs(int N, double &out_sum_rewards, int &success)
{
    double sum_rewards = 0;
    success = 0;
    for (int step = 0; step < N; step++)
    {
        // Select an action
        double decision_time = 0;
        vector<int> actionIndicies = this->SelectActions(decision_time, false, false);
        // Step
        tuple<int, bool, double, string> temp_res = this->Step(actionIndicies);
        // Get new obs and print
        int joI = get<0>(temp_res);
        vector<int> ObsIndicies = this->DecPomdpModel->JointToIndividualObsIndices(joI);
        // Get reward and print
        double reward = get<2>(temp_res);
        sum_rewards += pow(this->DecPomdpModel->GetDiscount(), step) * reward;
        // FSCs node update !!!
        for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
        {
            double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
            int nI = this->FSCs_current_node[i];
            int oI = ObsIndicies[i];
            this->FSCs_current_node[i] = this->FSCs[i].SampleToNewNodeI(nI, oI, random_p);
        }
        if (reward == this->terminal_reward)
        {
            success = 1;
            break;
        }
    }

    out_sum_rewards = sum_rewards;
}

void SimDecPomdp::SimulateNsteps(int N)
{
    double sum_rewards = 0;
    for (int step = 0; step < N; step++)
    {
        // Print current information
        cout << " --------  Current step: " << step << "  ---------" << endl;

        // visualization!
        // need different visulization for interactive interface
        if (this->visualization_states.size() != 0)
        {
            this->VisualizationState(this->stateI);
        }

        // Select an action
        double decision_time = 0;
        vector<int> actionIndicies = this->SelectActions(decision_time, false, false);
        for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
        {
            cout << "Selected action is: " << this->DecPomdpModel->GetAllActionsVecs()[i][actionIndicies[i]] << " for agent " << this->FSCs[i].GetAgentDescription() << endl;
        }

        // Step
        tuple<int, bool, double, string> temp_res = this->Step(actionIndicies);

        // visulization for interactive interface should be done here
        // Get new obs and print
        int joI = get<0>(temp_res);
        vector<int> ObsIndicies = this->DecPomdpModel->JointToIndividualObsIndices(joI);
        for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
        {
            int obs_agent_I = ObsIndicies[i];
            // int size_obs_agentI = this->DecPomdpModel->GetAllObservationsVecs()[i].size();
            // if (obs_agent_I >)
            // {
            //     /* code */
            // }
            cout << "Recieved obs is: " << this->DecPomdpModel->GetAllObservationsVecs()[i][obs_agent_I] << " for agent " << this->FSCs[i].GetAgentDescription() << endl;
        }

        // Get reward and print
        double reward = get<2>(temp_res);
        cout << "Recieved instant reward: " << reward << endl;
        sum_rewards += pow(this->DecPomdpModel->GetDiscount(), step) * reward;

        // FSCs node update !!!

        if (this->interactive)
        {
            double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
            int Robot_nI = this->FSCs_current_node[RobotIndex];
            int Robot_oI = ObsIndicies[RobotIndex];
            this->FSCs_current_node[RobotIndex] = this->FSCs[RobotIndex].SampleToNewNodeI(Robot_nI, Robot_oI, random_p);
            if (reward == this->terminal_reward)
            {
                break;
            }
        }
        else
        {
            cout << "Current state is: " << this->DecPomdpModel->GetAllStates()[this->stateI] << endl;

            for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
            {
                double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
                int nI = this->FSCs_current_node[i];
                int oI = ObsIndicies[i];
                this->FSCs_current_node[i] = this->FSCs[i].SampleToNewNodeI(nI, oI, random_p);
            }
        }
        if (this->PredictHumanActions)
        {
            int aHI = actionIndicies[HumanIndex];
            int oHI = ObsIndicies[HumanIndex];
            this->PredictHumanNextActions(aHI, oHI, this->current_stochastic_fsc_nI);
            cout << "current stochastic human fsc node index: " << this->current_stochastic_fsc_nI << endl;
        }

        cout << endl;
    }

    cout << "--------- Overall Information ----------" << endl;
    cout << "Final State Visualization" << endl;
    this->VisualizationState(this->stateI);

    cout << "Final state after " << N << " steps is: " << this->DecPomdpModel->GetAllStates()[this->stateI] << endl;
    cout << "Accumulated rewards: " << sum_rewards << endl;
}

int SimDecPomdp::SimulateNsteps(int N, int &final_step, double &final_acc_rewards, ofstream &log, bool flip_vertical, bool flip_horizontal) // for human experiments, log data
{
    double sum_rewards = 0;
    int finish_type = 0; // 0: normal finish, time runs out; 1: success collaboration;2: stop this round; 3: user choose stop earlier;
    int step = 0;
    time_t startTime, endTime;
    startTime = time(NULL);

    for (step = 0; step < N; step++)
    {
        // Print current information
        system("clear");

        cout << " --------  Current step is: " << step << "  ---------" << endl;

        // visualization!
        // need a different visualization for interactive interface
        if (this->visualization_states.size() != 0)
        {
            this->VisualizationStateUsingTabulate(this->stateI, flip_vertical, flip_horizontal);
        }

        // Select an action
        double decision_time = 0;
        vector<int> actionIndicies = this->SelectActions(decision_time, flip_vertical, flip_horizontal);
        // check human pressed stop or not
        log << ", " << actionIndicies[0] << "; " << actionIndicies[1] << "; ";
        if (actionIndicies[0] == -2) // quit experiment
        {
            finish_type = 2;
            return finish_type;
        }
        else if (actionIndicies[0] == -3) // stop this round
        {
            finish_type = 3;
            break;
        }

        // Step
        tuple<int, bool, double, string> temp_res = this->Step(actionIndicies);

        log << this->stateI << "; ";

        // visulization for interactive interface should be done here
        // Get new obs and print
        int joI = get<0>(temp_res);
        vector<int> ObsIndicies = this->DecPomdpModel->JointToIndividualObsIndices(joI);
        // for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
        // {
        // int obs_agent_I = ObsIndicies[i];
        // int size_obs_agentI = this->DecPomdpModel->GetAllObservationsVecs()[i].size();
        // if (obs_agent_I >)
        // {
        //     /* code */
        // }
        // cout << "Recieved obs is: " << this->DecPomdpModel->GetAllObservationsVecs()[i][obs_agent_I] << " for agent " << this->FSCs[i].GetAgentDescription() << endl;
        // }

        // Get reward and print
        double reward = get<2>(temp_res);

        log << reward << "; ";
        log << decision_time << " ";

        cout << "Recieved instant reward: " << reward << endl;
        sum_rewards += pow(this->DecPomdpModel->GetDiscount(), step) * reward;

        // FSCs node update !!!

        if (this->interactive)
        {
            double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
            int Robot_nI = this->FSCs_current_node[RobotIndex];
            int Robot_oI = ObsIndicies[RobotIndex];
            this->FSCs_current_node[RobotIndex] = this->FSCs[RobotIndex].SampleToNewNodeI(Robot_nI, Robot_oI, random_p);
            if (reward == this->terminal_reward)
            {
                final_step = step;
                final_acc_rewards = sum_rewards;
                finish_type = 1;
                break;
            }
        }
        else
        {
            cout << "Current state is: " << this->DecPomdpModel->GetAllStates()[this->stateI] << endl;

            for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
            {
                double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
                int nI = this->FSCs_current_node[i];
                int oI = ObsIndicies[i];
                this->FSCs_current_node[i] = this->FSCs[i].SampleToNewNodeI(nI, oI, random_p);
            }
        }
        if (this->PredictHumanActions)
        {
            int aHI = actionIndicies[HumanIndex];
            int oHI = ObsIndicies[HumanIndex];
            this->PredictHumanNextActions(aHI, oHI, this->current_stochastic_fsc_nI);
            cout << "current stochastic human fsc node index: " << this->current_stochastic_fsc_nI << endl;
        }

        cout << endl;
    }
    endTime = time(NULL);
    double round_time = (double)(endTime - startTime);

    system("clear");

    if (!finish_type)
    {
        log << ", ";
        // cout << "All time steps run out! This round is finished!" << endl;
        cout << "*****************" << endl
             << "* OUT OF TIME ! *" << endl
             << "*****************" << endl;
    }
    else if (finish_type == 1)
    {
        // cout << " Congratulations, you succeed in this round!" << endl;
        cout << "*************" << endl
             << "* YOU WON ! *" << endl
             << "*************" << endl;
    }
    else
    {
        // cout << "Player choosen to stop this round!" << endl;
        cout << "*************" << endl
             << "* ABORTION. *" << endl
             << "*************" << endl;
    }
    cout << endl;

    this->VisualizationStateUsingTabulate(this->stateI, flip_vertical, flip_horizontal);

    // outlogs
    for (int i = 0; i < N - step; i++)
    {
        log << " , ";
    }
    log << finish_type << ", " << round_time << ", " << step << ", " << sum_rewards << endl;

    /*
    cout << "--------- Overall Information ----------" << endl;
    cout << "Steps: " << step << endl;
    cout << "Accumulated rewards: " << sum_rewards << endl;
    */

    return finish_type;
}

void SimDecPomdp::SetStochasticHumanFsc(SampleStochasticLocalFsc &fsc)
{
    this->StochasticHumanFsc = fsc;
    this->PredictHumanActions = true;
}
void SimDecPomdp::PredictHumanNextActions(int current_aHI, int oHI, int &current_nI)
{
    this->StochasticHumanFsc.PrintNextNodeActionDistribution(current_aHI, oHI, current_nI);
}

double SimDecPomdp::ValueEvaluation(double error_gap)
{
    double sum_rewards = 0;
    bool converge = false;
    int step = 0;
    while (!converge)
    {
        // Select an action
        double decision_time = 0;
        vector<int> actionIndicies = this->SelectActions(decision_time, false, false);
        tuple<int, bool, double, string> temp_res = this->Step(actionIndicies);
        int joI = get<0>(temp_res);
        vector<int> ObsIndicies = this->DecPomdpModel->JointToIndividualObsIndices(joI);
        // Get reward and print
        double reward = get<2>(temp_res);

        double temp_sum_rewards = sum_rewards + pow(this->DecPomdpModel->GetDiscount(), step) * reward;
        // check converge
        double dis = abs(temp_sum_rewards - sum_rewards);
        if (dis < error_gap)
        {
            converge = true;
        }
        sum_rewards = temp_sum_rewards;
        for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
        {
            double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
            int nI = this->FSCs_current_node[i];
            int oI = ObsIndicies[i];
            this->FSCs_current_node[i] = this->FSCs[i].SampleToNewNodeI(nI, oI, random_p);
        }

        step += 1;
    }

    return sum_rewards;
}

void SimDecPomdp::SetTerminalReward(double terminal_reward)
{
    this->terminal_reward = terminal_reward;
}

int GetHumanActionInput(double &decision_time, bool flip_vertical, bool flip_horizontal)
{

    struct timeval start;
    gettimeofday(&start, NULL);

    // Set terminal to raw mode
    system("stty raw");

    // Wait for single character
    char input = getchar();
    int aHI = -1; // default value should be wait in case of user's wrong inputs

    if (input == 'r' || input == 'R')
    {
        aHI = 4;
    }

    if (input == 'p' || input == 'P')
    {
        aHI = 5;
    }

    if (input == 'w' || input == 'W')
    {
        aHI = 6;
    }

    if (input == 's' || input == 'S')
    {
        aHI = -3;
    }

    if (input == 'q' || input == 'Q')
    {
        aHI = -2;
    }

    if (input == '\033')
    {
        getchar();
        input = getchar();
        switch (input)
        {
        case 'A':
            // code for arrow up
            // cout << "UP" << endl;
            if (flip_vertical)
            {
                aHI = 1;
            }
            else
            {
                aHI = 0;
            }

            break;
        case 'B':
            // code for arrow down
            // cout << "DOWN" << endl;
            if (flip_vertical)
            {
                aHI = 0;
            }
            else
            {
                aHI = 1;
            }
            break;
        case 'C':
            // code for arrow right
            // cout << "RIGHT" << endl;
            if (flip_horizontal)
            {
                aHI = 2;
            }
            else
            {
                aHI = 3;
            }
            break;
        case 'D':
            // code for arrow left
            // cout << "LEFT" << endl;
            if (flip_horizontal)
            {
                aHI = 3;
            }
            else
            {
                aHI = 2;
            }
            break;
        }
    }

    // Reset terminal to normal "cooked" mode
    system("stty cooked");

    // And we're out of here

    struct timeval end;
    gettimeofday(&end, NULL);
    decision_time = double(end.tv_sec - start.tv_sec) + 0.000001 * (end.tv_usec - start.tv_usec);

    return aHI;
}

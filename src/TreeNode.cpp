#include "../Include/TreeNode.h"

TreeNode::TreeNode(BeliefSparse b, double pr, string belief_id)
{
      this->b_ = b;
      this->pr_ = pr;
      this->belief_id = belief_id;
}

TreeNode::TreeNode(BeliefSparse b, double pr)
{
      this->b_ = b;
      this->pr_ = pr;
}

void TreeNode::AddParentNode(TreeNode *ParentNode)
{
      this->ParentNode_ = ParentNode;
}
void TreeNode::AddChildNode(int aI, int oI, TreeNode *ChildNode)
{
      pair<int, int> t(aI, oI);
      this->ChildNodes_[t] = ChildNode;
}

map<pair<int, int>, TreeNode *> *TreeNode::GetChildNodes()
{
      return &this->ChildNodes_;
}
double TreeNode::GetValue()
{
      return this->value_;
}

BeliefSparse TreeNode::GetBeliefSparse()
{
      return this->b_;
}

void TreeNode::SetValue(double value)
{
      this->value_ = value;
}

double TreeNode::GetChildNodeValue(int aI, int oI)
{
      if (this->ChildNodes_.size() == 0)
      {
            cout << "error, no childs!" << endl;
            throw "";
      }
      else
      {
            TreeNode *child = this->ChildNodes_[make_pair(aI, oI)];
            double v = child->GetValue();
            return v;
      }
}

// int TreeNode::GetChildNodeNbVisit(int aI, int oI)
// {
//       if (this->ChildNodes_.size() == 0)
//       {
//             cout << "error, no childs!" << endl;
//             throw "";
//       }
//       else
//       {
//             TreeNode *child = this->ChildNodes_[make_pair(aI, oI)];
//             int nb_visit = child->GetNbVisit();
//             return nb_visit;
//       }
// }

bool TreeNode::isRoot()
{
      if (!this->ParentNode_)
      {
            return true;
      }
      else
      {
            return false;
      }
}

TreeNode *TreeNode::GetParentNode()
{
      return this->ParentNode_;
}

void TreeNode::SetBestAction(int aI)
{
      this->best_aI = aI;
};
int TreeNode::GetBestAction()
{
      return this->best_aI;
};
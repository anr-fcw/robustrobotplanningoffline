#include "../Include/MCTS.h"

MCTS::MCTS(DecPomdpInterface *pb, vector<AlphaVector> &alpha_vecs)
{
      this->pb = pb;
      // const map<int, double> *pb_init = pb->GetInitBeliefSparse();
      // int state_size = pb->GetSizeOfS();
      // BeliefSparse b0(*pb_init, state_size);
      // TreeNode start_node(b0, 1.0);
      // this->rootnode = start_node;
      // SimPomdpMCTS sim(pb);
      // this->simulator = sim;
      this->size_A = pb->GetSizeOfJointA();
      this->discount = pb->GetDiscount();
      this->alpha_vecs = alpha_vecs;
      // this->current_node_number += 1;
}
// int MCTS::RolloutPolicy(BeliefSparse &b)
// {
//       return RandT<int>(0, this->pb->GetSizeOfA());
// }

// double MCTS::Simulation(BeliefSparse &b, int nb_restarts, double epsilon)
// {
//       double sum_accumlated_rewards = 0.0;
//       for (int i = 0; i < nb_restarts; i++)
//       {
//             double current_discount = 1.0;
//             double temp_res = 0;
//             map<int, double> *pb_states = b.GetBeliefSparse();
//             map<int, double>::iterator it_sI;
//             for (it_sI = pb_states->begin(); it_sI != pb_states->end(); it_sI++)
//             {
//                   int sI = it_sI->first;
//                   double pb_sI = it_sI->second;

//                   while (current_discount > epsilon)
//                   {
//                         int aI = RandT<int>(0, this->pb->GetSizeOfA());
//                         tuple<int, int, bool, double, string> res_step = this->simulator.Step(sI, aI);
//                         double reward = get<3>(res_step);
//                         temp_res += pb_sI * reward * current_discount;
//                         current_discount *= this->pb->GetDiscount();

//                         // cout << "reward: " << reward << endl;
//                         // update sI
//                         sI = get<0>(res_step);
//                   }
//             }
//             // cout << "temp_res: " << temp_res << endl;
//             sum_accumlated_rewards += temp_res;
//       }
//       return sum_accumlated_rewards / nb_restarts;
// }

TreeNode *MCTS::Selection(TreeNode *start_node)
{
      // start_node->AddNbVisit();
      BeliefSparse b = start_node->GetBeliefSparse();
      string b_id = b.computeID();
      this->all_belief_nb_visit[b_id] += 1;
      // should do
      // 1. First select action according to average observation prob to compute Q
      // 2. POMCP using particles (states) not directly belief
      // 3. Select action using UCB

      map<int, double> all_action_Q = start_node->GetAllActionQ();

      // map<int, int> all_action_visit = start_node->GetNbAllActionsVisit();
      // int nb_node_visit = start_node->GetNbVisit();

      map<int, int> all_action_visit = this->all_belief_action_nb_visit[b_id];
      int nb_node_visit = this->all_belief_nb_visit[b_id];

      // check if have children
      map<pair<int, int>, TreeNode *> *childs = start_node->GetChildNodes();
      if (childs->size() != 0)
      {
            // sampling according to childs' value
            map<pair<int, int>, TreeNode *>::iterator it;
            TreeNode *next_node = nullptr;

            double Max_value = -DBL_MAX;
            int selected_aI = -1;
            for (int aI = 0; aI < size_A; aI++)
            {
                  double ratio_visit = 0;
                  int nb_aI_visit = all_action_visit[aI];
                  if (nb_aI_visit == 0)
                  {
                        // cout << "found a nb_aI_visit = 0!" << endl;
                        ratio_visit = nb_node_visit / 0.1;
                  }
                  else
                  {
                        ratio_visit = nb_node_visit / nb_aI_visit;
                  }

                  // cout << "ratio:" << ratio_visit << endl;

                  double value = all_action_Q[aI] + this->c * sqrt(ratio_visit);

                  if (value > Max_value)
                  {
                        Max_value = value;
                        selected_aI = aI;
                  }
            }

            // selected an action, then sample an observation to get next node
            map<int, double> dist_pr_oba = start_node->GetPrOrOba(selected_aI);
            map<int, double>::iterator it_oI;
            double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
            double temp_p = 0;
            int sample_oI = -1;
            for (it_oI = dist_pr_oba.begin(); it_oI != dist_pr_oba.end(); it_oI++)
            {
                  int oI = it_oI->first;
                  double pr_oba = it_oI->second;
                  temp_p += pr_oba;
                  if (temp_p >= random_p)
                  {
                        sample_oI = oI;
                        break;
                  }
            }

            // start_node->AddNbActionsVisit(selected_aI);

            this->all_belief_action_nb_visit[b_id][selected_aI] += 1;

            next_node = childs->find(make_pair(selected_aI, sample_oI))->second;
            // recursive depth search until the leaf node
            return this->Selection(next_node);
      }
      else
      {
            // start_node->AddNbVisit();
            return start_node;
      }
}

void MCTS::Expansion(TreeNode *node)
{
      // cout << node << endl;
      BeliefSparse b = node->GetBeliefSparse();
      for (int aI = 0; aI < size_A; aI++)
      {

            map<int, double> dist_pr_oba = compute_dist_p_oba(b, aI, this->pb);
            node->SetPrOrOba(aI, dist_pr_oba);
            map<int, double>::iterator it_oI;
            for (it_oI = dist_pr_oba.begin(); it_oI != dist_pr_oba.end(); it_oI++)
            {
                  int oI = it_oI->first;
                  double pr_oba = it_oI->second;

                  BeliefSparse new_b = Update(this->pb, b, aI, oI, pr_oba);
                  // add this belief
                  string id_new_b = new_b.computeID();
                  this->all_beliefs.insert(id_new_b);

                  TreeNode *child_node = new TreeNode(new_b, pr_oba);
                  // TreeNode child_node(new_b, pr_oba);
                  // add parent
                  // child_node.AddParentNode(node);
                  // child_node->AddParentNode(node);

                  // do a simulation to estimate the value of next node(b_new)
                  // double esti_value = this->Simulation(new_b, this->nb_restarts_simulation, this->epsilon);

                  double esti_value = 0;
                  double esti_value_history = 0;
                  double esti_value_alpha_vecs = 0;
                  // check if this belief is already visited
                  if (this->all_belief_value.count(id_new_b))
                  {
                        esti_value_history = this->all_belief_value[id_new_b];
                  }
                  else
                  {
                        // directly use alphavecs to estimate the value
                        esti_value_alpha_vecs = best_value(alpha_vecs, new_b);
                  }

                  // esti_value = best_value(alpha_vecs, new_b);
                  if (esti_value_history > esti_value_alpha_vecs)
                  {
                        esti_value = esti_value_history;
                  }
                  else
                  {
                        esti_value = esti_value_alpha_vecs;
                  }

                  // compute the estimate Q
                  // child_node.SetValue(esti_value);
                  child_node->SetValue(esti_value);

                  // add this child node
                  this->all_nodes.push_back(child_node);
                  int child_node_index = all_nodes.size() - 1;

                  // node->AddChildNode(aI, oI, child_node);
                  node->AddChildNode(aI, oI, this->all_nodes[child_node_index]);
                  // cout << "something" << endl;
                  this->current_node_number += 1;
            }
      }
      // Backpropagate the value to all parent nodes
      this->BackPropagation(node);
}

// Back propagate value and visit number
void MCTS::BackPropagation(TreeNode *node)
{

      BeliefSparse b = node->GetBeliefSparse();
      string id = node->GetBeliefId();

      // must have childs, the node which just finish expansion
      double max_Q = -DBL_MAX;
      for (int aI = 0; aI < size_A; aI++)
      {
            double esti_Q = ComputeR(this->pb, b, aI);
            // map<int, double> dist_pr_oba = compute_dist_p_oba(b, aI, this->pb);
            map<int, double> dist_pr_oba = node->GetPrOrOba(aI);
            map<int, double>::iterator it_oI;
            for (it_oI = dist_pr_oba.begin(); it_oI != dist_pr_oba.end(); it_oI++)
            {
                  // compute Q
                  int oI = it_oI->first;
                  double pr_oba = it_oI->second;
                  TreeNode *child_node = node->GetChildNode(aI, oI);
                  double v = child_node->GetValue();
                  esti_Q += discount * v * pr_oba;
            }

            if (esti_Q > max_Q)
            {
                  max_Q = esti_Q;
                  // node->SetBestAction(aI); //ueselss
            }

            node->SetActionQ(aI, esti_Q);
            // store this Q
            this->all_belief_action_Q[id][aI] = esti_Q;
      }

      node->SetValue(max_Q);
      this->all_belief_value[id] = max_Q;

      if (!node->isRoot())
      {
            this->BackPropagation(node->GetParentNode());
      }
}

int MCTS::Plan(BeliefSparse b, double &value)
{

      TreeNode start_node(b, 1.0);
      this->rootnode = start_node;
      this->current_node_number = 0;
      this->all_nodes.clear();

      string id_b = b.GetID();
      this->all_beliefs.insert(id_b);

      vector<double> start_node_value_history;
      vector<double> all_belief_size_history;

      while (this->current_node_number < this->max_node_number)
      {
            TreeNode *node = this->Selection(&this->rootnode);
            // cout << "search finished" << endl;
            Expansion(node);
            // cout << "expansion finished" << endl;

            start_node_value_history.push_back(this->rootnode.GetValue());
            all_belief_size_history.push_back(double(this->all_beliefs.size()));
      }

      double v_b0 = this->rootnode.GetValue();
      // cout << "the value at b0:" << v_b0 << endl;
      int best_aI = this->rootnode.GetBestAction();
      // cout << "the best action is: " << this->pb->GetAllActions()[best_aI] << endl;

      // int size_all_belief = this->all_beliefs.size();
      // int size_all_child = this->rootnode.GetNbAllChilds();
      // cout << "visit belief size: " << size_all_belief << endl;
      // cout << "root node's all child size: " << size_all_child << endl;
      // PrintMap(this->all_belief_action_Q[id_b]);
      // PrintVector(start_node_value_history);
      // PrintVector(all_belief_size_history);

      // PrintMap(this->all_belief_value);

      value = v_b0;
      CleanNodes();

      return best_aI;
}

void MCTS::Plan(BeliefSparse b, map<int, double> &belief_Q)
{
      // get belief id
      string id_b = b.GetID();

      // int start belief size
      int belief_size_before_expansion = this->all_beliefs.size();

      TreeNode start_node(b, 1.0);
      this->rootnode = start_node;
      this->current_node_number = 0;
      this->all_nodes.clear();

      this->all_beliefs.insert(id_b);

      // belief size after expansion
      int belief_size_after_expansion = this->all_beliefs.size();

      vector<double> start_node_value_history;
      vector<double> all_belief_size_history;

      // no more new beliefs, stop
      // while (belief_size_before_expansion != belief_size_after_expansion)
      while (this->current_node_number < this->max_node_number)
      {
            // belief_Q = this->all_belief_action_Q[id_b];
            // PrintMap(belief_Q);

            belief_size_before_expansion = this->all_beliefs.size();
            TreeNode *node = this->Selection(&this->rootnode);
            // cout << "search finished" << endl;
            Expansion(node);
            // cout << "expansion finished" << endl;
            belief_size_after_expansion = this->all_beliefs.size();

            // cout << "processing!" << endl;
            // belief_Q = this->all_belief_action_Q[id_b];
            // PrintMap(belief_Q);
      }

      // double v_b0 = this->rootnode.GetValue();

      // int size_all_belief = this->all_beliefs.size();
      // cout << "visit belief size: " << size_all_belief << endl;

      belief_Q = this->all_belief_action_Q[id_b];
      CleanNodes();
}

void MCTS::Init(int nb_nodes, double c)
{
      BeliefSparse *b0 = pb->GetInitialBeliefSparse();
      this->max_node_number = nb_nodes;
      this->c = c;
      double v;
      this->Plan(*b0, v);
}

void MCTS::CleanNodes()
{
      for (size_t i = 0; i < this->all_nodes.size(); i++)
      {
            delete this->all_nodes[i];
      }

      this->all_nodes.clear();
}

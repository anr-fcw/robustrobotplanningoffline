

#include "../Include/SimPomdp.h"

SimPomdp::SimPomdp(PomdpInterface *PomdpModel, string fsc_path, string VisfilePath)
{
      this->PomdpModel = PomdpModel;

      int ObsSize = PomdpModel->GetSizeOfObs();
      FSCBase FscAgent(fsc_path, ObsSize, 0);
      this->FSC = FscAgent;

      // Start node = formalization type number
      this->FSCs_current_node = 0;

      // Sample the init state
      this->stateI = this->SampleStartState();

      this->LoadVisualizationFile(VisfilePath);
};

SimPomdp::SimPomdp(PomdpInterface *PomdpModel, string fsc_path, double terminal_reward)
{
      this->PomdpModel = PomdpModel;

      int ObsSize = PomdpModel->GetSizeOfObs();
      FSCBase FscAgent(fsc_path, ObsSize, 0);
      this->FSC = FscAgent;
      // Start node = formalization type number
      this->FSCs_current_node = 0;
      // Sample the init state
      this->stateI = this->SampleStartState();
      this->terminal_reward = terminal_reward;
};

SimPomdp::SimPomdp(PomdpInterface *PomdpModel, string VisfilePath)
{
      this->PomdpModel = PomdpModel;
      this->interactive = true;
      // Sample the init state
      this->stateI = this->SampleStartState();
      this->LoadVisualizationFile(VisfilePath);
}

SimPomdp::~SimPomdp() {}

int SimPomdp::SampleStartState()
{
      map<int, double> *b0 = this->PomdpModel->GetInitBeliefSparse();
      double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
      double temp_p = 0;

      map<int, double>::iterator it_sI;

      for (it_sI = b0->begin(); it_sI != b0->end(); it_sI++)
      {
            temp_p += it_sI->second;
            if (temp_p >= random_p)
            {
                  return it_sI->first;
            }
      }

      // return impossibe output
      return -1;
};

int SimPomdp::GetObsFromState(int jaI)
{

      double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
      // cout << "rand in GetObsFromState is: " << random_p << endl;
      double temp_p = 0;
      for (int joI = 0; joI < this->PomdpModel->GetSizeOfObs(); joI++)
      {
            if (this->PomdpModel->ObsFunc(joI, this->stateI, jaI) > 0)
            {
                  temp_p += this->PomdpModel->ObsFunc(joI, this->stateI, jaI);
                  // if the current sum_pb > rand, then select this observation
                  if (temp_p >= random_p)
                  {
                        return joI;
                  }
            }
      }

      // return impossible output
      return -1;
};

// JoI, Done, Reward, info
tuple<int, bool, double, string> SimPomdp::Step(int aI)
{

      // Need a check Done part
      bool done = false;
      // Currently cannot check done for the model in Cassendra .pomdp format

      int current_sI = this->stateI;
      double reward = this->PomdpModel->Reward(current_sI, aI);

      double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
      // cout << "rand in Step is: " << random_p << endl;
      double temp_p = 0;

      if (!done)
      {
            for (int s_newI = 0; s_newI < this->PomdpModel->GetSizeOfS(); s_newI++)
            {
                  if (this->PomdpModel->TransFunc(current_sI, aI, s_newI) > 0)
                  {
                        temp_p += this->PomdpModel->TransFunc(current_sI, aI, s_newI);
                        // if the current sum_pb > rand, then select this new state
                        if (temp_p >= random_p)
                        {
                              this->stateI = s_newI;
                              break;
                        }
                  }
            }
      }

      int obsI = this->GetObsFromState(aI);

      // info store the current state string
      string info = this->PomdpModel->GetAllStates()[this->stateI];

      // Build the result
      tuple<int, bool, double, string> res(obsI, done, reward, info);

      return res;
};

void SimPomdp::Reset()
{
      this->stateI = this->SampleStartState();
      this->FSCs_current_node = 0;
}

void addNewLines(std::string *text)
{
      for (unsigned int i = 0; i < text->length(); i++)
      {
            if ((*text)[i] == '.')
            {
                  (*text)[i] = '\n';
            }
      }
}

void SimPomdp::VisualizationState(int sI)
{
      string res = this->visualization_states[sI];
      addNewLines(&res);
      cout << res << endl;
};

void SimPomdp::LoadVisualizationFile(string fileName)
{
      // Open the File
      std::ifstream in(fileName.c_str());
      // Check if object is valid
      if (!in)
      {
            std::cerr << "Cannot open the File : " << fileName << std::endl;
      }
      std::string str;
      // Read the next line from File until it reaches the end.
      while (std::getline(in, str))
      {
            // Line contains string of length > 0 then save it in vector
            if (str.size() > 0)
                  this->visualization_states.push_back(str);
      }
}

int SimPomdp::SelectActions()
{
      int aI;
      if (this->interactive)
      {
            int aHI;
            cout << "Please enter an action index for the human player" << endl;
            vector<string> human_actions = this->PomdpModel->GetAllActions();
            for (unsigned int i = 0; i < human_actions.size(); i++)
            {
                  cout << i << ":" << human_actions[i] << " ";
            }
            cout << endl;
            cin >> aHI;
            aI = aHI;
      }
      else
      {

            // first find the current agent fsc and current agent fsc's node index, use fsc and node index to get this agent's action index
            aI = this->FSC.GetActionIndexForNodeI(FSCs_current_node);
      }

      return aI;
};

void SimPomdp::SimulateNsteps(int N)
{
      double sum_rewards = 0;
      for (int step = 0; step < N; step++)
      {
            // Print current information
            cout << " --------  Current step is: " << step << "  ---------" << endl;
            cout << "Current state is: " << this->PomdpModel->GetAllStates()[this->stateI] << endl;

            // visualization!
            // need different visulization for interactive interface
            if (this->visualization_states.size() != 0)
            {
                  this->VisualizationState(this->stateI);
            }

            // Select an action
            int aI = this->SelectActions();
            if (aI >= this->PomdpModel->GetSizeOfA())
            {
                  cout << "Error input aI" << endl;
                  throw "";
            }

            cout << "Selected action is: " << this->PomdpModel->GetAllActions()[aI] << " for agent " << this->FSC.GetAgentDescription() << endl;

            // Step
            tuple<int, bool, double, string> temp_res = this->Step(aI);

            // visulization for interactive interface should be done here
            // Get new obs and print
            int ObsI = get<0>(temp_res);

            cout << "Recieved obs is: " << this->PomdpModel->GetAllObservations()[ObsI] << " for agent " << this->FSC.GetAgentDescription() << endl;

            // Get reward and print
            double reward = get<2>(temp_res);
            cout << "Recieved instant reward: " << reward << endl;
            sum_rewards += pow(this->PomdpModel->GetDiscount(), step) * reward;

            // FSCs node update !!!

            if (this->interactive)
            {
                  // nothing to do
            }
            else
            {

                  double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
                  int nI = this->FSCs_current_node;
                  this->FSCs_current_node = this->FSC.SampleToNewNodeI(nI, ObsI, random_p);
            }
            // cout << "something" << endl;

            cout << endl;
      }

      cout << "--------- Overall Information ----------" << endl;
      cout << "Final state after " << N << " steps is: " << this->PomdpModel->GetAllStates()[this->stateI] << endl;
      cout << "Accumulated rewards: " << sum_rewards << endl;
}

int SimPomdp::SimulateNstepsCheckSuccess(int N)
{
      double sum_rewards = 0;
      for (int step = 0; step < N; step++)
      {
            // Select an action
            int aI = this->SelectActions();
            if (aI >= this->PomdpModel->GetSizeOfA())
            {
                  cout << "Error input aI" << endl;
                  throw "";
            }

            // Step
            tuple<int, bool, double, string> temp_res = this->Step(aI);

            // Get new obs and print
            int ObsI = get<0>(temp_res);

            // Get reward and print
            double reward = get<2>(temp_res);
            sum_rewards += pow(this->PomdpModel->GetDiscount(), step) * reward;

            if (reward == this->terminal_reward)
            {
                  return 1;
            }

            // FSCs node update !!!

            if (this->interactive)
            {
                  // nothing to do
            }
            else
            {

                  double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
                  int nI = this->FSCs_current_node;
                  this->FSCs_current_node = this->FSC.SampleToNewNodeI(nI, ObsI, random_p);
            }
      }

      // cout << "Acc reward:" << sum_rewards << endl;

      return 0;
}

int SimPomdp::SimulateNstepsCheckSuccess(int N, double &sum_rewards)
{
      sum_rewards = 0;
      for (int step = 0; step < N; step++)
      {
            // Select an action
            int aI = this->SelectActions();
            if (aI >= this->PomdpModel->GetSizeOfA())
            {
                  cout << "Error input aI" << endl;
                  throw "";
            }

            // Step
            tuple<int, bool, double, string> temp_res = this->Step(aI);

            // Get new obs and print
            int ObsI = get<0>(temp_res);

            // Get reward and print
            double reward = get<2>(temp_res);
            sum_rewards += pow(this->PomdpModel->GetDiscount(), step) * reward;

            if (reward == this->terminal_reward)
            {
                  return 1;
            }

            // FSCs node update !!!

            if (this->interactive)
            {
                  // nothing to do
            }
            else
            {

                  double random_p = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
                  int nI = this->FSCs_current_node;
                  this->FSCs_current_node = this->FSC.SampleToNewNodeI(nI, ObsI, random_p);
            }
      }

      // cout << "Acc reward:" << sum_rewards << endl;

      return 0;
}
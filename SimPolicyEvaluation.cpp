#include <iostream>
#include "Include/ParserDecPOMDPSparse.h"
#include "Include/ParserPOMDPSparse.h"
#include "Include/ParserSarsopResult.h"
#include "Include/BuildFSC.h"
#include "Include/RobotRobustPlanModelSparse.h"
#include "Include/SampleLocalFsc.h"
#include "Include/BestResponseMomdpModelSparse.h"
#include "Include/SampleStochasticLocalFsc.h"
#include "Include/SimDecPomdp.h"
#include "Include/SimPomdp.h"
#include <numeric>

#include <filesystem>
using std::__fs::filesystem::directory_iterator;

double ComputeSem(vector<int> &v, double &mean);
double ComputeSem(vector<double> &v, double &mean);

int main()
{
    srand(time(NULL));

    string BRM_HL_pomdp_dir_path = "ExperimentsData_big_map/BRM_HL";
    string BRM_HR_pomdp_dir_path = "ExperimentsData_big_map/BRM_HR";
    string BRM_pomdp_dir_path = "ExperimentsData_big_map/RobotBestResponsePomdp";
    string path_pb_1 = "problem_folder/fcw_icra_human_prefer_left_test_big.dpomdp";
    string robot_policy_decpomdp = "HumanExperimentsData/robot_policies/robot_policy_decpomdp.fsc";
    string robot_policy_T0 = "results/RobustRobotPolicyT0.0.fsc";
    string robot_policy_T03S200 = "results/RobustRobotPolicyT0.3s200.fsc";
    string robot_policy_T03S400 = "results/RobustRobotPolicyT0.3s400.fsc";
    string robot_policy_T03S600 = "results/RobustRobotPolicyT0.3s600.fsc";
    string robot_policy_T05S200 = "results/RobustRobotPolicyT0.5s200.fsc";
    string robot_policy_T05S400 = "results/RobustRobotPolicyT0.5s400.fsc";
    string robot_policy_T05S600 = "results/RobustRobotPolicyT0.5s600.fsc";

    // BR is special, a set of robot polices
    string robot_policy_BR_folder = "ExperimentsData_big_map/RobotBestResponseToSampledHumanFSC";
    size_t size_robot_fsc_dir_path_string = robot_policy_BR_folder.size();
    map<int, string> all_sample_BR_robot_fsc_paths;
    for (const auto &file : directory_iterator(robot_policy_BR_folder))
    {
        string robot_fsc_path = file.path();
        string name = robot_fsc_path;
        name.erase(0, size_robot_fsc_dir_path_string + 1);
        int sample_index = stoi(name);
        all_sample_BR_robot_fsc_paths[sample_index] = robot_fsc_path;
    }

    // -------------------------------------

    vector<string> robot_fsc_paths = {robot_policy_decpomdp, robot_policy_T0, robot_policy_T03S200, robot_policy_T03S400, robot_policy_T03S600, robot_policy_T05S200, robot_policy_T05S400, robot_policy_T05S600};

    size_t size_brm_dir_path_string = BRM_pomdp_dir_path.size();
    size_t size_brm_HL_dir_path_string = BRM_HL_pomdp_dir_path.size();
    size_t size_brm_HR_dir_path_string = BRM_HR_pomdp_dir_path.size();

    map<int, string> all_sample_BRM_paths;
    map<int, string> all_sample_BRM_HL_paths;
    map<int, string> all_sample_BRM_HR_paths;

    for (const auto &file : directory_iterator(BRM_HL_pomdp_dir_path))
    {
        string brm_path = file.path();
        string name = brm_path;
        name.erase(0, size_brm_HL_dir_path_string + 1);
        int sample_index = stoi(name);
        all_sample_BRM_HL_paths[sample_index] = brm_path;
    }

    for (const auto &file : directory_iterator(BRM_HR_pomdp_dir_path))
    {
        string brm_path = file.path();
        string name = brm_path;
        name.erase(0, size_brm_HR_dir_path_string + 1);
        int sample_index = stoi(name);
        all_sample_BRM_HR_paths[sample_index] = brm_path;
    }

    for (const auto &file : directory_iterator(BRM_pomdp_dir_path))
    {
        string brm_path = file.path();
        string name = brm_path;
        name.erase(0, size_brm_dir_path_string + 1);
        int sample_index = stoi(name);
        all_sample_BRM_paths[sample_index] = brm_path;
    }

    vector<PomdpInterface *> all_pomdps;    // both objective
    vector<PomdpInterface *> all_pomdps_HL; // human prefer left
    vector<PomdpInterface *> all_pomdps_HR; // human prefer right

    double terminal_reward = 96;
    int max_steps = 30; // stay the same with the real human experiments
    for (size_t sample_brm_index = 0; sample_brm_index < all_sample_BRM_paths.size(); sample_brm_index++)
    {
        string BRM_HL_path = all_sample_BRM_HL_paths[sample_brm_index];
        string BRM_HR_path = all_sample_BRM_HR_paths[sample_brm_index];
        string BRM_HLR_path = all_sample_BRM_paths[sample_brm_index];

        PomdpInterface *BRM_HL = new ParsedPOMDPSparse(BRM_HL_path);
        all_pomdps_HL.push_back(BRM_HL);
        PomdpInterface *BRM_HR = new ParsedPOMDPSparse(BRM_HR_path);
        all_pomdps_HR.push_back(BRM_HR);
        PomdpInterface *BRMPb = new ParsedPOMDPSparse(BRM_HLR_path);
        all_pomdps.push_back(BRMPb);
    }

    int restarts = 1;
    int restarts_BR = 1;

    map<int, vector<int>> all_robot_policy_success_counts_both_objectives;
    map<int, vector<int>> all_robot_policy_success_counts_HL;
    map<int, vector<int>> all_robot_policy_success_counts_HR;
    map<int, vector<double>> all_robot_policy_acc_rewards_both_objectives;
    map<int, vector<double>> all_robot_policy_acc_rewards_HL;
    map<int, vector<double>> all_robot_policy_acc_rewards_HR;

    string outfile = "success_rates_robot_policies.csv";
    ofstream outlogs;
    outlogs.open(outfile.c_str());

    for (size_t j = 0; j < robot_fsc_paths.size(); j++)
    {
        // double sum_both = 0;
        // double sum_HL = 0;
        // double sum_HR = 0;

        for (size_t i = 0; i < all_pomdps.size(); i++)
        {
            PomdpInterface *BRMPb = all_pomdps[i];
            PomdpInterface *BRM_HL = all_pomdps_HL[i];
            PomdpInterface *BRM_HR = all_pomdps_HR[i];

            string robot_fsc = robot_fsc_paths[j];
            SimPomdp sim_both_objective(BRMPb, robot_fsc, terminal_reward);
            SimPomdp sim_HL(BRM_HL, robot_fsc, terminal_reward);
            SimPomdp sim_HR(BRM_HR, robot_fsc, terminal_reward);

            for (size_t k = 0; k < restarts; k++)
            {
                double temp_sum_both = 0;
                double temp_sum_HL = 0;
                double temp_sum_HR = 0;

                int result_success_both = sim_both_objective.SimulateNstepsCheckSuccess(max_steps, temp_sum_both);
                all_robot_policy_success_counts_both_objectives[j].push_back(result_success_both);
                all_robot_policy_acc_rewards_both_objectives[j].push_back(temp_sum_both);

                int result_success_HL = sim_HL.SimulateNstepsCheckSuccess(max_steps, temp_sum_HL);
                all_robot_policy_success_counts_HL[j].push_back(result_success_HL);
                all_robot_policy_acc_rewards_HL[j].push_back(temp_sum_HL);

                int result_success_HR = sim_HR.SimulateNstepsCheckSuccess(max_steps, temp_sum_HR);
                all_robot_policy_success_counts_HR[j].push_back(result_success_HR);
                all_robot_policy_acc_rewards_HR[j].push_back(temp_sum_HR);

                sim_both_objective.Reset();
                sim_HL.Reset();
                sim_HR.Reset();
            }
        }
    }

    outlogs << "robot, mean HL, sem HL, mean HR, sem HR, mean both, sem both" << endl;

    for (size_t j = 0; j < robot_fsc_paths.size(); j++)
    {
        double mean_both, mean_HL, mean_HR = 0;
        double sem_both = ComputeSem(all_robot_policy_success_counts_both_objectives[j], mean_both);
        double sem_HL = ComputeSem(all_robot_policy_success_counts_HL[j], mean_HL);
        double sem_HR = ComputeSem(all_robot_policy_success_counts_HR[j], mean_HR);

        double mean_return_both, mean_return_HL, mean_return_HR = 0;
        double sem_return_both = ComputeSem(all_robot_policy_acc_rewards_both_objectives[j], mean_return_both);
        double sem_return_HL = ComputeSem(all_robot_policy_acc_rewards_HL[j], mean_return_HL);
        double sem_return_HR = ComputeSem(all_robot_policy_acc_rewards_HR[j], mean_return_HR);

        outlogs << j << ",";
        outlogs << mean_return_HL << "; " << mean_HL << "," << sem_return_HL << "; " << sem_HL << ",";
        outlogs << mean_return_HR << "; " << mean_HR << "," << sem_return_HR << "; " << sem_HR << ",";
        outlogs << mean_return_both << "; " << mean_both << "," << sem_return_both << "; " << sem_both << endl;
    }
    outlogs.close();

    // compute BR success rates and sems
    vector<int> robot_policy_BR_success_counts_HL;
    vector<int> robot_policy_BR_success_counts_HR;
    vector<int> robot_policy_BR_success_counts_HLR;
    vector<double> robot_policy_BR_acc_rewards_HL;
    vector<double> robot_policy_BR_acc_rewards_HR;
    vector<double> robot_policy_BR_acc_rewards_HLR;

    string outfile_BR = "success_rates_robot_policies_BR.csv";
    outlogs.open(outfile_BR.c_str());

    // check sim both models are correctly built !!!

    for (size_t j = 0; j < all_sample_BR_robot_fsc_paths.size(); j++)
    {
        for (size_t i = 0; i < all_pomdps.size(); i++)
        {
            PomdpInterface *BRMPb = all_pomdps[i];
            PomdpInterface *BRM_HL = all_pomdps_HL[i];
            PomdpInterface *BRM_HR = all_pomdps_HR[i];

            string robot_fsc = all_sample_BR_robot_fsc_paths[j];
            SimPomdp sim_both_objective(BRMPb, robot_fsc, terminal_reward);
            SimPomdp sim_HL(BRMPb, robot_fsc, terminal_reward);
            SimPomdp sim_HR(BRMPb, robot_fsc, terminal_reward);

            for (size_t k = 0; k < restarts_BR; k++)
            {
                double temp_sum_both = 0;
                double temp_sum_HL = 0;
                double temp_sum_HR = 0;

                int result_success_both = sim_both_objective.SimulateNstepsCheckSuccess(max_steps, temp_sum_both);
                robot_policy_BR_success_counts_HLR.push_back(result_success_both);
                robot_policy_BR_acc_rewards_HLR.push_back(temp_sum_both);

                int result_success_HL = sim_HL.SimulateNstepsCheckSuccess(max_steps, temp_sum_HL);
                robot_policy_BR_success_counts_HL.push_back(result_success_HL);
                robot_policy_BR_acc_rewards_HL.push_back(temp_sum_HL);

                int result_success_HR = sim_HR.SimulateNstepsCheckSuccess(max_steps, temp_sum_HR);
                robot_policy_BR_success_counts_HR.push_back(result_success_HR);
                robot_policy_BR_acc_rewards_HR.push_back(temp_sum_HR);

                sim_both_objective.Reset();
                sim_HL.Reset();
                sim_HR.Reset();
            }
        }
    }

    outlogs << "mean HL, sem HL, mean HR, sem HR, mean both, sem both" << endl;

    double mean_both, mean_HL, mean_HR = 0;
    double sem_HL = ComputeSem(robot_policy_BR_success_counts_HL, mean_HL);
    double sem_HR = ComputeSem(robot_policy_BR_success_counts_HR, mean_HR);
    double sem_both = ComputeSem(robot_policy_BR_success_counts_HLR, mean_both);

    double mean_return_both, mean_return_HL, mean_return_HR = 0;
    double sem_return_both = ComputeSem(robot_policy_BR_acc_rewards_HLR, mean_return_both);
    double sem_return_HL = ComputeSem(robot_policy_BR_acc_rewards_HL, mean_return_HL);
    double sem_return_HR = ComputeSem(robot_policy_BR_acc_rewards_HR, mean_return_HR);

    outlogs << mean_return_HL << "; " << mean_HL << "," << sem_return_HL << "; " << sem_HL << ",";
    outlogs << mean_return_HR << "; " << mean_HR << "," << sem_return_HR << "; " << sem_HR << ",";
    outlogs << mean_return_both << "; " << mean_both << "," << sem_return_both << "; " << sem_both << endl;

    // outlogs << mean_HL << "," << sem_HL << ",";
    // outlogs << mean_HR << "," << sem_HR << ",";
    // outlogs << mean_both << "," << sem_both << endl;

    outlogs.close();

    // ------------

    FreeAllPomdpModels(all_pomdps);
    FreeAllPomdpModels(all_pomdps_HL);
    FreeAllPomdpModels(all_pomdps_HR);
    return 0;
}

double ComputeSem(vector<int> &v, double &mean)
{
    int sum = std::accumulate(v.begin(), v.end(), 0);
    mean = (1.0 * sum) / v.size();
    cout << "v size success rate: " << v.size() << endl;
    cout << "sum success rate: " << sum << endl;

    double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size() - mean * mean);
    double sem = stdev / sqrt(v.size());

    return sem;
}

double ComputeSem(vector<double> &v, double &mean)
{
    double sum = std::accumulate(v.begin(), v.end(), 0.0);
    mean = sum / v.size();

    cout << "v size value: " << v.size() << endl;
    cout << "sum value: " << sum << endl;

    double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size() - mean * mean);
    double sem = stdev / sqrt(v.size());

    return sem;
}
